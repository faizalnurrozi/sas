<?php
if(@$_REQUEST['ajax']=='true'){
	include "globals/config.php";
	include "globals/functions.php";
}
$db = new Database();
$func = new Functions();
$qNotif = $db->sql("SELECT id, judul_notifikasi, isi_notifikasi, link, icon, status_push FROM _notifikasi WHERE (usernames = '".$_SESSION[_APP_.'s_userAdmin']."' OR usernames = '".$_SESSION[_APP_.'s_userMahasiswa.']."' OR usernames = '".$_SESSION[_APP_.'s_userDosen']."') AND status_open = 'BELUM' ORDER BY tanggal ASC");
$cNotif = $db->num_rows($qNotif);
?>
<a href="#" class="dropdown-toggle" data-toggle="dropdown">
	<i class="fa fa-envelope" style="font-size:19px;" ></i>
	<?php if($cNotif == 0){ ?>
	<script>window.document.getElementById('hdnotift').value='<?php echo $func->get_web_title(); ?>';</script>
	<?php }else{ ?>
	<span class="label label-danger"><?php echo $cNotif; ?></span>
	<script>window.document.getElementById('hdnotift').value='<?php echo $cNotif; ?> Pemberitahuan Baru.';</script>
	<?php } ?>
</a>
<?php if($cNotif != 0){ ?>
<ul class="dropdown-menu">	
	<li class="header">Anda memiliki <?php echo $cNotif; ?> pemberitahuan</li>
	<li>
		<ul class="menu">
			<?php
			while(list($id, $judul, $isi, $link, $icon, $push) = $db->fetch_row($qNotif)){
			?>
			<li><a href="#" onclick="javascript: sendRequest('content.php', 'module=general&component=notifications&action=process&proc=link&id=<?php echo $id; ?>', 'content', 'div');"><?php echo $icon; ?> <?php echo $isi; ?></a></li>
			<?php 
			} 
			?>
		</ul>
	</li>
	<li class="footer"><a href="#" onclick="javascript: sendRequest('content.php', 'module=general&component=notifications&add=false&sort=reset', 'content', 'div');">Tampilkan semua</a></li>
</ul>
<?php }else{ ?>
<ul class="dropdown-menu">	
	<li class="header">Anda tidak memiliki pemberitahuan baru.</li>
	<li class="footer"><a href="#" onclick="javascript: sendRequest('content.php', 'module=general&component=notifications&add=false&sort=reset', 'content', 'div');">Tampilkan semua pemberitahuan.</a></li>
</ul>
<?php } ?>