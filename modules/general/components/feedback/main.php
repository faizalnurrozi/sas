<?php
//session_start();
include "globals/config.php";
include "globals/functions.php";
$conn = $GLOBALS['conn'];
?>
<html>
	<head>
		<title>Laporkan Error/Bug</title>
		<script type="text/javascript" src="includes/jquery-1.8.2.min.js"></script>
		<script language="javascript">
			$(document).ready(function(){
				$(document).focus();
			});
			$(document).keyup(function(event){
				if (event.keyCode == 27) {
					window.top.hidePopup(1000);
				}
			});
		</script>
		<link rel="stylesheet" href="includes/bootstrap/bootstrap.css"></link>
		<link rel="stylesheet" href="includes/bootstrap/bootstrap-adds.css"></link>
		<link rel="stylesheet" href="includes/bootstrap/font-awesome.css"></link>
		
		<!-- TinyMCE -->
		<script type="text/javascript" src="includes/tiny-mce/tiny_mce.js"></script>
		<script type="text/javascript">
			tinyMCE.init({
				// General options
				mode : "textareas",
				theme : "advanced",
				plugins : "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,inlinepopups,autosave",

				setup : function(ed){
					ed.onBeforeSetContent.add(function(ed, o){
						o.content = o.content.replace(/<\?/gi, "&lt;&63;");
						o.content = o.content.replace(/\?>/gi, "&63;&gt;");
					});
				},
				
				// Theme options
				theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,emotions,formatselect,fontselect,fontsizeselect,|,undo,redo,|,hr,|,justifyleft,justifycenter,justifyright,justifyfull,|,image,|",
				theme_advanced_buttons2 : "",
				theme_advanced_buttons3 : "",
				theme_advanced_buttons4 : "",
				theme_advanced_toolbar_location : "top",
				theme_advanced_toolbar_align : "left",
				theme_advanced_statusbar_location : "bottom",
				theme_advanced_resizing : true,
				
				// Example word content CSS (should be your site CSS) this one removes paragraph margins
				content_css : "css/word.css",

				// Drop lists for link/image/media/template dialogs
				template_external_list_url : "lists/template_list.js",
				external_link_list_url : "lists/link_list.js",
				external_image_list_url : "lists/image_list.js",
				media_external_list_url : "lists/media_list.js",

				// Replace values for the template plugin
				template_replace_values : {
					username : "Some User",
					staffid : "991234"
				},
				file_browser_callback: "openKCFinder"
			});
			
			function openKCFinder(field_name, url, type, win) {
				tinyMCE.activeEditor.windowManager.open({                    
					file            		: "includes/tiny-mce/plugins/kcfinder/browse.php?opener=tinymce&type=images&lang=en&dir=../&type=" + type,
					title			: "Browse File",
					width          	: 420,
					height         	: 300,
					resizable      	: "yes",
					inline         	: true,
					close_previous 	: "no",
					popup_css      	: false,
				}, {
					window          : win,
					input           : field_name
				});
					return false;
			}  
		</script>
		<!-- /TinyMCE -->
		
		<style type="text/css">
			.header{ position:fixed; background-color:#ccc; left:0; text-align:center; top:0; }
			.formx{ margin-top:15px; }
		</style>
	</head>
	<body bgcolor="white">
		<legend class="header">Laporkan Bug/Error</legend>
		<div class="alert alert-info" style="margin:45px 10px 0px 10px;text-align:justify;">
			<strong>Perhatian : </strong> <br>
			Jika anda menemukan Error/Bug pada program kami mohon memberikan Feedback untuk perbaikan dengan menuliskan <b>Error Report</b> di bawah ini. terima kasih.
		</div>
		<form class="formx" name="form_feedback" id="form_feedback" enctype="mulipart/form-data" action="javascript: void(null);" method="post">
			<input type="hidden" name="proc" id="proc" value="add" />
			<table width="95%" style="margin:20px;">
				<tr>
					<td width="17%"><label>Module</label></td>
					<td width="2%">:</td>
					<td>
						<select name="txtmodule" id="txtmodule" class="combobox">
							<option value="0">--Module--</option>
							<?php
							$dir = dir("modules/");
							while($res = $dir->read()){
								if($res != "." && $res != ".."){
									echo "<option value='$res' ";
									if($resultEdit['MODULE'] == $res) echo "selected";
									echo ">".ucfirst(str_replace("_", " ", $res))."</option>";
								}
							}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td><label>Menu</label></td>
					<td>:</td>
					<td><input type="text" name="txtmenu" id="txtmenu" class="input-xlarge" placeholder="Menu ..." autocomplete="off" /></td>
				</tr>
				<tr>
					<td valign="top"><label>Isi</label></td>
					<td valign="top">:</td>
					<td>
						<textarea name="txtisi" id="txtisi"></textarea>
					</td>
				</tr>
			</table>
			
			<table style="position:fixed; bottom:10px; right:10px;">
				<tr>
					<td>
						<button type="button" class="btn btn-primary" onclick="javascript:
							var obj = document.form_feedback;
							var err = '';
							if(obj.txtmodule.value=='0'){ $('#txtmodule').removeClass('input-xlarge').addClass('input-error').focus(); err+='<li>Module harus di pilih</li>'; }
							if(obj.txtmenu.value==''){ $('#txtmenu').removeClass('input-xlarge').addClass('input-error').focus(); err+='<li>Menu harus di isi</li>'; }
							if(err==''){
								obj.action='content.php?module=general&component=feedback&action=process';
								obj.submit();
							}else { 
								$('#Modal').click(); $('#error-text').html(err);
							}
						"><i class="icon-save"></i> Simpan</button>
					</td>
					<td>
						<button type="button" class="btn btn-danger" onClick="javascript: window.top.hidePopup(10);"><i class="icon-remove"></i> Tutup</button>
					</td>
				</tr>
			</table>
		</form>
		
		<!-- Alert Validation Form -->
		<input type="hidden" id="Modal" onclick="javascript: $('#s_alert').fadeIn();" />
		<div class="alert alert-danger" id="s_alert">
			<button type="button" class="close" onclick="javascript: $('#s_alert').fadeOut();">x</button>
			<strong>Warning : </strong> <div id="error-text" class="error-text"><ul></ul></div>
		</div>
		<!-- End Alert Validation Form -->
	</body>
</html>