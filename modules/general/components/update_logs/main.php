<?php
//session_start();
include "globals/config.php";
include "globals/functions.php";
$conn = $GLOBALS['conn'];
?>
<html>
	<head>
		<title>Update Logs</title>
		<script type="text/javascript" src="includes/jquery-1.8.2.min.js"></script>
		<script language="javascript">
			$(document).ready(function(){
				$(document).focus();
			});
			$(document).keyup(function(event){
				if (event.keyCode == 27) {
					window.top.hidePopup(1000);
				}
			});
		</script>
		<link rel="stylesheet" href="includes/bootstrap/bootstrap.css"></link>
		<link rel="stylesheet" href="includes/bootstrap/bootstrap-adds.css"></link>
		<link rel="stylesheet" href="includes/bootstrap/font-awesome.css"></link>
		
		<!-- TinyMCE -->
		<script type="text/javascript" src="includes/tinymce/tinymce.min.js"></script>
		<script type="text/javascript">
			tinymce.init({
				selector: "textarea",
				plugins: [
				],
				toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
			});
		</script>
		<!-- End of TinyMCE -->
		<style type="text/css">
			.header{ position:fixed; background-color:#ccc; left:0; text-align:center; top:0; }
			.formx{ margin-top:15px; }
		</style>
	</head>
	<body bgcolor="white">
		<legend class="header">Update Logs</legend>
		<p style="margin-top:40px;">
		<?php
		$filename = "./update_logs.txt";
		if(file_exists($filename)){
			//$handle = fopen($filename, "r");
			//$contents = fread($handle, filesize($filename));
			$data = file_get_contents($filename, true);
			$convert = explode("\n", $data); //create array separate by new line

			for ($i=0;$i<count($convert);$i++) 
			{
				echo $convert[$i].'<br> '; //write value by index
			}
			//echo str_replace(array('\n','\r') , '<br>', $contents);
			//fclose($handle);
		}
		?>
		</p>
		<table style="position:fixed; bottom:10px; right:10px;">
			<tr>
				<td>
					<button type="button" class="btn btn-danger" onClick="javascript: window.top.hidePopup(10);"><i class="icon-remove"></i> Tutup</button>
				</td>
			</tr>
		</table>
	</body>
</html>