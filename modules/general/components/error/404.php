<?php session_start(); ?>
<html>
	<head>
		<title>Page not found...</title>
		<link rel="icon" href="images/remove.png" type="image/x-icon" />
		<link rel="shortcut icon" href="images/remove.png" type="image/x-icon" />
		<script type="text/javascript" src="includes/ajax.js"></script>
		<link rel="stylesheet" href="includes/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="includes/bootstrap/css/font-awesome.css">
		<link rel="stylesheet" href="includes/bootstrap/css/bootstrap-glyphicons.css">
		<link rel="stylesheet" href="includes/dist/css/AdminLTE.css">
		<link rel="stylesheet" href="includes/dist/css/ionicons.min.css">
	</head>
	<body>        
        <!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				404 Error Page
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
				<li class="active">404 error</li>
			</ol>
		</section>

		<!-- Main content -->
		<section class="content">	
			<div class="row">
				<div class="col-xs-12">
					<div class="box">
						<div class="error-page">
							<!--<h2 class="headline text-info"> 404</h2>-->
							<img class="headline text-info" src="images/404.png" width="200px" />
							<div class="error-content">
								<h3><i class="fa fa-warning text-yellow"></i> Oops! Page not found.</h3>
								<p>
									Halaman yang anda akses belum aktif.
								</p>
							</div><!-- /.error-content -->
						</div><!-- /.error-page -->
					</div>
				</div>
			</div>
		</section><!-- /.content -->
	</body>
</html>