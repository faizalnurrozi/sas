<html>
	<head>
		<title>Forbidden...</title>
		<link rel="icon" href="images/remove.png" type="image/x-icon" />
		<link rel="shortcut icon" href="images/remove.png" type="image/x-icon" />
		<script type="text/javascript" src="includes/ajax.js"></script>
		<link rel="stylesheet" type="text/css" href="includes/bootstrap/bootstrap.css"></link>
		<link rel="stylesheet" type="text/css" href="includes/bootstrap/theme.css"></link>
		<link rel="stylesheet" href="includes/bootstrap/font-awesome.css"></link>
		<link rel="stylesheet" href="includes/bootstrap/bootstrap-adds.css"></link>
		<style type="text/css">
			body{ background-image:url(images/bg-403.png); }
			a{ color:#123; }
			.cso{ font-size:normal; text-align:center; }
		</style>
	</head>
	<body>
		<div class="row-fluid">
			<div class="http-error">
				<h1>Maaf !</h1>
				<p class="info">Anda tidak berhak mengakses aplikasi ini.</p>
				<p><image src="images/403.png" width="200"></p>
				<p class="cso">Hubungi : <i class="icon-phone"></i> PT. Mitra Bisnis Informatika<br/>Telp. (031)-8547418</p>
			</div>
		</div>
		<script src="includes/bootstrap/bootstrap.js"></script>
		<script type="text/javascript">
			$("[rel=tooltip]").tooltip();
			$(function() {
				$('.demo-cancel-click').click(function(){return false;});
			});
		</script>
	</body>
</html>