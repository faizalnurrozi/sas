<?php
if(@$_REQUEST['ajax']=='true'){
	include "globals/config.php";
	include "globals/functions.php";
	$db = new Database();
	$func = new Functions();
}
$limit = _LIMIT_;
?>

<!-- Alert Process -->
<?php if(isset($_SESSION[_APP_.'s_message_info'])){ ?>
<div class="alert alert-info">
	<button type="button" class="close" data-dismiss="alert">x</button>
	<strong>Status : </strong> <?php echo $_SESSION[_APP_.'s_message_info']; unset($_SESSION[_APP_.'s_message_info']); ?>
</div>
<?php } ?>
<?php if(isset($_SESSION[_APP_.'s_message_error'])){ ?>
<div class="alert alert-danger">
	<button type="button" class="close" data-dismiss="alert">x</button>
	<strong>Error : </strong> <?php echo $_SESSION[_APP_.'s_message_error']; unset($_SESSION[_APP_.'s_message_error']); ?>
</div>
<?php } ?>
<!-- Alert Process -->

<?php
/*Sorting*/
$iconsort = "";
if(@$_POST['sort']=='reset'){
	$_SESSION[_APP_.'s_field_admin_menus'] = "A.level, A.urutan";
	$_SESSION[_APP_.'s_sort_admin_menus'] = "ASC";
	$iconsort = "<i class='fa fa-fw fa-caret-down'></i>";
}
switch(@$_POST['field']){
	case 'A.nama' : $_SESSION[_APP_.'s_field_admin_menus'] = "A.nama"; break;
	case 'B.nama' : $_SESSION[_APP_.'s_field_admin_menus'] = "B.nama"; break;
	case 'A.icon' : $_SESSION[_APP_.'s_field_admin_menus'] = "A.icon"; break;
	case 'A.urutan' : $_SESSION[_APP_.'s_field_admin_menus'] = "A.urutan"; break;
	case 'B.urutan' : $_SESSION[_APP_.'s_field_admin_menus'] = "B.urutan"; break;
	default : 
		if(!isset($_SESSION[_APP_.'s_field_admin_menus'])){
			$_SESSION[_APP_.'s_field_admin_menus'] = "A.nama";
		}
		break;
}
if(!isset($_SESSION[_APP_.'s_sort_admin_menus'])){
	$_SESSION[_APP_.'s_sort_admin_menus'] = "ASC";
	$iconsort = "<i class='fa fa-fw fa-caret-down'></i>";
}else{
	switch(@$_POST['act']){
		case 'sort' :
			if($_SESSION[_APP_.'s_sort_admin_menus'] == "ASC"){
				$_SESSION[_APP_.'s_sort_admin_menus'] = "DESC";
				$iconsort = "<i class='fa fa-fw fa-caret-up'></i>";
			}else if($_SESSION[_APP_.'s_sort_admin_menus'] == "DESC"){
				$_SESSION[_APP_.'s_sort_admin_menus'] = "ASC";
				$iconsort = "<i class='fa fa-fw fa-caret-down'></i>";
			}
			break;
		case 'paging' :
			if($_SESSION[_APP_.'s_sort_admin_menus'] == "ASC"){
				$iconsort = "<i class='fa fa-fw fa-caret-down'></i>";
			}else if($_SESSION[_APP_.'s_sort_admin_menus'] == "DESC"){
				$iconsort = "<i class='fa fa-fw fa-caret-up'></i>";
			}
			break;
	}
}
/*End Sorting*/

if(@$_REQUEST['start']=='') $start = 0; else $start = $_REQUEST['start'];

if(!isset($msc)) $msc = microtime(true);
@$keyword = @$_REQUEST['keyword'];
$qSQL 	= "SELECT A.id_admin_menus, A.level, A.nama as menu, B.nama AS menuParent, A.icon AS icon, A.link, A.urutan, B.icon AS iconChild, A.baru FROM _admin_menus AS A LEFT JOIN _admin_menus AS B ON (A.id_admin_menus_parent=B.id_admin_menus) WHERE (A.nama LIKE :keyword OR B.nama LIKE :keyword) ORDER BY ".$_SESSION[_APP_.'s_field_admin_menus']." ".$_SESSION[_APP_.'s_sort_admin_menus'];
$hqSQL 	= $db->query($qSQL);
$db->bind($hqSQL, ":keyword", "%".$keyword."%", "str");
$db->exec($hqSQL);
$totalData = $db->num_rows($hqSQL);
$qSQL	.= " LIMIT $start, $limit";
$hqSQL = $db->query($qSQL);
$db->bind($hqSQL, ":keyword", "%".$keyword."%", "str");
$db->exec($hqSQL);
$totalLimit = $db->num_rows($hqSQL);
?>

<div class="box-body table-responsive">
	<table class="table table-bordered table-hover table-striped">
		<thead>
			<tr>
				<th width="1%">No.</th>
				<th class="sort" onclick="javascript: sendRequest('content.php', 'module=admin&component=admin_menus&action=list&ajax=true&act=sort&field=A.nama&keyword=<?php echo $keyword; ?>', 'list', 'div');">Nama&nbsp;Menu&nbsp;<?php if($_SESSION[_APP_.'s_field_admin_menus'] == 'A.nama') echo $iconsort; ?></th>
				<th class="sort" onclick="javascript: sendRequest('content.php', 'module=admin&component=admin_menus&action=list&ajax=true&act=sort&field=B.urutan&keyword=<?php echo $keyword; ?>', 'list', 'div');">Parent&nbsp;Menu&nbsp;<?php if($_SESSION[_APP_.'s_field_admin_menus'] == 'B.urutan') echo $iconsort; ?></th>
				<th class="sort" onclick="javascript: sendRequest('content.php', 'module=admin&component=admin_menus&action=list&ajax=true&act=sort&field=A.icon&keyword=<?php echo $keyword; ?>', 'list', 'div');">Icon&nbsp;<?php if($_SESSION[_APP_.'s_field_admin_menus'] == 'A.icon') echo $iconsort; ?></th>
				<th class="sort">Level&nbsp;</th>
				<th class="sort" onclick="javascript: sendRequest('content.php', 'module=admin&component=admin_menus&action=list&ajax=true&act=sort&field=A.urutan&keyword=<?php echo $keyword; ?>', 'list', 'div');">Urutan&nbsp;<?php if($_SESSION[_APP_.'s_field_admin_menus'] == 'A.urutan') echo $iconsort; ?></th>
				<th class="sort">Baru&nbsp;</th>
				<th width="80">&nbsp;</th>
			</tr>
		</thead>
		
		<tbody>
			<?php
			if($totalData=='0'){
				echo "<tr><td colspan='8' align='center'>Data belum ada</td></tr>";
				$db->close($hqSQL);
			}else{
				$no = @$start+1; $x = 0;
				while($hasil = $db->fetch_assoc($hqSQL)){
					echo "<tr class='table-list-row'>";
					echo "<td align='center'>".$no.".</td>";
					echo "<td align='left'>".$func->highlight($hasil['menu'], $keyword)."</td>";
					if($hasil['menuParent']=="") $parent = "-";
					else $parent = stripslashes($hasil['iconChild'])."&nbsp;&nbsp;".$func->highlight($hasil['menuParent'], $keyword);
					echo "<td align='left'>$parent</td>";			
					echo "<td align='center'>".stripslashes($hasil['icon'])."</td>";			
					echo "<td align='center'>$hasil[level]</td>";			
					echo "<td align='center'>".$func->highlight($hasil['urutan'], $keyword)."</td>";
					$baru = ($hasil['baru']=='TRUE') ? "checked" : "";
					echo "<td align='center'><input type='checkbox' name='cbbaru$x' id='cbbaru$x' $baru onclick=\"javascript: if(this.checked==true){ ajaxCustom('content.php', 'module=admin&component=admin_menus&action=process&proc=baru&idx=$hasil[id_admin_menus]&status=TRUE', 'spanbaru$x'); }else{ ajaxCustom('content.php', 'module=admin&component=admin_menus&action=process&proc=baru&idx=$hasil[id_admin_menus]&status=FALSE', 'spanbaru$x'); } \" />&nbsp;<span id='spanbaru$x'></span></td>";			
					echo "<td align='center' valign='top'>";
					echo "<div class='btn-group btn-group-xs' role='group' aria-label='...'>";
							
					/* Akses menu View */
					if($func->akses('R') == true){
						echo "<button type='button' title='View' class='btn btn-info' data-toggle='modal' data-target='#modal-view' onclick=\"javascript:  document.getElementById('iframe_view').src='content.php?module=admin&component=admin_menus&action=view&id=$hasil[id_admin_menus]'; \" onmouseover=\"$(this).tooltip();\" ><i class='fa fa-search'></i> </button>";
					}else{
						echo "<button type='button' title='View' class='btn btn-default' disabled ><i class='fa fa-search'></i> </button>";
					}
					/* End Akses menu View */
					
					/* Akses menu Edit */
					if($func->akses('U') == true){
						echo "<button type='button' title='Edit' class='btn btn-success' data-toggle='modal' data-target='#myModal' onclick=\"javascript: document.getElementById('iframe_admin_menus').src='content.php?module=admin&component=admin_menus&action=add&id=$hasil[id_admin_menus]&keyword=$keyword&start=$start'; \" onmouseover=\"$(this).tooltip();\" ><i class='fa fa-edit'></i> </button>";
					}else{
						echo "<button type='button' title='Edit' class='btn btn-default' disabled ><i class='fa fa-edit'></i> </button>";
					}
					/* End Akses menu edit */

					/* Akses menu Delete */
					if($func->akses('D') == true){
						echo "<button type='button' title='Delete' class='btn btn-danger' data-toggle='modal' data-target='#modal-delete' onclick=\"javascript:  document.getElementById('id_delete').value='$hasil[id_admin_menus]'; \" onmouseover=\"$(this).tooltip();\" ><i class='fa fa-trash'></i> </button>";
					}else{
						echo "<button type='button' title='Delete' class='btn btn-default' disabled ><i class='fa fa-trash'></i> </button>";
					}
					/* End Akses menu delete */
					echo "</div>
							</td>";
					echo "</tr>";
					
					$no++; $x++;
				}
				$db->close($hqSQL);
			}
			?>
		</tbody>
	</table>
</div>
<?php $msc = microtime(true) - @$msc; ?>
<div class="box-body">
	<div class="row">
		<div class="col-sm-5">
			<i>
				<?php 
				echo "Ditampilkan <b>".($totalLimit)."</b> sampai <b>".($start+$totalLimit)."</b> dari <b>$totalData</b> total data <b>(".round($msc,4)." detik)</b>"; 
				?>
			</i>
		</div>
		<div class="col-sm-7">
			<ul class="pagination pagination-sm pull-right">
			<?php
			if($start != 0) echo "<li><a href='#' onclick=\"sendRequest('content.php','module=admin&component=admin_menus&action=list&ajax=true&start=".($start-$limit)."&keyword=$keyword', 'list', 'div');\">Prev</a></li>";
			$jumlahPage = $totalData/$limit;
			$x=0;
			for($a=0;$a<$jumlahPage;$a++){
				$x = $a * $limit;
				if($start==$a*$limit){
					echo "<li class='active'><a href='#'>".($a+1)."</a></li>";
				}else{
					echo "<li><a href='#' onclick=\"sendRequest('content.php','module=admin&component=admin_menus&action=list&ajax=true&start=".($a*$limit)."&keyword=$keyword', 'list', 'div');\">".($a+1)."</a></li>";
				}
			}
			 if($start != @$x) echo "<li><a href='#' onclick=\"sendRequest('content.php','module=admin&component=admin_menus&action=list&ajax=true&start=".($start+$limit)."&keyword=$keyword', 'list', 'div');\">Next</a></li>";
			?>
			</ul>
		</div>
	</div>
</div>


<!-- Modals -->
<div class="modal fade" id="modal-view" data-backdrop="static">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">View</h4>
			</div>
			<div class="modal-body">
				<iframe name="iframe_view" id="iframe_view" width="98%" height="300px;" frameborder="0" onload="javascript: /*autoResize('iframe_view');*/ " scrolling="no"></iframe>
			</div>
			<div class="modal-footer">
				<button class="btn btn-warning" data-dismiss="modal" aria-hidden="true" id="dismissView"><i class="fa fa-remove"></i> Tutup</button>
			</div>
		</div>
	</div>
</div>

<input type="hidden" id="id_delete" value="" />
<div class="modal fade" id="modal-delete" data-backdrop="static">
	<div class="modal-dialog modal-xs">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Hapus</h4>
			</div>
			<div class="modal-body">
				Apakah anda yakin ingin menghapus ?
			</div>
			<div class="modal-footer">
				<button class="btn btn-default" data-dismiss="modal" aria-hidden="true" id="dismissDelete"><i class="fa fa-repeat"></i> Batal</button>
				<button class="btn btn-danger" data-dismiss="modal" aria-hidden="true" onclick="javascript: sendRequest('content.php', 'module=admin&component=admin_menus&action=process&proc=delete&start=<?php echo $start; ?>&keyword=<?php echo $keyword; ?>&id='+document.getElementById('id_delete').value, 'list', 'div');"><i class="fa fa-remove"></i> Hapus</button>
			</div>
		</div>
	</div>
</div>
<!-- End of Modals -->