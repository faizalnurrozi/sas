<?php
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();

$id = @$_REQUEST['id'];
?>
<html>
	<head>
		<title>&nbsp;</title>		
		<script type="text/javascript" src="includes/ajax.js"></script>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="includes/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="includes/bootstrap/css/font-awesome.css">
		<link rel="stylesheet" href="includes/dist/css/ionicons.min.css">
		<link rel="stylesheet" href="includes/dist/css/AdminLTE.css">
		<!--<link rel="stylesheet" href="includes/dist/css/skins/_all-skins.min.css">-->	
	</head>
<body>
	<?php
	$hqData = $db->sql("SELECT * FROM _admin_menus WHERE id_admin_menus = '$id'");
	$result = $db->fetch_assoc($hqData);
	$db->close($hqData);
	?>
	<div class="container-fluid">
	<table class="table">
		<tr>
			<td width="20%"><label>Nama&nbsp;</label></td>
			<td width="1%">&nbsp;:&nbsp;</td>
			<td><?php echo $result['nama']; ?></td>
		</tr>
		<tr>
			<td><label>Parent&nbsp;Menu&nbsp;</label></td>
			<td>&nbsp;:&nbsp;</td>
			<td>
			<?php
			$hqData = $db->sql("SELECT nama FROM _admin_menus WHERE id_admin_menus = '$result[id_admin_menus_parent]'");
			$resultX = $db->fetch_assoc($hqData);
			echo $resultX['nama'];
			$db->close($hqData);
			?>
			</td>
		</tr>
		<tr>
			<td><label>Icon&nbsp;</label></td>
			<td>&nbsp;:&nbsp;</td>
			<td><?php echo stripslashes($result['icon']); ?></td>
		</tr>
		<tr>
			<td valign="top"><label>Link&nbsp;</label></td>
			<td valign="top">&nbsp;:&nbsp;</td>
			<td><div style="width:98%;min-height:100px;border:1px solid #000;padding:5px 5px 5px 5px;"><?php echo stripslashes($result['link']); ?></div></td>
		</tr>
		<tr>
			<td><label>Level&nbsp;</label></td>
			<td>&nbsp;:&nbsp;</td>
			<td><?php echo $result['level']; ?></td>
		</tr>
		<tr>
			<td><label>Urutan&nbsp;</label></td>
			<td>&nbsp;:&nbsp;</td>
			<td><?php echo $result['urutan']; ?></td>
		</tr>
		<tr>
			<td><label>Deskripsi&nbsp;</label></td>
			<td>&nbsp;:&nbsp;</td>
			<td><?php echo $result['deskripsi']; ?></td>
		</tr>
	</table>
	</div>
</body>
</html>