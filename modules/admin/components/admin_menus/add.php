<?php
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();
?>
<html>
	<head>		
		<script type="text/javascript" src="includes/ajax.js"></script>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="includes/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="includes/bootstrap/css/font-awesome.css">
		<link rel="stylesheet" href="includes/dist/css/ionicons.min.css">
		<link rel="stylesheet" href="includes/dist/css/AdminLTE.css">
		<!--<link rel="stylesheet" href="includes/dist/css/skins/_all-skins.min.css">-->

		<script src="includes/plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script src="includes/bootstrap/js/bootstrap.min.js"></script>
		<script src="includes/dist/js/app.min.js"></script>
		
		<style type="text/css">
		.label{ display:inline-block; font-size:14px; }
		input[type="radio"] {
			-webkit-appearance: checkbox; /* Chrome, Safari, Opera */
			-moz-appearance: checkbox;    /* Firefox */
			-ms-appearance: checkbox;     /* not currently supported */
			margin:5px;
		}
		input, textarea, select{
			text-transform: none !important;
		}
		</style>
		
	</head>
	
	<body>
		<?php
		if(@$_REQUEST['id'] != ''){
			$qEditAdmin = "SELECT * FROM _admin_menus WHERE id_admin_menus = '$_REQUEST[id]'";
			$dataEdit = $db->sql($qEditAdmin);
			$resultEdit = $db->fetch_assoc($dataEdit);
			$db->close($dataEdit);
		}
		?>
		<div class="container-fluid">
		<form name="form_admin_menus" method="POST" action="javascript: void(null);" enctype="multipart/form-data">
			<?php if(@$_REQUEST['id'] == ''){ ?>
			<input type="hidden" id="proc" name="proc" value="add" />
			<?php }else{ ?>
			<input type="hidden" id="proc" name="proc" value="update" />
			<input type="hidden" id="txtid" name="txtid" value="<?php echo @$resultEdit['id_admin_menus']; ?>" />
			<input type="hidden" id="start" name="start" value="<?php echo @$_REQUEST['start']; ?>" />
			<input type="hidden" id="keyword" name="keyword" value="<?php echo @$_REQUEST['keyword']; ?>" />
			<?php } ?>
			
			<div class="row">
				<div class="form-group col-xs-8 has-nama">
					<label>Nama&nbsp;Admin&nbsp;Menu</label>
					<input type="text" name="txtnama" id="txtnama" value="<?php echo @$resultEdit['nama']; ?>" autocomplete="off" class="form-control input-sm" placeholder="Input Nama Menu..." />
				</div>
				<div class="form-group col-xs-4 has-icon">
					<label>Icon</label>
					<div class="input-group">
						<input type="text" name="txticon" id="txticon" value="<?php echo stripslashes(str_replace("\"", "'", @$resultEdit['icon'])); ?>" readonly class="form-control" placeholder="Icon Menu..." />
						<span class="input-group-addon" style="cursor:pointer;" data-toggle="modal" data-target="#myModal" onclick="javascript: window.top.document.getElementById('bntModalIcon').click(); "><i class="fa fa-plus"></i></span>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-xs-8 has-cbmenu">
					<label>Parent&nbsp;Menu</label>
					<select name="cbmenu" id="cbmenu" onchange="javascript: ajaxCustom('content.php', 'module=admin&component=admin_menus&action=process&proc=level&id='+this.value, 'spanlevel');" class="form-control">
						<option value="0">--Root Menu--</option>
						<?php
						$qMenu0 = "SELECT id_admin_menus, nama FROM _admin_menus WHERE id_admin_menus_parent = '0' ORDER BY level ASC, urutan ASC";
						$hqMenu0 = $db->sql($qMenu0);
						while($result0 = $db->fetch_assoc($hqMenu0)){
							$idMenu0 	= $result0['id_admin_menus'];
							$namaMenu0	= $result0['nama'];
							echo "<option value='$idMenu0' ";
							if($idMenu0 == @$resultEdit['id_admin_menus_parent']) echo "selected";
							echo ">-&nbsp;$namaMenu0</option>";
							$qMenu1 = "SELECT id_admin_menus, nama FROM _admin_menus WHERE  id_admin_menus_parent = '$idMenu0' ORDER BY level ASC, urutan ASC";
							$hqMenu1 = $db->sql($qMenu1);
							while($result1 = $db->fetch_assoc($hqMenu1)){
								$idMenu1 	= $result1['id_admin_menus'];
								$namaMenu1	= $result1['nama'];
								echo "<option value='$idMenu1' ";
								if($idMenu1 == @$resultEdit['id_admin_menus_parent']) echo "selected";
								echo ">&nbsp;&nbsp;&nbsp;-&nbsp;$namaMenu1</option>";
								$qMenu2 = "SELECT id_admin_menus, nama FROM _admin_menus WHERE  id_admin_menus_parent = '$idMenu1' ORDER BY level ASC, urutan ASC";
								$hqMenu2 = $db->sql($qMenu2);
								while($result2 = $db->fetch_assoc($hqMenu2)){
									$idMenu2 	= $result2['id_admin_menus'];
									$namaMenu2	= $result2['nama'];
									echo "<option value='$idMenu2' ";
									if($idMenu2 == @$resultEdit['id_admin_menus_parent']) echo "selected";
									echo ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;$namaMenu2</option>";
									$qMenu3 = "SELECT id_admin_menus, nama FROM _admin_menus WHERE  id_admin_menus_parent = '$idMenu2' ORDER BY level ASC, urutan ASC";
									$hqMenu3 = $db->sql($qMenu3);
									while($result3 = $db->fetch_assoc($hqMenu3)){
										$idMenu3 	= $result3['id_admin_menus'];
										$namaMenu3	= $result3['nama'];
										echo "<option value='$idMenu3' ";
										if($idMenu3 == @$resultEdit['id_admin_menus_parent']) echo "selected";
										echo ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;$namaMenu3</option>";
										$qMenu4 = "SELECT id_admin_menus, nama FROM _admin_menus WHERE id_admin_menus_parent = '$idMenu3' ORDER BY level ASC, urutan ASC";
										$hqMenu4 = $db->sql($qMenu4);
										while($result4 = $db->fetch_assoc($hqMenu4)){
											$idMenu4 	= $result4['id_admin_menus'];
											$namaMenu4	= $result4['nama'];
											echo "<option value='$idMenu4' ";
											if($idMenu4 == @$resultEdit['id_admin_menus_parent']) echo "selected";
											echo ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;$namaMenu4</option>";
										}
										$db->close($hqMenu4);
									}
									$db->close($hqMenu3);
								}
								$db->close($hqMenu2);
							}
							$db->close($hqMenu1);
						}
						$db->close($hqMenu0);
						?>
					</select>&nbsp;
					<span id="spanlevel"></span>
				</div>
				<div class="form-group col-xs-2">
					<label>Level</label>
					<input type="text" name="txtlevel" id="txtlevel" class="form-control input-sm" placeholder="Level..." value="<?php if(@$resultEdit['level'] == "") echo "0"; else echo $resultEdit['level']; ?>" readonly />
				</div>
				<div class="form-group col-xs-2 has-urutan">
					<label>Urutan</label>
					<input type="text" name="txturutan" id="txturutan" value="<?php echo @$resultEdit['urutan']; ?>" autocomplete="off" class="form-control input-sm" onkeyup="javascript: if(isNaN(this.value)){ alert('Harus angka !'); this.value=''; }" placeholder="Urutan..." />
				</div>
			</div>
			<div class="row">
				<div class="form-group col-xs-12 has-link">
					<label>Link</label>
					<textarea name="txtlink" id="txtlink" class="form-control" placeholder="Link..."><?php if(@$resultEdit['link']=="") echo "javascript: sendRequest('content.php', 'module=admin&component=ABC&sort=reset', 'content', 'div')"; else echo stripslashes(@$resultEdit['link']); ?></textarea>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-xs-12 has-link">
					<label>Deskripsi</label>
					<textarea name="txtdeskripsi" id="txtdeskripsi" class="form-control" placeholder="Deskripsi..."><?php echo @$resultEdit['deskripsi']; ?></textarea>
				</div>
			</div>
			
			<table border="0" cellpadding="0" cellspacing="0" width="90%">
				<tr><td colspan="3">&nbsp;</td></tr>
				<tr>
					<td colspan="3" align="left">
						<button id="save" class="hide" onclick="javascript:
							var obj = document.form_admin_menus;
							var err = '';
							if(obj.txtnama.value==''){ $('.has-nama').addClass('has-error').focus(); err+='<li>Nama menu harus di isi</li>'; }
							/*if(obj.cbmenu.value=='0'){ $('.has-menu').addClass('has-error').focus(); err+='<li>Pilih parent menu mana</li>'; }*/
							if(obj.txticon.value==''){ $('.has-icon').addClass('has-error').focus(); err+='<li>Icon harus di isi</li>'; }
							if(obj.txturutan.value==''){ $('.has-urutan').addClass('has-error').focus(); err+='<li>Urutan harus di isi</li>'; }
							if(err==''){
								obj.action='content.php?module=admin&component=admin_menus&action=process';
								obj.submit();
								
								if(window.top.document.getElementById('cbadd').checked == false){
									window.top.document.getElementById('dismiss').click();
								}
							}else{ 
								//window.top.alert(err);
								$('#Modal').click(); $('#error-text').html(err);
							}
						">&nbsp;</button>
					</td>
				</tr>
			</table>
		</form>
		</div>
		
		<!-- Alert Validation Form -->
		<input type="hidden" id="Modal" onclick="javascript: $('#s_alert').fadeIn(); $('#s_alert').delay(3000); $('#s_alert').fadeOut(500);" style="z-index:3;" />
		<div class="alert alert-danger" id="s_alert">
			<button type="button" class="close" onclick="javascript: $('#s_alert').fadeOut();">x</button>
			<strong>Warning : </strong> <div id="error-text" class="error-text"><ul></ul></div>
		</div>
		<!-- End Alert Validation Form -->
	</body>
</html>
