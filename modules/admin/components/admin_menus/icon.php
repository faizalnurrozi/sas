<?php
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();
?>
<html>
	<head>
		<title>Pilih Icon</title>
		<script type="text/javascript" src="includes/ajax.js"></script>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="includes/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="includes/bootstrap/css/font-awesome.css">
		<link rel="stylesheet" href="includes/dist/css/ionicons.min.css">
		<link rel="stylesheet" href="includes/dist/css/AdminLTE.css">
		<link rel="stylesheet" href="includes/dist/css/skins/_all-skins.min.css">

		<script src="includes/plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script src="includes/bootstrap/js/bootstrap.min.js"></script>
		<script src="includes/dist/js/app.min.js"></script>
		
		<style>
		body{
			padding:10px;
		}
		.col-xs-1{
			border:1px solid #CCC;
			text-align:center;
			padding:10px;
			cursor:pointer;
		}
		.col-xs-1:hover{
			background:#f2f2f2;
		}
		</style>
	</head>
	<body onmouseover="javascript: window.top.autoResize('iframe_icon');">
		<section id="web-application">
			<?php
			$query = $db->sql("SELECT nama FROM _fa ORDER BY nama ASC");
			while($result = $db->fetch_assoc($query)){
			?>
			<div class='col-xs-1' title="<?php echo $result['nama']; ?>" onclick="window.parent.document.getElementById('dismissIcon').click(); window.top.document.getElementById('iframe_admin_menus').contentDocument.getElementById('txticon').value=this.innerHTML;"><i class='<?php echo $result['nama']; ?>'></i></div>
			<?php } $db->close($query); ?>
		</section>
	</body>
</html>