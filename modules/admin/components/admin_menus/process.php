<?php 
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();

switch($_REQUEST['proc']){
	/* --- Insert Tabel --- */
	case 'add' :
		$id 	= strtoupper(uniqid());
		$level	= @$_POST['txtlevel'];
		$parent	= @$_POST['cbmenu'];
		$nama 	= @$_POST['txtnama'];
		$link	= str_replace("content.php", "content.php?menu=".$id, @$_POST['txtlink']);
		$icon	= @$_POST['txticon'];
		$urutan	= @$_POST['txturutan'];
		$desc	= @$_POST['txtdeskripsi'];
		
		$qData = $db->insert("_admin_menus", 
			array(
				'id_admin_menus' => $id, 
				'level' => $level,
				'id_admin_menus_parent' => $parent, 
				'nama' => $nama, 
				'link' => $link,  
				'icon' => $icon, 
				'urutan' => $urutan,
				'deskripsi' => $desc
			)
		);
		
		if($qData==true) $_SESSION[_APP_.'s_message_info'] = "Tambah data berhasil";
		else $_SESSION[_APP_.'s_message_error'] = "Tambah data gagal";
		$db->close($qData);
		
		/* ----- Activity Logs (Insert) ------ */
		$func->activity_logs_insert("_admin_menus", $id, @$_SESSION[_APP_.'s_userAdmin']);
		/* ----- End Activity Logs (Insert) ------ */
		
		echo "<script>window.parent.sendRequest('content.php', 'module=admin&component=admin_menus&action=list&ajax=true', 'list', 'div');</script>";
		echo "<meta http-equiv='refresh' content='0;url=content.php?module=admin&component=admin_menus&action=add' />";
	break;
	/* --- End Insert Tabel --- */
	
	/* --- Update Tabel --- */
	case 'update' :
		$id 	= @$_POST['txtid'];
		$nama 	= @$_POST['txtnama'];
		$level	= @$_POST['txtlevel'];
		$parent	= @$_POST['cbmenu'];
		$icon	= @$_POST['txticon'];
		if(preg_match('/menu=/i', @$_POST['txtlink'])==0){
			$link	= str_replace("content.php", "content.php?menu=".$id, @$_POST['txtlink']);
		}else{
			$link	= @$_POST['txtlink'];
		}
		$urutan	= @$_POST['txturutan'];
		$desc	= @$_POST['txtdeskripsi'];
		$start	= @$_POST['start'];
		$keyword= @$_POST['keyword'];
		
		/*Update child menu*/
		$hqData = $db->sql("SELECT id_admin_menus FROM _admin_menus WHERE id_admin_menus_parent = '$id'");
		while($result = $db->fetch_assoc($hqData)){
			$idChild= $result['id_admin_menus'];
			$qData = $db->update("_admin_menus", array('level' => ($level+1)), array('id_admin_menus' => $idChild));
		}		
		
		$hqData = $db->update("_admin_menus", 
			array(
				'level' => $level, 
				'id_admin_menus_parent' => $parent, 
				'nama' => $nama, 
				'link' => $link, 
				'icon' => $icon, 
				'urutan' => $urutan,
				'deskripsi'	=> $desc
			), array('id_admin_menus' => $id)
		);
			
		if($hqData==true) $_SESSION[_APP_.'s_message_info'] = "Update data berhasil";
		else $_SESSION[_APP_.'s_message_error'] = "Update data gagal";
		$db->close($hqData);
		
		/* ----- Activity Logs (Update) ------ */
		$func->activity_logs_update("_admin_menus",
			array(
				'level'	=>	$level,
				'nama'	=>	$nama,
				'urutan'=>	$urutan,
				'deskripsi'	=> $desc
			), array('id_admin_menus' => $id), 'OR', $_SESSION[_APP_.'s_userAdmin']
		);
		/* ----- End Activity Logs (Update) ------ */
		
		echo "<script>window.parent.sendRequest('content.php', 'module=admin&component=admin_menus&action=list&start=$start&ajax=true&keyword=$keyword', 'list', 'div');</script>";
		echo "<meta http-equiv='refresh' content='0;url=content.php?module=admin&component=admin_menus&action=add' />";
	break;
	/* --- End Update Tabel --- */
	
	/* -- Delete data tabel -- */
	case 'delete' :
		$start = $_REQUEST['start'];
		$keyword = $_REQUEST['keyword'];
		$idx = $_REQUEST['id'];
		
		$qData = $db->delete("_admin_menus_access", array('id_admin_menus' => $idx));
		$qData2 = $db->delete("_admin_menus", array('id_admin_menus' => $idx));
		
		if($qData==true) $_SESSION[_APP_.'s_message_info'] = "Hapus data berhasil";
		else $_SESSION[_APP_.'s_message_error'] = "Hapus data gagal";
		$db->close($qData);
		$db->close($qData2);
		
		/* ----- Activity Logs (Delete) ------ */
		$func->activity_logs_delete("_admin_menus", $idx, $_SESSION[_APP_.'s_userAdmin']);
		/* ----- End Activity Logs (Delete) ------ */
		
		header("location: content.php?module=admin&component=admin_menus&action=list&&start=$start&ajax=true&keyword=$keyword");
	break;
	/* --- End delete data tabel --- */
	
	case 'level' :
		$id = @$_REQUEST['id'];
		if($id == '0') $x = 0;
		else{
			$query = $db->sql("SELECT level FROM _admin_menus WHERE id_admin_menus = '$id'");
			$result = $db->fetch_assoc($query);
			$x = $result['level'] + 1;
			$db->close($query);
		}
		echo "<script>document.getElementById('txtlevel').value='$x';</script>";
	break;
	
	case 'baru' :
		$idx = @$_REQUEST['idx'];
		$status = @$_REQUEST['status'];
		$hqData = $db->update("_admin_menus", array('baru' => $status), array('id_admin_menus' => $idx));
		if($hqData == true){
			echo "<i class='fa fa-check' style='color:green;'></i>";
		}else{
			echo "<i class='fa fa-remove' style='color:red;'></i>";
		}
		$db->close($hqData);
	break;
}
?>