<?php
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();
$msc = microtime(true);

if(isset($_SESSION[_APP_.'s_userAdmin'])){
	$_SESSION[_APP_.'s_adminPage'] = "module=admin&component=admin_menus&add=false";
	
	/* User Akses Menu :
		3. Akses Semua
		2. Akses Hapus
		1. Akses Tambah/Edit
		0. Akses View
	*/
	unset($_SESSION[_APP_.'s_accessMenu']);
	if(@$_REQUEST['menu']!='') $_SESSION[_APP_.'s_menuPage'] = @$_REQUEST['menu'];
	list($access) = $db->result_row("SELECT access FROM _admin_menus_access WHERE id_admin_group = '".$_SESSION[_APP_.'s_idGroupAdmin']."' AND id_admin_menus = '".$_SESSION[_APP_.'s_menuPage']."' "); 
	$_SESSION[_APP_.'s_accessMenu'] = $access;
	/* End User Akses Menu */
	
	?>
	
	<!-- Header & Breadcrumb -->
	<?php
	list($level, $menus, $icon, $link, $parent) = $db->result_row("SELECT level, nama, icon, link, id_admin_menus_parent FROM _admin_menus WHERE id_admin_menus = '".$_SESSION[_APP_.'s_menuPage']."' ");
	?>
	<section class="content-header">
		<h1><?php echo $menus; ?></h1>
		<ol class="breadcrumb">
		<?php
		$menux[] = "<li class='active'><a style='text-decoration:none;'>".stripslashes($icon)." ".str_replace(" ", "&nbsp;", $menus)."</a></li>";
		for($i=($level-1); $i>=0; $i--){
			list($levelx, $menusx, $iconx, $linkx, $parentx) = $db->result_row("SELECT level, nama, icon, link, id_admin_menus_parent FROM _admin_menus WHERE id_admin_menus = '$parent' AND level = '$i' ");
			$menux[] = "<li><a style='text-decoration:none;cursor:pointer;' onclick=\"".stripslashes($linkx)."\">".stripslashes($iconx)." ".str_replace(" ", "&nbsp;", $menusx)."</a></li>";
			$parent = $parentx;
		}
		$cmenu = count($menux);
		for($a=$cmenu; $a>=0; $a--){
			echo @$menux[$a];
		}
		?>
		</ol>
	</section>
	<!-- End Header & Breadcrumb -->
	
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">&nbsp;</h3>
						<div class="box-tools">
							<div class="input-group">
								<div class="input-group-btn">
									<button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#myModal" onclick="javascript: document.getElementById('iframe_admin_menus').src='content.php?module=admin&component=admin_menus&action=add'; "><i class="fa fa-plus"></i> Tambah data</button>
									<button class="hide" data-toggle="modal" data-target="#myModalIcon" id="bntModalIcon" onclick="javascript: document.getElementById('iframe_icon').src='content.php?module=admin&component=admin_menus&action=icon'; "><i class="fa fa-plus"></i> Modal</button>
								</div>
								<input type="text" name="table_search" class="form-control input-sm pull-right" id="keyword" placeholder="Search" onkeyup="javascript: if(event.keyCode==13){ sendRequest('content.php', 'module=admin&component=admin_menus&action=list&ajax=true&keyword='+document.getElementById('keyword').value, 'list', 'div'); }" />
								<div class="input-group-btn">
									<button class="btn btn-sm btn-default" onclick="javascript: sendRequest('content.php', 'module=admin&component=admin_menus&action=list&ajax=true&keyword='+document.getElementById('keyword').value, 'list', 'div');"><i class="fa fa-search"></i></button>
								</div>
							</div>
						</div>
					</div>
					
					<div id="list"><?php include "list.php"; ?></div>
				</div>
			</div>
		</div>
	</section>
	
	
	<div class="modal fade" id="myModal" data-backdrop="static">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Input <?php echo $menus; ?></h4>
				</div>
				<div class="modal-body">
					<iframe name="iframe_admin_menus" id="iframe_admin_menus" width="98%" height="300" frameborder="0" onload="javascript: autoResize('iframe_admin_menus')"></iframe>
				</div>
				<div class="modal-footer">
					<label class="pull-left">
						<input type="checkbox" name="cbadd" id="cbadd" value="1" />
						Tambah data lagi.
					</label>
					<button class="btn btn-primary" onclick="javascript: 
					var iframe = document.getElementById('iframe_admin_menus');
					iframe.contentDocument.getElementById('save').click();  
					"><i class="fa fa-save"></i> Simpan</button>
					<button class="btn btn-danger" data-dismiss="modal" aria-hidden="true" id="dismiss"><i class="fa fa-remove"></i> Batal</button>
				</div>
			</div>
		</div>
	</div>
	
	<div class="modal fade" id="myModalIcon" data-backdrop="static">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Font Awesome 4.5.0</h4>
				</div>
				<div class="modal-body">
					<iframe name="iframe_icon" id="iframe_icon" width="98%" height="800" frameborder="0" ></iframe>
				</div>
				<div class="modal-footer">
					<button class="btn btn-danger" data-dismiss="modal" aria-hidden="true" id="dismissIcon"><i class="fa fa-remove"></i> Batal</button>
				</div>
			</div>
		</div>
	</div>
<?php 
}else{
	include "modules/admin/components/auth/timeout.php";
}
?>