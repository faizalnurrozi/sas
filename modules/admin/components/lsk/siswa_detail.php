<?php
	if($_REQUEST['ajax'] == 'true'){
		include "globals/config.php";
		include "globals/functions.php";

		$db = new Database();
		$func = new Functions();
	}

	$limit = _LIMIT_;

	if(@$_REQUEST['start']=='') $start = 0; else $start = @$_REQUEST['start'];

	@$keyword = @$_REQUEST['keyword'];
	$qSQL 	= "SELECT A.*, B.nama AS kelas FROM _siswa AS A INNER JOIN _kelas AS B ON(A.id_kelas = B.id_kelas) WHERE (nis LIKE :key OR A.nama LIKE :key OR B.nama LIKE :key) ORDER BY B.nama, A.nis ASC";
	$hqSQL = $db->query($qSQL);
	$db->bind($hqSQL, ":key", "%".$keyword."%", "str");
	$db->exec($hqSQL);
	$totalData = $db->num_rows($hqSQL);
	$qSQL	.= " LIMIT ".$start.", ".$limit;
	$hqSQL = $db->query($qSQL);
	$db->bind($hqSQL, ":key", "%".$keyword."%", "str");
	$db->exec($hqSQL);
	$totalLimit = $db->num_rows($hqSQL);
?>

<div class="box-body table-responsive">
	<table class="table table-bordered table-hover table-striped">
		<thead>
			<tr>
				<th width="1%">No.</th>
				<th>NIS</th>
				<th>Nama Siswa</th>
				<th>Kelas</th>
			</tr>
		</thead>
		
		<tbody>
			<?php
			if($totalData=='0'){
				echo "<tr><td colspan='7' align='center'>Data belum ada</td></tr>";
			}else{
				$no = $start+1;
				while($hasil = $db->fetch_assoc($hqSQL)){
					echo "<tr class='table-list-row' style='cursor:pointer;' 
					onclick=\"javascript: 
						var iframe = window.top.document.getElementById('iframe_laporan_surat_keluar').contentDocument;
						var iframeW = window.top.document.getElementById('iframe_laporan_surat_keluar').contentWindow;
						iframe.getElementById('nis').value='$hasil[nis]';
						iframe.getElementById('nama_siswa').value='$hasil[nis] ($hasil[nama])';
						window.top.document.getElementById('dismissChoode').click();
					\">";
					echo "<td align='center'>".$no.".</td>";
					echo "<td align='left'>".$func->highlight($hasil['nis'], $keyword)."</td>";
					echo "<td align='left'>".$func->highlight($hasil['nama'], $keyword)."</td>";
					echo "<td align='left'>".$func->highlight($hasil['kelas'], $keyword)."</td>";
					echo "</tr>";
					
					$no++;
				}
			}
			?>
		</tbody>
	</table>
</div>
<div class="box-body">
	<div class="row">
		<div class="col-sm-5">
			<i>
				<?php 
				echo "Ditampilkan <b>".($totalLimit)."</b> sampai <b>".($start+$totalLimit)."</b> dari <b>$totalData</b> total data"; 
				?>
			</i>
		</div>
		<div class="col-sm-7">
			<ul class="pagination pagination-sm pull-right">
			<?php
			if($start != 0) echo "<li><a href='#' onclick=\"sendRequest('content.php','module=admin&component=laporan_surat_keluar&action=siswa_detail&ajax=true&keyword=$keyword&start=".($start-$limit)."', 'siswa_detail', 'div');\">Prev</a></li>";
			$jumlahPage = $totalData/$limit;
			for($a=0;$a<$jumlahPage;$a++){
				$x = $a * $limit;
				if($start==$a*$limit){
					echo "<li class='active'><a href='#'>".($a+1)."</a></li>";
				}else{
					echo "<li><a href='#' onclick=\"sendRequest('content.php','module=admin&component=laporan_surat_keluar&action=siswa_detail&ajax=true&keyword=$keyword&start=".($a*$limit)."', 'siswa_detail', 'div');\">".($a+1)."</a></li>";
				}
			}
			 if($start != $x) echo "<li><a href='#' onclick=\"sendRequest('content.php','module=admin&component=laporan_surat_keluar&action=siswa_detail&ajax=true&keyword=$keyword&start=".($start+$limit)."', 'siswa_detail', 'div');\">Next</a></li>";
			?>
			</ul>
		</div>
	</div>
</div>