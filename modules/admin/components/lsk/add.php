<?php
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();
?>
<html>
	<head>
		<script type="text/javascript" src="includes/ajax.js"></script>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="includes/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="includes/bootstrap/css/font-awesome.css">
		<link rel="stylesheet" href="includes/dist/css/ionicons.min.css">
		<link rel="stylesheet" href="includes/dist/css/AdminLTE.css">
		<link rel="stylesheet" href="includes/dist/css/skins/_all-skins.min.css">

		<script src="includes/plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script src="includes/bootstrap/js/bootstrap.min.js"></script>

		<!-- Input-mask -->
		<script src="includes/plugins/select2/select2.full.min.js"></script>
		<script src="includes/plugins/input-mask/jquery.inputmask.js"></script>
		<script src="includes/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
		<script src="includes/plugins/input-mask/jquery.inputmask.extensions.js"></script>
		<!-- End of Input-mask -->
		
		<!-- Input-mask -->
		<script src="includes/plugins/select2/select2.full.min.js"></script>
		<script src="includes/plugins/input-mask/jquery.inputmask.js"></script>
		<script src="includes/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
		<script src="includes/plugins/input-mask/jquery.inputmask.extensions.js"></script>
		<!-- End of Input-mask -->

		<script type="text/javascript" src="includes/plugins/datepicker/bootstrap-datepicker.min.js"></script>
		<link rel="stylesheet" href="includes/plugins/datepicker/bootstrap-datepicker3.css"/>

		<script language="JavaScript">
			$(function () {
				var options={
					format: 'dd/mm/yyyy',
					todayHighlight: true,
					autoclose: true,
				};
				$('#tanggal').datepicker(options);
			});
		</script>
	</head>
	<body>
		<div class="container-fluid">
		<form name="form_laporan_surat_keluar" method="POST" action="javascript: void(null);" enctype="multipart/form-data">
			<div class="row">
				<div class="col-xs-6">
					<div class="row">
						<div class="form-group col-xs-6">
							<label>Periode</label>
							<div class="input-group col-xs-12">
								<span class="input-group-addon" style="cursor: pointer;" onclick="javascript: window.top.document.getElementById('iframe_choose').src='content.php?module=admin&component=laporan_surat_keluar&action=siswa'; window.top.document.getElementById('bntModalChoose').click(); "><i class="fa fa-calendar"></i></span>
								<input type="hidden" name="nis" id="nis" value="<?php echo $nis ?>" />
								<input type="text" name="nama_siswa" id="nama_siswa" class="form-control input-sm" value="01/07/2018 - 30/07/2018" />
							</div>
						</div>
					</div>

					<div class="row">
						<div class="form-group col-xs-12">
							<button class="btn btn-default" type="button" onclick="javascript: sendRequest('content.php', 'module=admin&component=laporan_surat_keluar&action=detail&ajax=true&nis='+document.getElementById('nis').value+'&tahun='+document.getElementById('tahun').value+'&bulan='+document.getElementById('bulan').value, 'detail', 'div'); "><i class="fa fa-database"></i> Proses</button>
						</div>
					</div>
				</div>

				<style type="text/css">
					.detail tr th, .detail tr td{
						font-size: 12px;
						padding: 5px;
					}
				</style>
				
				<div class="col-xs-12">
					<table class="detail table-bordered table-hover table-striped">
						<thead>
							<tr>
								<th width="1%">No.</th>
								<th>No. Surat</th>
								<th>Tgl. Surat</th>
								<th>Pengolah</th>
								<th>Perihal</th>
							</tr>
						</thead>
						
						<tbody>
							<tr class="table-list-row">
								<td align="center">1.</td>
								<td>292239/LP/X/2018</td>
								<td>08/07/2018</td>
								<td>BEPDA</td>
								<td>Undangan Rapat Koordinasi</td>
							</tr>
							<tr class="table-list-row">
								<td align="center">1.</td>
								<td>292239/LP/X/2018</td>
								<td>08/07/2018</td>
								<td>Bupati Cirebon</td>
								<td>Undangan Rapat Koordinasi</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</form>
		</div>
		
		<!-- Alert Validation Form -->
		<input type="hidden" id="Modal" onclick="javascript: $('#s_alert').fadeIn(); $('#s_alert').delay(3000); $('#s_alert').fadeOut(500);" />
		<div class="alert alert-danger" id="s_alert">
			<button type="button" class="close" onclick="javascript: $('#s_alert').hide();">x</button>
			<strong>Warning : </strong> <div id="error-text" class="error-text"><ul></ul></div>
		</div>
		<!-- End Alert Validation Form -->		
	</body>
</html>