<?php
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();
?>
<html>
	<head>
		<script type="text/javascript" src="includes/ajax.js"></script>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="includes/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="includes/bootstrap/css/font-awesome.css">
		<link rel="stylesheet" href="includes/dist/css/ionicons.min.css">
		<link rel="stylesheet" href="includes/dist/css/AdminLTE.css">
		<link rel="stylesheet" href="includes/dist/css/skins/_all-skins.min.css">

		<script src="includes/plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script src="includes/bootstrap/js/bootstrap.min.js"></script>
		<script src="includes/dist/js/app.min.js"></script>

		<script type="text/javascript" src="includes/plugins/datepicker/bootstrap-datepicker.min.js"></script>
		<link rel="stylesheet" href="includes/plugins/datepicker/bootstrap-datepicker3.css"/>

		<script language="JavaScript">
			$(function () {
				var options={
					format: 'dd/mm/yyyy',
					todayHighlight: true,
					autoclose: true,
				};
				$('#tanggal_surat').datepicker(options);
				$('#tanggal_terima').datepicker(options);
			});
		</script>
		
		<script src="includes/tinymce/tinymce.min.js"></script>
		<script>
			tinymce.init({
				selector:'.tinymce',
				plugins: [
					"table", 
					"advlist autolink lists link image charmap preview hr anchor pagebreak", 
					"searchreplace wordcount visualblocks visualchars code",
					"insertdatetime media nonbreaking table contextmenu directionality",
					"emoticons template paste textcolor colorpicker textpattern imagetools codesample"
				],
				toolbar: 'undo redo | bold italic sizeselect fontselect fontsizeselect | hr alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor backcolor',
				fontsize_formats: "8px 10px 11px 12px 13px 14px 18px 24px 36px",
			});
		</script>
	</head>
	<body>
		<?php
		if(@$_REQUEST['id'] != ''){
			$qEditAdmin = "SELECT * FROM _surat_masuk WHERE id_surat_masuk = '$_REQUEST[id]'";
			$dataEdit = $db->sql($qEditAdmin);
			$resultEdit = $db->fetch_assoc($dataEdit);
		}
		?>		
		<div class="container-fluid">
		<form name="form_surat_masuk" method="POST" action="javascript: void(null);" enctype="multipart/form-data">
			<?php if(@$_REQUEST['id'] == ''){ ?>
			<input type="hidden" id="proc" name="proc" value="add" />
			<?php }else{ ?>
			<input type="hidden" id="proc" name="proc" value="update" />
			<input type="hidden" id="txtidx" name="txtidx" value="<?php echo $resultEdit['id_surat_masuk']; ?>" />
			<?php } ?>
			<div class="row">
				<div class="col-xs-6">
					<div class="row">
						<div class="form-group col-xs-4 has-no_agenda">
							<label>No. Agenda <span class="required-input">*</span></label>
							<input type="text" autocomplete="off" class="form-control input-sm" placeholder="No. agenda" name="no_agenda" id="no_agenda" value="<?php echo @$resultEdit['no_agenda']; ?>" readonly style="font-weight: bold; text-align: center;" />
						</div>
						<div class="form-group col-xs-4 has-jenis_surat">
							<label>Jenis Surat <span class="required-input">*</span></label>
							<select class="form-control input-sm" name="jenis_surat" id="jenis_surat" onchange="javascript: ajaxInput('content.php', 'module=admin&component=surat_masuk&action=process&proc=generate_id&jenis_surat='+document.getElementById('jenis_surat').value, 'no_agenda');" <?php if($resultEdit['id_surat_masuk'] != '') echo "disabled"; ?>>
								<option value="">-Jenis Surat-</option>
								<option value="UMUM" <?php if(@$resultEdit['jenis_surat'] == 'UMUM') echo "selected"; ?>>Umum</option>
								<option value="DINAS" <?php if(@$resultEdit['jenis_surat'] == 'DINAS') echo "selected"; ?>>Dinas</option>
								<option value="PENAWARAN" <?php if(@$resultEdit['jenis_surat'] == 'PENAWARAN') echo "selected"; ?>>Penawaran</option>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="form-group col-xs-12 has-no_surat">
							<label>No. Surat <span class="required-input">*</span></label>
							<input type="text" autocomplete="off" class="form-control input-sm" placeholder="No. surat ..." name="no_surat" id="no_surat" value="<?php echo @$resultEdit['no_surat']; ?>" />
						</div>
					</div>
					<div class="row">
						<div class="form-group col-xs-6 has-tanggal_surat">
							<label>Tanggal Surat</label>
							<div class="input-group col-xs-12">
								<input type="text" name="tanggal_surat" id="tanggal_surat" class="form-control input-sm" value="<?php if(@$resultEdit['id_surat_masuk'] != '') echo $func->implode_date($resultEdit['tanggal_surat']); else echo date("d/m/Y"); ?>" readonly />
								<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
							</div>
						</div>

						<div class="form-group col-xs-6 has-id">
							<label>Tanggal Terima</label>
							<div class="input-group col-xs-12">
								<input type="text" name="tanggal_terima" id="tanggal_terima" class="form-control input-sm" value="<?php if(@$resultEdit['id_surat_masuk'] != '') echo $func->implode_date($resultEdit['tanggal_terima']); else echo date("d/m/Y"); ?>" readonly />
								<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-6">
					<div class="form-group col-xs-12 has-sumber">
						<label>Sumber Surat <span class="required-input">*</span></label>
						<input type="text" autocomplete="off" class="form-control input-sm" placeholder="Sumber surat ..." name="sumber" id="sumber" value="<?php echo @$resultEdit['sumber']; ?>" />
					</div>
					<div class="form-group col-xs-12 has-lampiran">
						<label>Lampiran Surat (Gambar)</label>
						<input type="file" autocomplete="off" class="form-control input-sm" name="lampiran" id="lampiran" <?php if(@$resultEdit['id_surat_masuk'] != '') echo "disabled"; ?> />
						<?php if(@$resultEdit['id_surat_masuk'] != ''){ ?>
						<div class="checkbox-parent">
							<div class="checkbox-item">
								<label class="checkbox-label">
									<input type="checkbox" name="cblampiran" value="TRUE" onclick="javascript: if(this.checked == true){ document.getElementById('lampiran').disabled=false; }else{ document.getElementById('lampiran').disabled=true; } " />
									<font>Centang untuk mengganti file</font>
								</label>
							</div>
						</div>
						<?php } ?>
					</div>
					<div class="form-group col-xs-12 has-perihal">
						<label>Perihal</label>
						<textarea class="form-control input-sm" placeholder="Perihal ..." name="perihal" id="perihal"><?php echo @$resultEdit['perihal']; ?></textarea>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="form-group col-xs-12 has-keterangan">
					<label>Keterangan</label>
					<textarea class="form-control input-sm tinymce" placeholder="Keterangan ..." name="keterangan" id="keterangan" style="height: 400px;"><?php echo @$resultEdit['keterangan']; ?></textarea>
				</div>
			</div>
			
			<table class="hide">
				<tr><td colspan="3">&nbsp;</td></tr>
				<tr>
					<td colspan="3" align="left">
						<button style="display:none;" id="save" class="btn btn-primary" onclick="javascript:
							var obj = document.form_surat_masuk;
							var err = '';
							if(obj.no_agenda.value==''){ $('.has-no_agenda').addClass('has-error').focus(); err+='<li>No. agenda harus di isi</li>'; }
							if(obj.jenis_surat.value==''){ $('.has-jenis_surat').addClass('has-error').focus(); err+='<li>Jenis surat harus di isi</li>'; }
							if(obj.no_surat.value==''){ $('.has-no_surat').addClass('has-error').focus(); err+='<li>Nomor surat harus di isi</li>'; }
							if(obj.sumber.value==''){ $('.has-sumber').addClass('has-error').focus(); err+='<li>Sumber surat harus di isi</li>'; }
							if(err==''){
								obj.action='content.php?module=admin&component=surat_masuk&action=process';
								obj.submit();
								
								if(window.top.document.getElementById('cbadd').checked == false){
									window.top.document.getElementById('dismiss').click();
								}
							}else{ 
								$('#Modal').click(); $('#error-text').html(err);
							}
						"></button>
						<a class="btn hidden" id="reset" onclick="javascript: document.form_surat_masuk.reset();"></a>
					</td>
				</tr>
			</table>
		</form>
		</div>
		
		<!-- Alert Validation Form -->
		<input type="hidden" id="Modal" onclick="javascript: $('#s_alert').fadeIn(); $('#s_alert').delay(3000); $('#s_alert').fadeOut(500);" />
		<div class="alert alert-danger" id="s_alert">
			<button type="button" class="close" onclick="javascript: $('#s_alert').fadeOut();">x</button>
			<strong>Warning : </strong> <div id="error-text" class="error-text"><ul></ul></div>
		</div>
		<!-- End Alert Validation Form -->
		
		<script src="includes/bootstrap/bootstrap.js"></script>
		<script type="text/javascript">
			$("[rel=tooltip]").tooltip();
			$(function() {
				$('.demo-cancel-click').click(function(){return false;});
			});
		</script>
		
	</body>
</html>