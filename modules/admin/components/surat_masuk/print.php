<?php
	ob_start();
	include "globals/config.php";
	include "globals/functions.php";
	$db = new Database();
	$func = new Functions();

	$id = @$_GET['id'];
?>


<style type="text/css">
	.kop-surat tr td{
		border-bottom: 0.5px solid #000;
		padding: 2mm;
	}

	h4{
		font-size: 13px;
		margin: 0;
		padding: 0;
	}

	h2{
		font-size: 17px;
		margin: 0;
		padding: 0;
	}

	p{
		font-size: 12px;
		margin: 0;
		padding: 0;
	}
</style>

<?php
	$result = $db->result_assoc(" SELECT * FROM _surat_masuk WHERE id_surat_masuk = '$id' ");
	$namaFile = "Surat-Masuk-".$result['no_agenda']."-".$result['jenis_surat'];
?>

<page backcolor="#FEFEFE" backtop="25mm" backbottom="10mm" backleft="10mm" backright="10mm" style="font-family: times;">
	<style type="text/css">
		.atas-kanan{
			margin-top: 10mm;
		}
		.atas-kanan tr td{
			padding: 1mm;
			font-size: 14px;
		}

		.p-isi{
			padding-top: 5mm;
			font-size: 14px;
		}
	</style>

	<table class="atas-kanan" cellspacing="0" cellspacing="0">
		<tr>
			<td style="width: 90mm;"></td>
			<td style="width: 17mm;">No. Surat</td>
			<td style="width: 1mm;" align="center">:</td>
			<td style="width: 57mm;"><?php echo $result['no_surat']; ?></td>
		</tr>
		<tr>
			<td style="width: 90mm;"></td>
			<td style="width: 17mm;" valign="top">Perihal</td>
			<td style="width: 1mm;" valign="top" align="center">:</td>
			<td style="width: 57mm;"><?php echo $result['perihal']; ?></td>
		</tr>
	</table>

	<br>

	<?php echo $result['keterangan']; ?>
</page>

<?php
	$content = ob_get_clean();

	require_once('includes/html2pdf/html2pdf.class.php');
	try{
		$html2pdf = new HTML2PDF('P', 'Legal', 'fr');
		$html2pdf->pdf->SetDisplayMode('fullpage');
		$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
		$html2pdf->Output($namaFile.'.pdf');
	}
	catch(HTML2PDF_exception $e) {
		echo $e;
		exit;
	}
?>
