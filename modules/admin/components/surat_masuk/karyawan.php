<?php
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();
?>
<html>
	<head>
		<script type="text/javascript" src="includes/ajax.js"></script>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="includes/bootstrap/css/bootstrap.css">
		<link rel="stylesheet" href="includes/bootstrap/css/font-awesome.css">
		<link rel="stylesheet" href="includes/dist/css/ionicons.min.css">
		<link rel="stylesheet" href="includes/dist/css/AdminLTE.css">
		<link rel="stylesheet" href="includes/dist/css/skins/_all-skins.min.css">

		<script src="includes/plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script src="includes/bootstrap/js/bootstrap.min.js"></script>
	</head>

	<body>
		<input type="text" name="table_search" class="form-control input-sm pull-right" id="keyword" placeholder="Search" onkeyup="javascript: sendRequest('content.php', 'module=admin&component=surat_masuk&action=karyawan_detail&ajax=true&keyword='+document.getElementById('keyword').value, 'karyawan_detail', 'div');" autofocus value="<?php echo $keyword; ?>" />

		<div id="karyawan_detail"><?php include("karyawan_detail.php"); ?></div>
	</body>
</html>
