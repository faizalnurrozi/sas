<?php 
include "globals/config.php";
include "globals/functions.php";
include "globals/upload.php";
$db 	= new Database();
$func 	= new Functions();

switch(@$_REQUEST['proc']){
	case 'add' :
		# ---------------------#
		# Penangkapan variable #
		# ---------------------#
		
		$id 			= strtoupper(uniqid());
		$no_agenda 		= $_POST['no_agenda'];
		$jenis_surat 	= $_POST['jenis_surat'];
		$no_surat 		= $_POST['no_surat'];
		$tanggal_surat 	= $func->explode_date($_POST['tanggal_surat']);
		$tanggal_terima = $func->explode_date($_POST['tanggal_terima']);
		$sumber 		= $_POST['sumber'];
		$perihal 		= $_POST['perihal'];
		$keterangan 	= $_POST['keterangan'];

		/**
		 * handel upload file
		 */
		
		$handle = new upload($_FILES['lampiran']);
		$lampiran = '';
		if ($handle->uploaded && $handle->file_is_image){
			$handle->file_new_name_body   = strtoupper(uniqid());
			/*$handle->image_resize         = true;
			$handle->image_ratio_crop     = true;
			$handle->image_y              = 220;
			$handle->image_x              = 190;*/
			$handle->process('./resources/');

			if ($handle->processed){

				$handle->clean();

				$lampiran = $handle->file_dst_pathname;
			}else{
				$_SESSION[_APP_.'s_message_error'] = $handle->error;
			}
		}else{
			$_SESSION[_APP_.'s_message_error'] = $handle->error;
		}

		$data_insert	= array('id_surat_masuk' => $id, 'jenis_surat' => $jenis_surat, 'no_agenda' => $no_agenda, 'no_surat' => $no_surat, 'tanggal_surat' => $tanggal_surat, 'tanggal_terima' => $tanggal_terima, 'sumber' => $sumber, 'perihal' => $perihal, 'keterangan' => $keterangan, 'lampiran' => $lampiran);
		
		$hqData = $db->insert("_surat_masuk", $data_insert);

		if($hqData==true) $_SESSION[_APP_.'s_message_info'] = "Tambah data berhasil";
		else $_SESSION[_APP_.'s_message_error'] = "Tambah data gagal";
		
		#---------- * ----------#
		
		
		# ----------------#
		# Pencatatan Logs #
		# ----------------#
		
		/* ----- Activity Logs (Insert) ------ */
		$func->activity_logs_insert("_surat_masuk", $id, $_SESSION[_APP_.'s_userAdmin']);
		/* ----- End Activity Logs (Insert) ------ */
		
		#---------- * ----------#
		
		
		# ------------#
		# Kirim balik #
		# ------------#
		
		echo "<script>window.parent.sendRequest('content.php', 'module=admin&component=surat_masuk&action=list&ajax=true', 'list', 'div');</script>";
		echo "<meta http-equiv='refresh' content='0;url=content.php?module=admin&component=surat_masuk&action=add' />";
		
		#---------- * ----------#
		
		break;
		
	case 'update' :
		# ---------------------#
		# Penangkapan variable #
		# ---------------------#
		
		$idx 			= @$_POST['txtidx'];
		$no_agenda 		= $_POST['no_agenda'];
		$no_surat 		= $_POST['no_surat'];
		$tanggal_surat 	= $func->explode_date($_POST['tanggal_surat']);
		$tanggal_terima = $func->explode_date($_POST['tanggal_terima']);
		$sumber 		= $_POST['sumber'];
		$perihal 		= $_POST['perihal'];
		$keterangan 	= $_POST['keterangan'];
		$cblampiran 	= @$_POST['cblampiran'];
		
		# ----------------#
		# Pencatatan Logs #
		# ----------------#

		$data_update	= array('no_agenda' => $no_agenda, 'no_surat' => $no_surat, 'tanggal_surat' => $tanggal_surat, 'tanggal_terima' => $tanggal_terima, 'sumber' => $sumber, 'perihal' => $perihal, 'keterangan' => $keterangan);


		if($cblampiran == 'TRUE'){
			/**
			 * handel upload file
			 */
			
			list($file_lampiran) = $db->result_row("SELECT lampiran FROM _surat_masuk WHERE id_surat_masuk = '$idx'");
			if(file_exists($file_lampiran)) unlink($file_lampiran);
			
			$handle = new upload($_FILES['lampiran']);
			$lampiran = '';
			if ($handle->uploaded && $handle->file_is_image){
				$handle->file_new_name_body   = strtoupper(uniqid());
				/*$handle->image_resize         = true;
				$handle->image_ratio_crop     = true;
				$handle->image_y              = 220;
				$handle->image_x              = 190;*/
				$handle->process('./resources/');

				if ($handle->processed){

					$handle->clean();

					$lampiran = $handle->file_dst_pathname;

					$data_update['lampiran'] = $lampiran;
				}else{
					$_SESSION[_APP_.'s_message_error'] = $handle->error;
				}
			}else{
				$_SESSION[_APP_.'s_message_error'] = $handle->error;
			}
		}

		
		/* ----- Activity Logs (Update) ------ */
		$func->activity_logs_update("_surat_masuk", $data_update, array('id_surat_masuk' => $idx), 'OR', $_SESSION[_APP_.'s_userAdmin']);
		/* ----- End Activity Logs (Update) ------ */
		
		#---------- * ----------#
		
		
		
		# --------------#
		# Proses Update #
		# --------------#
		
		$hqData = $db->update("_surat_masuk", $data_update, array('id_surat_masuk' => $idx));
		
		if($hqData==true) $_SESSION[_APP_.'s_message_info'] = "Update data berhasil";
		else $_SESSION[_APP_.'s_message_error'] = "Update data gagal";
		
		#---------- * ----------#
		
		
		# ------------#
		# Kirim balik #
		# ------------#
		
		echo "<script>window.parent.sendRequest('content.php', 'module=admin&component=surat_masuk&action=list&ajax=true', 'list', 'div');</script>";
		echo "<meta http-equiv='refresh' content='0;url=content.php?module=admin&component=surat_masuk&action=add' />";
		
		#---------- * ----------#
		break;
		
	case 'delete' :
		# ---------------------#
		# Penangkapan variable #
		# ---------------------#
		
		$idx = @$_REQUEST['id'];
		
		#---------- * ----------#
		
		
		
		# --------------#
		# Proses Delete #
		# --------------#
			
		list($file_lampiran) = $db->result_row("SELECT lampiran FROM _surat_masuk WHERE id_surat_masuk = '$idx'");
		if(file_exists($file_lampiran)) unlink($file_lampiran);
		
		$hqData = $db->delete("_surat_masuk", array('id_surat_masuk' => $idx));
		$hqData = $db->delete("_disposisi", array('id_surat_masuk' => $idx));
		
		if($hqData==true) $_SESSION[_APP_.'s_message_info'] = "Hapus data berhasil";
		else $_SESSION[_APP_.'s_message_error'] = "Hapus data gagal";
		
		#---------- * ----------#
		
		
		# ----------------#
		# Pencatatan Logs #
		# ----------------#
		
		/* ----- Activity Logs (Delete) ------ */
		$func->activity_logs_delete("_surat_masuk", $idx, $_SESSION[_APP_.'s_userAdmin']);
		/* ----- End Activity Logs (Delete) ------ */
		
		#---------- * ----------#
		
		
		# ------------#
		# Kirim balik #
		# ------------#
		
		header("location: content.php?module=admin&component=surat_masuk&action=list&ajax=true");
		
		#---------- * ----------#
		break;
		
	case 'disposisi' :
		# ---------------------#
		# Penangkapan variable #
		# ---------------------#
		
		$id_surat_masuk = @$_REQUEST['id_surat_masuk'];
		$nip 			= @$_REQUEST['nip'];
		$catatan 		= @$_REQUEST['catatan'];
		
		#---------- * ----------#
		
		
		
		# --------------#
		# Proses Delete #
		# --------------#
			
		list($cek_data) = $db->result_row("SELECT COUNT(nip) FROM _disposisi WHERE id_surat_masuk = '$id_surat_masuk'");

		if($cek_data > 0){
			$hqData = $db->update("_disposisi", array('nip' => $nip, 'catatan' => $catatan), array('id_surat_masuk' => $id_surat_masuk));
		}else{
			$hqData = $db->insert("_disposisi", array('id_surat_masuk' => $id_surat_masuk, 'nip' => $nip, 'catatan' => $catatan));
		}
		
		if($hqData==true) $_SESSION[_APP_.'s_message_info'] = "Berhasil didisposisikan";
		else $_SESSION[_APP_.'s_message_error'] = "Gagal didisposisikan";
		
		#---------- * ----------#
		
		
		# ------------#
		# Kirim balik #
		# ------------#
		
		header("location: content.php?module=admin&component=surat_masuk&action=main");
		
		#---------- * ----------#
		break;

	case "generate_id":
		if(@$_REQUEST['jenis_surat'] != ''){
			$jenis_surat = ($_REQUEST['jenis_surat'] == 'PENAWARAN') ? "jenis_surat = 'PENAWARAN'" : "jenis_surat IN('UMUM', 'DINAS')";
			list($lastID) = $db->result_row("SELECT MAX(no_agenda) FROM _surat_masuk WHERE $jenis_surat ORDER BY no_agenda DESC");
			$lastID++;
			echo $func->number_pad($lastID, 4);
		}
		break;
}
?>