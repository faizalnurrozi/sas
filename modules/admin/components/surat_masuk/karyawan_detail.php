<?php
	if($_REQUEST['ajax'] == 'true'){
		include "globals/config.php";
		include "globals/functions.php";

		$db = new Database();
		$func = new Functions();
	}

	$limit = _LIMIT_;

	if(@$_REQUEST['start']=='') $start = 0; else $start = @$_REQUEST['start'];

	@$keyword = @$_REQUEST['keyword'];
	$qSQL 	= "SELECT A.*, B.nama AS jabatan FROM _karyawan AS A INNER JOIN _jabatan AS B ON(A.id_jabatan = B.id_jabatan) WHERE (nip LIKE :key OR A.nama LIKE :key OR B.nama LIKE :key) ORDER BY A.nip ASC";
	$hqSQL = $db->query($qSQL);
	$db->bind($hqSQL, ":key", "%".$keyword."%", "str");
	$db->exec($hqSQL);
	$totalData = $db->num_rows($hqSQL);
	$qSQL	.= " LIMIT ".$start.", ".$limit;
	$hqSQL = $db->query($qSQL);
	$db->bind($hqSQL, ":key", "%".$keyword."%", "str");
	$db->exec($hqSQL);
	$totalLimit = $db->num_rows($hqSQL);
?>

<div class="box-body table-responsive">
	<table class="table table-bordered table-hover table-striped">
		<thead>
			<tr>
				<th width="1%">No.</th>
				<th>NIP</th>
				<th>Nama Karyawan</th>
				<th>Jabatan</th>
			</tr>
		</thead>
		
		<tbody>
			<?php
			if($totalData=='0'){
				echo "<tr><td colspan='7' align='center'>Data belum ada</td></tr>";
			}else{
				$no = $start+1;
				while($hasil = $db->fetch_assoc($hqSQL)){
					echo "<tr class='table-list-row' style='cursor:pointer;' 
					onclick=\"javascript:
						window.top.document.getElementById('nip').value='$hasil[nip]';
						window.top.document.getElementById('nama_karyawan').value='$hasil[nip] ($hasil[nama])';
						window.top.document.getElementById('dismissChoode').click();
					\">";
					echo "<td align='center'>".$no.".</td>";
					echo "<td align='left'>".$func->highlight($hasil['nip'], $keyword)."</td>";
					echo "<td align='left'>".$func->highlight($hasil['nama'], $keyword)."</td>";
					echo "<td align='left'>".$func->highlight($hasil['jabatan'], $keyword)."</td>";
					echo "</tr>";
					
					$no++;
				}
			}
			?>
		</tbody>
	</table>
</div>
<div class="box-body">
	<div class="row">
		<div class="col-sm-5">
			<i>
				<?php 
				echo "Ditampilkan <b>".($totalLimit)."</b> sampai <b>".($start+$totalLimit)."</b> dari <b>$totalData</b> total data"; 
				?>
			</i>
		</div>
		<div class="col-sm-7">
			<ul class="pagination pagination-sm pull-right">
			<?php
			if($start != 0) echo "<li><a href='#' onclick=\"sendRequest('content.php','module=admin&component=surat_masuk&action=karyawan_detail&ajax=true&keyword=$keyword&start=".($start-$limit)."', 'karyawan_detail', 'div');\">Prev</a></li>";
			$jumlahPage = $totalData/$limit;
			for($a=0;$a<$jumlahPage;$a++){
				$x = $a * $limit;
				if($start==$a*$limit){
					echo "<li class='active'><a href='#'>".($a+1)."</a></li>";
				}else{
					echo "<li><a href='#' onclick=\"sendRequest('content.php','module=admin&component=surat_masuk&action=karyawan_detail&ajax=true&keyword=$keyword&start=".($a*$limit)."', 'karyawan_detail', 'div');\">".($a+1)."</a></li>";
				}
			}
			 if($start != $x) echo "<li><a href='#' onclick=\"sendRequest('content.php','module=admin&component=surat_masuk&action=karyawan_detail&ajax=true&keyword=$keyword&start=".($start+$limit)."', 'karyawan_detail', 'div');\">Next</a></li>";
			?>
			</ul>
		</div>
	</div>
</div>