<?php
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();

$id = @$_GET['id'];
?>
<html>
	<head>
		<title><?php echo $menus; ?></title>
		
		<script type="text/javascript" src="includes/ajax.js"></script>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="includes/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="includes/bootstrap/css/font-awesome.css">
		<link rel="stylesheet" href="includes/dist/css/ionicons.min.css">
		<link rel="stylesheet" href="includes/dist/css/AdminLTE.css">
		<link rel="stylesheet" href="includes/dist/css/skins/_all-skins.min.css">
	</head>
	<body bgcolor="white">
		<?php $result = $db->result_assoc(" SELECT * FROM _surat_masuk WHERE id_surat_masuk = '$id' "); ?>
		<table class="table">
			<tr>
				<td width="20%">No. Agenda</td>
				<td width="1%">&nbsp;:&nbsp;</td>
				<td><?php echo $result['no_agenda']; ?></td>
			</tr>
			<tr>
				<td>No. Surat</td>
				<td>&nbsp;:&nbsp;</td>
				<td><?php echo $result['no_surat']; ?></td>
			</tr>
			<tr>
				<td>Tanggal Surat</td>
				<td>&nbsp;:&nbsp;</td>
				<td><?php echo $func->report_date($result['tanggal_surat']); ?></td>
			</tr>
			<tr>
				<td>Tanggal Terima</td>
				<td>&nbsp;:&nbsp;</td>
				<td><?php echo $func->report_date($result['tanggal_terima']); ?></td>
			</tr>
			<tr>
				<td>Sumber Surat</td>
				<td>&nbsp;:&nbsp;</td>
				<td><?php echo $result['sumber']; ?></td>
			</tr>
			<tr>
				<td>Perihal</td>
				<td>&nbsp;:&nbsp;</td>
				<td><?php echo $result['perihal']; ?></td>
			</tr>
			<tr>
				<td>Lampiran</td>
				<td>&nbsp;:&nbsp;</td>
				<td><a href="<?php echo $result['lampiran']; ?>" target="_blank">Download lampiran</a></td>
			</tr>
			<tr>
				<td colspan="3"><b>Keterangan</b></td>
			</tr>
			<tr>
				<td colspan="3"><?php echo $result['keterangan']; ?></td>
			</tr>
		</table>
	</body>
</html>