<?php
if(@$_REQUEST['ajax'] == 'true'){
	include "globals/config.php";
	include "globals/functions.php";	
	$db = new Database();
	$func = new Functions();
}
$limit = _LIMIT_;
?>

<div class="box-body table-responsive">
	<!-- Alert Process -->
	<?php if(isset($_SESSION[_APP_.'s_message_info'])){ ?>
	<div class="alert alert-info">
		<button type="button" class="close" data-dismiss="alert">x</button>
		<strong>Status : </strong> <?php echo $_SESSION[_APP_.'s_message_info']; unset($_SESSION[_APP_.'s_message_info']); ?>
	</div>
	<?php } ?>
	<?php if(isset($_SESSION[_APP_.'s_message_error'])){ ?>
	<div class="alert alert-danger">
		<button type="button" class="close" data-dismiss="alert">x</button>
		<strong>Error : </strong> <?php echo $_SESSION[_APP_.'s_message_error']; unset($_SESSION[_APP_.'s_message_error']); ?>
	</div>
	<?php } ?>
	<!-- Alert Process -->
</div>


<?php
	/*Sorting*/
	if($_POST['sort']=='reset'){
		$_SESSION[_APP_.'s_field_surat_masuk'] = "id_surat_masuk";
		$_SESSION[_APP_.'s_sort_surat_masuk'] = "ASC";
		$iconsort = "<i class='fa fa-fw fa-caret-up'></i>";
	}
	switch($_POST['field']){
		case 'id_surat_masuk' : $_SESSION[_APP_.'s_field_surat_masuk'] = "id_surat_masuk"; break;
		case 'no_agenda' : $_SESSION[_APP_.'s_field_surat_masuk'] = "no_agenda"; break;
		case 'sumber' : $_SESSION[_APP_.'s_field_surat_masuk'] = "sumber"; break;
		case 'perihal' : $_SESSION[_APP_.'s_field_surat_masuk'] = "perihal"; break;
		default : 
			if(!isset($_SESSION[_APP_.'s_field_surat_masuk'])){
				$_SESSION[_APP_.'s_field_surat_masuk'] = "id_surat_masuk";
			}
			break;
	}
	if(!isset($_SESSION[_APP_.'s_sort_surat_masuk'])){
		$_SESSION[_APP_.'s_sort_surat_masuk'] = "ASC";
		$iconsort = "<i class='fa fa-fw fa-caret-down'></i>";
	}else{
		switch($_POST['act']){
			case 'sort' :
				if($_SESSION[_APP_.'s_sort_surat_masuk'] == "ASC"){
					$_SESSION[_APP_.'s_sort_surat_masuk'] = "DESC";
					$iconsort = "<i class='fa fa-fw fa-caret-down'></i>";
				}else if($_SESSION[_APP_.'s_sort_surat_masuk'] == "DESC"){
					$_SESSION[_APP_.'s_sort_surat_masuk'] = "ASC";
					$iconsort = "<i class='fa fa-fw fa-caret-up'></i>";
				}
				break;
			case 'paging' :
				if($_SESSION[_APP_.'s_sort_surat_masuk'] == "ASC"){
					$iconsort = "<i class='fa fa-fw fa-caret-up'></i>";
				}else if($_SESSION[_APP_.'s_sort_surat_masuk'] == "DESC"){
					$iconsort = "<i class='fa fa-fw fa-caret-down'></i>";
				}
				break;
		}
	}
	/*End Sorting*/
	
	if(@$_REQUEST['start']=='') $start = 0; else $start = @$_REQUEST['start'];
	
	$keyword = @$_REQUEST['keyword'];
	$qSQL = "SELECT * FROM _surat_masuk WHERE (no_agenda LIKE :key OR no_surat LIKE :key OR sumber LIKE :key OR perihal LIKE :key OR keterangan LIKE :key) ORDER BY ".$_SESSION[_APP_.'s_field_surat_masuk']." ".$_SESSION[_APP_.'s_sort_surat_masuk'];
	$hqSQL = $db->query($qSQL);
	$db->bind($hqSQL, ":key", "%".$keyword."%", "str");
	$db->exec($hqSQL);
	$totalData = $db->num_rows($hqSQL);
	$qSQL	.= " LIMIT ".$start.", ".$limit;
	$hqSQL = $db->query($qSQL);
	$db->bind($hqSQL, ":key", "%".$keyword."%", "str");
	$db->exec($hqSQL);
	$totalLimit = $db->num_rows($hqSQL);
?>

<div class="box-body table-responsive">
	<table class="table table-bordered table-hover table-striped">
		<thead>
			<tr>
				<th width="1%">No.</th>
				<th>Agenda</th>
				<th>No. Surat</th>
				<th>Tgl. Terima</th>
				<th>Pengirim</th>
				<th>Perihal</th>
				<th width="250">&nbsp;</th>
			</tr>
		</thead>
		
		<tbody>
			<?php
			if($totalData=='0'){
				echo "<tr><td colspan='7' align='center'>Data belum ada</td></tr>";
			}else{
				$no = $start+1;
				while($hasil = $db->fetch_assoc($hqSQL)){
					echo "<tr class='table-list-row'>";
					echo "<td align='center'>".$no.".</td>";
					echo "<td align='left'>".$func->highlight($hasil['no_agenda'], $keyword)."</td>";
					echo "<td align='left'>".$func->highlight($hasil['no_surat'], $keyword)."</td>";
					echo "<td align='left'>".$func->report_date($hasil['tanggal_terima'])."</td>";
					echo "<td align='left'>".$func->highlight($hasil['sumber'], $keyword)."</td>";
					echo "<td align='left'>".$func->highlight($hasil['perihal'], $keyword)."</td>";
					
					echo "<td align='center' valign='top'>";
					echo "<div class='btn-group btn-group-xs' role='group' aria-label='...'>";

					echo "<button type='button' class='btn btn-default' title='Print' onclick=\"javascript: window.open('content.php?module=admin&component=surat_masuk&action=print&id=$hasil[id_surat_masuk]', '_blank'); \"><i class='fa fa-print'></i> Print</button>";
					echo "<button type='button' class='btn btn-default' title='Disposisi surat' onclick=\"javascript: sendRequest('content.php', 'module=admin&component=surat_masuk&action=main&disposisi=true&id=$hasil[id_surat_masuk]', 'content', 'div'); \"><i class='fa fa-refresh'></i> Disposisi surat</button>";
					
					/* -- Akses View */
					if($func->akses('R') == true){
						echo "<button type='button' class='btn btn-info' data-toggle='modal' data-target='#modal-view' onclick=\"javascript: document.getElementById('iframe_view').src='content.php?module=admin&component=surat_masuk&action=view&id=$hasil[id_surat_masuk]'; \" onmouseover=\"$(this).tooltip();\" title='View'><i class='fa fa-search'></i></button>";
					}else{
						echo "<button type='button' title='View' class='btn btn-default' disabled ><i class='fa fa-search'></i></button>";
					}
					/* -- End Akses View -- */
					
					/* Akses menu Edit */
					if($func->akses('U') == true){
						echo "<button type='button' class='btn btn-success' data-toggle='modal' data-target='#myModal' onclick=\"javascript: document.getElementById('iframe_surat_masuk').src='content.php?module=admin&component=surat_masuk&action=add&id=$hasil[id_surat_masuk]'; \" onmouseover=\"$(this).tooltip();\" title='Edit'><i class='fa fa-edit'></i></button>";
					}else{
						echo "<button type='button' title='Edit' class='btn btn-default' disabled ><i class='fa fa-edit'></i></button>";
					}
					/* End Akses menu edit */
					
					/* Akses menu Delete */
					if($func->akses('D') == true){
						echo "<button type='button' class='btn btn-danger' data-toggle='modal' data-target='#modal-delete' onclick=\"javascript:  document.getElementById('id_delete').value='$hasil[id_surat_masuk]'; \" onmouseover=\"$(this).tooltip();\" title='Delete'><i class='fa fa-trash'></i></button>";
					}else{
						echo "<button type='button' title='Delete' class='btn btn-default' disabled ><i class='fa fa-trash'></i></button>";
					}
					/* End Akses menu delete */
					echo "</div>
							</td>";
					echo "</tr>";
					
					$no++;
				}
			}
			?>
		</tbody>
	</table>
</div>
<div class="box-body">
	<div class="row">
		<div class="col-sm-5">
			<i>
				<?php 
				echo "Ditampilkan <b>1</b> sampai <b>10</b> dari <b>2</b> total data"; 
				?>
			</i>
		</div>
		<div class="col-sm-7">
			<ul class="pagination pagination-sm pull-right">
			<?php
			if($start != 0) echo "<li><a href='#' onclick=\"sendRequest('content.php','module=admin&component=surat_masuk&action=list&ajax=true&keyword=$keyword&start=".($start-$limit)."', 'list', 'div');\">Prev</a></li>";
			$jumlahPage = $totalData/$limit;
			for($a=0;$a<$jumlahPage;$a++){
				$x = $a * $limit;
				if($start==$a*$limit){
					echo "<li class='active'><a href='#'>".($a+1)."</a></li>";
				}else{
					echo "<li><a href='#' onclick=\"sendRequest('content.php','module=admin&component=surat_masuk&action=list&keyword=$keyword&ajax=true&start=".($a*$limit)."', 'list', 'div');\">".($a+1)."</a></li>";
				}
			}
			 if($start != $x) echo "<li><a href='#' onclick=\"sendRequest('content.php','module=admin&component=surat_masuk&action=list&keyword=$keyword&ajax=true&start=".($start+$limit)."', 'list', 'div');\">Next</a></li>";
			?>
			</ul>
		</div>
	</div>
</div>

<!-- Modals -->
<div class="modal fade" id="modal-view" data-backdrop="static">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">View</h4>
			</div>
			<div class="modal-body">
				<iframe name="iframe_view" id="iframe_view" width="98%" height="400" frameborder="0"></iframe>
			</div>
			<div class="modal-footer">
				<button class="btn btn-warning" data-dismiss="modal" aria-hidden="true" id="dismissView"><i class="fa fa-remove"></i> Tutup</button>
			</div>
		</div>
	</div>
</div>

<input type="hidden" id="id_delete" value="" />
<div class="modal fade" id="modal-delete" data-backdrop="static">
	<div class="modal-dialog modal-xs">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Hapus</h4>
			</div>
			<div class="modal-body">
				Apakah anda yakin ingin menghapus ?
			</div>
			<div class="modal-footer">
				<button class="btn btn-default" data-dismiss="modal" aria-hidden="true" id="dismissDelete"><i class="fa fa-repeat"></i> Batal</button>
				<button class="btn btn-danger" data-dismiss="modal" aria-hidden="true" onclick="javascript: sendRequest('content.php', 'module=admin&component=surat_masuk&action=process&proc=delete&start=<?php echo $start; ?>&keyword=<?php echo $keyword; ?>&id='+document.getElementById('id_delete').value, 'list', 'div');"><i class="fa fa-remove"></i> Hapus</button>
			</div>
		</div>
	</div>
</div>
<!-- End of Modals -->