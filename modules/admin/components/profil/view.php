<?php
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();

$user = @$_GET['user'];
?>
<html>
	<head>
		<title>Admin</title>
		<script type="text/javascript" src="includes/jquery-1.8.2.min.js"></script>
		<script language="javascript">
			$(document).ready(function(){
				$(document).focus();
			});
			$(document).keyup(function(event){
				if (event.keyCode == 27) {
					window.top.hidePopup(1000);
				}
			});
		</script>
		<link rel="stylesheet" href="includes/popup-window/popup.css"></link>
	</head>
	<body>
		<?php
		$qData = "SELECT * FROM _admin WHERE usernames = '$user'";
		$hqData = $db->sql($qData);
		$result = $db->fetch_assoc($hqData);
		?>
		<h3>DATA ADMIN</h3>
		<table width="100%">
			<tr>
				<td width="15%">Username</td>
				<td width="3%">:</td>
				<td><?php echo $result['usernames']; ?></td>
			</tr>
			<tr>
				<td width="15%">Nama</td>
				<td width="3%">:</td>
				<td><?php echo $result['nama']; ?></td>
			</tr>
			<tr>
				<td width="15%">Email</td>
				<td width="3%">:</td>
				<td><?php echo $result['email']; ?></td>
			</tr>			
			<tr>				
				<td width="15%" valign="top">Foto</td>				
				<td width="3%" valign="top">:</td>				
				<td valign="top"><?php if(file_exists("component/admin/photos/".$result['foto'])) echo "<img src='component/admin/photos/$result[foto]' width='100' />"; else echo "<img src='images/fotokosong.gif' width='100' />"; ?></td>			
			</tr>
		</table>
		<input type="button" class="button" value="Tutup" onclick="javascript: window.top.hidePopup(1000);" />
	</body>
</html>