<div class="box box-primary">
	<div class="box-body box-profile">
		<img class="profile-user-img img-responsive img-circle" src="<?php echo (file_exists($_SESSION[_APP_.'s_fotoAdmin'])) ? $_SESSION[_APP_.'s_fotoAdmin'] : "images/icon-user.png"; ?>" alt="User profile picture">
		<h3 class="profile-username text-center" id="nama_preload-parent"><?php echo $_SESSION[_APP_.'s_namaAdmin']; ?></h3>

		<ul class="list-group list-group-unbordered">
			<li class="list-group-item"><b>Username : </b> <?php echo $result['usernames']; ?></li>
			<li class="list-group-item"><b>Nama : </b> <span id="nama_preload"><?php echo $result['nama']; ?></span></li>
		</ul>
	</div>
</div>