<?php
if(isset($_REQUEST['ajax']) && $_REQUEST['ajax'] == 'true'){
	include "globals/config.php";
	include "globals/functions.php";
	$db = new Database();
	$func = new Functions();
	
	$qData = "SELECT * FROM _admin WHERE usernames = '".$_SESSION[_APP_.'s_userAdmin']."'";
	$hqData = $db->sql($qData);
	$result = $db->fetch_assoc($hqData);
	$db->close($qData);
}
?>

<!-- Alert Process -->
<?php if(isset($_SESSION[_APP_.'s_message_info'])){ ?>
<div class="alert alert-success">
	<button type="button" class="close" data-dismiss="alert">x</button>
	<strong><?php echo $_SESSION[_APP_.'s_message_info']; unset($_SESSION[_APP_.'s_message_info']); ?></strong>
</div>
<?php } ?>
<?php if(isset($_SESSION[_APP_.'s_message_error'])){ ?>
<div class="alert alert-warning">
	<button type="button" class="close" data-dismiss="alert">x</button>
	<strong><?php echo $_SESSION[_APP_.'s_message_error']; unset($_SESSION[_APP_.'s_message_error']); ?></strong>
</div>
<?php } ?>
<!-- Alert Process -->

<form class="form-horizontal" action="javascript: void(null);">
	<div class="form-group">
		<label for="txtnama" class="col-sm-2 control-label">Password&nbsp;Lama</label>
		<div class="col-sm-10">
			<input type="password" class="form-control" name="txtpasslama" id="txtpasslama" placeholder="********" />
		</div>
	</div>
	<div class="form-group">
		<label for="txtnama" class="col-sm-2 control-label">Password&nbsp;Baru</label>
		<div class="col-sm-10">
			<input type="password" class="form-control" name="txtpassbaru" id="txtpassbaru" placeholder="[0-9][a-z][A-Z]" />
		</div>
	</div>
	<div class="form-group" id="has-retype">
		<label for="txtnama" class="col-sm-2 control-label">Password&nbsp;Baru&nbsp;(ulangi)</label>
		<div class="col-sm-10">
			<input type="password" class="form-control" name="txtpassbaruretype" id="txtpassbaruretype" placeholder="[0-9][a-z][A-Z]" onkeyup="javascript: cekPassword('txtpassbaru', 'txtpassbaruretype');" />
			<i class="fa fa-times error-validate" id="error-validate" style="position:absolute; right:20px; top:8px; color: #DD4B39; display:none;"></i>
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<button type="button" class="btn btn-primary btn-flat" id="simpan" onclick="javascript: sendRequest('content.php', 'module=admin&component=profil&action=process&proc=password&passlama='+document.getElementById('txtpasslama').value+'&passbaru='+document.getElementById('txtpassbaru').value+'&passbaruretype='+document.getElementById('txtpassbaruretype').value, 'password', 'div'); "><i class="fa fa-edit"></i> Ubah</button>
		</div>
	</div>
</form>