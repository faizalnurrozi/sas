<?php 
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();

$maxsize = 100000;

switch(@$_REQUEST['proc']){
	case 'profil' :		
		//Tangkap variabel		
		$usernames 	= @$_POST['username'];		
		$nama		= @$_POST['nama'];	
		$jk			= @$_POST['jk'];
		$email		= @$_POST['email'];
		
		list($cekemail) = $db->result_row("SELECT COUNT(email) AS jumlah FROM _admin WHERE email = '$email' AND usernames <> '".@$_SESSION[_APP_.'s_userAdmin']."' AND email <> '' ");
		if($cekemail == 0){
			$qData = $db->update("_admin", array('nama' => $nama, 'jenis_kelamin' => $jk, 'email' => $email), array('usernames' => $usernames));
			if($qData==true){
				$_SESSION[_APP_.'s_message_info'] = "Profil berhasil di rubah.";
				$_SESSION[_APP_.'s_namaAdmin'] = $nama;
				echo "
				<script>
					document.getElementById('nama_preload-index-1').innerHTML='$nama';
					document.getElementById('nama_preload-index-2').innerHTML='$nama';
					document.getElementById('nama_preload-parent').innerHTML='$nama';
				</script>
				";
			}else $_SESSION[_APP_.'s_message_error'] = "Profil gagal di ganti.";
			$db->close($qData);
			
			#-- Logs Progress
			$func->activity_logs_update("_admin", array('nama' => $nama, 'jenis_kelamin' => $jk, 'email' => $email), array('usernames' => $usernames), 'OR', @$_SESSION[_APP_.'s_userAdmin']);
		}else $_SESSION[_APP_.'s_message_error'] = "Email sudah terpakai, mohon pakai email yang lain.";
		
		echo "
			<script>
				sendRequest('content.php', 'module=admin&component=profil&action=form_profil&ajax=true', 'settings', 'div');
			</script>
		";
		break;
	
	case 'password' :	
		$username		= @$_SESSION[_APP_.'s_userAdmin'];
		$passlama		= $func->encrypt_md5(@$_REQUEST['passlama']);
		$passbaru		= $func->encrypt_md5(@$_REQUEST['passbaru']);
		$passbaruretype	= $func->encrypt_md5(@$_REQUEST['passbaruretype']);
		
		$queryCek = $db->sql("SELECT COUNT(*) FROM _admin WHERE (passwords = '$passlama' OR passwords = '".@$_REQUEST['passlama']."') AND usernames = '$username' ");
		list($cek)	= $db->fetch_row($queryCek);
		$db->close($queryCek);
		
		if($cek == 0){
			$_SESSION[_APP_.'s_message_error'] = "Ganti password gagal, password tidak sama dengan sebelumnya.";
		}else{
			if($passbaru != $passbaruretype){
				$_SESSION[_APP_.'s_message_error'] = "Password baru tidak cocok";
			}else{
				$hqData = $db->update("_admin", array('passwords' => $passbaru), array('usernames' => $username));
				if($hqData==true){
					$_SESSION[_APP_.'s_message_info'] = "Password telah berhasil diubah.";
				}else $_SESSION[_APP_.'s_message_error'] = "Password gagal diubah.";
				$db->close($hqData);
				
				#-- Logs Progress
				$func->activity_logs_update("_admin", array('passwords' => $passbaru), array('usernames' => $usernames), 'OR', @$_SESSION[_APP_.'s_userAdmin']);
			}
		}
		
		echo "
			<script>
				sendRequest('content.php', 'module=admin&component=profil&action=form_password&ajax=true', 'password', 'div');
			</script>
		";
		break;
		
	case 'upload_foto':
		$usernames 	= @$_SESSION[_APP_.'s_userAdmin'];
		if($func->check_token(@$_SESSION[_APP_.'s_token'], 'form_foto') === true){
			if(@$_FILES['txtfoto']['size'] < $maxsize){
				if($func->is_image(@$_FILES['txtfoto']) == true){
					$asal		= @$_FILES['txtfoto']['tmp_name'];
					$tujuan		= "modules/admin/photos/".strtolower(uniqid().str_replace(" ", "-", trim(@$_FILES['txtfoto']['name'])));
					if(move_uploaded_file($asal, $tujuan)==true){
						#-- Foto lama kalau ada hapus
						list($iconx) = $db->result_row("SELECT icon FROM _admin WHERE usernames = '$usernames' ");
						if($iconx != '' && file_exists($iconx)){
							unlink($iconx);
						}
						#-- Update foto dalam database
						$hqData = $db->update("_admin", array('icon' => $tujuan), array('usernames' => $usernames));
						if($hqData==true){
							$_SESSION[_APP_.'s_fotoAdmin'] = $tujuan;
							$_SESSION[_APP_.'s_message_info'] = "Foto telah berhasil diubah.";
						}else $_SESSION[_APP_.'s_message_error'] = "Foto gagal diubah.";
						$db->close($hqData);

						#-- Logs Progress
						$func->activity_logs_update("_admin", array('icon' => $tujuan), array('usernames' => $usernames), 'OR', @$_SESSION[_APP_.'s_userAdmin']);
				
						echo "
							<script>
								window.top.document.location.reload();
							</script>
						";
					}
				}else{
					$_SESSION[_APP_.'s_message_error'] = "Upload gagal, File sepertinya <b>bukan</b> Image / Foto.";
					echo "
						<script>
							window.location.href='content.php?module=admin&component=profil&action=foto&ajax=true';
						</script>
					";
				}
			}else{
				$_SESSION[_APP_.'s_message_error'] = "Upload gagal, Ukuran file maksimal <b>100Kb</b>.";
				echo "
					<script>
						window.location.href='content.php?module=admin&component=profil&action=foto&ajax=true';
					</script>
				";
			}
		}else{
			$_SESSION[_APP_.'s_message_error'] = "Upload gagal, <b>Illegal</b>.";
			echo "
				<script>
					window.location.href='content.php?module=admin&component=profil&action=foto&ajax=true';
				</script>
			";
		}
		break;
		
	case 'email' :
		$email = trim(@$_REQUEST['email']);
		list($cekemail) = $db->result_row("SELECT COUNT(email) AS jumlah FROM _admin WHERE email = '$email' AND usernames <> '".@$_SESSION[_APP_.'s_userAdmin']."' ");
		if($cekemail == 0){
			echo "<script>$('#emailvalidno').addClass('hide'); $('#emailvalid').removeClass('hide'); $('.has-email').addClass('has-valid'); document.getElementById('hdstatus').value='yes';</script>";
		}else{
			echo "<script>$('#emailvalidno').removeClass('hide'); $('#emailvalid').addClass('hide'); $('.has-email').addClass('has-error'); document.getElementById('hdstatus').value='no';</script>";
		}
		#-- echo "<script>alert('$cekemail');</script>";
		break;
}
?>