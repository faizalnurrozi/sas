<?php 	
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();
$msc = microtime(true);

if(isset($_SESSION[_APP_.'s_userAdmin'])){
	$_SESSION[_APP_.'s_adminPage'] = "module=admin&component=profil";
	
	$qData = "
	SELECT A.*, B.nama AS groups FROM _admin AS A LEFT JOIN _admin_group AS B ON (A.id_admin_group = B.id_admin_group) WHERE A.usernames = '".$_SESSION[_APP_.'s_userAdmin']."'
	";
	$hqData = $db->sql($qData);
	$result = $db->fetch_assoc($hqData);
	$db->close($hqData);
	$foto = $_SESSION[_APP_.'s_fotoAdmin'];
?>

<section class="content-header">
	<h1>Profil <?php echo $_SESSION[_APP_.'s_namaAdmin']; ?></h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-user"></i> Profil</a></li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-3" id="profil">
			<?php include "profil.php"; ?>
		</div>
		
		<div class="col-md-9">
			<div class="nav-tabs-custom">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#settings" data-toggle="tab"><i class="fa fa-user"></i> Profil User</a></li>
					<li><a href="#password" data-toggle="tab" id="changepass"><i class="fa fa-key"></i> Ganti Password</a></li>
					<li><a href="#foto" data-toggle="tab"><i class="fa fa-camera"></i> Ganti Foto</a></li>
					<li><a href="#logs" data-toggle="tab"><i class="fa fa-history"></i> Login History</a></li>
				</ul>
				<div class="tab-content">
					<div class="active tab-pane" id="settings">
						<?php include "form_profil.php"; ?>
					</div>
					<div class="tab-pane" id="password">
						<?php include "form_password.php"; ?>
					</div>
					<div class="tab-pane" id="foto">
						<iframe src="content.php?module=admin&component=profil&action=foto&ajax=true" width="100%" height="350" frameborder="0" scrolling="no"></iframe>
					</div>
					<div class="tab-pane" id="logs">
						<iframe src="content.php?module=admin&component=profil&action=logs" width="100%" height="500" frameborder="0" scrolling="auto"></iframe>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php
}else{
	include "modules/admin/components/auth/timeout.php";
}
?>