<?php 	
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();

if(isset($_SESSION[_APP_.'s_userAdmin'])){
	$_SESSION['s_adminPage'] = "module=admin&component=profil&action=change";
	
	$qEdit = "SELECT * FROM _admin WHERE usernames = '".$_SESSION[_APP_.'s_userAdmin']."'"; 
	$dataEdit = $db->sql($qEdit);
	$resultEdit = $db->fetch_assoc($dataEdit);
	?>
	<div class="header">           
		<h1 class="page-title">Ganti Password</h1>
	</div>
	<ul class="breadcrumb">
		<li><a onclick="javascript: sendRequest('content.php', 'module=admin&component=home', 'content', 'div');">Home</a> <span class="divider">/</span></li>
		<li class="active">Ganti Password</li>
	</ul>
	
	<div class="container-fluid">
		<div class="row-fluid">
			<form name="form_admin" id="form_admin" action="javascript:void(null);">		
			<table width="80%" cellpadding="0" cellspacing="3">			
				<tr>				
					<td width="15%">NIM&nbsp;</td>				
					<td width="3%">&nbsp;:&nbsp;</td>				
					<td><input type="text" name="txtusernames" id="txtusernames" value="<?php echo $resultEdit['usernames']; ?>"  readonly onclick="javascript: this.select();" size="30" /></td>			
				</tr>			
				<tr>				
					<td>Password&nbsp;Baru</td>				
					<td>&nbsp;:&nbsp;</td>				
					<td><input type="password" name="txtpassnew" id="txtpassnew" value="" size="30" /></td>			
				</tr>
				<tr>				
					<td>Password&nbsp;Baru&nbsp;<i>(Retype)</i></td>				
					<td>&nbsp;:&nbsp;</td>				
					<td><input type="password" name="txtpassnewre" id="txtpassnewre" value="" size="30" onkeyup="javascript: if(this.value!=document.getElementById('txtpassnew').value){ $('.unvalid').fadeIn(1000); $('.valid').fadeOut(1000); }else{ $('.unvalid').fadeOut(1000); $('.valid').fadeIn(1000); } if(this.value==''){ $('.unvalid').fadeOut(1000); $('.valid').fadeOut(1000); }" />&nbsp;<span class="valid"><img src="images/approve.png" /></span><span class="unvalid"><i>Password tidak sama</i></span></td>			
				</tr>
				<tr>				
					<td>Password&nbsp;Lama</td>				
					<td>&nbsp;:&nbsp;</td>				
					<td><input type="password" name="txtpassold" id="txtpassold" value="" size="30" /></td>			
				</tr>			
				<tr>				
					<td>Nama&nbsp;</td>				
					<td>&nbsp;:&nbsp;</td>				
					<td><input type="text" name="txtnama" id="txtnama" value="<?php echo $resultEdit['nama']; ?>"  onclick="javascript: this.select();" readonly size="30" /></td>			
				</tr>
				<tr>				
					<td align="right" colspan="2">&nbsp;</td>
					<td><button class="btn btn-primary" onclick="javascript: 
					if(confirm('Yakin password diganti?')) sendRequest('content.php', 'module=admin&component=profil&action=process&proc=change&passnew='+this.form.txtpassnew.value+'&passnewre='+this.form.txtpassnewre.value+'&passold='+this.form.txtpassold.value+'&nama='+this.form.txtnama.value, 'content', 'div');"> <i class="icon-save"></i> Ganti Password </button></td>
				</tr>		
			</table>		
			</form>
		</div>
	</div>
<?php 
}else{
	echo "<script language='javascript'>window.location.href='index.php';</script>";
}
?>