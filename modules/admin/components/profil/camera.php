<!DOCTYPE html>
<html>
	<head>
		<script language="JavaScript" src="includes/webcam/jquery.min.js"></script>
		<script language="JavaScript" src="includes/webcam/swfobject.js"></script>
		<script language="JavaScript" src="includes/webcam/scriptcam.js"></script>
		<script language="JavaScript"> 
			$(document).ready(function() {
				$("#webcam").scriptcam({
					showMicrophoneErrors:false,
					onError:onError,
					cornerRadius:20,
					cornerColor:'e3e5e2',
					onWebcamReady:onWebcamReady,
					uploadImage:'includes/webcam/upload.gif',
					onPictureAsBase64:base64_tofield_and_image
				});
			});
			function base64_tofield() {
				$('#formfield').val($.scriptcam.getFrameAsBase64());
			};
			function base64_toimage() {
				$('#image').attr("src","data:image/png;base64,"+$.scriptcam.getFrameAsBase64());
			};
			function base64_tofield_and_image(b64) {
				$('#formfield').val(b64);
				$('#image').attr("src","data:image/png;base64,"+b64);
			};
			function changeCamera() {
				$.scriptcam.changeCamera($('#cameraNames').val());
			}
			function onError(errorId,errorMsg) {
				$( "#btn1" ).attr( "disabled", true );
				$( "#btn2" ).attr( "disabled", true );
				alert(errorMsg);
			}			
			function onWebcamReady(cameraNames,camera,microphoneNames,microphone,volume) {
				$.each(cameraNames, function(index, text) {
					$('#cameraNames').append( $('<option></option>').val(index).html(text) )
				}); 
				$('#cameraNames').val(camera);
			}
		</script>
		<!-- Admin CSS -->
		<link rel="stylesheet" href="includes/bootstrap/bootstrap.css"></link>
		<link rel="stylesheet" href="includes/bootstrap/font-awesome.css"></link>
		<!-- End of Admin CSS -->
	</head>
	<body>
		<div style="width:330px;float:left;">
			<div id="webcam"></div>
			<div style="margin:5px;">
				<img src="includes/webcam/webcamlogo.png" style="vertical-align:text-top"/>
				<select id="cameraNames" size="1" onChange="changeCamera()" style="width:245px;font-size:10px;height:25px;">
				</select>
			</div>
		</div>
		<div style="width:135px;float:left;">
			<!--<p><button class="btn btn-small" id="btn1" onclick="base64_tofield()">Snapshot to form</button></p>-->
			<p><button class="btn btn-small" id="btn2" onclick="base64_toimage()">Ambil Gambar</button></p>
		</div>
		<div style="width:200px;float:left;">
			<!--<p><textarea id="formfield" style="width:190px;height:70px;"></textarea></p>-->
			<p><img id="image" style="width:200px;height:153px;"/></p>
		</div>
		<p style="clear:both;text-align:center;">
			<button class="btn btn-great" onclick="javascript: window.opener.document.getElementById('iframe_camera').contentWindow.document.getElementById('foto-profil').src=document.getElementById('image').src; 
window.close();">Gunakan Gambar</button>
		</p>
	</body>
</html>
