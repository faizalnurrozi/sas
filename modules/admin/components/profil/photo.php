<?php
session_start();
$path = @$_SESSION['path'];
?>
<html>
<head>
	<title><?php echo @$_SESSION['s_namaAdmin']; ?></title>
	<style type="text/css">
		body{ background-color:white; }
		a{ font-size:9px; text-shadow:6px 6px 10px #cacaca; cursor:pointer; color:blue; }
		input{ font-size:9px;}
	</style>
</head>
<body>
	<form id="form_photo" method="POST" action="content.php?module=admin&component=profil&action=process&proc=change_photo" enctype="multipart/form-data">
		<?php $w = @$_REQUEST['w']; $h = @$_REQUEST['h']; ?>
		<input type="hidden" name="proc" id="proc" value="change_photo" />
		<input type="hidden" name="w" id="w" value="<?php echo $w; ?>" />
		<input type="hidden" name="h" id="h" value="<?php echo $h; ?>" />
		<p align="center"><img src="<?php if(file_exists($path."/photos/".$_SESSION['s_fotoAdmin']) && $_SESSION['s_fotoAdmin']!="") echo $path."/photos/".$_SESSION['s_fotoAdmin']; else echo "images/fotokosong.gif"; ?>" height="85%" alt="Gambar Hilang" /></p>
		<p style="line-height:2px; margin-top:-10px; width:98%;" align="center">
			<input type="file" name="file_foto" id="file_foto" style="display:none;" onchange="javascript: document.getElementById('form_photo').submit(); " />
			<a onclick="javascript: document.getElementById('file_foto').style.display='block'; this.style.display='none';">Ganti Foto</a>
		</p>
	</form>
</body>
</html>