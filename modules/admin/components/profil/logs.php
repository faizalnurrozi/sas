<?php
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();
?>
<html>
	<head>
		<script type="text/javascript" src="includes/ajax.js"></script>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="includes/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="includes/bootstrap/css/font-awesome.css">
		<link rel="stylesheet" href="includes/dist/css/ionicons.min.css">
		<link rel="stylesheet" href="includes/dist/css/AdminLTE.css">
		<link rel="stylesheet" href="includes/dist/css/skins/_all-skins.min.css">

		<script src="includes/plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script src="includes/bootstrap/js/bootstrap.min.js"></script>
		<script src="includes/dist/js/app.min.js"></script>
		
	</head>
	<body>
		<div class="container-fluid">
		<form name="form_user_logs" method="POST" action="javascript: void(null);">
			<input type="hidden" id="proc" name="proc" value="add" />
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td colspan="3">
					<?php
					if(@$_REQUEST['bulan']=="") @$bulan = date("m"); else @$bulan = $_REQUEST['bulan'];
					if(@$_REQUEST['tahun']=="") @$tahun = date("Y"); else @$tahun = $_REQUEST['tahun'];
					?>
					Bulan : 
						<select name="slbulan" id="slbulan" class="input-sm col-sm" >
							<option value="01" <?php if($bulan=='01') echo " selected"; ?>>Januari</option>
							<option value="02" <?php if($bulan=='02') echo " selected"; ?>>Februari</option>
							<option value="03" <?php if($bulan=='03') echo " selected"; ?>>Maret</option>
							<option value="04" <?php if($bulan=='04') echo " selected"; ?>>April</option>
							<option value="05" <?php if($bulan=='05') echo " selected"; ?>>Mei</option>
							<option value="06" <?php if($bulan=='06') echo " selected"; ?>>Juni</option>
							<option value="07" <?php if($bulan=='07') echo " selected"; ?>>Juli</option>
							<option value="08" <?php if($bulan=='08') echo " selected"; ?>>Agustus</option>
							<option value="09" <?php if($bulan=='09') echo " selected"; ?>>September</option>
							<option value="10" <?php if($bulan=='10') echo " selected"; ?>>Oktober</option>
							<option value="11" <?php if($bulan=='11') echo " selected"; ?>>November</option>
							<option value="12" <?php if($bulan=='12') echo " selected"; ?>>Desember</option>
						</select>&nbsp;&nbsp;&nbsp;
						Tahun : <select name="sltahun" id="sltahun" class="input-sm col-sm">
						<?php
						$now=date("Y"); $awal=$now-4;
						for($i=$awal;$i<=$now;$i++){ echo "<option value='$i'"; if($tahun==$i) echo " selected"; echo ">$i</option>"; }
						?>
						</select>
						<button class="btn btn-sm btn-warning" onclick="javascript: window.location.href='content.php?module=admin&component=profil&action=logs&bulan='+document.getElementById('slbulan').value+'&tahun='+document.getElementById('sltahun').value; "><i class="fa fa-search"></i> View</button>
					</td>
				</tr>
				
				<tr><td colspan="3">&nbsp;</td></tr>
				<tr>
					<td colspan="3">
						<div class="box-body table-responsive">
						<table class="table table-bordered table-hover" style="font-size:12px;">
							<thead>
							<tr class="table-list-header">
								<th width="2%">No.</th>
								<th width="20%">Tanggal</th>
								<th width="10%">Jam</th>
								<th width="17%">IP Address</th>
								<th width="17%">Status</th>
								<th>Deskripsi</th>
							</tr>
							</thead>
							<tbody>
							<?php
							$hqData = $db->sql("SELECT DISTINCT DATE_FORMAT(tanggal, '%Y-%m-%d') FROM _admin_logs WHERE SUBSTRING(tanggal, 6, 2) = '$bulan' AND SUBSTRING(tanggal, 1, 4) = '$tahun' AND id_user = '".@$_SESSION[_APP_.'s_userAdmin']."' ORDER BY tanggal DESC");
							$jum = $db->num_rows($hqData);
							$ada = 0;
							if($jum == '0'){
								echo "<tr class='table-list-row'><td colspan='6' align='center'>Data belum ada</td></tr>";
							}else{
								$no=1;
								while(list($tanggal) = $db->fetch_row($hqData)){
									echo "<tr class='table-list-row'>";
									echo "<td align='center' valign='top'>$no.</td>";
									/* -- Tanggal -- */
									echo "<td align='left' valign='top'>".$func->search_day($tanggal).", ".$func->implode_date_time($tanggal)."</td>";
									
									/* -- Jam -- */
									echo "<td align='left' valign='top'>";
									$hqSQL = $db->sql("SELECT DATE_FORMAT(tanggal, '%k:%i') FROM _admin_logs WHERE DATE_FORMAT(tanggal, '%Y-%m-%d') = '$tanggal' AND id_user = '".$_SESSION[_APP_.'s_userAdmin']."' ORDER BY tanggal DESC");
									while(list($jam) = $db->fetch_row($hqSQL)){
										echo $jam."<br>";
									}
									$db->close($hqSQL);
									echo "</td>";
									
									/* -- IP Address -- */
									echo "<td align='left' valign='top'>";
									$hqSQL = $db->sql("SELECT ip_address, country, region, city, latitude, longitude, browser FROM _admin_logs WHERE DATE_FORMAT(tanggal, '%Y-%m-%d') = '$tanggal' AND id_user = '".$_SESSION[_APP_.'s_userAdmin']."' ORDER BY tanggal DESC");
									while(list($ipAddess, $country, $region, $city, $latitude, $longitude, $browser) = $db->fetch_row($hqSQL)){
										echo ($ipAddess == '::1') ? "localhost" : "<a style='cursor:pointer;' onmouseover='$(this).tooltip();' title='$browser'>".$ipAddess."</a>"; echo "<br>";
									}
									$db->close($hqSQL);
									echo "</td>";
																	
									/* -- Status -- */
									echo "<td align='left' valign='top'>";
									$hqSQL = $db->sql("SELECT status FROM _admin_logs WHERE DATE_FORMAT(tanggal, '%Y-%m-%d') = '$tanggal' AND id_user = '".$_SESSION[_APP_.'s_userAdmin']."' ORDER BY tanggal DESC");
									while(list($status) = $db->fetch_row($hqSQL)){
										echo $status."<br>";
									}
									$db->close($hqSQL);
									echo "</td>";
									
									/* -- Deskripsi -- */
									echo "<td align='left' valign='top'>";
									$hqSQL = $db->sql("SELECT deskripsi FROM _admin_logs WHERE DATE_FORMAT(tanggal, '%Y-%m-%d') = '$tanggal' AND id_user = '".$_SESSION[_APP_.'s_userAdmin']."' ORDER BY tanggal DESC");
									while(list($deskripsi) = $db->fetch_row($hqSQL)){
										echo $deskripsi."<br>";
									}
									$db->close($hqSQL);
									echo "</td>";
									echo "</tr>";
									$no++;
								}
							}
							$db->close($hqData);
							?>
							</tbody>
						</table>
					</td>
				</tr>
			</table>
		</form>
		</div>
	</body>
</html>