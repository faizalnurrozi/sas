<?php
if(@$_REQUEST['ajax']=='true'){
	include "globals/config.php";
	include "globals/functions.php";
	$db = new Database();
	$func = new Functions();
	
	$qData = "SELECT * FROM _admin WHERE usernames = '".@$_SESSION[_APP_.'s_userAdmin']."'";
	$hqData = $db->sql($qData);
	$result = $db->fetch_assoc($hqData);
	$db->close($hqData);
	
	#-- Generate CRSF Token
	$_SESSION[_APP_.'s_token'] = $func->generate_token('form_foto');
}
?>
<html>
	<head>
		<script type="text/javascript" src="includes/ajax.js"></script>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="includes/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="includes/bootstrap/css/font-awesome.css">
		<link rel="stylesheet" href="includes/dist/css/ionicons.min.css">
		<link rel="stylesheet" href="includes/dist/css/AdminLTE.css">
		<!--<link rel="stylesheet" href="includes/dist/css/skins/_all-skins.min.css">-->

		<script src="includes/plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script src="includes/bootstrap/js/bootstrap.min.js"></script>
		<script src="includes/dist/js/app.min.js"></script>
		
		<script language="JavaScript">		
		// File
		var openFile = function(event, _ouput) {
			var input = event.target;
			var reader = new FileReader();
			reader.onload = function(){
				var dataURL = reader.result;
				var output = document.getElementById(_ouput);
				output.src = dataURL;
			};
			reader.readAsDataURL(input.files[0]);
		};
		// End of File
		
		function getFileExtension(filename) {
			return filename.split('.').pop();
		}
		</script>
	</head>
	<body >
		<!-- Alert Process -->
		<?php if(isset($_SESSION[_APP_.'s_message_info'])){ ?>
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">x</button>
			<strong><?php echo $_SESSION[_APP_.'s_message_info']; unset($_SESSION[_APP_.'s_message_info']); ?></strong>
		</div>
		<?php } ?>
		<?php if(isset($_SESSION[_APP_.'s_message_error'])){ ?>
		<div class="alert alert-danger">
			<button type="button" class="close" data-dismiss="alert">x</button>
			<strong>Error : </strong> <?php echo $_SESSION[_APP_.'s_message_error']; unset($_SESSION[_APP_.'s_message_error']); ?>
		</div>
		<?php } ?>
		<!-- Alert Process -->

		<?php 
		/*if(file_exists($_SESSION[_APP_.'s_fotoAdmin'])) $foto = $_SESSION[_APP_.'s_fotoAdmin']; else $foto = "images/icon-user.png";*/
		$foto = @$_SESSION[_APP_.'s_fotoAdmin'];
		?>

		<form class="form-horizontal" action="content.php?module=admin&component=profil&action=process" enctype="multipart/form-data" name="form_foto" method="post">
			<input type="hidden" name="proc" id="proc" value="upload_foto" />
			<div class="form-group form-ganti-foto">
				<div class="row">
					<div class="col-xs-3 foto">
						<img src="<?php echo @$foto; ?>" id="foto-profil" />
					</div>
				</div>
				
				<div class="row">
					<div class="col-xs-8" style="margin:20px;">
						<input type="file" class="hide" name="txtfoto" id="txtfoto" onchange="javascript: openFile(event, 'foto-profil'); $('#group-save').removeClass('hide'); $('#btn-browse').addClass('hide');" style="width:200; height:300;" />
						<button id="btn-browse"  class="btn btn-flat btn-sm btn-warning" onclick="javascript: $('#txtfoto').click();" type="button"><i class="fa fa-upload"></i> Browse</button>
						
						<div class="btn-group btn-group-sm hide" role="group" id="group-save">
							<button type="submit" class="btn btn-flat btn-sm btn-primary"><i class="fa fa-save"></i> Simpan</button>
							<button type="button" class="btn btn-flat btn-sm btn-default" onclick="javascript: $('#group-save').addClass('hide'); $('#btn-browse').removeClass('hide'); document.getElementById('foto-profil').src='<?php echo $foto; ?>'; "><i class="fa fa-times"></i> Batal</button>
						</div>
					</div>
				</div>
			</div>
		</form>
		
	</body>
</html>