<?php
if(@$_REQUEST['ajax'] == 'true'){
	include "globals/config.php";
	include "globals/functions.php";
	$db = new Database();
	$func = new Functions();
}else{
	global $db;
	global $func;
}
$qData = "SELECT A.*, B.nama AS groups FROM _admin AS A LEFT JOIN _admin_group AS B ON (A.id_admin_group = B.id_admin_group) WHERE A.usernames = '".$_SESSION[_APP_.'s_userAdmin']."' ";
$hqData = $db->sql($qData);
$result = $db->fetch_assoc($hqData);
$db->close($hqData);
?>

<!-- Alert Process -->
<?php if(isset($_SESSION[_APP_.'s_message_info'])){ ?>
<div class="alert alert-success">
	<button type="button" class="close" data-dismiss="alert">x</button>
	<strong><?php echo $_SESSION[_APP_.'s_message_info']; unset($_SESSION[_APP_.'s_message_info']); ?></strong>
</div>
<?php } ?>
<?php if(isset($_SESSION[_APP_.'s_message_error'])){ ?>
<div class="alert alert-danger">
	<button type="button" class="close" data-dismiss="alert">x</button>
	<strong>Error : </strong> <?php echo $_SESSION[_APP_.'s_message_error']; unset($_SESSION[_APP_.'s_message_error']); ?>
</div>
<?php } ?>
<!-- Alert Process -->

<div class="alert alert-info">
	<button type="button" class="close" data-dismiss="alert">x</button>
	<strong>Catatan : </strong> Email dapat digunakan sebagai username untuk Login.
</div>

<form class="form-horizontal" action="javascript: void(null);">
	<div class="form-group has-feedback">
		<label for="txtusername" class="col-sm-2 control-label">Username</label>
		<div class="col-sm-5">
			<input type="text" class="form-control" name="txtusername" id="txtusername" placeholder="Username"  value="<?php echo @$result['usernames']; ?>" readonly />
		</div>
		<div class="col-sm-5 has-email">
			<input type="text" class="form-control" name="txtemail" id="txtemail" placeholder="Email : contoh@email.com" value="<?php echo @$result['email']; ?>" autocomplete="off" onclick="javascript: this.select();" onblur="javascript: if(this.value!=''){ $('#cekemail').load('content.php?module=admin&component=profil&action=process&proc=email&email='+this.value); } " />
			<span id="emailvalidno" class="glyphicon glyphicon-remove form-control-feedback hide" style="color:red;"></span>
			<span id="emailvalid" class="glyphicon glyphicon-check form-control-feedback hide" style="color:green;"></span>
			<span id="cekemail"></span>
			<input type="hidden" name="hdstatus" id="hdstatus" value="" />
		</div>
	</div>
	<div class="form-group">
		<label for="txtnama" class="col-sm-2 control-label">Nama</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" name="txtnama" id="txtnama" placeholder="Nama" value="<?php echo @$result['nama']; ?>" autocomplete="off" />
		</div>
	</div>
	<div class="form-group">
		<label for="txtnama" class="col-sm-2 control-label">Gender</label>
		<div class="col-sm-10">
			<label><input type="radio" name="rdjk" id="rdjkl" value="L" <?php if(@$result['jenis_kelamin']=='L') echo "checked"; ?> onclick="javascript: document.getElementById('hdjk').value='L'; " />&nbsp;Laki - laki</label>
			<label><input type="radio" name="rdjk" id="rdjkp" value="P" <?php if(@$result['jenis_kelamin']=='P') echo "checked"; ?> onclick="javascript: document.getElementById('hdjk').value='P'; "/>&nbsp;Perempuan</label>
			<input type="hidden" name="hdjk" id="hdjk" value="<?php echo @$result['jenis_kelamin']; ?>" />
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<button type="button" class="btn btn-primary btn-flat" onclick="javascript: 
			var err = '';
			var RegExp = /^((([a-z]|[0-9]|!|#|$|%|&|'|\*|\+|\-|\/|=|\?|\^|_|`|\{|\||\}|~)+(\.([a-z]|[0-9]|!|#|$|%|&|'|\*|\+|\-|\/|=|\?|\^|_|`|\{|\||\}|~)+)*)@((((([a-z]|[0-9])([a-z]|[0-9]|\-){0,61}([a-z]|[0-9])\.))*([a-z]|[0-9])([a-z]|[0-9]|\-){0,61}([a-z]|[0-9])\.)[\w]{2,4}|(((([0-9]){1,3}\.){3}([0-9]){1,3}))|(\[((([0-9]){1,3}\.){3}([0-9]){1,3})\])))$/ 
			if(!RegExp.test(document.getElementById('txtemail').value)){
				$('.has-email').addClass('has-error'); $('#txtemail').focus(); err += 'Format Email tidak valid.\n';
			}
			if(document.getElementById('hdstatus').value=='no'){
				err += 'Email sudah terpakai, ganti email lain.\n';
			}
			if(err==''){
				sendRequest('content.php', 'module=admin&component=profil&action=process&proc=profil&username='+document.getElementById('txtusername').value+'&nama='+document.getElementById('txtnama').value+'&jk='+document.getElementById('hdjk').value+'&email='+document.getElementById('txtemail').value, 'settings', 'div');
			}else{ 
				$('#Modal').click(); $('#error-text').html(err);
			} "><i class="fa fa-save"></i> Update</button>
		</div>
	</div>
</form>

<!-- Alert Validation Form -->
<input type="hidden" id="Modal" onclick="javascript: $('#s_alert').fadeIn(); $('#s_alert').delay(3000); $('#s_alert').fadeOut(500);" />
<div class="alert alert-danger" id="s_alert" style="top:100px;">
	<button type="button" class="close" onclick="javascript: $('#s_alert').fadeOut();">x</button>
	<strong>Warning : </strong> <div id="error-text" class="error-text"><ul></ul></div>
</div>
<!-- End Alert Validation Form -->