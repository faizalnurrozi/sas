<?php
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();

$id = @$_GET['id'];
list($level, $menus, $icon, $link, $parent) = $db->result_row("SELECT level, nama, icon, link, id_admin_menus_parent FROM _admin_menus WHERE id_admin_menus = '".$_SESSION[_APP_.'s_menuPage']."' ");
?>
<html>
	<head>
		<title><?php echo $menus; ?></title>
		
		<script type="text/javascript" src="includes/ajax.js"></script>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="includes/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="includes/bootstrap/css/font-awesome.css">
		<link rel="stylesheet" href="includes/dist/css/ionicons.min.css">
		<link rel="stylesheet" href="includes/dist/css/AdminLTE.css">
		<link rel="stylesheet" href="includes/dist/css/skins/_all-skins.min.css">
	</head>
	<body bgcolor="white">
		<?php
		list($idJenis, $namaJenis) = $db->result_row(" SELECT id_jenis_surat, nama FROM _jenis_surat WHERE id_jenis_surat = '$id' ");
		?>
		<table class="table">
			<tr>
				<td width="20%">Kode Jenis Surat&nbsp;</td>
				<td width="1%">&nbsp;:&nbsp;</td>
				<td><?php echo $idJenis; ?></td>
			</tr>
			<tr>
				<td>Nama Jenis Surat&nbsp;</td>
				<td>&nbsp;:&nbsp;</td>
				<td><?php echo $namaJenis; ?></td>
			</tr>
		</table>
	</body>
</html>