<?php 
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();

switch(@$_REQUEST['proc']){
	case 'add' :
		# ---------------------#
		# Penangkapan variable #
		# ---------------------#
		
		$id 		= @$_POST['txtid'];
		$nama 		= @$_POST['txtnama'];
		$status_umum= ($_POST['status_umum'] == 'TRUE') ? 'TRUE' : 'FALSE';
		
		$hqData = $db->insert("_jenis_surat", array('id_jenis_surat' => $id, 'nama' => $nama, 'status_umum' => $status_umum));

		if($hqData==true) $_SESSION[_APP_.'s_message_info'] = "Tambah data berhasil";
		else $_SESSION[_APP_.'s_message_error'] = "Tambah data gagal";
		
		#---------- * ----------#
		
		
		# ----------------#
		# Pencatatan Logs #
		# ----------------#
		
		/* ----- Activity Logs (Insert) ------ */
		$func->activity_logs_insert("_jenis_surat", $id, $_SESSION[_APP_.'s_userAdmin']);
		/* ----- End Activity Logs (Insert) ------ */
		
		#---------- * ----------#
		
		
		# ------------#
		# Kirim balik #
		# ------------#
		
		echo "<script>window.parent.sendRequest('content.php', 'module=admin&component=jenis_surat&action=list&ajax=true', 'list', 'div');</script>";
		echo "<meta http-equiv='refresh' content='0;url=content.php?module=admin&component=jenis_surat&action=add' />";
		
		#---------- * ----------#
		
		break;
		
	case 'update' :
		# ---------------------#
		# Penangkapan variable #
		# ---------------------#
		
		$idx 		= @$_POST['txtidx'];
		$id 		= @$_POST['txtid'];
		$nama 		= @$_POST['txtnama'];
		$status_umum= ($_POST['status_umum'] == 'TRUE') ? 'TRUE' : 'FALSE';
		
		# ----------------#
		# Pencatatan Logs #
		# ----------------#
		
		/* ----- Activity Logs (Update) ------ */
		$func->activity_logs_update("_jenis_surat", array('id_jenis_surat' => $id, 'nama' => $nama, 'status_umum' => $status_umum), array('id_jenis_surat' => $idx), 'OR', $_SESSION[_APP_.'s_userAdmin']);
		/* ----- End Activity Logs (Update) ------ */
		
		#---------- * ----------#
		
		
		
		# --------------#
		# Proses Update #
		# --------------#
		
		$hqData = $db->update("_jenis_surat", array('id_jenis_surat' => $id, 'nama' => $nama, 'status_umum' => $status_umum), array('id_jenis_surat' => $idx));
		
		if($hqData==true) $_SESSION[_APP_.'s_message_info'] = "Update data berhasil";
		else $_SESSION[_APP_.'s_message_error'] = "Update data gagal";
		
		#---------- * ----------#
		
		
		# ------------#
		# Kirim balik #
		# ------------#
		
		echo "<script>window.parent.sendRequest('content.php', 'module=admin&component=jenis_surat&action=list&ajax=true', 'list', 'div');</script>";
		echo "<meta http-equiv='refresh' content='0;url=content.php?module=admin&component=jenis_surat&action=add' />";
		
		#---------- * ----------#
		break;
		
	case 'delete' :
		# ---------------------#
		# Penangkapan variable #
		# ---------------------#
		
		$idx = @$_REQUEST['id'];
		
		#---------- * ----------#
		
		
		
		# --------------#
		# Proses Delete #
		# --------------#
		
		$hqData = $db->delete("_jenis_surat", array('id_jenis_surat' => $idx));
		
		if($hqData==true) $_SESSION[_APP_.'s_message_info'] = "Hapus data berhasil";
		else $_SESSION[_APP_.'s_message_error'] = "Hapus data gagal";
		
		#---------- * ----------#
		
		
		# ----------------#
		# Pencatatan Logs #
		# ----------------#
		
		/* ----- Activity Logs (Delete) ------ */
		$func->activity_logs_delete("_jenis_surat", $idx, $_SESSION[_APP_.'s_userAdmin']);
		/* ----- End Activity Logs (Delete) ------ */
		
		#---------- * ----------#
		
		
		# ------------#
		# Kirim balik #
		# ------------#
		
		header("location: content.php?module=admin&component=jenis_surat&action=list&ajax=true");
		
		#---------- * ----------#
		break;
}
?>