<?php
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();

$id = @$_GET['id'];
$icon = @$_GET['icon'];
?>
<html>
	<head>
		<script type="text/javascript" src="includes/ajax.js"></script>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="includes/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="includes/bootstrap/css/font-awesome.css">
		<link rel="stylesheet" href="includes/dist/css/ionicons.min.css">
		<link rel="stylesheet" href="includes/dist/css/AdminLTE.css">
		<!--<link rel="stylesheet" href="includes/dist/css/skins/_all-skins.min.css">-->
	</head>
	<body onmouseover="javascript: window.top.autoResize('iframe_view')">
		<div class="container-fluid">
		<?php
		$hasil = $db->result_assoc(" 
		SELECT A.*, B.nama AS kabupaten 
		FROM _cabang AS A LEFT JOIN _kabupaten AS B ON (A.id_kabupaten=B.id_kabupaten) 
		WHERE A.id_cabang = '$id' ");
		?>
		<table class="table">
			<tr>
				<td width="20%">Kode&nbsp;</td>
				<td width="1%">&nbsp;:&nbsp;</td>
				<td><?php echo $hasil['id_cabang']; ?></td>
			</tr>
			<tr>
				<td>Nama&nbsp;Cabang&nbsp;</td>
				<td>&nbsp;:&nbsp;</td>
				<td><?php echo $hasil['nama']; ?></td>
			</tr>
			<tr>
				<td>Kab./Kota</td>
				<td>&nbsp;:&nbsp;</td>
				<td><?php echo $hasil['kabupaten']; ?></td>
			</tr>
			<tr>
				<td>Kepala&nbsp;Cabang</td>
				<td>&nbsp;:&nbsp;</td>
				<td><?php echo $hasil['kepala_cabang']; ?></td>
			</tr>
			<tr>
				<td>Alamat</td>
				<td>&nbsp;:&nbsp;</td>
				<td><?php echo $hasil['alamat']; ?></td>
			</tr>
			<tr>
				<td>Website</td>
				<td>&nbsp;:&nbsp;</td>
				<td><?php echo $hasil['website']; ?></td>
			</tr>
			<tr>
				<td>Email</td>
				<td>&nbsp;:&nbsp;</td>
				<td><?php echo $hasil['email']; ?></td>
			</tr>
			<tr>
				<td>Telp.</td>
				<td>&nbsp;:&nbsp;</td>
				<td><?php echo $hasil['telepon']; ?></td>
			</tr>
			<tr>
				<td>Fax.</td>
				<td>&nbsp;:&nbsp;</td>
				<td><?php echo $hasil['fax']; ?></td>
			</tr>
			<?php
			if($icon != ''){
				echo "
				<tr>
					<td>Icon&nbsp;</td>
					<td>&nbsp;:&nbsp;</td>
					<td><img src='$icon' width='200' /></td>
				</tr>
				";
			}
			?>
		</table>
		</div>
	</body>
</html>