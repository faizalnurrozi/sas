<?php
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();
$id = @$_GET['id'];
$icon = @$_GET['icon'];
?>
<html>
	<head>
		<script type="text/javascript" src="includes/ajax.js"></script>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="includes/bootstrap/css/bootstrap.min.css"></link>
		<link rel="stylesheet" href="includes/bootstrap/css/font-awesome.css"></link>
		<link rel="stylesheet" href="includes/dist/css/ionicons.min.css"></link>
		<link rel="stylesheet" href="includes/dist/css/AdminLTE.css"></link>
		<!--<link rel="stylesheet" href="includes/dist/css/skins/_all-skins.min.css"></link>-->
	</head>
	<body onmouseover="javascript: window.top.autoResize('iframe_view');">
		<?php
		$qData = "
		SELECT 	A.id_sekolah, A.nama AS sekolah, A.jenjang, A.tanggal_pendirian, A.status, A.kepala_sekolah, 
				B.nama AS propinsi, C.nama AS kabupaten, D.nama AS kecamatan, E.nama AS kelurahan, A.alamat, 
				F.nama AS cabang, A.email, A.website, A.kodepos
		FROM _sekolah AS A
			LEFT JOIN _propinsi AS B ON (A.id_propinsi = B.id_propinsi)
			LEFT JOIN _kabupaten AS C ON (A.id_kabupaten = C.id_kabupaten)
			LEFT JOIN _kecamatan AS D ON (A.id_kecamatan = D.id_kecamatan)
			LEFT JOIN _kelurahan AS E ON (A.id_kelurahan = E.id_kelurahan)
			LEFT JOIN _cabang AS F ON (A.id_cabang = F.id_cabang)
		WHERE A.id_sekolah = '$id' ";
		$hqData = $db->sql($qData);
		$hasil = $db->fetch_assoc($hqData);
		$db->close($hqData);
		?>
		<div class="container-fluid">
		<table class="table">
			<tr>
				<td width="20%">NPSN&nbsp;</td>
				<td width="1%">&nbsp;:&nbsp;</td>
				<td><?php echo $hasil['id_sekolah']; ?></td>
			</tr>					
			<tr>
				<td>Nama&nbsp;Sekolah</td>
				<td>&nbsp;:&nbsp;</td>
				<td><?php echo $hasil['sekolah']; ?></td>
			</tr>
			<tr>
				<td>Jenjang</td>
				<td>&nbsp;:&nbsp;</td>
				<td><?php echo $hasil['jenjang']; ?></td>
			</tr>
			<tr>
				<td>Tgl.&nbsp;Pendirian</td>
				<td>&nbsp;:&nbsp;</td>
				<td><?php echo $func->implode_date($hasil['tanggal_pendirian']); ?></td>
			</tr>
			<tr>
				<td>Status</td>
				<td>&nbsp;:&nbsp;</td>
				<td><?php echo $hasil['status']; ?></td>
			</tr>
			<tr>
				<td>Kepala&nbsp;Sekolah</td>
				<td>&nbsp;:&nbsp;</td>
				<td><?php echo $hasil['kepala_sekolah']; ?></td>
			</tr>					
			<tr>
				<td>Alamat</td>
				<td>&nbsp;:&nbsp;</td>
				<td>
				<?php
				echo $hasil['alamat']." ".$hasil['kelurahan']." ".$hasil['kecamatan']." ".$hasil['kabupaten']." ".$hasil['propinsi']." ".$hasil['kodepos'];
				?>
				</td>
			</tr>					
			<tr>
				<td>Email&nbsp;</td>
				<td>&nbsp;:&nbsp;</td>
				<td><?php echo $hasil['email']; ?></td>
			</tr>									
			<tr>
				<td>Website&nbsp;</td>
				<td>&nbsp;:&nbsp;</td>
				<td><?php echo $hasil['website']; ?></td>
			</tr>
			<?php
			if($icon != ''){
				echo "
				<tr>
					<td>Icon&nbsp;</td>
					<td>&nbsp;:&nbsp;</td>
					<td><img src='$icon' width='200' /></td>
				</tr>
				";
			}
			?>
		</table>
		
		<?php
		$text = "";			
		$cekedit = 0;
		switch(@$hasil['jenjang']){
			case 'SMK' : 
				list($cekedit) = $db->result_row("SELECT COUNT(id) FROM _sekolah_rombel WHERE id_sekolah = '$id' AND id_smk_kompetensi NOT IN (SELECT id_smk_kompetensi FROM _smk_kompetensi)"); 
				$text = "Kompetensi Keahlian (SMK)";
				break;
			case 'SMA' : 
				list($cekedit) = $db->result_row("SELECT COUNT(id) FROM _sekolah_rombel WHERE id_sekolah = '$id' AND id_sma_peminatan NOT IN (SELECT id_sma_peminatan FROM _sma_peminatan)"); 
				$text = "Peminatan (SMA)";
				break;
			case 'TKLB':
			case 'SDLB':
			case 'SMPLB' :
			case 'SMALB':
			case 'SLB' : 
				list($cekedit) = $db->result_row("SELECT COUNT(id) FROM _sekolah_rombel WHERE id_sekolah = '$id' AND id_pkplk_ketunaan NOT IN (SELECT id_pkplk_ketunaan FROM _pkplk_ketunaan)"); 
				$text = "Ketunaan (PK-PLK)";
				break;
		}
		?>
		
		<form name="form_sekolah" method="POST" action="javascript: void(null);">	
			<ul class="nav nav-tabs" id="myTab">
				<li class="active"><a href="#rombel"><i class="fa fa-briefcase"></i> &nbsp; <?php echo $text; ?> & Mata Pelajaran</a></li>
			</ul>
			
			<div class="tab-content">
				<!-- Panel Rombongan Belajar -->
				<div class="tab-pane active" id="rombel" style="min-height:100px; overflow:auto;">
					<h4><?php echo $text; ?> / Rombongan Belajar (Kelas).</h4>
					<?php
					switch($hasil['jenjang']){
						case 'SMK' : $text = "Kompetensi Keahlian (SMK)"; break; 
						case 'SMA' : $text = "Peminatan (SMA)"; break; 
						case 'TKLB':
						case 'SDLB':
						case 'SMPLB' :
						case 'SMALB':
						case 'SLB' : $text = "Ketunaan (PK-PLK)"; break; 
					}
					?>
					<table class="table table-bordered table-hover" style="font-size:12px;">
						<thead>
							<tr>
								<th width="1%">No.</th>
								<th class="sort">Tingkat&nbsp;</th>
								<th class="sort"><?php echo $text; ?>&nbsp;</th>
								<th class="sort">Jml.&nbsp;Romb.&nbsp;Belajar</th>
								<th class="sort">Jml.&nbsp;Peserta&nbsp;Didik</th>
							</tr>
						</thead>					
						<?php
						$qData = $db->sql("SELECT * FROM _sekolah_rombel WHERE id_sekolah = '$id' ");
						$jumlah = $db->num_rows($qData);
						if($jumlah == 0){
							echo "<tbody><tr class='table-list-row'><td colspan='5' align='center'>Data belum ada.</td></tr></tbody>";
						}else{
							echo "<tbody>";
							$x=0; $no=1;
							$total_rombel = 0;
							$total_siswa = 0;
							while($result = $db->fetch_assoc($qData)){
								list($tingkat) = $db->result_row("SELECT nama FROM _tingkat WHERE id_tingkat = '$result[id_tingkat]' ");
								/**
								 * $class_warning digunakan untuk menandai data yang masih lama
								 */
								$class_warning = 'success';
								$wkt = true;
								switch($hasil['jenjang']){
									case 'SMK' :
										/**
										 * Melakukan pengecekan data pada tabel baru `_smk_kompetensi_keahlian`
										 * Jika tidak ditemukan maka ambil pada tabel yang lama `_kompetensi`
										 */
										list($jurusan, $kurikulum) = $db->result_row("SELECT CONCAT(kurikulum, ' - ', nama), kurikulum FROM _smk_kompetensi WHERE id_smk_kompetensi = '$result[id_smk_kompetensi]' ");
										$idj = $result['id_smk_kompetensi'];
										/** Cek data **/
										if($jurusan == ''){
											/**
											 *	Mengambil data dari tabel lama `_komptensi`
											 */
											list($jurusan) = $db->result_row("SELECT nama FROM _kompetensi WHERE id_kompetensi = '$result[id_kompetensi]' ");
											$class_warning = "danger";
											$wkt = false;
										}
										break;
									case 'SMA' :
										/**
										 * Melakukan pengecekan data pada tabel baru `_sma_peminatan`
										 * Jika tidak ditemukan maka ambil pada tabel yang lama `_peminatan`
										 */
										list($jurusan, $kurikulum) = $db->result_row("SELECT CONCAT(kurikulum, ' - ', nama), kurikulum FROM _sma_peminatan WHERE id_sma_peminatan = '$result[id_sma_peminatan]' ");
										$idj = $result['id_sma_peminatan'];
										/** Cek data **/
										if($jurusan == ''){
											/**
											 *	Mengambil data dari tabel lama `_peminatan`
											 */
											list($jurusan) = $db->result_row("SELECT nama FROM _peminatan WHERE id_peminatan = '$result[id_peminatan]' ");
											$class_warning = "danger";
											$wkt = false;
										}
										break;
									case 'TKLB':
									case 'SDLB':
									case 'SMPLB' :
									case 'SMALB':
									case 'SLB' :
										/**
										 * Melakukan pengecekan data pada tabel baru `_pkplk_ketunaan`
										 * Jika tidak ditemukan maka ambil pada tabel yang lama `_pkplk_jenjang`
										 */
										list($jurusan, $kurikulum) = $db->result_row("SELECT CONCAT(kurikulum, ' - ', nama), kurikulum FROM _pkplk_ketunaan WHERE id_pkplk_ketunaan = '$result[id_pkplk_ketunaan]' ");
										$idj = $result['id_pkplk_ketunaan'];
										/** Cek data **/
										if($jurusan == ''){
											/**
											 *	Mengambil data dari tabel lama `_pkplk_jenjang`
											 */
											list($jurusan) = $db->result_row("SELECT nama FROM _pkplk_jenjang WHERE id_pkplk_jenjang = '$result[id_pkplk_jenjang]' ");
											$class_warning = "danger";
											$wkt = false;
										}
										break;
								}

								echo "<tr class='table-list-row $class_warning'>";
								echo "<td align='center'>$no.</td>";
								echo "<td align='left'>$tingkat</td>";
								echo "<td align='left'>$jurusan</td>";
								echo "<td align='right'>$result[jumlah_rombel]</td>";
								echo "<td align='right'>$result[jumlah_siswa]</td>";
																
								echo "</tr>";
								$total_rombel += $result['jumlah_rombel'];
								$total_siswa += $result['jumlah_siswa'];
								$x++; $no++;
							}
							echo "</tbody>";
							echo "<thead>";
							echo "<tr>";
							echo "<th colspan='3' style='text-align:center;'>Total</th>";
							echo "<th style='text-align:right;'>$total_rombel</th>";
							echo "<th style='text-align:right;'>$total_siswa</th>";
							
							echo "</tr>";
							echo "</thead>";
						}
						$db->close($qData);
						?>
					</table>
				</div>
			</div>
		</form>
		</div>
	</body>
</html>