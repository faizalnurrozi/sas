<?php
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();

$id = @$_GET['id'];
list($ids) = $db->result_row("SELECT id_sekolah FROM _gtk WHERE id_gtk = '$id' ");
$icon = @$_GET['icon'];
?>
<html>
	<head>
		<script type="text/javascript" src="includes/ajax.js"></script>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="includes/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="includes/bootstrap/css/font-awesome.css">
		<link rel="stylesheet" href="includes/dist/css/ionicons.min.css">
		<link rel="stylesheet" href="includes/dist/css/AdminLTE.css">
		<!--<link rel="stylesheet" href="includes/dist/css/skins/_all-skins.min.css">-->
		
		<script src="includes/plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script src="includes/bootstrap/js/bootstrap.min.js"></script>
		<script src="includes/dist/js/app.min.js"></script>
	</head>
	<body onmouseover="javascript: window.top.autoResize('iframe_view'); ">
		<?php
		$result = $db->result_assoc(" 
		SELECT 	A.*, B.nama AS agama, C.nama AS status_pegawai, D.nama AS pangkat, E.nama AS jenis_ptk, 
				F.nama AS tugas_tambahan, G.nama AS sekolah, fn_progress_gtk(A.id_gtk) AS progress_gtk
		FROM _gtk AS A
			LEFT JOIN _agama AS B ON (A.id_agama = B.id_agama)
			LEFT JOIN _status_pegawai AS C ON (A.id_status_pegawai = C.id_status_pegawai)
			LEFT JOIN _pangkat AS D ON (A.id_pangkat = D.id_pangkat)
			LEFT JOIN _jenis_ptk AS E ON (A.id_jenis_ptk = E.id_jenis_ptk)
			LEFT JOIN _tugas_tambahan AS F ON (A.id_tugas_tambahan = F.id_tugas_tambahan)
			INNER JOIN _sekolah AS G ON (A.id_sekolah = G.id_sekolah)
		WHERE A.id_gtk = '$id' ");
		?>
		<div class="container-fluid">
			<legend><?php echo $result['id_sekolah']." - ".$result['sekolah']; ?></legend>
			<table class="table" style="font-size:12px;">
				<tr>
					<td width="20%">NIK&nbsp;</td>
					<td width="1%">&nbsp;:&nbsp;</td>
					<td><?php echo $result['nik']; ?></td>
					<td width="10%">&nbsp;</td>
					<td width="20%">NIP&nbsp;</td>
					<td width="1%">&nbsp;:&nbsp;</td>
					<td><?php echo $result['nip']; ?></td>
				</tr>
				<tr>
					<td width="20%">Nama&nbsp;GTK&nbsp;</td>
					<td width="1%">&nbsp;:&nbsp;</td>
					<td><?php echo (($result['gelar_depan']=='') ? "" : $result['gelar_depan'].". ").$result['nama'].(($result['gelar_belakang']=='') ? "" : ", ".$result['gelar_belakang']); ?></td>
					<td width="10%">&nbsp;</td>
					<td width="20%">NUPTK&nbsp;</td>
					<td width="1%">&nbsp;:&nbsp;</td>
					<td><?php echo $result['nuptk']; ?></td>
				</tr>
				<tr>
					<td>Jenis&nbsp;Kelamin&nbsp;</td>
					<td>&nbsp;:&nbsp;</td>
					<td><?php echo $result['jenis_kelamin']; ?></td>
					<td width="10%">&nbsp;</td>
					<td width="20%">Tempat,&nbsp;Tgl.&nbsp;Lahir&nbsp;</td>
					<td width="1%">&nbsp;:&nbsp;</td>
					<td><?php echo $result['tempat_lahir'].", ".(($result['tanggal_lahir']=='0000-00-00') ? "-" : $func->implode_date($result['tanggal_lahir'])); ?></td>
				</tr>
				<tr>
					<td width="20%">Agama&nbsp;</td>
					<td width="1%">&nbsp;:&nbsp;</td>
					<td><?php echo $result['agama']; ?></td>
					<td width="10%">&nbsp;</td>
					<td width="20%">Status&nbsp;Pegawai&nbsp;</td>
					<td width="1%">&nbsp;:&nbsp;</td>
					<td><?php echo $result['status_pegawai']; ?></td>
				</tr>
				<tr>
					<td width="20%">Pangkat&nbsp;</td>
					<td width="1%">&nbsp;:&nbsp;</td>
					<td><?php echo $result['pangkat']; ?></td>
					<td width="10%">&nbsp;</td>
					<td width="20%">Sekolah&nbsp;Induk</td>
					<td width="1%">&nbsp;:&nbsp;</td>
					<td><?php echo ($result['status_induk']=='1') ? "Ya" : "Tidak"; ?></td>
				</tr>
				<tr>
					<td width="20%">Jenis&nbsp;GTK</td>
					<td width="1%">&nbsp;:&nbsp;</td>
					<td>
					<?php 
					switch($result['jenis_gtk']){
						case 'G' : echo "Guru"; break;
						case 'TK' : echo "Tenaga Kependidikan"; break;
						case 'KS' : echo "Kepala Sekolah"; break;
						case 'BK' : echo "BK / Konselor"; break;
						case 'BKIT' : echo "BKIT / BKTIK"; break;
					}					
					?>
					</td>
					<td width="10%">&nbsp;</td>
					<td width="20%">Jenis&nbsp;PTK</td>
					<td width="1%">&nbsp;:&nbsp;</td>
					<td><?php echo $result['jenis_ptk']; ?></td>
				</tr>
				<?php 
				list($kelompok) = $db->result_row("SELECT kelompok FROM _status_pegawai WHERE id_status_pegawai = '$result[id_status_pegawai]' "); 
				if($kelompok == 'PNS'){
				?>
				<tr>
					<td width="20%">No.&nbsp;SK&nbsp;CPNS</td>
					<td width="1%">&nbsp;:&nbsp;</td>
					<td><?php echo $result['sk_cpns']; ?></td>
					<td width="10%">&nbsp;</td>
					<td width="20%">Tgl.&nbsp;SK&nbsp;CPNS</td>
					<td width="1%">&nbsp;:&nbsp;</td>
					<td><?php echo ($result['tanggal_cpns']=='0000-00-00') ? "" : $func->implode_date($result['tanggal_cpns']); ?></td>
				</tr>
				<tr>
					<td width="20%">No.&nbsp;SK&nbsp;PNS</td>
					<td width="1%">&nbsp;:&nbsp;</td>
					<td><?php echo $result['sk_pengangkatan']; ?></td>
					<td width="10%">&nbsp;</td>
					<td width="20%">Tgl.&nbsp;SK&nbsp;PNS</td>
					<td width="1%">&nbsp;:&nbsp;</td>
					<td><?php echo ($result['tmt_pengangkatan']=='0000-00-00') ? "" : $func->implode_date($result['tmt_pengangkatan']); ?></td>
				</tr>
				<?php }else{ ?>
				<tr>
					<td width="20%">No.&nbsp;SK&nbsp;Pegawai</td>
					<td width="1%">&nbsp;:&nbsp;</td>
					<td><?php echo $result['sk_pengangkatan']; ?></td>
					<td width="10%">&nbsp;</td>
					<td width="20%">Tgl.&nbsp;SK&nbsp;Pegawai</td>
					<td width="1%">&nbsp;:&nbsp;</td>
					<td><?php echo ($result['tmt_pengangkatan']=='0000-00-00') ? "" : $func->implode_date($result['tmt_pengangkatan']); ?></td>
				</tr>
				<?php } ?>
				<tr>
					<td width="20%">TMT&nbsp;Unit&nbsp;Kerja</td>
					<td width="1%">&nbsp;:&nbsp;</td>
					<td><?php echo ($result['tmt_unit_kerja']=='0000-00-00') ? "" : $func->implode_date($result['tmt_unit_kerja']); ?></td>
					<td width="10%">&nbsp;</td>
					<td width="20%">Tugas&nbsp;Tambahan</td>
					<td width="1%">&nbsp;:&nbsp;</td>
					<td><?php echo $result['tugas_tambahan']; ?></td>
				</tr>
				<tr>
					<td width="20%">NPWP&nbsp;</td>
					<td width="1%">&nbsp;:&nbsp;</td>
					<td><?php echo $result['npwp']; ?></td>
					<td width="10%">&nbsp;</td>
					<?php if($result['jenis_gtk']=='KS'){ ?>
					<td width="20%">TMT&nbsp;Kepsek&nbsp;</td>
					<td width="1%">&nbsp;:&nbsp;</td>
					<td><?php echo ($result['tmt_kepsek_awal']=='0000-00-00') ? "" : $func->implode_date($result['tmt_kepsek_awal']); ?> - <?php echo ($result['tmt_kepsek_akhir']=='0000-00-00') ? "" : $func->implode_date($result['tmt_kepsek_akhir']); ?></td>
					<?php }else{ ?>
					<td width="20%">&nbsp;</td>
					<td width="1%">&nbsp;</td>
					<td>&nbsp;</td>
					<?php } ?>
				</tr>
				<tr>
					<td width="20%">Status&nbsp;</td>
					<td width="1%">&nbsp;:&nbsp;</td>
					<td><?php echo ($result['status_cuti']=='TRUE') ? "CUTI" : "AKTIF"; ?></td>
					<td width="10%">&nbsp;</td>
					<?php if($result['status_cuti']=='TRUE'){ ?>
					<td width="20%">Keterangan&nbsp;Cuti</td>
					<td width="1%">&nbsp;:&nbsp;</td>
					<td><?php echo $result['keterangan_cuti']; ?></td>
					<?php }else{ ?>
					<td width="20%">&nbsp;</td>
					<td width="1%">&nbsp;</td>
					<td>&nbsp;</td>
					<?php } ?>
				</tr>
				<?php
				if($icon != ''){
					echo "
					<tr>
						<td>Icon&nbsp;</td>
						<td>&nbsp;:&nbsp;</td>
						<td colspan='8'><img src='$icon' width='200' /></td>
					</tr>
					";
				}
				?>
			</table>
			
			<?php
			list($jenjang) = $db->result_row("SELECT jenjang FROM _sekolah WHERE id_sekolah = '$ids' ");
			switch($jenjang){
				case 'SMK' : $text = "Kompetensi Keahlian (SMK)"; $mapgur = "Mata Pelajaran"; break; 
				case 'SMA' : $text = "Peminatan (SMA)"; $mapgur = "Mata Pelajaran"; break; 
				case 'TKLB':
				case 'SDLB':
				case 'SMPLB' :
				case 'SMALB':
				case 'SLB' : $text = "Ketunaan (PKPLK)"; $mapgur = "Jenis Guru"; break; 
			}
			?>
			
			<ul class="nav nav-tabs" id="myTab" style="font-size:12px;">
				<?php if($result['jenis_gtk']=='G' || $result['jenis_gtk']=='BK' || $result['jenis_gtk']=='BKIT' || ($result['jenis_gtk']=='KS' && in_array($jenjang, array('TKLB','SDLB','SMPLB','SMALB','SLB')))){ ?>
				<li class="active" onclick="javascript: window.top.autoResize('iframe_view');"><a href="#rombel"><i class="fa fa-university"></i> Rombel&nbsp;&&nbsp;Mapel&nbsp;Diampu</a></li>
				<?php } ?>
				<li <?php if($result['jenis_gtk']=='TK' || ($result['jenis_gtk']=='KS' && in_array($jenjang, array('SMA','SMK')))){ echo "class='active'"; } ?> onclick="javascript: window.top.autoResize('iframe_view');"><a href="#pendidikan"><i class="fa fa-graduation-cap"></i> Rwyt. Pendidikan</a></li>
				<li onclick="javascript: window.top.autoResize('iframe_view');"><a href="#sertifikasi"><i class="fa fa-certificate"></i> Rwyt. Sertifikasi</a></li>
				<li onclick="javascript: window.top.autoResize('iframe_view');"><a href="#diklat"><i class="fa fa-train"></i> Rwyt. Diklat</a></li>
				<li onclick="javascript: window.top.autoResize('iframe_view');"><a href="#prestasi"><i class="fa fa-trophy"></i> Rwyt. Prestasi</a></li>
				<li onclick="javascript: window.top.autoResize('iframe_view');"><a href="#karya_tulis"><i class="fa fa-pencil"></i> Rwyt. Karya Tulis</a></li>
			</ul>
									
			<div class="tab-content">
				<?php if($result['jenis_gtk']=='G' || $result['jenis_gtk']=='BK' || $result['jenis_gtk']=='BKIT' || ($result['jenis_gtk']=='KS' && in_array($jenjang, array('TKLB','SDLB','SMPLB','SMALB','SLB')))){ ?>
				<!-- Panel Rombel -->
				<div class="tab-pane active" id="rombel">
					<p>&nbsp;</p>
					<table class="table table-bordered table-hover" style="font-size:12px;">
						<thead>
							<tr>
								<th width="1%">No.</th>
								<th class="sort">Tingkat&nbsp;</th>
								<th class="sort"><?php echo @$text; ?>&nbsp;</th>
								<th class="sort"><?php echo $mapgur; ?></th>
								<th class="sort">Jml.&nbsp;Rombel.</th>
								<th class="sort">Jml.&nbsp;Siswa</th>
								<?php if($jenjang == 'SMA' || $jenjang == 'SMK') echo '<th class="sort">Jml.&nbsp;Jam</th>'; ?>
							</tr>
						</thead>					
						<?php
						$qData = $db->sql("SELECT * FROM _gtk_rombel WHERE id_gtk = '$id' ");
						$jumlah = $db->num_rows($qData);
						if($jumlah == 0){
							if($jenjang == 'SMA' || $jenjang == 'SMK') echo "<tbody><tr class='table-list-row'><td colspan='7' align='center'>Data belum ada</td></tr></tbody>";
							else echo "<tbody><tr class='table-list-row'><td colspan='6' align='center'>Data belum ada</td></tr></tbody>";
						}else{
							echo "<tbody>";
							$x=0; $no=1;
							$total_rombel = 0;
							$total_siswa = 0;
							$total_jam = 0;
							$class_warning = "";
							$kurikulum = "";
							while($resultx = $db->fetch_assoc($qData)){
								list($tingkat) = $db->result_row("SELECT nama FROM _tingkat WHERE id_tingkat = '$resultx[id_tingkat]' ");
								switch($jenjang){
									case 'SMK' : 
										$jurusanx = ($resultx['id_smk_kompetensi']=='') ? $resultx['id_kompetensi'] : $resultx['id_smk_kompetensi'];
										list($mapelx, $jumlah_jam) = $db->result_row("SELECT IF(id_smk_mapel='', id_mapel, id_smk_mapel) AS mapelx, jumlah_jam FROM _gtk_mapel WHERE id_gtk = '$id' AND idx = '$resultx[idx]' ");
										list($jurusan, $kurikulum) = $db->result_row("SELECT nama, kurikulum FROM _smk_kompetensi WHERE id_smk_kompetensi = '$jurusanx' ");
										list($mapel) = $db->result_row("SELECT nama FROM _smk_mapel WHERE id_smk_mapel = '$mapelx' ");
										#-- Muatan Lokal
										if($mapel == ''){
											list($mapel) = $db->result_row("SELECT nama FROM _smk_mulok WHERE id_smk_mapel = '$mapelx' ");
										}
										$class_warning = 'success';
										if($jurusan == ''){
											/**
											 * Mengambil data jika data baru tidak ditemukan dari table 
											 */
											list($jurusan) = $db->result_row("SELECT nama FROM _kompetensi WHERE id_kompetensi = '$jurusanx' ");
											list($mapel) = $db->result_row("SELECT nama FROM _mapel WHERE id_mapel = '$mapelx' ");
											$class_warning = 'danger';
										}
										break;
									
									case 'SMA' : 
										$jurusanx = ($resultx['id_sma_peminatan']=='') ? $resultx['id_peminatan'] : $resultx['id_sma_peminatan']; 
										list($mapelx, $jumlah_jam, $nama_kelas) = $db->result_row("SELECT IF(id_sma_mapel='', id_mapel, id_sma_mapel) AS mapelx, jumlah_jam, B.nama FROM _gtk_mapel AS A LEFT JOIN _sma_kelas AS B ON(A.id_sma_kelas = B.id_sma_kelas) WHERE id_gtk = '$id' AND idx = '$resultx[idx]' ");
										list($jurusan, $kurikulum) = $db->result_row("SELECT nama, kurikulum FROM _sma_peminatan WHERE id_sma_peminatan = '$jurusanx' ");
										list($mapel, $lintas_minat) = $db->result_row("SELECT nama, lintas_minat FROM _sma_mapel WHERE id_sma_mapel = '$mapelx' ");
										if($lintas_minat == 'TRUE' && $nama_kelas != ''){
											$mapel = "($nama_kelas) $mapel";
										}
										#-- Muatan Lokal & Mapel Wajib Tambahan
										if($mapel == ''){
											list($mapel) = $db->result_row("SELECT nama FROM _sma_mulok WHERE id_sma_mapel = '$mapelx' ");
											if($mapel == ''){
												list($mapel) = $db->result_row("SELECT CONCAT(nama, ' [Tambahan]') FROM _sma_wajib_tambahan WHERE id_sma_mapel = '$mapelx' ");
											}
										}
										$class_warning = 'success';
										if($jurusan == ''){
											/**
											 * Mengambil data jika data baru tidak ditemukan dari table 
											 */
											list($jurusan) = $db->result_row("SELECT nama FROM _peminatan WHERE id_peminatan = '$jurusanx' ");
											list($mapel) = $db->result_row("SELECT nama FROM _mapel WHERE id_mapel = '$mapelx' ");
											$class_warning = 'danger';
										}
										break;
									case 'SLB' :
									case 'SMALB':
									case 'SMPLB' :
									case 'SDLB':
									case 'TKLB': 
										$jurusanx = ($resultx['id_pkplk_ketunaan']=='') ? $resultx['id_pkplk_jenjang'] : $resultx['id_pkplk_ketunaan']; 
										list($gurux) = $db->result_row("SELECT id_pkplk_guru FROM _gtk_pkplk_guru WHERE id_gtk = '$id' AND id_sekolah = '$ids' AND id_tingkat = '$resultx[id_tingkat]' AND id_pkplk_ketunaan = '$jurusanx' ");
										list($jurusan, $kurikulum) = $db->result_row("SELECT nama, kurikulum FROM _pkplk_ketunaan WHERE id_pkplk_ketunaan = '$jurusanx' ");
										list($mapel) = $db->result_row("SELECT nama FROM _pkplk_guru WHERE id_pkplk_guru = '$gurux' ");
										$class_warning = 'success';
										if($jurusan == ''){
											/**
											 * Mengambil data jika data baru tidak ditemukan dari table 
											 */
											list($jurusan) = $db->result_row("SELECT nama FROM _pkplk_jenjang WHERE id_pkplk_jenjang = '$jurusanx' ");
											list($mapel) = $db->result_row("SELECT nama FROM _mapel WHERE id_mapel = '$mapelx' ");
											$class_warning = 'danger';
										}
										break;
								}
								echo "<tr class='table-list-row $class_warning'>";
								echo "<td align='center'>$no.</td>";
								echo "<td align='left'>$tingkat</td>";
								echo "<td align='left'>".(($kurikulum=='') ? "" : "[".$kurikulum."] - ").$jurusan."</td>";
								echo "<td align='left'>".(($mapel=='') ? "-" : $mapel)."</td>";
								echo "<td align='center'>$resultx[jumlah_rombel]</td>";
								echo "<td align='center'>$resultx[jumlah_siswa]</td>";								
								if($jenjang == 'SMA' || $jenjang == 'SMK') echo "<td align='center'>".(($jumlah_jam=='') ? "-" : $jumlah_jam)."</td>";
								
								echo "</tr>";
								$total_rombel += $resultx['jumlah_rombel'];
								$total_siswa += $resultx['jumlah_siswa'];
								$total_jam += @$jumlah_jam;
								$x++; $no++;
							}
							echo "</tbody>";
							echo "<thead>";
							echo "<tr>";
							echo "<th colspan='4' style='text-align:center;'>Total</th>";
							echo "<th style='text-align:center;'>$total_rombel</th>";
							echo "<th style='text-align:center;'>$total_siswa</th>";
							if($jenjang == 'SMA' || $jenjang == 'SMK')  echo "<th style='text-align:center;'>".(($total_jam=='') ? "-" : $total_jam)."</th>";
							
							echo "</tr>";
							echo "</thead>";
						}
						$db->close($qData);
						?>
					</table>
				</div>
				<?php } ?>
				
				<!-- Panel Pendidikan -->
				<div class="tab-pane <?php if($result['jenis_gtk']=='TK' || ($result['jenis_gtk']=='KS' && in_array($jenjang, array('SMA','SMK')))){ echo "active"; } ?>" id="pendidikan">
					<p>&nbsp;</p>
					<table class="table table-bordered table-hover" style="font-size:12px;">
						<thead>
							<tr>
								<th width="1%">No.</th>
								<th class="sort" width="20%">Jenjang&nbsp;Pendidikan</th>
								<th class="sort">Lembaga&nbsp;</th>
								<th class="sort">Jurusan</th>
								<th class="sort">Th.&nbsp;Lulus</th>
							</tr>
						</thead>					
						<?php
						$qData = $db->sql("SELECT * FROM _gtk_pendidikan WHERE id_gtk = '$id' ");
						$jumlah = $db->num_rows($qData);
						if($jumlah == 0){
							echo "<tbody><tr class='table-list-row'><td colspan='5' align='center'>Data belum ada</td></tr></tbody>";
						}else{
							echo "<tbody>";
							$x=0; $no=1;
							while($resultx = $db->fetch_assoc($qData)){
								echo "<tr class='table-list-row'>";
								echo "<td align='center'>$no.</td>";
								list($jenjangx) = $db->result_row("SELECT nama FROM _pendidikan_jenjang WHERE id_pendidikan_jenjang = '$resultx[id_pendidikan_jenjang]' ");
								echo "<td align='left'>$jenjangx</td>";
								echo "<td align='left'>$resultx[lembaga]</td>";
								echo "<td align='left'>$resultx[jurusan]</td>";
								echo "<td align='left'>$resultx[tahun_lulus]</td>";
								
								echo "</tr>";
								$x++; $no++;
							}
							echo "</tbody>";
						}
						$db->close($qData);
						?>
					</table>
				</div>
				
				<!-- Panel Sertifikasi -->
				<div class="tab-pane" id="sertifikasi">
					<p>&nbsp;</p>
					<table class="table table-bordered table-hover" style="font-size:12px;">
						<thead>
							<tr>
								<th width="1%">No.</th>
								<th class="sort">Bidang&nbsp;Studi</th>
								<th class="sort">Tgl.&nbsp;Sertifikat</th>
								<th class="sort">No.&nbsp;Sertifikat</th>
							</tr>
						</thead>					
						<?php
						$qData = $db->sql("SELECT * FROM _gtk_sertifikasi WHERE id_gtk = '$id' ");
						$jumlah = $db->num_rows($qData);
						if($jumlah == 0){
							echo "<tbody><tr class='table-list-row'><td colspan='4' align='center'>Data belum ada</td></tr></tbody>";
						}else{
							echo "<tbody>";
							$x=0; $no=1;
							while($resultx = $db->fetch_assoc($qData)){
								echo "<tr class='table-list-row'>";
								echo "<td align='center'>$no.</td>";
								list($idx, $bidang) = $db->result_row("SELECT id_sertifikasi_bidang, nama FROM _sertifikasi_bidang WHERE id_sertifikasi_bidang = '$resultx[id_sertifikasi_bidang]' ");
								echo "<td align='left'>$idx - $bidang</td>";
								echo "<td align='left'>".$func->implode_date($resultx['tanggal_sertifikat'])."</td>";
								echo "<td align='left'>".($resultx['nomor_sertifikat'])."</td>";
								
								echo "</tr>";
								$x++; $no++;
							}
							echo "</tbody>";
						}
						$db->close($qData);
						?>
					</table>
				</div>
				
				<!-- Panel Diklat -->
				<div class="tab-pane" id="diklat">
					<p>&nbsp;</p>
					<table class="table table-bordered table-hover" style="font-size:12px;">
						<thead>
							<tr>
								<th width="1%">No.</th>
								<th class="sort">Nama&nbsp;Diklat</th>
								<th class="sort">Status</th>
								<th class="sort">Penyelenggara</th>
								<th class="sort">Jml.&nbsp;Jam</th>
								<th class="sort">Tgl.&nbsp;Pelaksanaan</th>
							</tr>
						</thead>					
						<?php
						$qData = $db->sql("SELECT * FROM _gtk_diklat WHERE id_gtk = '$id' ");
						$jumlah = $db->num_rows($qData);
						if($jumlah == 0){
							echo "<tbody><tr class='table-list-row'><td colspan='6' align='center'>Data belum ada</td></tr></tbody>";
						}else{
							echo "<tbody>";
							$x=0; $no=1;
							while($resultx = $db->fetch_assoc($qData)){
								echo "<tr class='table-list-row'>";
								echo "<td align='center'>$no.</td>";
								echo "<td align='left'>$resultx[diklat].</td>";
								echo "<td align='left'>$resultx[status]</td>";
								echo "<td align='left'>$resultx[penyelenggara]</td>";
								echo "<td align='left'>$resultx[jumlah_jam]</td>";
								echo "<td align='center'>".$func->implode_date($resultx['tanggal_awal'])." - ".$func->implode_date($resultx['tanggal_akhir'])."</td>";
								
								echo "</tr>";
								
								$x++; $no++;
							}
							echo "</tbody>";
						}
						$db->close($qData);
						?>
					</table>
				</div>
				
				<!-- Panel Prestasi -->
				<div class="tab-pane" id="prestasi">
					<p>&nbsp;</p>
					<table class="table table-bordered table-hover" style="font-size:12px;">
						<thead>
							<tr>
								<th width="1%">No.</th>
								<th class="sort">Nama&nbsp;Kejuaraan</th>
								<th class="sort">Tingkat&nbsp;Kejuaraan</th>
								<th class="sort">Tanggal</th>
								<th class="sort">Peringkat</th>
							</tr>
						</thead>					
						<?php
						$qData = $db->sql("SELECT * FROM _gtk_prestasi WHERE id_gtk = '$id' ");
						$jumlah = $db->num_rows($qData);
						if($jumlah == 0){
							echo "<tbody><tr class='table-list-row'><td colspan='5' align='center'>Data belum ada</td></tr></tbody>";
						}else{
							echo "<tbody>";
							$x=0; $no=1;
							while($resultx = $db->fetch_assoc($qData)){
								echo "<tr class='table-list-row'>";
								echo "<td align='center'>$no.</td>";
								echo "<td align='left'>$resultx[nama_kejuaraan]</td>";
								switch($resultx['tingkat_kejuaraan']){
									case 'KOTAKAB' : $tingkat = "Kabupaten/Kota"; break;
									case 'PROVINSI' : $tingkat = "Provinsi"; break;
									case 'NASIONAL' : $tingkat = "Nasional"; break;
									case 'INTERNASIONAL' : $tingkat = "Internasional"; break;
								}
								echo "<td align='left'>".$tingkat."</td>";
								echo "<td align='center'>".$func->implode_date($resultx['tanggal'])."</td>";
								echo "<td align='left'>$resultx[peringkat]</td>";
								
								echo "</tr>";
								
								$x++; $no++;
							}
							echo "</tbody>";
						}
						$db->close($qData);
						?>
					</table>
				</div>
				
				<!-- Panel Karya Tulis -->
				<div class="tab-pane" id="karya_tulis">
					<p>&nbsp;</p>
					<table class="table table-bordered table-hover" style="font-size:12px;">
						<thead>
							<tr>
								<th width="1%">No.</th>
								<th class="sort">Jenis</th>
								<th class="sort">Judul</th>
								<th class="sort">Penerbit</th>
								<th class="sort">Tahun</th>
							</tr>
						</thead>					
						<?php
						$qData = $db->sql("SELECT * FROM _gtk_karya_tulis WHERE id_gtk = '$id' ");
						$jumlah = $db->num_rows($qData);
						if($jumlah == 0){
							echo "<tbody><tr class='table-list-row'><td colspan='5' align='center'>Data belum ada</td></tr></tbody>";
						}else{
							echo "<tbody>";
							$x=0; $no=1;
							while($resultx = $db->fetch_assoc($qData)){
								echo "<tr class='table-list-row'>";
								echo "<td align='center'>$no.</td>";
								echo "<td align='left'>".str_replace('_', ' ', $resultx['jenis'])."</td>";
								echo "<td align='left'>$resultx[judul]</td>";
								echo "<td align='left'>$resultx[penerbit]</td>";
								echo "<td align='left'>$resultx[tahun]</td>";
								
								echo "</tr>";
								
								$x++; $no++;
							}
							echo "</tbody>";
						}
						$db->close($qData);
						?>
					</table>
				</div>
			</div>
			
			<script>
			$('#myTab a').click(function (e) {
				e.preventDefault();
				$(this).tab('show');
			});
			</script>
		</div>
	</body>
</html>