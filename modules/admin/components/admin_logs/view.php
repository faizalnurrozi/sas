<?php
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();
?>
<html>
	<head>
		<script type="text/javascript" src="includes/ajax.js"></script>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="includes/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="includes/bootstrap/css/font-awesome.css">
		<link rel="stylesheet" href="includes/dist/css/ionicons.min.css">
		<link rel="stylesheet" href="includes/dist/css/AdminLTE.css">
		<!--<link rel="stylesheet" href="includes/dist/css/skins/_all-skins.min.css">-->

		<script src="includes/plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script src="includes/bootstrap/js/bootstrap.min.js"></script>
		<script src="includes/dist/js/app.min.js"></script>
	</head>
	<body>
		<div class="container-fluid">
		<form name="form_user_logs" method="POST" action="javascript: void(null);">
			<input type="hidden" id="proc" name="proc" value="add" />
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td colspan="3">
					<?php
					if(@$_REQUEST['hari']=="") $hari = date("d"); else $hari = $_REQUEST['hari'];
					if(@$_REQUEST['bulan']=="") $bulan = date("m"); else $bulan = $_REQUEST['bulan'];
					if(@$_REQUEST['tahun']=="") $tahun = date("Y"); else $tahun = $_REQUEST['tahun'];
					?>
					Tanggal : 
					<select name="sltanggal" id="sltanggal" class="input-sm col-sm" >
					<?php
					for($i=1; $i<=31; $i++){
						if($i < 10) $x = "0".$i;
						else $x = $i;
						echo "<option value='$x' "; if($hari==$x) echo "selected"; echo ">$x</option>";
					}
					?>
					</select>&nbsp;&nbsp;&nbsp;
					Bulan : 
					<select name="slbulan" id="slbulan" class="input-sm col-sm" >
						<option value="01" <?php if($bulan=='01') echo " selected"; ?>>Januari</option>
						<option value="02" <?php if($bulan=='02') echo " selected"; ?>>Februari</option>
						<option value="03" <?php if($bulan=='03') echo " selected"; ?>>Maret</option>
						<option value="04" <?php if($bulan=='04') echo " selected"; ?>>April</option>
						<option value="05" <?php if($bulan=='05') echo " selected"; ?>>Mei</option>
						<option value="06" <?php if($bulan=='06') echo " selected"; ?>>Juni</option>
						<option value="07" <?php if($bulan=='07') echo " selected"; ?>>Juli</option>
						<option value="08" <?php if($bulan=='08') echo " selected"; ?>>Agustus</option>
						<option value="09" <?php if($bulan=='09') echo " selected"; ?>>September</option>
						<option value="10" <?php if($bulan=='10') echo " selected"; ?>>Oktober</option>
						<option value="11" <?php if($bulan=='11') echo " selected"; ?>>November</option>
						<option value="12" <?php if($bulan=='12') echo " selected"; ?>>Desember</option>
					</select>&nbsp;&nbsp;&nbsp;
					Tahun : <select name="sltahun" id="sltahun" class="input-sm col-sm">
					<?php
					$now=date("Y"); $awal=$now-4;
					for($i=$awal;$i<=$now;$i++){ echo "<option value='$i'"; if($tahun==$i) echo " selected"; echo ">$i</option>"; }
					?>
					</select>
					<button class="btn btn-sm btn-warning" onclick="javascript: window.location.href='content.php?module=admin&component=admin_logs&action=view&hari='+document.getElementById('sltanggal').value+'&bulan='+document.getElementById('slbulan').value+'&tahun='+document.getElementById('sltahun').value; "><i class="fa fa-search"></i> View</button>
					</td>
				</tr>
				
				<tr><td colspan="3">&nbsp;</td></tr>
				<tr>
					<td colspan="3">
						<div class="box-body table-responsive">
						Jumlah : <span id="spanjumlah">0</span> User.
						<table class="table table-bordered table-hover table-striped" style="font-size:12px;">
							<thead>
							<tr class="table-list-header">
								<th width="3%">No.</th>
								<th width="15%">Tanggal</th>
								<th>User</th>
								<th width="10%">Jam</th>
								<th width="10%">IP Address</th>
								<th width="17%">Status</th>
								<th>Deskripsi</th>
							</tr>
							</thead>
							<tbody>
							<?php
							$hqData = $db->sql("SELECT DISTINCT DATE_FORMAT(tanggal, '%Y-%m-%d') FROM _admin_logs WHERE SUBSTRING(tanggal, 9, 2) = '$hari' AND SUBSTRING(tanggal, 6, 2) = '$bulan' AND SUBSTRING(tanggal, 1, 4) = '$tahun' ORDER BY tanggal DESC");
							$jum = $db->num_rows($hqData);
							$ada = 0;
							if($jum == '0'){
								echo "<tr class='table-list-row'><td colspan='7' align='center'>Data belum ada</td></tr>";
							}else{
								$no=1;
								while(list($tanggal) = $db->fetch_row($hqData)){
									echo "<tr class='table-list-row' style='font-size:12px;'>";
									echo "<td align='center' valign='top'>$no.</td>";
									echo "<td align='left' valign='top'>".$func->search_day($tanggal).", ".$func->implode_date_time($tanggal)."</td>";
									/* -- User Login -- */
									echo "<td align='left' valign='top'>";
									$hqSQL = $db->sql("SELECT A.id_user, B.nama, A.browser, A.status, B.tipe, B.id_gtk, B.id_sekolah, B.id_cabang, B.icon FROM _admin_logs AS A LEFT JOIN _admin AS B ON (A.id_user = B.usernames OR A.id_user = B.email) WHERE DATE_FORMAT(A.tanggal, '%Y-%m-%d') = '$tanggal' AND A.id_user <> '' ORDER BY A.tanggal DESC");
									while(list($idUser, $namaUser, $browser, $status, $tipe, $gtk, $sekolah, $cabang, $icon) = $db->fetch_row($hqSQL)){
										switch($status){ case 'SUCCESS' : $warna = "green"; break;	case 'FAILED' : $warna = "red"; break; }
										$namasekolah = ""; $link = "";
										switch($tipe){
											case 'GTK' :
												list($namasekolah) = $db->result_row("SELECT B.nama FROM _gtk AS A INNER JOIN _sekolah AS B ON (A.id_sekolah = B.id_sekolah) WHERE A.id_gtk = '$gtk' ");
												$link = "onclick=\"javascript: window.top.document.getElementById('btnModalView').click(); window.top.document.getElementById('iframe_view').src='content.php?module=admin&component=admin_logs&action=gtk&id=$gtk&icon=$icon'; \"";
												break;
											case 'SEKOLAH' :
												$link = "onclick=\"javascript: window.top.document.getElementById('btnModalView').click(); window.top.document.getElementById('iframe_view').src='content.php?module=admin&component=admin_logs&action=sekolah&id=$sekolah&icon=$icon'; \"";
												break;
											case 'CABANG' :
												$link = "onclick=\"javascript: window.top.document.getElementById('btnModalView').click(); window.top.document.getElementById('iframe_view').src='content.php?module=admin&component=admin_logs&action=cabang&id=$cabang&icon=$icon'; \"";
												break;
										}
										echo "<a style='cursor:pointer; color:$warna;' onmouseover='$(this).tooltip();' title=\"".(($namaUser=='') ? "" : stripslashes($namaUser)." - ".(($namasekolah=='') ? "" : "[".$namasekolah."] - ")).stripslashes($browser)."\" $link >".$idUser."</a><br>";
									}
									$db->close($hqSQL);
									echo "</td>";
									
									/* -- Jam -- */
									echo "<td align='left' valign='top'>";
									$hqSQL = $db->sql("SELECT DATE_FORMAT(tanggal, '%k:%i'), status FROM _admin_logs WHERE DATE_FORMAT(tanggal, '%Y-%m-%d') = '$tanggal' AND id_user <> '' ORDER BY tanggal DESC");
									while(list($jam, $status) = $db->fetch_row($hqSQL)){
										switch($status){
											case 'SUCCESS' : echo "<font color='green'>".$jam."</font><br>"; break;
											case 'FAILED' : echo "<font color='red'>".$jam."</font><br>"; break;
										}
									}
									$db->close($hqSQL);
									echo "</td>";
									
									/* -- IP Address -- */
									echo "<td align='left' valign='top'>";
									$hqSQL = $db->sql("SELECT ip_address, country, region, city, latitude, longitude FROM _admin_logs WHERE DATE_FORMAT(tanggal, '%Y-%m-%d') = '$tanggal' AND id_user <> '' ORDER BY tanggal DESC");
									while(list($ipAddess, $country, $region, $city, $latitude, $longitude) = $db->fetch_row($hqSQL)){
										echo ($ipAddess == '::1') ? "localhost" : "<a style='cursor:pointer;' onmouseover='$(this).tooltip();' title='$country - $region - $city'>".$ipAddess."</a>"; echo "<br>";
									}
									$db->close($hqSQL);
									echo "</td>";
																		
									/* -- Status -- */
									echo "<td align='left' valign='top'>";
									$hqSQL = $db->sql("SELECT status FROM _admin_logs WHERE DATE_FORMAT(tanggal, '%Y-%m-%d') = '$tanggal' AND id_user <> '' ORDER BY tanggal DESC");
									while(list($status) = $db->fetch_row($hqSQL)){
										switch($status){
											case 'SUCCESS' : echo "<font color='green'>".$status."</font><br>"; break;
											case 'FAILED' : echo "<font color='red'>".$status."</font><br>"; break;
										}
									}
									$db->close($hqSQL);
									echo "</td>";
									
									/* -- Deskripsi -- */
									echo "<td align='left' valign='top'>";
									$hqSQL = $db->sql("SELECT deskripsi, status FROM _admin_logs WHERE DATE_FORMAT(tanggal, '%Y-%m-%d') = '$tanggal' AND id_user <> '' ORDER BY tanggal DESC");
									while(list($deskripsi, $status) = $db->fetch_row($hqSQL)){
										switch($status){
											case 'SUCCESS' : echo "<font color='green'>".$deskripsi."</font><br>"; break;
											case 'FAILED' : echo "<font color='red'>".$deskripsi."</font><br>"; break;
										}
									}
									$db->close($hqSQL);
									echo "</td>";
									echo "</tr>";
									$no++;
									
									$hqSQL = $db->sql("SELECT DISTINCT(id_user) FROM _admin_logs WHERE DATE_FORMAT(tanggal, '%Y-%m-%d') = '$tanggal' AND id_user <> '' ORDER BY tanggal DESC");
									$jumlahUser = $db->num_rows($hqSQL);
									$db->close($hqSQL);
								}
							}
							$db->close($hqData);
							?>
							</tbody>
						</table>
						<script>document.getElementById('spanjumlah').innerHTML='<?php echo $jumlahUser; ?>';</script>
						</div>
					</td>
				</tr>
			</table>
		</form>
		</div>
	</body>
</html>