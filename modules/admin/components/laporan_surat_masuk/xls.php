<?php
error_reporting(E_ALL);
ini_set('max_execution_time', 300);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
require_once 'includes/phpexcel/PHPExcel.php';

$filex = "SuratMasuk.xls";

$objPHPExcel = new PHPExcel();

$objPHPExcel->getProperties()->setCreator("SD Muhammadiyah 26")
							 ->setLastModifiedBy("SD Muhammadiyah 26")
							 ->setTitle("Surat Masuk")
							 ->setSubject("Surat Masuk")
							 ->setDescription("Surat Masuk")
							 ->setKeywords("Surat Masuk")
							 ->setCategory("Laporan");

$bold = array(
	'font'  => array(
		'bold' => true,
		'color' => array('rgb' => '000000'),
		'size'  => 11,
		'name'  => 'Arial'
	),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	),
);

$normal = array(
	'font'  => array(
		'color' => array('rgb' => '000000'),
		'size'  => 11,
		'name'  => 'Arial'
	)
);

$rowHeader = array(
	'font'  => array(
		'bold' => true,
		'color' => array('rgb' => '000000'),
		'size'  => 11,
		'name'  => 'Arial'
	),
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('RGB' => '000000'),
		),
	),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	),
);

$rowBody = array(
	'font'  => array(
		'color' => array('rgb' => '000000'),
		'size'  => 11,
		'name'  => 'Arial'
	),
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('RGB' => '000000'),
		),
	),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
	),
);

$rowRight = array(
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
	),
);

$rowCenter = array(
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
	),
);

$rowRight = array(
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
	),
);

function cellColor($cells,$color){
    global $objPHPExcel;

    $objPHPExcel->getActiveSheet()->getStyle($cells)->getFill()->applyFromArray(array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array(
             'rgb' => $color
        )
    ));
}

for($i = 'A'; $i < 'Z'; $i++){
	$abjad[] = $i;
}

/**
 * Title
 */

$objPHPExcel->getActiveSheet()->getStyle('A1:F1')->applyFromArray($bold);
$objPHPExcel->getActiveSheet()->mergeCells("A1:F1");
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'SURAT KELUAR');

$objPHPExcel->getActiveSheet()->getStyle('A3:F3')->applyFromArray($rowHeader);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A3', 'No.')->getColumnDimension('A')->setWidth(8);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B3', 'No. Agenda')->getColumnDimension('B')->setWidth(25);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C3', 'No. Surat')->getColumnDimension('C')->setWidth(25);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D3', 'Tanggal Terima')->getColumnDimension('D')->setWidth(40);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E3', 'Pengirim')->getColumnDimension('E')->setWidth(40);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F3', 'Perihal')->getColumnDimension('F')->setWidth(40);

$tanggal_awal 	= substr($func->explode_date(@$_REQUEST['tanggal_awal']),0,10);
$tanggal_akhir 	= substr($func->explode_date(@$_REQUEST['tanggal_akhir']),0,10);

$query_sm = $db->sql("SELECT * FROM _surat_masuk WHERE tanggal_terima BETWEEN '$tanggal_awal' AND '$tanggal_akhir'");

$no = 1;
$row = 4;
while($result_sm = $db->fetch_assoc($query_sm)){
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$row, $no.'.');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$row, $result_sm['no_agenda']);
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$row, $result_sm['no_surat']);
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$row, $func->report_date($result_sm['tanggal_terima'])." M (".$func->report_date_hijriah($result_sm['tanggal_terima'])." H)");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$row, $result_sm['sumber']);
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$row, $result_sm['perihal']);

	$no++;
	$row++;
}
$objPHPExcel->getActiveSheet()->getStyle('A4:F'.($row-1))->applyFromArray($rowBody);


$objPHPExcel->setActiveSheetIndex(0);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
//$objWriter->save(str_replace('.php', '.xlsx', __FILE__));

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment; filename='.$filex);
$objWriter->save('php://output');
?>
