<?php
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();
?>
<html>
	<head>
		<script type="text/javascript" src="includes/ajax.js"></script>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="includes/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="includes/bootstrap/css/font-awesome.css">
		<link rel="stylesheet" href="includes/dist/css/ionicons.min.css">
		<link rel="stylesheet" href="includes/dist/css/AdminLTE.css">
		<link rel="stylesheet" href="includes/dist/css/skins/_all-skins.min.css">

		<script src="includes/plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script src="includes/bootstrap/js/bootstrap.min.js"></script>

		<!-- Input-mask -->
		<script src="includes/plugins/select2/select2.full.min.js"></script>
		<script src="includes/plugins/input-mask/jquery.inputmask.js"></script>
		<script src="includes/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
		<script src="includes/plugins/input-mask/jquery.inputmask.extensions.js"></script>
		<!-- End of Input-mask -->
		
		<!-- Input-mask -->
		<script src="includes/plugins/select2/select2.full.min.js"></script>
		<script src="includes/plugins/input-mask/jquery.inputmask.js"></script>
		<script src="includes/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
		<script src="includes/plugins/input-mask/jquery.inputmask.extensions.js"></script>
		<!-- End of Input-mask -->

		<script type="text/javascript" src="includes/plugins/datepicker/bootstrap-datepicker.min.js"></script>
		<link rel="stylesheet" href="includes/plugins/datepicker/bootstrap-datepicker3.css"/>

		<script language="JavaScript">
			$(function () {
				var options={
					format: 'dd/mm/yyyy',
					todayHighlight: true,
					autoclose: true,
				};
				$('#tanggal_awal').datepicker(options);
				$('#tanggal_akhir').datepicker(options);
			});
		</script>
	</head>
	<body>
		<div class="container-fluid">
		<form name="form_laporan_surat_masuk" method="POST" action="javascript: void(null);" enctype="multipart/form-data">
			<div class="row">
				<div class="col-xs-6">
					<div class="row">
						<div class="form-group col-xs-4">
							<label>Periode Awal</label>
							<div class="input-group col-xs-12">
								<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								<input type="text" name="tanggal_awal" id="tanggal_awal" class="form-control input-sm" value="<?php echo "01".date("/m/Y"); ?>" />
							</div>
						</div>
						<div class="form-group col-xs-4">
							<label>Periode Akhir</label>
							<div class="input-group col-xs-12">
								<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								<input type="text" name="tanggal_akhir" id="tanggal_akhir" class="form-control input-sm" value="<?php echo date("d/m/Y"); ?>" />
							</div>
						</div>
					</div>

					<div class="row">
						<div class="form-group col-xs-12">
							<button class="btn btn-default" type="button" onclick="javascript: sendRequest('content.php', 'module=admin&component=laporan_surat_masuk&action=detail&ajax=true&tanggal_awal='+document.getElementById('tanggal_awal').value+'&tanggal_akhir='+document.getElementById('tanggal_akhir').value, 'detail', 'div'); "><i class="fa fa-database"></i> Proses</button>
							<button class="btn btn-success" type="button" onclick="javascript: window.open('content.php?module=admin&component=laporan_surat_masuk&action=xls&tanggal_awal='+document.getElementById('tanggal_awal').value+'&tanggal_akhir='+document.getElementById('tanggal_akhir').value, '_blank'); "><i class="fa fa-file-o"></i> Excel</button>
						</div>
					</div>
				</div>

				<style type="text/css">
					.detail tr th, .detail tr td{
						font-size: 12px;
						padding: 5px;
					}
				</style>
				
				<div class="col-xs-12" id="detail">
					<?php include("detail.php"); ?>
				</div>
			</div>
		</form>
		</div>
		
		<!-- Alert Validation Form -->
		<input type="hidden" id="Modal" onclick="javascript: $('#s_alert').fadeIn(); $('#s_alert').delay(3000); $('#s_alert').fadeOut(500);" />
		<div class="alert alert-danger" id="s_alert">
			<button type="button" class="close" onclick="javascript: $('#s_alert').hide();">x</button>
			<strong>Warning : </strong> <div id="error-text" class="error-text"><ul></ul></div>
		</div>
		<!-- End Alert Validation Form -->		
	</body>
</html>