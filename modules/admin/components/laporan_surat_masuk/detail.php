<?php
	if(@$_REQUEST['ajax'] == 'true'){
		include "globals/config.php";
		include "globals/functions.php";	
		$db = new Database();
		$func = new Functions();
	}

	$tanggal_awal 	= substr($func->explode_date(@$_REQUEST['tanggal_awal']),0,10);
	$tanggal_akhir 	= substr($func->explode_date(@$_REQUEST['tanggal_akhir']),0,10);

	$query_sm = $db->sql("SELECT * FROM _surat_masuk WHERE tanggal_terima BETWEEN '$tanggal_awal' AND '$tanggal_akhir'");
	$jumlah_data = $query_sm->rowCount();
?>
<table class="table-bordered table-hover table-striped detail">
	<thead>
		<tr>
			<th width="1%">No.</th>
			<th>No. Agenda</th>
			<th>No. Surat</th>
			<th>Tgl. Terima</th>
			<th>Pengirim</th>
			<th>Perihal</th>
		</tr>
	</thead>
	
	<tbody>
		<?php
			if($jumlah_data == 0){
				echo "<tr>";
				echo "	<td align='center' colspan='8'><i>Data kosong.</i></td>";
				echo "</tr>";
			}else{
				$no = 1;
				while($result_sm = $db->fetch_assoc($query_sm)){
					echo "<tr>";
					echo "	<td align='center'>$no.</td>";
					echo "	<td>$result_sm[no_agenda]</td>";
					echo "	<td>$result_sm[no_surat]</td>";
					echo "	<td>".$func->report_date($result_sm['tanggal_terima'])." M (".$func->report_date_hijriah($result_sm['tanggal_terima'])." H)</td>";
					echo "	<td>$result_sm[sumber]</td>";
					echo "	<td>$result_sm[perihal]</td>";
					echo "</tr>";

					$no++;
				}
			}
		?>
	</tbody>
</table>