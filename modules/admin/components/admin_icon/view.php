<?php
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();

$id = @$_REQUEST['id'];
?>
<html>
	<head>
		<title>admin_icon</title>
		
		<script type="text/javascript" src="includes/ajax.js"></script>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="includes/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="includes/bootstrap/css/font-awesome.css">
		<link rel="stylesheet" href="includes/bootstrap/css/ionicons.min.css">
		<link rel="stylesheet" href="includes/dist/css/AdminLTE.css">
		<link rel="stylesheet" href="includes/dist/css/skins/_all-skins.min.css">
	</head>
	<body>
		<?php			
		$hqData = $db->sql("SELECT A.*, B.nama AS menu FROM _admin_icon_home AS A INNER JOIN _admin_menus AS B ON(A.id_admin_menus = B.id_admin_menus) WHERE id_admin_icon_home = '$id'");
		$result = $db->fetch_assoc($hqData);
		$db->close($hqData);
		?>
		<table class="table">
			<tr>
				<td width="20%"><label>Nama Menu</label></td>
				<td width="1%">&nbsp;:&nbsp;</td>
				<td><?php echo $result['menu']; ?></td>
			</tr>
			<tr>
				<td><label>Shortcut</label></td>
				<td>&nbsp;:&nbsp;</td>
				<td><?php echo $result['nama']; ?></td>
			</tr>
			<tr>
				<td valign="top"><label>Icon&nbsp;</label></td>
				<td valign="top">&nbsp;:&nbsp;</td>
				<td><img src="<?php echo $result['icon']; ?>" width="120" /></td>
			</tr>
		</table>
	</body>
</html>