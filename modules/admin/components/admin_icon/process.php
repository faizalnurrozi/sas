<?php 
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();

switch(@$_REQUEST['proc']){
	case 'add' :
		$id 	= uniqid("iconmenu-");
		$nama 	= @$_POST['txtnama'];
		$menu	= @$_POST['cbmenu'];
		$urutan	= @$_POST['txturutan'];
		
		$queryIcon = $db->sql("SELECT icon FROM _admin_icon_home WHERE id_admin_icon_home = '$id'");
		list($icon) = $db->fetch_row($queryIcon);
		$db->close($queryIcon);
		if(file_exists($icon)){
			unlink($icon);
		}
		$asal 	= @$_FILES['txticon']['tmp_name'];
		$tujuan	= "modules/admin/icons/".uniqid("img")."-".@$_FILES['txticon']['name'];
		move_uploaded_file($asal, $tujuan);
		
		$qData = $db->insert('_admin_icon_home', 
			array(
				'id_admin_icon_home' => $id, 
				'id_admin_menus' => $menu, 
				'nama' => $nama, 
				'icon' => $tujuan, 
				'urutan' => $urutan
			)
		);		
		if($qData==true) $_SESSION[_APP_.'s_message_info'] = "Tambah data berhasil";
		else $_SESSION[_APP_.'s_message_error'] = "Tambah data gagal";
		$db->close($qData);
		
		echo "<script>window.parent.sendRequest('content.php', 'module=admin&component=admin_icon&action=list&ajax=true', 'list', 'div');</script>";
		echo "<meta http-equiv='refresh' content='0;url=content.php?module=admin&component=admin_icon&action=add' />";
		break;
		
	case 'update' :
		$id 	= @$_POST['txtid'];
		$nama 	= @$_POST['txtnama'];
		$menu	= @$_POST['cbmenu'];
		$urutan	= @$_POST['txturutan'];
		$start	= @$_POST['start'];
		$cekicon= @$_POST['cbicon'];
		
		$tujuan = "";
		if($cekicon=='1'){			
			
			$asal 	= @$_FILES['txticon']['tmp_name'];
			$tujuan	= "modules/admin/icons/".uniqid("img")."-".@$_FILES['txticon']['name'];
			if(move_uploaded_file($asal, $tujuan)){
				$queryIcon = $db->sql("SELECT icon FROM _admin_icon_home WHERE id_admin_icon_home = '$id'");
				list($icon) = $db->fetch_row($queryIcon);
				if(file_exists($icon)){
					unlink($icon);
				}
				$db->close($queryIcon);
			}
			
			$qData = $db->update('_admin_icon_home', 
				array(
					'id_admin_menus' => $menu, 
					'nama' => $nama, 
					'icon' => $tujuan, 
					'urutan' => $urutan
				), array('id_admin_icon_home' => $id)
			);
			if($qData==true) $_SESSION[_APP_.'s_message_info'] = "Update data berhasil";
			else $_SESSION[_APP_.'s_message_error'] = "Update data gagal";
			$db->close($qData);
		}else{
			$qData = $db->update('_admin_icon_home', 
				array(
					'id_admin_menus' => $menu, 
					'nama' => $nama, 
					'urutan' => $urutan
				), array('id_admin_icon_home' => $id)
			);
			if($qData==true) $_SESSION[_APP_.'s_message_info'] = "Update data berhasil";
			else $_SESSION[_APP_.'s_message_error'] = "Update data gagal";
			$db->close($qData);
		}
		
		echo "<script>window.parent.sendRequest('content.php', 'module=admin&component=admin_icon&action=list&start=$start&ajax=true', 'list', 'div');</script>";
		echo "<meta http-equiv='refresh' content='0;url=content.php?module=admin&component=admin_icon&action=add' />";
		break;
		
	case 'delete' :
		$start = @$_REQUEST['start'];
		$id = @$_REQUEST['id'];
		
		$queryIcon = $db->sql("SELECT icon FROM _admin_icon_home WHERE id_admin_icon_home = '$id'");
		list($icon) = $db->fetch_row($queryIcon);
		if(file_exists($icon)){
			unlink($icon);
		}
		$db->close($queryIcon);
		
		$hqData = $db->delete('_admin_icon_home', array('id_admin_icon_home' => $id));
		
		if($hqData==true) $_SESSION[_APP_.'s_message_info'] = "Hapus data berhasil";
		else $_SESSION[_APP_.'s_message_error'] = "Hapus data gagal";
		$db->close($hqData);
		
		header("location: content.php?module=admin&component=admin_icon&action=list&&start=$start&ajax=true");
		break;
}
?>