<?php
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();
?>
<html>
	<head>
		<script type="text/javascript" src="includes/ajax.js"></script>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="includes/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="includes/bootstrap/css/font-awesome.css">
		<link rel="stylesheet" href="includes/dist/css/ionicons.min.css">
		<link rel="stylesheet" href="includes/dist/css/AdminLTE.css">
		<link rel="stylesheet" href="includes/dist/css/skins/_all-skins.min.css">

		<script src="includes/plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script src="includes/bootstrap/js/bootstrap.min.js"></script>
		<script src="includes/dist/js/app.min.js"></script>
		
	</head>
	<body>
		<?php
		if(@$_REQUEST['id'] != ''){
			$qEditAdmin = "SELECT * FROM _admin_icon_home WHERE id_admin_icon_home = '$_REQUEST[id]'";
			$dataEdit = $db->sql($qEditAdmin);
			$resultEdit = $db->fetch_assoc($dataEdit);
			$db->close($dataEdit);
		}
		?>		
		<div class="container-fluid">
		<form name="form_admin_icon" method="POST" action="javascript: void(null);" enctype="multipart/form-data">
			<?php if(@$_REQUEST['id'] == ''){ ?>
			<input type="hidden" id="proc" name="proc" value="add" />
			<?php }else{ ?>
			<input type="hidden" id="proc" name="proc" value="update" />
			<input type="hidden" id="txtid" name="txtid" value="<?php echo @$resultEdit['id_admin_icon_home']; ?>" />
			<input type="hidden" id="start" name="start" value="<?php echo @$_REQUEST['start']; ?>" />
			<?php } ?>
			<div class="row">
				<div class="form-group col-xs-8 has-nama">
					<label>Nama&nbsp;Shortcut</label>
					<input type="text" name="txtnama" id="txtnama" value="<?php echo @$resultEdit['nama']; ?>" autocomplete="off" class="form-control input-sm" style="width:350px" />
				</div>
			</div>
			<div class="row">
				<div class="form-group col-xs-8 has-menu">
					<label>Dari&nbsp;Menu</label>
					<select name="cbmenu" id="cbmenu" class="form-control input-sm">
						<option value="0">--Root Menu--</option>
						<?php
						$qMenu0 = "SELECT id_admin_menus, nama FROM _admin_menus WHERE id_admin_menus_parent = '0' ORDER BY level ASC, urutan ASC";
						$hqMenu0 = $db->sql($qMenu0);
						while($result0 = $db->fetch_assoc($hqMenu0)){
							$idMenu0 	= $result0['id_admin_menus'];
							$namaMenu0	= $result0['nama'];
							echo "<option value='$idMenu0' ";
							if($idMenu0 == @$resultEdit['id_admin_menus']) echo "selected";
							echo ">-&nbsp;$namaMenu0</option>";
							$qMenu1 = "SELECT id_admin_menus, nama FROM _admin_menus WHERE  id_admin_menus_parent = '$idMenu0' ORDER BY level ASC, urutan ASC";
							$hqMenu1 = $db->sql($qMenu1);
							while($result1 = $db->fetch_assoc($hqMenu1)){
								$idMenu1 	= $result1['id_admin_menus'];
								$namaMenu1	= $result1['nama'];
								echo "<option value='$idMenu1' ";
								if($idMenu1 == @$resultEdit['id_admin_menus']) echo "selected";
								echo ">&nbsp;&nbsp;&nbsp;-&nbsp;$namaMenu1</option>";
								$qMenu2 = "SELECT id_admin_menus, nama FROM _admin_menus WHERE  id_admin_menus_parent = '$idMenu1' ORDER BY level ASC, urutan ASC";
								$hqMenu2 = $db->sql($qMenu2);
								while($result2 = $db->fetch_assoc($hqMenu2)){
									$idMenu2 	= $result2['id_admin_menus'];
									$namaMenu2	= $result2['nama'];
									echo "<option value='$idMenu2' ";
									if($idMenu2 == @$resultEdit['id_admin_menus']) echo "selected";
									echo ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;$namaMenu2</option>";
									$qMenu3 = "SELECT id_admin_menus, nama FROM _admin_menus WHERE  id_admin_menus_parent = '$idMenu2' ORDER BY level ASC, urutan ASC";
									$hqMenu3 = $db->sql($qMenu3);
									while($result3 = $db->fetch_assoc($hqMenu3)){
										$idMenu3 	= $result3['id_admin_menus'];
										$namaMenu3	= $result3['nama'];
										echo "<option value='$idMenu3' ";
										if($idMenu3 == @$resultEdit['id_admin_menus']) echo "selected";
										echo ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;$namaMenu3</option>";
										$qMenu4 = "SELECT id_admin_menus, nama FROM _admin_menus WHERE id_admin_menus_parent = '$idMenu3' ORDER BY level ASC, urutan ASC";
										$hqMenu4 = $db->sql($qMenu4);
										while($result4 = $db->fetch_assoc($hqMenu4)){
											$idMenu4 	= $result4['id_admin_menus'];
											$namaMenu4	= $result4['nama'];
											echo "<option value='$idMenu4' ";
											if($idMenu4 == @$resultEdit['id_admin_menus']) echo "selected";
											echo ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;$namaMenu4</option>";
										}
										$db->close($hqMenu4);
									}
									$db->close($hqMenu3);
								}
								$db->close($hqMenu2);
							}
							$db->close($hqMenu1);
						}
						$db->close($hqMenu0);
						?>
					</select>&nbsp;
				</div>
			</div>
			<div class="row">
				<div class="form-group col-xs-8 has-icon">
					<label>Icon</label>
					<img id="imgicon" src="<?php echo @$resultEdit['icon']; ?>" width="50" height="50" <?php if(@$_REQUEST['id'] == ''){ echo "style='display:none;float:left;'"; } ?> />
					<input type="file" name="txticon" id="txticon" <?php if(@$_REQUEST['id'] != ''){ echo "style='display:none;'"; } ?> />
					<?php if(@$_REQUEST['id'] != ''){ ?>
					<input type="checkbox" name="cbicon" id="cbicon" value="1" onclick="javascript: if(this.checked==true){ $('#imgicon').fadeOut(); $('#txticon').fadeIn(); }else{ $('#imgicon').fadeIn(); $('#txticon').fadeOut(); }" /> <span style="font-size:11px;display:inline-block;">&nbsp;Centang jika ingin mengganti icon.</span>
					<?php } ?>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-xs-8 has-urutan">
					<label>Urutan</label>
					<?php
					/*Cari urutan terbesar & yang belum terpakai*/
					$queryMaxnum = $db->sql("SELECT MAX(urutan) FROM _admin_icon_home");
					list($maxnum) = $db->fetch_row($queryMaxnum);
					$db->close($queryMaxnum);
					
					$n=1;
					for($i=1; $i<=$maxnum; $i++){
						$queryCek = $db->sql("SELECT COUNT(*) FROM _admin_icon_home WHERE urutan = '$i'");
						list($cek) = $db->fetch_row($queryCek);
						if($cek==0){ break; }else{ $n++; }
						$db->close($queryCek);
					}
					?>
					<input type="text" name="txturutan" id="txturutan" value="<?php if(@$_REQUEST['id']=='') echo $n; else echo @$resultEdit['urutan']; ?>" autocomplete="off" class="form-control input-sm" style="width:50px;" onkeyup="javascript: if(isNaN(this.value)){ alert('Harus angka !'); this.value=''; }" />
				</div>
			</div>
			
			<table class="hide">
				<tr><td colspan="3">&nbsp;</td></tr>
				<tr>
					<td colspan="3" align="left">
						<button style="display:none;" id="save" class="btn btn-primary" onclick="javascript:
							var obj = document.form_admin_icon;
							var err = '';
							if(obj.txtnama.value==''){ $('.has-nama').addClass('has-error').focus(); err+='<li>Nama icon harus di isi</li>'; }
							if(obj.cbmenu.value==''){ $('.has-menu').addClass('has-error').focus(); err+='<li>Menu icon harus di isi</li>'; }
							/*if(obj.txticon.value==''){ $('.has-icon').addClass('has-error').focus(); err+='<li>Icon harus di isi</li>'; }*/
							if(obj.txturutan.value==''){ $('.has-urutan').addClass('has-error').focus(); err+='<li>Urutan harus di isi</li>'; }
							if(err==''){
								obj.action='content.php?module=admin&component=admin_icon&action=process';
								obj.submit();
								
								if(window.top.document.getElementById('cbadd').checked == false){
									window.top.document.getElementById('dismiss').click();
								}
							}else{ 
								$('#Modal').click(); $('#error-text').html(err);
							}
						">&nbsp;</button>
						<a class="btn hidden" id="reset" onclick="javascript: document.form_admin_icon.reset();">&nbsp;</a>
					</td>
				</tr>
			</table>
		</form>
		</div>
		
		<!-- Alert Validation Form -->
		<input type="hidden" id="Modal" onclick="javascript: $('#s_alert').fadeIn(); $('#s_alert').delay(3000); $('#s_alert').fadeOut(500);" />
		<div class="alert alert-danger" id="s_alert">
			<button type="button" class="close" onclick="javascript: $('#s_alert').fadeOut();">x</button>
			<strong>Warning : </strong> <div id="error-text" class="error-text"><ul></ul></div>
		</div>
		<!-- End Alert Validation Form -->
		
		<script src="includes/bootstrap/bootstrap.js"></script>
		<script type="text/javascript">
			$("[rel=tooltip]").tooltip();
			$(function() {
				$('.demo-cancel-click').click(function(){return false;});
			});
		</script>
		
	</body>
</html>