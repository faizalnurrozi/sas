<?php
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();
?>
<html>
	<head>
		<title>Data Agama</title>
		<!-- JQuery dan AJAX-->
		<script type="text/javascript" src="includes/ajax.js"></script>
		<script type="text/javascript" src="includes/jquery-1.8.2.min.js"></script>
		<!-- End of JQuery dan AJAX -->
		
		<!-- popup calendar -->
		<link rel="stylesheet" href="includes/popup-calendar/dhtmlgoodies_calendar.css" media="screen"></link>
		<script type="text/javascript" src="includes/popup-calendar/dhtmlgoodies_calendar.js"></script>
		<!-- End of popup calendar -->
		
		<!-- AutoComplete -->
		<script type="text/javascript" src="includes/autocomplete/jquery-1.4.js"></script>
		<script type="text/javascript" src="includes/autocomplete/jquery.autocomplete.js"></script>
		<link rel="stylesheet" type="text/css" href="includes/autocomplete/jquery.autocomplete.css" />
		<!-- End of AutoComplete -->
		
		<!-- Admin CSS -->
		<link rel="stylesheet" href="includes/popup.css"></link>
		<!-- End of Admin CSS -->
	</head>
	<body>
		<?php
			if(@$_REQUEST['id'] != ''){
				$qEdit = "SELECT * FROM _agama WHERE id_agama = '$_REQUEST[id]'";
				$dataEdit = $db->sql($qEdit);
				$resultEdit = $db->fetch_assoc($dataEdit);
			}
		?>
		<h3>DATA AGAMA</h3>
		<form id="form_agama" method="POST" action="content.php?module=admin&component=karyawan&action=process" enctype="multipart/form-data">
			<?php if($_REQUEST['id'] == ''){ ?>
			<input type="hidden" id="proc" name="proc" value="agama_add" />
			<?php }else{ ?>
			<input type="hidden" id="proc" name="proc" value="agama_update" />
			<input type="hidden" id="txtid" name="txtid" value="<?php echo $resultEdit['id_agama']; ?>" />
			<?php } ?>
			<table border="0" cellpadding="0" cellspacing="0" width="90%">
				<tr>
					<td width="15%">Nama Agama</td>
					<td width="3%">&nbsp;:&nbsp;</td>
					<td>
						<input type="text" name="txtnama" id="txtnama" value="<?php echo $resultEdit['nama']; ?>" autocomplete="off"  />
					</td>
				</tr>
				<tr><td colspan="3">&nbsp;</td></tr>
				<tr>
					<td colspan="3" align="left">
						<?php if($_REQUEST['id'] != '') { ?>
						<input type="button" class="button" onClick="document.getElementById('form_agama').submit();" value="Update" />
						<?php } else { ?>
						<input type="button" class="button" onClick="document.getElementById('form_agama').submit();" value="Simpan" />
						<input type="reset" class="button" value="Reset" />
						<?php } ?>
					</td>
				</tr>
			</table>
		</form>
		<p>&nbsp;</p>
		<table class="table-list">
			<tr class="table-list-header">
				<th width="3%">No.</th>
				<th>Nama Agama</th>
				<th colspan="3" width="10%">&nbsp;</th>
			</tr>
			<?php
			$hqData = mysql_query("SELECT * FROM _agama ORDER BY id_agama ASC") or die (mysql_error());
			$rowData = mysql_num_rows($hqData);
			if($rowData == 0){
				echo "<tr><td colspan='5' align='center'><i>Data belum ada</i></td></tr>";
			}else{
				$no=1;
				while(list($idAgama, $namaAgama) = mysql_fetch_row($hqData)){
					echo "<tr class='table-list-row'>";
					echo "<td align='center'>$no.</td>";
					echo "<td>$namaAgama</td>";
					echo "<td align='center'><a href='content.php?module=admin&component=karyawan&action=agama&id=$idAgama'><img src='images/edit.gif' border=0 /></a></td>";
					echo "<td align='center'><a href='content.php?module=admin&component=karyawan&action=process&proc=agama_delete&id=$idAgama'><img src='images/delete.gif' border=0 /></a></td>";
					echo "<td align='center'><input type='button' class='button' value='Pilih' onclick=\"window.top.document.getElementById('iframe_karyawan').contentDocument.getElementById('hdagama').value='$idAgama'; window.top.document.getElementById('iframe_karyawan').contentDocument.getElementById('txtagama').value='$namaAgama'; window.top.hidePopup(500);\" /></td>";
					echo "</tr>";
					$no++;
				}
			}
			?>
		</table>
		<p>&nbsp;</p>
		<table width="98%" style="position:fixed;left:5px;bottom:5px;">
			<tr>
				<td align="left"><input type="button" class="button" value="Tutup" onclick="javascript: window.top.hidePopup(10);" /></td>
				<td align="right">&nbsp;</td>
			</tr>
		</table>
	</body>
</html>