<?php
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();
?>
<html>
	<head>
		<script type="text/javascript" src="includes/ajax.js"></script>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="includes/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="includes/bootstrap/css/font-awesome.css">
		<link rel="stylesheet" href="includes/dist/css/ionicons.min.css">
		<link rel="stylesheet" href="includes/dist/css/AdminLTE.css">
		<link rel="stylesheet" href="includes/dist/css/skins/_all-skins.min.css">
		
		<script src="includes/plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script src="includes/bootstrap/js/bootstrap.min.js"></script>
		<script src="includes/dist/js/app.min.js"></script>
		
		<!-- Input-mask -->
		<script src="includes/plugins/select2/select2.full.min.js"></script>
		<script src="includes/plugins/input-mask/jquery.inputmask.js"></script>
		<script src="includes/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
		<script src="includes/plugins/input-mask/jquery.inputmask.extensions.js"></script>
		<!-- End of Input-mask -->
		
		<!-- Typeahead.js -->
		<link rel="stylesheet" href="includes/bootstrap/css/typeahead.css">
		<script src="includes/bootstrap/js/bootstrap-typeahead.js"></script>
		<!-- End of Typeahead.js -->
		
		<style type="text/css">
		.note{
			font-style: italic; color: red;
		}
		</style>
		
		<script language="JavaScript">
		function autoResize(id){
			var newheight;
			if(document.getElementById){
				newheight=document.getElementById(id).contentWindow.document .body.scrollHeight;
			}
			document.getElementById(id).height= (newheight) + "px";			
		}
		
		$(function () {
			$("[data-mask]").inputmask();
		});
		</script>	
	</head>
	<body>
		<!-- Alert Process -->
		<?php if(isset($_SESSION[_APP_.'s_message_info'])){ ?>
		<div class="alert alert-info">
			<button type="button" class="close" data-dismiss="alert">x</button>
			<strong>Status : </strong> <?php echo $_SESSION[_APP_.'s_message_info']; unset($_SESSION[_APP_.'s_message_info']); ?>
		</div>
		<?php } ?>
		<?php if(isset($_SESSION[_APP_.'s_message_error'])){ ?>
		<div class="alert alert-danger">
			<button type="button" class="close" data-dismiss="alert">x</button>
			<strong>Error : </strong> <?php echo $_SESSION[_APP_.'s_message_error']; unset($_SESSION[_APP_.'s_message_error']); ?>
		</div>
		<?php } ?>
		<!-- Alert Process -->
		
		<?php
		$nip = @$_REQUEST['id'];
		list($usernames) = $db->result_row("SELECT usernames FROM _admin WHERE nip = '$nip' ");
		list($namaKaryawan, $tanggal_lahir) = $db->result_row("SELECT nama, tanggal_lahir FROM _karyawan WHERE nip = '$nip' ");
		if(@$_REQUEST['id'] != ''){
			$qEdit = "SELECT * FROM _admin WHERE usernames = '$usernames'";
			$dataEdit = $db->sql($qEdit);
			$resultEdit = $db->fetch_assoc($dataEdit);
		}
		?>
		
		<div class="container-fluid">
		<form name="form_admin" method="POST" action="javascript: void(null);" enctype="multipart/form-data">
			<?php if($usernames==""){ ?>
			<input type="hidden" id="proc" name="proc" value="add_admin" />
			<?php }else{ ?>
			<input type="hidden" id="proc" name="proc" value="update_admin" />
			<?php } ?>
			<input type="hidden" id="txtnip" name="txtnip" value="<?php echo $nip; ?>" />
			<input type="hidden" id="txtid" name="txtid" value="<?php echo $resultEdit['usernames']; ?>" />
			<div class="row">
				<div class="form-group col-xs-6 has-nip">
					<label>NIP</label>
					<input type="text" class="form-control input-sm" value="<?php echo $nip; ?>" readonly />
				</div>
				<div class="form-group col-xs-6 has-nama">
					<label>Nama</label>
					<input type="text" name="txtnama" id="txtnama" value="<?php echo $namaKaryawan; ?>" autocomplete="off" class="form-control input-sm" readonly />
				</div>
			</div>
			<div class="row">
				<div class="form-group col-xs-6 has-user">
					<label>Usernames</label>
					<input type="text" name="txtuser" id="txtuser" value="<?php echo ($resultEdit['usernames']=='') ? $nip : $resultEdit['usernames']; ?>" autocomplete="off" class="form-control input-sm" onblur="javascript: ajaxCustom('content.php', 'module=admin&component=karyawan&action=process&proc=cek_user&user='+this.value, 'cek_user'); " onclick="javascript: this.select();" placeholder="Isikan Username..." />
					<span id="cek_user"></span>
				</div>
				<div class="form-group col-xs-6 has-group">
					<label>Group</label>
					<div class="input-group">
						<?php
						list($namaGroup) = $db->result_row("SELECT nama FROM _admin_group WHERE id_admin_group = '$resultEdit[id_admin_group]' ");
						?>
						<input id="hdgroup" name="hdgroup" type="text" class="hide" autocomplete="off" value="<?php echo $resultEdit['id_admin_group']; ?>" />
						<input id="txtgroup" name="txtgroup" type="text" class="col-md-12 form-control" placeholder="Pilih Group ..." autocomplete="off" value="<?php echo $namaGroup; ?>" />
						<span class="input-group-addon" style="cursor:pointer;" data-toggle="modal" data-target="#myModalx" onclick="javascript: document.getElementById('iframe_group').src='content.php?module=admin&component=karyawan&action=group'; "><i class="fa fa-plus"></i></span>
					</div>
					<script>
					$(function() {
						function displayResult(item) {
							$('#hdgroup').val(item.value);
						}
						$('#txtgroup').typeahead({
							source: [
							<?php
							$qGroup = $db->sql("SELECT * FROM _admin_group");
							while($hqGroup = $db->fetch_assoc($qGroup)){
								echo "{id: '$hqGroup[id_admin_group]', name: '$hqGroup[nama]'},";
							}
							?>
							],
							onSelect: displayResult
						});
					});
					</script>
				</div>
			</div>
			<div class="row">
				<?php
				if($usernames==""){
				?>
				<div class="form-group col-xs-6 has-pass">
					<label>Passwords</label>
					<input type="password" name="txtpass" id="txtpass" autocomplete="off" class="form-control input-sm" value="<?php echo str_replace('/', '', $func->implode_date($tanggal_lahir)); ?>" onclick="javascript: this.select();" title="Password standart adalah tanggal lahir." placeholder="Isikan Password..." />&nbsp;<i style="color:red; font-size:9px;">Password standart adalah NIP, segera ganti untuk keamanan.</i>
				</div>
				<?php }else{ ?>
				<div class="form-group col-xs-6 has-pass">
					<label><input type="checkbox" name="cbcek" id="cbcek" value="1" onclick="javascript: if(this.checked==true){ document.getElementById('txtpass').readOnly=false; document.getElementById('txtpass').select(); }else{ document.getElementById('txtpass').readOnly=true; }" />&nbsp;Ganti Password.</label>
					<input type="password" name="txtpass" id="txtpass" autocomplete="off" class="form-control input-sm" value="*******" onclick="javascript: this.select();" title="Password standart adalah tanggal lahir." placeholder="Isikan Password..." readonly="true" />
				</div>
				<?php } ?>
				<div class="form-group col-xs-6 has-pass">
					<label><input type="checkbox" name="cbsuperuser" id="cbsuperuser" value="TRUE" <?php if($resultEdit['superuser']=='TRUE') echo "checked"; ?> />&nbsp;Superuser.</label><br/>
					<span class="note">Jika pilihan superuser dicentang, user ini akan mendapatkan pemberitahuan dari semua kejadian.</span>
				</div>
			</div>
			
			<table class="hide">		
				<tr>
					<td colspan="3" align="right">
						<?php if($usernames==""){ ?>
						<button id="save" style="display:none;" class="btn btn-primary" onClick="javascript:
							var obj = document.form_admin;
							var err = '';
							if(obj.txtuser.value==''){ $('#txtuser').removeClass('input-xlarge').addClass('input-error').focus(); err+='<li>User harus di isi</li>'; }
							if(obj.txtpass.value==''){ $('#txtpass').removeClass('input-xlarge').addClass('input-error').focus(); err+='<li>Passwords harus di isi</li>'; }
							if(obj.txtnama.value==''){ $('#txtnama').removeClass('input-xlarge').addClass('input-error').focus(); err+='<li>Nama harus di isi</li>'; }
							if(err==''){
								obj.action='content.php?module=admin&component=karyawan&action=process';
								obj.submit();
								//if(window.top.document.getElementById('cbadd').checked == false){
									window.top.document.getElementById('dismissAdmin').click();
								//}
							}else {
								$('#Modal').click(); $('#error-text').html(err);
							}
						">&nbsp;</button>
						<?php }else{ ?>
						<button id="save" style="display:none;" class="btn btn-primary" onClick="javascript:
							var obj = document.form_admin;
							var err = '';
							if(obj.txtuser.value==''){ $('#txtuser').removeClass('input-xlarge').addClass('input-error').focus(); err+='<li>User harus di isi</li>'; }
							if(obj.txtnama.value==''){ $('#txtnama').removeClass('input-xlarge').addClass('input-error').focus(); err+='<li>Nama harus di isi</li>'; }
							if(err==''){
								obj.action='content.php?module=admin&component=karyawan&action=process';
								obj.submit();
								//if(window.top.document.getElementById('cbadd').checked == false){
									window.top.document.getElementById('dismissAdmin').click();
								//}
							}else {
								$('#Modal').click(); $('#error-text').html(err);
							}
						">&nbsp;</button>
						<?php } ?>
					</td>
				</tr>
			</table>
		</form>
		</div>
		
		<!-- Alert Validation Form -->
		<input type="hidden" id="Modal" onclick="javascript: $('#s_alert').fadeIn();" />
		<div class="alert alert-danger" id="s_alert" style="display:none;">
			<button type="button" class="close" onclick="javascript: $('#s_alert').fadeOut();">x</button>
			<strong>Warning : </strong> <div id="error-text" class="error-text"><ul></ul></div>
		</div>
		<!-- End Alert Validation Form -->
		
		<div class="modal fade" id="myModalx" data-backdrop="static">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Group&nbsp;Admin</h4>
					</div>
					<div class="modal-body">
						<iframe name="iframe_group" id="iframe_group" width="98%" height="80" frameborder="0" onload="javascript: autoResize('iframe_group')"></iframe>
					</div>
					<div class="modal-footer">
						<button class="btn btn-danger btn-sm" data-dismiss="modal" aria-hidden="true" id="dismissButton"><i class="fa fa-remove"></i> Batal</button>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>