<html>
	<head>
		<!-- JQuery dan AJAX-->
		<script type="text/javascript" src="includes/ajax.js"></script>
		<script type="text/javascript" src="includes/jquery-1.8.2.min.js"></script>
		<!-- End of JQuery dan AJAX -->
		
		<script type="text/javascript" src="includes/ajax.js"></script>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="includes/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="includes/bootstrap/css/font-awesome.css">
		<link rel="stylesheet" href="includes/dist/css/ionicons.min.css">
		<link rel="stylesheet" href="includes/dist/css/AdminLTE.css">
		<link rel="stylesheet" href="includes/dist/css/skins/_all-skins.min.css">

		<script src="includes/plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script src="includes/bootstrap/js/bootstrap.min.js"></script>
		<script src="includes/dist/js/app.min.js"></script>
	</head>
	<body>
		<p>Untuk mengisi data via upload file excel, silahkan download terlebih dahulu template nya kemudian setelah data terisi upload lagi.</p>
		<form id="form_upload" method="POST" action="content.php?module=admin&component=karyawan&action=process" enctype="multipart/form-data">
			<input type="hidden" id="proc" name="proc" value="upload_data" />
			<button type="button" class="btn btn-primary" onclick="javascript: window.open('content.php?module=admin&component=karyawan&action=template_xls', '_blank');"><i class="fa fa-download"></i> Download Template</button>
			<button type="button" id="tombol" class="btn btn-success" onclick="javascript: $('#ffile').click();"><i class="fa fa-upload"></i> Upload Data</button>
			<input type="file" name="ffile" id="ffile" style="width:1px;height:1px;background-color:transparent;" onchange="javascript: document.getElementById('tombol').disabled=true; document.getElementById('tulisan').style.display='none'; document.getElementById('loading').style.display='block'; document.getElementById('form_upload').submit(); " />
			<p><b id="tulisan" style="font-size:12px;">&nbsp;</b><img id="loading" src="images/loadings.gif" style="display:none;" /></p>
		</form>
	</body>
</html>