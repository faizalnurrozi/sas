<?php
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();

$id = @$_GET['id'];
?>
<html>
	<head>
		<script type="text/javascript" src="includes/ajax.js"></script>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="includes/bootstrap/css/bootstrap.min.css"></link>
		<link rel="stylesheet" href="includes/bootstrap/css/font-awesome.css"></link>
		<link rel="stylesheet" href="includes/dist/css/ionicons.min.css"></link>
		<link rel="stylesheet" href="includes/dist/css/AdminLTE.css"></link>
		<link rel="stylesheet" href="includes/dist/css/skins/_all-skins.min.css"></link>
		
		<script src="includes/plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script src="includes/bootstrap/js/bootstrap.min.js"></script>
		<script src="includes/dist/js/app.min.js"></script>
	</head>
	<body>
		<?php
		$qData = " SELECT * FROM _karyawan WHERE nip = '$id' ";
		$hqData = $db->sql($qData);
		$hasil = $db->fetch_assoc($hqData);
		?>
		<table class="table">
			<tr>
				<td width="20%">NIP&nbsp;</td>
				<td width="1%">&nbsp;:&nbsp;</td>
				<td><?php echo $hasil['nip']; ?></td>
			</tr>					
			<tr>
				<td>Nama&nbsp;Karyawan</td>
				<td>&nbsp;:&nbsp;</td>
				<td><?php echo $hasil['nama']; ?></td>
			</tr>					
			<tr>
				<td>Tempat,&nbsp;Tgl. Lahir</td>
				<td>&nbsp;:&nbsp;</td>
				<td><?php echo $hasil['tempat_lahir']; ?>, <?php echo $func->report_date($hasil['tanggal_lahir']); ?></td>
			</tr>					
			<tr>
				<td>Jenis&nbsp;Kelamin</td>
				<td>&nbsp;:&nbsp;</td>
				<td>
				<?php
					switch($hasil['jenis_kelamin']){ case 'L' : echo "Laki - laki"; break; case 'P' : echo "Perempuan"; break; }
				?>
				</td>
			</tr>					
			<tr>
				<td>Alamat&nbsp;</td>
				<td>&nbsp;:&nbsp;</td>
				<td><?php echo $hasil['alamat']; ?></td>
			</tr>					
			<tr>
				<td>Agama&nbsp;</td>
				<td>&nbsp;:&nbsp;</td>
				<td>
				<?php 
				list($namaAgama) = $db->result_row("SELECT nama FROM _agama WHERE id_agama = '$hasil[id_agama]' ");
				echo $namaAgama; 
				?>
				</td>
			</tr>					
			<tr>
				<td>Telepon&nbsp;</td>
				<td>&nbsp;:&nbsp;</td>
				<td><?php echo $hasil['telepon']; ?></td>
			</tr>					
			<tr>
				<td>Pendidikan&nbsp;Terakhir</td>
				<td>&nbsp;:&nbsp;</td>
				<td><?php echo $hasil['pendidikan_terakhir']; ?></td>
			</tr>
			<tr>
				<td>Jabatan</td>
				<td>&nbsp;:&nbsp;</td>
				<td>
				<?php 
				list($namaJabatan) = $db->result_row("SELECT nama FROM _jabatan WHERE id_jabatan = '$hasil[id_jabatan]' ");
				echo $namaJabatan; 
				?>
				</td>
			</tr>
			<tr>
				<td valign="top">Foto&nbsp;</td>
				<td valign="top">&nbsp;:&nbsp;</td>
				<td>
				<?php if($hasil['foto']!=''){ ?>
				<img src="<?php echo $hasil['foto']; ?>" width="150" />
				<?php }else{ echo "-"; } ?>
				</td>
			</tr>					
		</table>
	</body>
</html>