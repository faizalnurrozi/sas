<?php 
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();

switch(@$_REQUEST['proc']){
	case 'add' :
		# ---------------------#
		# Penangkapan variable #
		# ---------------------#
		
		$id 	= @$_POST['txtid'];
		$nama 	= @$_POST['txtnama'];
		
		$hqData = $db->insert("_admin_group", array('id_admin_group' => $id, 'nama' => $nama));
		if($hqData==true) $_SESSION[_APP_.'s_message_info'] = "Tambah data berhasil";
		else $_SESSION[_APP_.'s_message_error'] = "Tambah data gagal";
		$db->close($hqData);
		#---------- * ----------#
		
		
		# ----------------#
		# Pencatatan Logs #
		# ----------------#
		
		/* ----- Activity Logs (Insert) ------ */
		$func->activity_logs_insert("_admin_group", $id, $_SESSION[_APP_.'s_userAdmin']);
		/* ----- End Activity Logs (Insert) ------ */
		
		#---------- * ----------#
		
		
		# ------------#
		# Kirim balik #
		# ------------#
		
		echo "<script>window.parent.sendRequest('content.php', 'module=admin&component=admin_group&action=list&ajax=true', 'list', 'div');</script>";
		echo "<meta http-equiv='refresh' content='0;url=content.php?module=admin&component=admin_group&action=add' />";
		
		#---------- * ----------#
		
		break;
		
	case 'update' :
		# ---------------------#
		# Penangkapan variable #
		# ---------------------#
		
		$idx 	= @$_POST['txtidx'];
		$id 	= @$_POST['txtid'];
		$nama 	= @$_POST['txtnama'];
		
		# ----------------#
		# Pencatatan Logs #
		# ----------------#
		
		/* ----- Activity Logs (Update) ------ */
		$func->activity_logs_update("_admin_group", array('id_admin_group' => $id, 'nama' => $nama), array('id_admin_group' => $idx), 'OR', $_SESSION[_APP_.'s_userAdmin']);
		/* ----- End Activity Logs (Update) ------ */
		
		#---------- * ----------#		
		
		
		# --------------#
		# Proses Update #
		# --------------#
		
		$hqData = $db->update("_admin_group", array('id_admin_group' => $id, 'nama' => $nama), array('id_admin_group' => $idx));
		if($hqData==true) $_SESSION[_APP_.'s_message_info'] = "Update data berhasil";
		else $_SESSION[_APP_.'s_message_error'] = "Update data gagal";
		$db->close($hqData);
		#---------- * ----------#
		
		
		# ------------#
		# Kirim balik #
		# ------------#
		
		echo "<script>window.parent.sendRequest('content.php', 'module=admin&component=admin_group&action=list&ajax=true', 'list', 'div');</script>";
		echo "<meta http-equiv='refresh' content='0;url=content.php?module=admin&component=admin_group&action=add' />";
		
		#---------- * ----------#
		break;
		
	case 'delete' :
		# ---------------------#
		# Penangkapan variable #
		# ---------------------#
		
		$idx = @$_REQUEST['id'];
		
		#---------- * ----------#		
		
		# --------------#
		# Proses Delete #
		# --------------#
		
		$hqData = $db->delete("_admin_group", array('id_admin_group' => $idx));
		$hqData = $db->delete("_admin_menus_access", array('id_admin_group' => $idx));
		
		if($hqData==true) $_SESSION[_APP_.'s_message_info'] = "Hapus data berhasil";
		else $_SESSION[_APP_.'s_message_error'] = "Hapus data gagal";
		$db->close($hqData);
		#---------- * ----------#
		
		
		# ----------------#
		# Pencatatan Logs #
		# ----------------#
		
		/* ----- Activity Logs (Delete) ------ */
		$func->activity_logs_delete("_admin_group", $idx, $_SESSION[_APP_.'s_userAdmin']);
		/* ----- End Activity Logs (Delete) ------ */
		
		#---------- * ----------#
		
		
		# ------------#
		# Kirim balik #
		# ------------#
		
		header("location: content.php?module=admin&component=admin_group&action=list&ajax=true");
		
		#---------- * ----------#
		break;
	
	/* -- Update Data -- */
	case 'update_admin' :
		$nama 		= @$_POST['txtnama'];
		$idx 		= @$_POST['txtidx'];
		
		//Delete di _admin_akses_menu		
		$hqData = $db->delete('_admin_menus_access', array('id_admin_group' => $idx));
		
		//Insert ke _admin_akses_menu
		/* Level 0 */
		$qData0 = "SELECT id_admin_menus, nama, link, icon FROM _admin_menus WHERE level = '0' ORDER BY urutan ASC ";
		$hqData0 = $db->sql($qData0);
		while(list($idMenus0, $nama0, $link0, $icon0) = $db->fetch_row($hqData0)){
			$menu0 = @$_POST['cbmenu_'.$idMenus0];
			$access0C = (@$_POST['cbakses_add_'.$idMenus0]=='') ? '0' : $_POST['cbakses_add_'.$idMenus0];
			$access0R = (@$_POST['cbakses_view_'.$idMenus0]=='') ? '0' : $_POST['cbakses_view_'.$idMenus0];
			$access0U = (@$_POST['cbakses_update_'.$idMenus0]=='') ? '0' : $_POST['cbakses_update_'.$idMenus0];
			$access0D = (@$_POST['cbakses_delete_'.$idMenus0]=='') ? '0' : $_POST['cbakses_delete_'.$idMenus0];
			$access0 = $access0C . $access0R . $access0U . $access0D ;
			if($menu0 != ""){
				$stmt0 = $db->sql("INSERT INTO _admin_menus_access VALUES ('$idx', '$menu0', '$access0')");
				$db->close($stmt0);
			}
			/* Level 1 */
			$qData1 = "SELECT id_admin_menus, nama, link, icon FROM _admin_menus WHERE level = '1' AND id_admin_menus_parent = '$idMenus0' ORDER BY urutan ASC ";
			$hqData1 = $db->sql($qData1);
			$jumData1 = $db->num_rows($hqData1);
			if($jumData1 != 0){
				while(list($idMenus1, $nama1, $link1, $icon1) = $db->fetch_row($hqData1)){
					$menu1 = @$_POST['cbmenu_'.$idMenus1];
					$access1C = (@$_POST['cbakses_add_'.$idMenus1]=='') ? '0' : $_POST['cbakses_add_'.$idMenus1];
					$access1R = (@$_POST['cbakses_view_'.$idMenus1]=='') ? '0' : $_POST['cbakses_view_'.$idMenus1];
					$access1U = (@$_POST['cbakses_update_'.$idMenus1]=='') ? '0' : $_POST['cbakses_update_'.$idMenus1];
					$access1D = (@$_POST['cbakses_delete_'.$idMenus1]=='') ? '0' : $_POST['cbakses_delete_'.$idMenus1];
					$access1 = $access1C . $access1R . $access1U . $access1D ;
					if($menu1 != ""){
						$stmt1 = $db->sql("INSERT INTO _admin_menus_access VALUES ('$idx', '$menu1', '$access1')");
						$db->close($stmt1);
					}
					/* Level 2 */
					$qData2 = "SELECT id_admin_menus, nama, link, icon FROM _admin_menus WHERE level = '2' AND id_admin_menus_parent = '$idMenus1' ORDER BY urutan ASC ";
					$hqData2 = $db->sql($qData2);
					$jumData2 = $db->num_rows($hqData2);
					if($jumData2 != 0){
						while(list($idMenus2, $nama2, $link2, $icon2) = $db->fetch_row($hqData2)){
							$menu2 = @$_POST['cbmenu_'.$idMenus2];
							$access2C = (@$_POST['cbakses_add_'.$idMenus2]=='') ? '0' : $_POST['cbakses_add_'.$idMenus2];
							$access2R = (@$_POST['cbakses_view_'.$idMenus2]=='') ? '0' : $_POST['cbakses_view_'.$idMenus2];
							$access2U = (@$_POST['cbakses_update_'.$idMenus2]=='') ? '0' : $_POST['cbakses_update_'.$idMenus2];
							$access2D = (@$_POST['cbakses_delete_'.$idMenus2]=='') ? '0' : $_POST['cbakses_delete_'.$idMenus2];
							$access2 = $access2C . $access2R . $access2U . $access2D ;
							if($menu2 != ""){
								$stmt2 = $db->sql("INSERT INTO _admin_menus_access VALUES ('$idx', '$menu2', '$access2')");
								$db->close($stmt2);
							}
							/* Level 3 */							
							$qData3 = "SELECT id_admin_menus, nama, link, icon FROM _admin_menus WHERE level = '3' AND id_admin_menus_parent = '$idMenus2' ORDER BY urutan ASC ";
							$hqData3 = $db->sql($qData3);
							$jumData3 = $db->num_rows($hqData3);
							
							if($jumData3 != 0){
								while(list($idMenus3, $nama3, $link3, $icon3) = $db->fetch_row($hqData3)){
									$menu3 = @$_POST['cbmenu_'.$idMenus3];
									$access3C = (@$_POST['cbakses_add_'.$idMenus3]=='') ? '0' : $_POST['cbakses_add_'.$idMenus3];
									$access3R = (@$_POST['cbakses_view_'.$idMenus3]=='') ? '0' : $_POST['cbakses_view_'.$idMenus3];
									$access3U = (@$_POST['cbakses_update_'.$idMenus3]=='') ? '0' : $_POST['cbakses_update_'.$idMenus3];
									$access3D = (@$_POST['cbakses_delete_'.$idMenus3]=='') ? '0' : $_POST['cbakses_delete_'.$idMenus3];
									$access3 = $access3C . $access3R . $access3U . $access3D ;
									if($menu3 != ""){
										$stmt3 = $db->sql("INSERT INTO _admin_menus_access VALUES ('$idx', '$menu3', '$access3')");
										$db->close($stmt3);
									}
									/* Level 4 */
									$qData4 = "SELECT id_admin_menus, nama, link, icon FROM _admin_menus WHERE level = '4' AND id_admin_menus_parent = '$idMenus3' ORDER BY urutan ASC ";									
									$hqData4 = $db->sql($qData4);
									$jumData4 = $db->num_rows($hqData4);
									if($jumData4 != 0){
										while(list($idMenus4, $nama4, $link4, $icon4) = $db->fetch_row($hqData4)){
											$menu4 = @$_POST['cbmenu_'.$idMenus4];
											$access4C = (@$_POST['cbakses_add_'.$idMenus4]=='') ? '0' : $_POST['cbakses_add_'.$idMenus4];
											$access4R = (@$_POST['cbakses_view_'.$idMenus4]=='') ? '0' : $_POST['cbakses_view_'.$idMenus4];
											$access4U = (@$_POST['cbakses_update_'.$idMenus4]=='') ? '0' : $_POST['cbakses_update_'.$idMenus4];
											$access4D = (@$_POST['cbakses_delete_'.$idMenus4]=='') ? '0' : $_POST['cbakses_delete_'.$idMenus4];
											$access4 = $access4C . $access4R . $access4U . $access4D ;
											if($menu4 != ""){
												$stmt4 = $db->sql("INSERT INTO _admin_menus_access VALUES ('$idx', '$menu4', '$access4')");
												$db->close($stmt4);
											}
										}
										$db->close($hqData4);
									}
								}
								$db->close($hqData3);
							}
						}
						$db->close($hqData2);
					}
				}
				$db->close($hqData1);
			}
		}
		$db->close($hqData0);
		
		if($hqData==true) $_SESSION[_APP_.'s_message_info'] = "Update Akses User berhasil";
		else $_SESSION[_APP_.'s_message_error'] = "Update Akses User gagal";
		$db->close($hqData);
		
		echo "<script>window.parent.location.reload();</script>";
		//echo "<script>window.parent.sendRequest('content.php', 'module=admin&component=admin&action=list&ajax=true', 'list', 'div');</script>";
		//echo "<meta http-equiv='refresh' content='0;url=content.php?module=admin&component=admin&action=add' />";
	break;
	/* -- End Update data -- */
}
?>