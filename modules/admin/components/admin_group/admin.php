<?php
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();
?>
<html>
	<head>
		<script type="text/javascript" src="includes/ajax.js"></script>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="includes/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="includes/bootstrap/css/font-awesome.css">
		<link rel="stylesheet" href="includes/dist/css/ionicons.min.css">
		<link rel="stylesheet" href="includes/dist/css/AdminLTE.css">
		<!--<link rel="stylesheet" href="includes/dist/css/skins/_all-skins.min.css">-->

		<script src="includes/plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script src="includes/bootstrap/js/bootstrap.min.js"></script>
		<script src="includes/dist/js/app.min.js"></script>
		
		<script language="JavaScript">
		function centang(idmenu){
			var add = document.getElementById('cbakses_add_'+idmenu); 
			var view = document.getElementById('cbakses_view_'+idmenu); 
			var edit = document.getElementById('cbakses_update_'+idmenu); 
			var del = document.getElementById('cbakses_delete_'+idmenu); 
			var all = document.getElementById('cbakses_all_'+idmenu);
			if(add.checked==true && view.checked==true && edit.checked==true && del.checked==true){
				all.checked=true; 				
			}else{
				all.checked=false;
			}
		}
		
		function pilih_menu(idmenu){
			if(document.getElementById('cbmenu_'+idmenu).checked==true){
				document.getElementById('divakses_'+idmenu).style.display='inline-block'; 
				document.getElementById('cbakses_all_'+idmenu).checked=true; 
				document.getElementById('cbakses_add_'+idmenu).checked=true; 
				document.getElementById('cbakses_view_'+idmenu).checked=true; 
				document.getElementById('cbakses_update_'+idmenu).checked=true; 
				document.getElementById('cbakses_delete_'+idmenu).checked=true;
			}else{
				document.getElementById('divakses_'+idmenu).style.display='none';
			}
		}
		
		function centang_all(idmenu){
			if(document.getElementById('cbakses_all_'+idmenu).checked==true){ 
				document.getElementById('cbakses_add_'+idmenu).checked=true; 
				document.getElementById('cbakses_view_'+idmenu).checked=true; 
				document.getElementById('cbakses_update_'+idmenu).checked=true; 
				document.getElementById('cbakses_delete_'+idmenu).checked=true; 
			}else{  
				document.getElementById('cbakses_add_'+idmenu).checked=false; 
				document.getElementById('cbakses_view_'+idmenu).checked=false; 
				document.getElementById('cbakses_update_'+idmenu).checked=false; 
				document.getElementById('cbakses_delete_'+idmenu).checked=false; 
			}
		}
		</script>
		
		<style type="text/css">
			body{ overflow-x:hidden; overflow-y:scroll;	}
			.input-xlarge{ height:30px !important; }
			label{ display:inline-block; font-size:14px; font-weight:bold; }
			.label{ display:inline-block; font-size:12px; font-weight:bold; color:green !important; font-style:italic; }
		</style>
	</head>
	<body>
		<?php
		if(@$_REQUEST['id'] != ''){
			$qEditAdmin = "SELECT * FROM _admin WHERE id_admin_group = '$_REQUEST[id]'";
			$dataEdit = $db->sql($qEditAdmin);
			$resultEdit = $db->fetch_assoc($dataEdit);
			$db->close($dataEdit);
		}
		
		list($namaGroup) = $db->result_row("SELECT nama FROM _admin_group WHERE id_admin_group = '".@$_REQUEST['id']."' ");
		?>	
		<div class="container-fluid">
		<form name="form_admin" method="POST" action="javascript: void(null);" style="font-size:12px;">
			<input type="hidden" id="proc" name="proc" value="update_admin" />
			<input type="hidden" id="txtidx" name="txtidx" value="<?php echo @$_REQUEST['id']; ?>" />
			<div class="row">
				<div class="form-group col-xs-12 has-nama">
					<label>Group</label>
					<input type="text" name="txtnama" id="txtnama" value="<?php echo $namaGroup; ?>" autocomplete="off" class="form-control input-sm" readonly />
				</div>
			</div>
					
			<div class="row">
				<div class="form-group col-xs-12 has-akses">
					<label>Akses Menu</label>
					<div class="form-group col-xs-12">
						<ul class="nav nav-tabs" id="myTab">
						<?php
						$qMenu = $db->sql("SELECT id_admin_menus, nama, icon, deskripsi FROM _admin_menus WHERE level = '0' ORDER BY urutan ASC");
						$x=0;
						while(list($idMenus, $namas, $icons, $deskripsis) = $db->fetch_row($qMenu)){
							echo "<li onclick=\"javascript: window.top.autoResize('iframe_admin');\"";
							if($x==0) echo "class='active'";
							echo "><a href='#menu$x' title='$deskripsis'>".stripslashes($icons)." $namas</a></li>";
							$x++;
						}
						$db->close($qMenu);
						?>
						</ul>
						<div class="tab-content">
							<?php
							$qMenu = $db->sql("SELECT id_admin_menus, nama, icon, deskripsi FROM _admin_menus WHERE level = '0' ORDER BY urutan ASC");
							$x=0;
							while(list($idMenus, $namas, $icons, $deskripsis) = $db->fetch_row($qMenu)){
							?>
							<div class="tab-pane<?php if($x==0) echo " active"; ?>" id="menu<?php echo $x; ?>">
								<legend><?php echo $namas; ?></legend>
								<div id="access-menu" style="font-size:12px;">
									<ul style="list-style-type:none;margin-left:-35px;">
									<?php
									/* Level 0 */
									$hqData0 = $db->sql("SELECT id_admin_menus, nama, link, icon, deskripsi FROM _admin_menus WHERE id_admin_menus = '$idMenus' ");
									while(list($idMenus0, $nama0, $link0, $icon0, $deskripsi0) = $db->fetch_row($hqData0)){
										$hqMenu0 = $db->sql("SELECT access FROM _admin_menus_access WHERE id_admin_menus = '$idMenus0' AND id_admin_group = '".@$_REQUEST['id']."'");
										$cekmenu0 = $db->num_rows($hqMenu0);
										list($access0) = $db->fetch_row($hqMenu0);
										$db->close($hqMenu0);
										
										if($cekmenu0==0){ $selected0 = ""; $akses0 = "display:none"; }else{ $selected0 = "checked"; $akses0 = "display:inline-block"; }
										$menus[] = $idMenus0;
										
										echo "<li><label style='display:inline-block;' title='$deskripsi0'><input type='checkbox' name='cbmenu_$idMenus0' id='cbmenu_$idMenus0' value='$idMenus0' $selected0 onclick=\"javascript: pilih_menu('$idMenus0'); \" />&nbsp;".stripslashes($icon0)." $nama0 </label> &nbsp;
										<div style='$akses0;width:auto;background-color:#eee;' id='divakses_$idMenus0'>
										<label class='label label-warning'><input type='checkbox' name='cbakses_all_$idMenus0' id='cbakses_all_$idMenus0' value='' "; if($access0 == '1111'){ echo "checked"; } echo " onclick=\"javascript: centang_all('$idMenus0'); \" /> Pilih Semua </label>&nbsp; 
										<label class='label'><input type='checkbox' name='cbakses_add_$idMenus0' id='cbakses_add_$idMenus0' value='1' "; if(substr($access0, 0, 1) == '1'){ echo "checked"; } echo " onclick=\"javascript: centang('$idMenus0');\" /> Tambah </label>&nbsp; 
										<label class='label'><input type='checkbox' name='cbakses_view_$idMenus0' id='cbakses_view_$idMenus0' value='1' ";if(substr($access0, 1, 1) == '1'){ echo "checked"; } echo " onclick=\"javascript: centang('$idMenus0');\" /> Lihat </label>&nbsp; 
										<label class='label'><input type='checkbox' name='cbakses_update_$idMenus0' id='cbakses_update_$idMenus0' value='1' ";if(substr($access0, 2, 1) == '1'){ echo "checked"; } echo " onclick=\"javascript: centang('$idMenus0');\" /> Update </label>&nbsp; 
										<label class='label'><input type='checkbox' name='cbakses_delete_$idMenus0' id='cbakses_delete_$idMenus0' value='1' ";if(substr($access0, 3, 1) == '1'){ echo "checked"; } echo " onclick=\"javascript: centang('$idMenus0');\" /> Hapus </label>&nbsp; 
										</div> ";
										/* Level 1 */
										$hqData1 = $db->sql("SELECT id_admin_menus, nama, link, icon, deskripsi FROM _admin_menus WHERE level = '1' AND id_admin_menus_parent = '$idMenus0' ORDER BY urutan ASC");
										$jumData1 = $db->num_rows($hqData1);
										if($jumData1 != 0){
											echo "<ul style='list-style-type:none;width:100%;'>";
											while(list($idMenus1, $nama1, $link1, $icon1, $deskripsi1) = $db->fetch_row($hqData1)){
												$hqMenu1 = $db->sql("SELECT access FROM _admin_menus_access WHERE id_admin_menus = '$idMenus1' AND id_admin_group = '".@$_REQUEST['id']."'");
												$cekmenu1 = $db->num_rows($hqMenu1);
												list($access1) = $db->fetch_row($hqMenu1);
												$db->close($hqMenu1);
												
												if($cekmenu1 == '0'){ $selected1 = ""; $akses1 = "display:none"; }else{ $selected1 = "checked"; $akses1 = "display:inline-block"; }
												$menus[] = $idMenus1;
												echo "<li><label style='display:inline-block;' title='$deskripsi1'><input type='checkbox' name='cbmenu_$idMenus1' id='cbmenu_$idMenus1' value='$idMenus1' $selected1 onclick=\"javascript: pilih_menu('$idMenus1'); \" />&nbsp;".stripslashes($icon1)." $nama1 </label>
												<div style='$akses1;width:auto;background-color:#eee;' id='divakses_$idMenus1'>
												<label class='label label-warning'><input type='checkbox' name='cbakses_all_$idMenus1' id='cbakses_all_$idMenus1' value='' "; if($access1 == '' || $access1 == '1111'){ echo "checked"; } echo " onclick=\"javascript: centang_all('$idMenus1'); \" /> Pilih Semua </label>&nbsp; 
										<label class='label'><input type='checkbox' name='cbakses_add_$idMenus1' id='cbakses_add_$idMenus1' value='1' "; if(substr($access1, 0, 1) == '1'){ echo "checked"; } echo " onclick=\"javascript: centang('$idMenus1');\" /> Tambah </label>&nbsp; 
										<label class='label'><input type='checkbox' name='cbakses_view_$idMenus1' id='cbakses_view_$idMenus1' value='1' ";if(substr($access1, 1, 1) == '1'){ echo "checked"; } echo " onclick=\"javascript: centang('$idMenus1');\" /> Lihat </label>&nbsp; 
										<label class='label'><input type='checkbox' name='cbakses_update_$idMenus1' id='cbakses_update_$idMenus1' value='1' ";if(substr($access1, 2, 1) == '1'){ echo "checked"; } echo " onclick=\"javascript: centang('$idMenus1');\" /> Update </label>&nbsp; 
										<label class='label'><input type='checkbox' name='cbakses_delete_$idMenus1' id='cbakses_delete_$idMenus1' value='1' ";if(substr($access1, 3, 1) == '1'){ echo "checked"; } echo " onclick=\"javascript: centang('$idMenus1');\" /> Hapus </label>&nbsp; 
												</div> ";
												/* Level 2 */									
												$hqData2 = $db->sql("SELECT id_admin_menus, nama, link, icon, deskripsi FROM _admin_menus WHERE level = '2' AND id_admin_menus_parent = '$idMenus1' ORDER BY urutan ASC");
												$jumData2 = $db->num_rows($hqData2);
												if($jumData2 != 0){
													echo "<ul style='list-style-type:none;width:100%;'>";
													while(list($idMenus2, $nama2, $link2, $icon2, $deskripsi2) = $db->fetch_row($hqData2)){
														$hqMenu2 = $db->sql("SELECT access FROM _admin_menus_access WHERE id_admin_menus = '$idMenus2' AND id_admin_group = '".@$_REQUEST['id']."'");
														$cekmenu2 = $db->num_rows($hqMenu2);
														list($access2) = $db->fetch_row($hqMenu2);
														$db->close($hqMenu2);

														if($cekmenu2 == '0'){ $selected2 = ""; $akses2 = "display:none"; }else{ $selected2 = "checked"; $akses2 = "display:inline-block"; }
														$menus[] = $idMenus2;
														echo "<li><label style='display:inline-block;' title='$deskripsi2'><input type='checkbox' name='cbmenu_$idMenus2' id='cbmenu_$idMenus2' value='$idMenus2' $selected2 onclick=\"javascript: pilih_menu('$idMenus2'); \" />&nbsp;".stripslashes($icon2)." $nama2 </label> <div style='$akses2;width:auto;background-color:#eee;' id='divakses_$idMenus2'>
												<label class='label label-warning'><input type='checkbox' name='cbakses_all_$idMenus2' id='cbakses_all_$idMenus2' value='' "; if($access2 == '' || $access2 == '1111'){ echo "checked"; } echo " onclick=\"javascript: centang_all('$idMenus2'); \" /> Pilih Semua </label>&nbsp; 
										<label class='label'><input type='checkbox' name='cbakses_add_$idMenus2' id='cbakses_add_$idMenus2' value='1' "; if(substr($access2, 0, 1) == '1'){ echo "checked"; } echo " onclick=\"javascript: centang('$idMenus2');\" /> Tambah </label>&nbsp; 
										<label class='label'><input type='checkbox' name='cbakses_view_$idMenus2' id='cbakses_view_$idMenus2' value='1' ";if(substr($access2, 1, 1) == '1'){ echo "checked"; } echo " onclick=\"javascript: centang('$idMenus2');\" /> Lihat </label>&nbsp; 
										<label class='label'><input type='checkbox' name='cbakses_update_$idMenus2' id='cbakses_update_$idMenus2' value='1' ";if(substr($access2, 2, 1) == '1'){ echo "checked"; } echo " onclick=\"javascript: centang('$idMenus2');\" /> Update </label>&nbsp; 
										<label class='label'><input type='checkbox' name='cbakses_delete_$idMenus2' id='cbakses_delete_$idMenus2' value='1' ";if(substr($access2, 3, 1) == '1'){ echo "checked"; } echo " onclick=\"javascript: centang('$idMenus2');\" /> Hapus </label>&nbsp; 
												</div> ";
														/* Level 3 */														
														$hqData3 = $db->sql("SELECT id_admin_menus, nama, link, icon, deskripsi FROM _admin_menus WHERE level = '3' AND id_admin_menus_parent = '$idMenus2' ORDER BY urutan ASC");
														$jumData3 = $db->num_rows($hqData3);
														if($jumData3 != 0){
															echo "<ul style='list-style-type:none;width:100%;'>";
															while(list($idMenus3, $nama3, $link3, $icon3, $deskripsi3) = $db->fetch_row($hqData3)){													
																$hqMenu3 = $db->sql("SELECT access FROM _admin_menus_access WHERE id_admin_menus = '$idMenus3' AND id_admin_group = '".@$_REQUEST['id']."'");
																$cekmenu3 = $db->num_rows($hqMenu3);
																list($access3) = $db->fetch_row($hqMenu3);
																$db->close($hqMenu3);
																
																if($cekmenu3 == '0'){ $selected3 = ""; $akses3 = "display:none"; }else{ $selected3 = "checked"; $akses3 = "display:inline-block"; }
																$menus[] = $idMenus3;
																echo "<li><label style='display:inline-block;' title='$deskripsi3'><input type='checkbox' name='cbmenu_$idMenus3' id='cbmenu_$idMenus3' value='$idMenus3' $selected3 onclick=\"javascript: pilih_menu('$idMenus3'); \" />&nbsp;".stripslashes($icon3)." $nama3 </label> <div style='$akses3;width:auto;background-color:#eee;' id='divakses_$idMenus3'>
												<label class='label label-warning'><input type='checkbox' name='cbakses_all_$idMenus3' id='cbakses_all_$idMenus3' value='' "; if($access3 == '' || $access3 == '1111'){ echo "checked"; } echo " onclick=\"javascript: centang_all('$idMenus3'); \" /> Pilih Semua </label>&nbsp; 
										<label class='label'><input type='checkbox' name='cbakses_add_$idMenus3' id='cbakses_add_$idMenus3' value='1' "; if(substr($access3, 0, 1) == '1'){ echo "checked"; } echo " onclick=\"javascript: centang('$idMenus3');\" /> Tambah </label>&nbsp; 
										<label class='label'><input type='checkbox' name='cbakses_view_$idMenus3' id='cbakses_view_$idMenus3' value='1' ";if(substr($access3, 1, 1) == '1'){ echo "checked"; } echo " onclick=\"javascript: centang('$idMenus3');\" /> Lihat </label>&nbsp; 
										<label class='label'><input type='checkbox' name='cbakses_update_$idMenus3' id='cbakses_update_$idMenus3' value='1' ";if(substr($access3, 2, 1) == '1'){ echo "checked"; } echo " onclick=\"javascript: centang('$idMenus3');\" /> Update </label>&nbsp; 
										<label class='label'><input type='checkbox' name='cbakses_delete_$idMenus3' id='cbakses_delete_$idMenus3' value='1' ";if(substr($access3, 3, 1) == '1'){ echo "checked"; } echo " onclick=\"javascript: centang('$idMenus3');\" /> Hapus </label>&nbsp; 
												</div> ";
																/* Level 4 */
																
																$hqData4 = $db->sql("SELECT id_admin_menus, nama, link, icon, deskripsi FROM _admin_menus WHERE level = '4' AND id_admin_menus_parent = '$idMenus3' ORDER BY urutan ASC");
																$jumData4 = $db->num_rows($hqData4);
																
																if($jumData4 != 0){
																	echo "<ul style='list-style-type:none;width:100%;'>";
																	while(list($idMenus4, $nama4, $link4, $icon4, $deskripsi4) = $db->fetch_row($hqData4)){															
																		$hqMenu4 = $db->sql("SELECT access FROM _admin_menus_access WHERE id_admin_menus = '$idMenus3' AND id_admin_group = '".@$_REQUEST['id']."'");
																		$cekmenu4 = $db->num_rows($hqMenu4);
																		list($access4) = $db->fetch_row($hqMenu4);
																		$db->close($hqMenu4);
																		
																		if($cekmenu4 == '0'){ $selected4 = ""; $akses4 = "display:none"; }else{ $selected4 = "checked"; $akses4 = "display:inline-block"; }
																		$menus[] = $idMenus4;
																		echo "<li><label style='display:inline-block;' title='$deskripsi4'><input type='checkbox' name='cbmenu_$idMenus4' id='cbmenu_$idMenus4' value='$idMenus4' $selected4 onclick=\"javascript: pilih_menu('$idMenus4'); \" />&nbsp;".stripslashes($icon4)." $nama4 </label> <div style='$akses4;width:auto;background-color:#eee;' id='divakses_$idMenus4'>
												<label class='label label-warning'><input type='checkbox' name='cbakses_all_$idMenus4' id='cbakses_all_$idMenus4' value='' "; if($access4 == '' || $access4 == '1111'){ echo "checked"; } echo " onclick=\"javascript: centang_all('$idMenus4'); \" /> Pilih Semua </label>&nbsp; 
										<label class='label'><input type='checkbox' name='cbakses_add_$idMenus4' id='cbakses_add_$idMenus4' value='1' "; if(substr($access4, 0, 1) == '1'){ echo "checked"; } echo " onclick=\"javascript: centang('$idMenus4');\" /> Tambah </label>&nbsp; 
										<label class='label'><input type='checkbox' name='cbakses_view_$idMenus4' id='cbakses_view_$idMenus4' value='1' ";if(substr($access4, 1, 1) == '1'){ echo "checked"; } echo " onclick=\"javascript: centang('$idMenus4');\" /> Lihat </label>&nbsp; 
										<label class='label'><input type='checkbox' name='cbakses_update_$idMenus4' id='cbakses_update_$idMenus4' value='1' ";if(substr($access4, 2, 1) == '1'){ echo "checked"; } echo " onclick=\"javascript: centang('$idMenus4');\" /> Update </label>&nbsp; 
										<label class='label'><input type='checkbox' name='cbakses_delete_$idMenus4' id='cbakses_delete_$idMenus4' value='1' ";if(substr($access4, 3, 1) == '1'){ echo "checked"; } echo " onclick=\"javascript: centang('$idMenus4');\" /> Hapus </label>&nbsp; 
												</div>";										
																		echo "</li>";
																	}
																	$db->close($hqData4);
																	echo "</ul>";
																}
																echo "</li>";
															}
															$db->close($hqData3);
															echo "</ul>";
														}
														echo "</li>";
													}
													$db->close($hqData2);
													echo "</ul>";
												}
												echo "</li>";
											}
											$db->close($hqData1);
											echo "</ul>";
										}
										echo "</li>";
									}
									$db->close($hqData0);
									?>
									</ul>
								</div>
							</div>
							<?php $x++; } $db->close($qMenu); ?>
						</div>
					</div>
				</div>
			</div>
			<script>
				$('#myTab a').click(function (e) {
					e.preventDefault();
					$(this).tab('show');
				})
			</script>
			
			<table class="hide">
				<tr><td colspan="3">&nbsp;</td></tr>
				<tr>
					<td colspan="3" align="left">
						<button id="save" class="btn btn-primary" onclick="javascript:
							var obj = document.form_admin;
							var err = '';
							if(obj.txtnama.value==''){ $('.has-nama').addClass('has-error').focus(); err+='<li>Nama harus di isi</li>'; }
							if(err==''){
								obj.action='content.php?module=admin&component=admin_group&action=process';
								obj.submit();
								
								//if(window.top.document.getElementById('cbadd').checked == false){
									window.top.document.getElementById('dismissAdmin').click();
								//}
							}else{ 
								$('#Modal').click(); $('#error-text').html(err);
							}
						" ></button>
					</td>
				</tr>
			</table>
			
			<div class="row">
				<div class="form-group col-xs-8 has-nama">
					<label style="display:none;">
					<input type="checkbox" name="cball" id="cball" onclick="javascript: 
					if(this.checked==true){ 
					<?php 
					if(isset($menus)){ 
						foreach($menus as $mnu){ 
							echo "document.getElementById('cbmenu_$mnu').click(); 
							document.getElementById('cbmenu_$mnu').checked=true; 
							document.getElementById('divakses_$mnu').style.display='inline-block';"; 
						}
					} 
					?> 
					}else{ 
					<?php 
					if(isset($menus)){ 
						foreach($menus as $mnu){ 
							echo "document.getElementById('cbmenu_$mnu').click(); 
							document.getElementById('cbmenu_$mnu').checked=false;
							document.getElementById('divakses_$mnu').style.display='none';"; 
						}
					} 
					?> 
					}
					" value="1" />&nbsp;Pilih&nbsp;Semua</label>
				</div>
			</div>
		</form>
		</div>
		
		<!-- Alert Validation Form -->
		<input type="hidden" id="Modal" onclick="javascript: $('#s_alert').fadeIn(); $('#s_alert').delay(3000); $('#s_alert').fadeOut(500);" />
		<div class="alert alert-danger" id="s_alert">
			<button type="button" class="close" onclick="javascript: $('#s_alert').fadeOut();">x</button>
			<strong>Warning : </strong> <div id="error-text" class="error-text"><ul></ul></div>
		</div>
		<!-- End Alert Validation Form -->
		
		<script src="includes/bootstrap/bootstrap.js"></script>
		<script type="text/javascript">
			$("[rel=tooltip]").tooltip();
			$(function() {
				$('.demo-cancel-click').click(function(){return false;});
			});
		</script>
		
	</body>
</html>