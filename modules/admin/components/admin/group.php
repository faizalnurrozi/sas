<?php
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();
?>
<html>
	<head>
		<script type="text/javascript" src="includes/ajax.js"></script>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="includes/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="includes/bootstrap/css/font-awesome.css">
		<link rel="stylesheet" href="includes/dist/css/ionicons.min.css">
		<link rel="stylesheet" href="includes/dist/css/AdminLTE.css">
		<link rel="stylesheet" href="includes/dist/css/skins/_all-skins.min.css">

		<script src="includes/plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script src="includes/bootstrap/js/bootstrap.min.js"></script>
		<script src="includes/dist/js/app.min.js"></script>
	</head>
	<body>
		<table class="table table-striped table-bordered">
			<tr>
				<th width="3%">No.</th>
				<th>Group</th>
				<th width="10%">&nbsp;</th>
			</tr>
			<?php
			$hqData = $db->sql("SELECT * FROM _admin_group ORDER BY id_admin_group ASC");
			$rowData = $db->num_rows($hqData);
			if($rowData == 0){
				echo "<tr><td colspan='3' align='center'><i>Data belum ada</i></td></tr>";
				$db->close($hqData);
			}else{
				$no=1;
				while(list($idGroup, $namaGroup) = $db->fetch_row($hqData)){
					echo "<tr class='table-list-row'>";
					echo "<td align='center'>$no.</td>";
					echo "<td>$namaGroup</td>";
					echo "
					<td align='center'>
						<button class='btn btn-sm btn-primary' onclick=\"window.top.document.getElementById('iframe_admin').contentDocument.getElementById('hdgroup').value='$idGroup'; window.top.document.getElementById('iframe_admin').contentDocument.getElementById('txtgroup').value='$namaGroup'; window.parent.document.getElementById('dismissButton').click();\"><i class='fa fa-plus'></i></button>
					</td>";
					echo "</tr>";
					$no++;
				}
				$db->close($hqData);
			}
			?>
		</table>
		<p>&nbsp;</p>
	</body>
</html>