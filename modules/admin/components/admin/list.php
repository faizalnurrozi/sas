<?php
if(@$_REQUEST['ajax']=='true'){
	include "globals/config.php";
	include "globals/functions.php";
	$db = new Database();
	$func = new Functions();
}
$limit = _LIMIT_;
?>

<!-- Alert Process -->
<?php if(isset($_SESSION[_APP_.'s_message_info'])){ ?>
<div class="alert alert-info">
	<button type="button" class="close" data-dismiss="alert">x</button>
	<strong>Status : </strong> <?php echo $_SESSION[_APP_.'s_message_info']; unset($_SESSION[_APP_.'s_message_info']); ?>
</div>
<?php } ?>
<?php if(isset($_SESSION[_APP_.'s_message_error'])){ ?>
<div class="alert alert-danger">
	<button type="button" class="close" data-dismiss="alert">x</button>
	<strong>Error : </strong> <?php echo $_SESSION[_APP_.'s_message_error']; unset($_SESSION[_APP_.'s_message_error']); ?>
</div>
<?php } ?>
<!-- Alert Process -->

<?php
/*Sorting*/
$iconsort = "";
if(isset($_POST['sort']) && @$_POST['sort']=='reset'){
	$_SESSION[_APP_.'s_field_admin'] = "A.usernames";
	$_SESSION[_APP_.'s_sort_admin'] = "ASC";
	$iconsort = "<i class='fa fa-fw fa-caret-up'></i>";
}
switch(@$_POST['field']){
	case 'A.usernames' : $_SESSION[_APP_.'s_field_admin'] = "A.usernames"; break;
	case 'A.nama' : $_SESSION[_APP_.'s_field_admin'] = "A.nama"; break;
	case 'B.nama' : $_SESSION[_APP_.'s_field_admin'] = "B.nama"; break;
	case 'A.tipe' : $_SESSION[_APP_.'s_field_admin'] = "A.tipe"; break;
	default : 
		if(!isset($_SESSION[_APP_.'s_field_admin'])){
			$_SESSION[_APP_.'s_field_admin'] = "a.usernames";
		}
		break;
}
if(!isset($_SESSION[_APP_.'s_sort_admin'])){
	$_SESSION[_APP_.'s_sort_admin'] = "ASC";
	$iconsort = "<i class='fa fa-fw fa-caret-up'></i>";
}else{
	switch(@$_POST['act']){
		case 'sort' :
			if($_SESSION[_APP_.'s_sort_admin'] == "ASC"){
				$_SESSION[_APP_.'s_sort_admin'] = "DESC";
				$iconsort = "<i class='fa fa-fw fa-caret-down'></i>";
			}else if($_SESSION[_APP_.'s_sort_admin'] == "DESC"){
				$_SESSION[_APP_.'s_sort_admin'] = "ASC";
				$iconsort = "<i class='fa fa-fw fa-caret-up'></i>";
			}
			break;
		case 'paging' :
			if($_SESSION[_APP_.'s_sort_admin'] == "ASC"){
				$iconsort = "<i class='fa fa-fw fa-caret-up'></i>";
			}else if($_SESSION[_APP_.'s_sort_admin'] == "DESC"){
				$iconsort = "<i class='fa fa-fw fa-caret-down'></i>";
			}
			break;
	}
}
/*End Sorting*/

if(@$_REQUEST['start']=='') $start = 0; else $start = $_REQUEST['start'];

if(!isset($msc)) $msc = microtime(true);
@$keyword = @$_REQUEST['keyword'];
$qSQL 	= "
SELECT A.usernames, A.nama AS users, B.nama AS groups
FROM _admin AS A 
	LEFT JOIN _admin_group AS B ON (A.id_admin_group=B.id_admin_group) 
WHERE (A.usernames LIKE :keyword OR A.nama LIKE :keyword) 
ORDER BY ".$_SESSION[_APP_.'s_field_admin']." ".$_SESSION[_APP_.'s_sort_admin'];
$hqSQL 	= $db->query($qSQL);
$db->bind($hqSQL, ":keyword", "%".$keyword."%", "str");
$db->exec($hqSQL);
$totalData = $db->num_rows($hqSQL);
$qSQL	.= " LIMIT $start, $limit";
$hqSQL = $db->query($qSQL);
$db->bind($hqSQL, ":keyword", "%".$keyword."%", "str");
$db->exec($hqSQL);
$totalLimit = $db->num_rows($hqSQL);
?>

<div class="box-body table-responsive">
	<table class="table table-bordered table-hover table-striped">
		<thead>
			<tr>
				<th width="1%">&nbsp;</th>
				<th width="20%" class="sort" onclick="javascript: sendRequest('content.php', 'module=admin&component=admin&action=list&ajax=true&act=sort&field=A.usernames&keyword=<?php echo $keyword; ?>', 'list', 'div');">Username&nbsp;<?php if($_SESSION[_APP_.'s_field_admin'] == 'A.usernames') echo $iconsort; ?></th>
				<th class="sort" onclick="javascript: sendRequest('content.php', 'module=admin&component=admin&action=list&ajax=true&act=sort&field=A.nama&keyword=<?php echo $keyword; ?>', 'list', 'div');">Nama&nbsp;<?php if($_SESSION[_APP_.'s_field_admin'] == 'A.nama') echo $iconsort; ?></th>
				<th class="sort" onclick="javascript: sendRequest('content.php', 'module=admin&component=admin&action=list&ajax=true&act=sort&field=B.nama&keyword=<?php echo $keyword; ?>', 'list', 'div');">Group&nbsp;<?php if($_SESSION[_APP_.'s_field_admin'] == 'B.nama') echo $iconsort; ?></th>
				<th width="140">&nbsp;</th>
			</tr>
		</thead>
		
		<tbody>
			<?php
			if($totalData=='0'){
				echo "<tr><td colspan='6' align='center'>Data belum ada</td></tr>";
				$db->close($hqSQL);
			}else{
				$no = $start+1;
				while($hasil = $db->fetch_assoc($hqSQL)){
					echo "<tr class='table-list-row'>";
					if($hasil['passwords'] == $func->encrypt_md5($hasil['usernames'])) $status = "<i class='fa fa-warning' style='color:yellow;'></i>";
					else $status = "<i class='fa fa-check' style='color:green;'></i>";
					echo "<td align='center'>$status</td>";
					echo "<td align='left'>".$func->highlight($hasil['usernames'], $keyword)."</td>";			
					echo "<td align='left'>".$func->highlight($hasil['users'], $keyword)."</td>";
					echo "<td align='left'>".$func->highlight($hasil['groups'], $keyword)."</td>";
					
					echo "<td align='center' valign='top'>";
					echo "<div class='btn-group btn-group-xs' role='group' aria-label='...'>";
					
					/* -- Akses View */
					if($func->akses('R') == true){
						echo "<button type='button' class='btn btn-info' data-toggle='modal' data-target='#modal-view' onclick=\"javascript:  document.getElementById('iframe_view').src='content.php?module=admin&component=admin&action=view&id=$hasil[usernames]'; \" onmouseover=\"$(this).tooltip();\" title='View'><i class='fa fa-search'></i></button>";
					}else{
						echo "<button type='button' title='View' class='btn btn-default' disabled ><i class='fa fa-search'></i></button>";
					}
					/* -- End Akses View -- */
					
					/* Akses menu Edit */
					if($func->akses('U') == true){
						echo "<button type='button' class='btn btn-success' data-toggle='modal' data-target='#myModal' onclick=\"javascript: document.getElementById('iframe_admin').src='content.php?module=admin&component=admin&action=add&id=$hasil[usernames]'; \" onmouseover=\"$(this).tooltip();\" title='Edit'><i class='fa fa-edit'></i></button>";
					}else{
						echo "<button type='button' title='Edit' class='btn btn-default' disabled ><i class='fa fa-edit'></i></button>";
					}
					/* End Akses menu edit */
					
					/* Akses menu Delete */
					if($func->akses('D') == true){
						echo "<button type='button' class='btn btn-danger' data-toggle='modal' data-target='#modal-delete' onclick=\"javascript:  document.getElementById('idx').value='$hasil[usernames]'; \" onmouseover=\"$(this).tooltip();\" title='Delete'><i class='fa fa-trash'></i></button>";
						echo "<button type='button' class='btn btn-warning' data-toggle='modal' data-target='#modal-reset' onclick=\"javascript:  document.getElementById('idx').value='$hasil[usernames]'; \" onmouseover=\"$(this).tooltip();\" title='Reset Password'><i class='fa fa-refresh'></i> Reset</button>";
					}else{
						echo "<button type='button' title='Delete' class='btn btn-default' disabled ><i class='fa fa-trash'></i></button>";
						echo "<button type='button' title='Reset Password' class='btn btn-default' disabled ><i class='fa fa-refresh'></i></button>";
					}
					/* End Akses menu delete */
					
					echo "</div>
							</td>";
					echo "</tr>";
					
					$no++;
				}
				$db->close($hqSQL);
			}
			?>
		</tbody>
	</table>
</div>
<div class="box-body">
	<div class="row">
		<div class="col-sm-5">
			<i>
				<?php 
				echo "Ditampilkan <b>".($totalLimit)."</b> sampai <b>".($start+$totalLimit)."</b> dari <b>$totalData</b> total data"; 
				?>
			</i>
		</div>
		<div class="col-sm-7">
			<ul class="pagination pagination-sm pull-right">
			<?php
			if($start != 0) echo "<li><a href='#' onclick=\"sendRequest('content.php','module=admin&component=admin&action=list&ajax=true&start=".($start-$limit)."&keyword=$keyword', 'list', 'div');\">Prev</a></li>";
			$jumlahPage = $totalData/$limit;
			for($a=0;$a<$jumlahPage;$a++){
				$x = $a * $limit;
				if($start==$a*$limit){
					echo "<li class='active'><a href='#'>".($a+1)."</a></li>";
				}else{
					echo "<li><a href='#' onclick=\"sendRequest('content.php','module=admin&component=admin&action=list&ajax=true&start=".($a*$limit)."&keyword=$keyword', 'list', 'div');\">".($a+1)."</a></li>";
				}
			}
			 if($start != @$x) echo "<li><a href='#' onclick=\"sendRequest('content.php','module=admin&component=admin&action=list&ajax=true&start=".($start+$limit)."&keyword=$keyword', 'list', 'div');\">Next</a></li>";
			?>
			</ul>
		</div>
	</div>
</div>


<!-- Modals -->
<div class="modal fade" id="modal-view" data-backdrop="static">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">View</h4>
			</div>
			<div class="modal-body">
				<iframe name="iframe_view" id="iframe_view" width="98%" height="300" frameborder="0" onload="javascript: autoResize('iframe_view')"></iframe>
			</div>
			<div class="modal-footer">
				<button class="btn btn-warning" data-dismiss="modal" aria-hidden="true" id="dismissView"><i class="fa fa-remove"></i> Tutup</button>
			</div>
		</div>
	</div>
</div>

<input type="hidden" id="idx" value="" />
<div class="modal fade" id="modal-delete" data-backdrop="static">
	<div class="modal-dialog modal-xs">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Hapus</h4>
			</div>
			<div class="modal-body">
				Apakah anda yakin ingin menghapus ?
			</div>
			<div class="modal-footer">
				<button class="btn btn-default" data-dismiss="modal" aria-hidden="true" id="dismissDelete"><i class="fa fa-repeat"></i> Batal</button>
				<button class="btn btn-danger" data-dismiss="modal" aria-hidden="true" onclick="javascript: sendRequest('content.php', 'module=admin&component=admin&action=process&proc=delete&start=<?php echo $start; ?>&keyword=<?php echo $keyword; ?>&id='+document.getElementById('idx').value, 'list', 'div');"><i class="fa fa-remove"></i> Hapus</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-reset" data-backdrop="static">
	<div class="modal-dialog modal-xs">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Reset Password.</h4>
			</div>
			<div class="modal-body">
				Apakah anda ingin me-reset password menjadi password default [USERNAME] ?
			</div>
			<div class="modal-footer">
				<button class="btn btn-success" data-dismiss="modal" aria-hidden="true" onclick="javascript: sendRequest('content.php', 'module=admin&component=admin&action=process&proc=reset&start=<?php echo $start; ?>&keyword=<?php echo $keyword; ?>&id='+document.getElementById('idx').value, 'list', 'div');"><i class="fa fa-refresh"></i> Reset Password</button>
				<button class="btn btn-default" data-dismiss="modal" aria-hidden="true" id="dismissReset"><i class="fa fa-remove"></i> Batal</button>
			</div>
		</div>
	</div>
</div>
<!-- End of Modals -->