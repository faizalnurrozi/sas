<?php 
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();

switch(@$_REQUEST['proc']){
	case 'add' :
		# ---------------------#
		# Penangkapan variable #
		# ---------------------#
		
		$user 	= @$_POST['txtuser'];
		$pass	= $func->encrypt_md5(@$_POST['txtpass']);
		$nama	= @$_POST['txtnama'];
		$jk		= @$_POST['rbjk'];
		$group	= @$_POST['slgroup'];
		
		$hqData = $db->insert("_admin", 
			array(
				'usernames' 		=> $user, 
				'nama' 				=> $nama,
				'jenis_kelamin'		=> $jk,
				'id_admin_group' 	=> $group,
				'passwords' 		=> $pass
			)
		);
		$db->close($hqData);

		if($hqData==true) $_SESSION[_APP_.'s_message_info'] = "Tambah data berhasil";
		else $_SESSION[_APP_.'s_message_error'] = "Tambah data gagal";
		
		#---------- * ----------#
		
		
		# ----------------#
		# Pencatatan Logs #
		# ----------------#
		
		/* ----- Activity Logs (Insert) ------ */
		$func->activity_logs_insert("_admin", $id, @$_SESSION[_APP_.'s_userAdmin']);
		/* ----- End Activity Logs (Insert) ------ */
		
		#---------- * ----------#
		
		
		# ------------#
		# Kirim balik #
		# ------------#
		
		echo "<script>window.parent.sendRequest('content.php', 'module=admin&component=admin&action=list&ajax=true', 'list', 'div');</script>";
		echo "<meta http-equiv='refresh' content='0;url=content.php?module=admin&component=admin&action=add' />";
		
		#---------- * ----------#
		
		break;
		
	case 'update' :
		# ---------------------#
		# Penangkapan variable #
		# ---------------------#
		
		$id 	= @$_POST['txtid'];
		$user 	= @$_POST['txtuser'];
		$pass	= $func->encrypt_md5(@$_POST['txtpass']);
		$nama	= @$_POST['txtnama'];
		$jk		= @$_POST['rbjk'];
		$group	= @$_POST['slgroup'];
		$cek	= @$_POST['cbcek'];
		
		# --------------#
		# Proses Update #
		# --------------#
		
		$data_update = array(
				'usernames' => $user, 
				'nama' => $nama,
				'jenis_kelamin'	=> $jk,
				'id_admin_group' => $group
			);
		
		if($cek == '1'){
			$data_update['passwords'] = $pass;
		}

		$hqData = $db->update("_admin", $data_update, array('usernames' => $id));
		if($hqData==true) $_SESSION[_APP_.'s_message_info'] = "Update data berhasil";
		else $_SESSION[_APP_.'s_message_error'] = "Update data gagal";
		$db->close($hqData);
		
		#---------- * ----------#
		
		# ----------------#
		# Pencatatan Logs #
		# ----------------#
		
		/* ----- Activity Logs (Update) ------ */
		$func->activity_logs_update("_admin", $data_update, array('usernames' => $id), 'OR', @$_SESSION[_APP_.'s_userAdmin']);
		/* ----- End Activity Logs (Update) ------ */
						
		
		# ------------#
		# Kirim balik #
		# ------------#
		
		echo "<script>window.parent.sendRequest('content.php', 'module=admin&component=admin&action=list&ajax=true', 'list', 'div');</script>";
		echo "<meta http-equiv='refresh' content='0;url=content.php?module=admin&component=admin&action=add' />";
		
		#---------- * ----------#
		break;
		
	case 'delete' :
		# ---------------------#
		# Penangkapan variable #
		# ---------------------#		
		$idx = @$_REQUEST['id'];		
		#---------- * ----------#		
		
		# --------------#
		# Proses Delete #
		# --------------#		
		$hqData = $db->delete("_admin", array('usernames' => $idx));
		
		if($hqData==true) $_SESSION[_APP_.'s_message_info'] = "Hapus data berhasil";
		else $_SESSION[_APP_.'s_message_error'] = "Hapus data gagal";	
		$db->close($hqData);
		#---------- * ----------#
				
		/* ----- Activity Logs (Delete) ------ */
		$func->activity_logs_delete("_admin", $idx, $_SESSION[_APP_.'s_userAdmin']);
		/* ----- End Activity Logs (Delete) ------ */
				
		# ------------#
		# Kirim balik #
		# ------------#		
		header("location: content.php?module=admin&component=admin&action=list&ajax=true");		
		#---------- * ----------#
		break;
	
	case 'reset' :
		# ---------------------#
		# Penangkapan variable #
		# ---------------------#		
		$idx = @$_REQUEST['id'];		
		#---------- * ----------#		
		
		# --------------#
		# Proses Reset #
		# --------------#		
		$hqData = $db->update("_admin", array('passwords' => $func->encrypt_md5($idx)), array('usernames' => $idx));
		
		if($hqData==true) $_SESSION[_APP_.'s_message_info'] = "Reset password untuk <b>$idx</b> berhasil";
		else $_SESSION[_APP_.'s_message_error'] = "Reset password gagal";
		$db->close($hqData);		
		#---------- * ----------#
				
		# ------------#
		# Kirim balik #
		# ------------#		
		header("location: content.php?module=admin&component=admin&action=list&ajax=true");		
		#---------- * ----------#
		break;
		
	case 'cek_user' :
		$user = @$_REQUEST['user'];
		list($cUser) = $db->result_row("SELECT COUNT(*) FROM _admin WHERE usernames = '$user' ");
		if($cUser == '0'){ 
			echo "<i class='fa fa-check'></i>";
			echo "<script>document.getElementById('tombol').disabled=false;</script>";
		}else{
			echo "<i class='fa fa-remove'></i>";
			echo "<script>document.getElementById('tombol').disable=true;</script>";
		}
	break;
}
?>