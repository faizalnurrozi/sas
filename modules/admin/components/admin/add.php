<?php
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();
?>
<html>
	<head>
		<script type="text/javascript" src="includes/ajax.js"></script>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="includes/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="includes/bootstrap/css/font-awesome.css">
		<link rel="stylesheet" href="includes/dist/css/ionicons.min.css">
		<link rel="stylesheet" href="includes/dist/css/AdminLTE.css">
		<link rel="stylesheet" href="includes/dist/css/skins/_all-skins.min.css">
		
		<script src="includes/plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script src="includes/bootstrap/js/bootstrap.min.js"></script>
		<script src="includes/dist/js/app.min.js"></script>
		
		<!-- Input-mask -->
		<script src="includes/plugins/select2/select2.full.min.js"></script>
		<script src="includes/plugins/input-mask/jquery.inputmask.js"></script>
		<script src="includes/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
		<script src="includes/plugins/input-mask/jquery.inputmask.extensions.js"></script>
		<!-- End of Input-mask -->
		
		<!-- Typeahead.js -->
		<link rel="stylesheet" href="includes/bootstrap/css/typeahead.css">
		<script src="includes/bootstrap/js/bootstrap-typeahead.js"></script>
		<!-- End of Typeahead.js -->
		
		<style type="text/css">
		.note{
			font-style: italic; color: red;
		}
		</style>
		
		<script language="JavaScript">
		$(function () {
			$("[data-mask]").inputmask();
		});
		</script>	
	</head>
	<body>
		<!-- Alert Process -->
		<?php if(isset($_SESSION[_APP_.'s_message_info'])){ ?>
		<div class="alert alert-info">
			<button type="button" class="close" data-dismiss="alert">x</button>
			<strong>Status : </strong> <?php echo $_SESSION[_APP_.'s_message_info']; unset($_SESSION[_APP_.'s_message_info']); ?>
		</div>
		<?php } ?>
		<?php if(isset($_SESSION[_APP_.'s_message_error'])){ ?>
		<div class="alert alert-danger">
			<button type="button" class="close" data-dismiss="alert">x</button>
			<strong>Error : </strong> <?php echo $_SESSION[_APP_.'s_message_error']; unset($_SESSION[_APP_.'s_message_error']); ?>
		</div>
		<?php } ?>
		<!-- Alert Process -->
		
		<?php
		$id = @$_REQUEST['id'];
		if(@$_REQUEST['id'] != ''){
			$qEdit = "SELECT * FROM _admin WHERE usernames = '$id'";
			$dataEdit = $db->sql($qEdit);
			$resultEdit = $db->fetch_assoc($dataEdit);
			$db->close($dataEdit);
		}
		?>
		
		<div class="container-fluid">
		<form name="form_admin" method="POST" action="javascript: void(null);" enctype="multipart/form-data">
			<?php if(@$_REQUEST['id']==""){ ?>
			<input type="hidden" id="proc" name="proc" value="add" />
			<?php }else{ ?>
			<input type="hidden" id="proc" name="proc" value="update" />
			<input type="hidden" id="txtid" name="txtid" value="<?php echo @$resultEdit['usernames']; ?>" />
			<?php } ?>
			<div class="row">
				<div class="form-group col-xs-6 has-user">
					<label>Usernames</label>
					<input type="text" name="txtuser" id="txtuser" value="<?php echo @$resultEdit['usernames']; ?>" autocomplete="off" class="form-control input-sm" onblur="javascript: if(this.value==''){ document.getElementById('cek_user').innerHTML=''; }else{ ajaxCustom('content.php', 'module=admin&component=admin&action=process&proc=cek_user&user='+this.value, 'cek_user'); }" onclick="javascript: this.select();" placeholder="Isikan Username..." autocomplete="off" />
					<span id="cek_user"></span>
				</div>
				<?php
				if(@$_REQUEST['id']==""){
				?>
				<div class="form-group col-xs-6 has-pass">
					<label>Passwords</label>
					<input type="password" name="txtpass" id="txtpass" autocomplete="off" class="form-control input-sm" value="" onclick="javascript: this.select();" placeholder="Isikan Password..." autocomplete="off" />
				</div>
				<?php }else{ ?>
				<div class="form-group col-xs-6 has-pass">
					<label><input type="checkbox" name="cbcek" id="cbcek" value="1" onclick="javascript: if(this.checked==true){ document.getElementById('txtpass').readOnly=false; document.getElementById('txtpass').select(); }else{ document.getElementById('txtpass').readOnly=true; }" />&nbsp;Ganti Password.</label>
					<input type="password" name="txtpass" id="txtpass" autocomplete="off" class="form-control input-sm" value="<?php echo @$resultEdit['usernames']; ?>" onclick="javascript: this.select();" title="Password standart adalah NIP." placeholder="Isikan Password..." readonly="true" />
				</div>
				<?php } ?>
			</div>
			<div class="row">
				<div class="form-group col-xs-12 has-nama">
					<label>Nama</label>
					<input type="text" name="txtnama" id="txtnama" value="<?php echo @$resultEdit['nama']; ?>" autocomplete="off" class="form-control input-sm" placeholder="Isikan Nama..." autocomplete="off" />
				</div>
			</div>
			<div class="row">
				<div class="form-group col-xs-6 has-jk">
					<label>Jenis Kelamin</label>
					<div class="checkbox-parent">
						<div class="checkbox-item">
							<label class="checkbox-label">
								<input type="radio" name="rbjk" value="L" checked />
								<font>Laki-laki</font>
							</label>
						</div>
						<div class="checkbox-item">
							<label class="checkbox-label">
								<input type="radio" name="rbjk" value="P" <?php if(@$resultEdit['jenis_kelamin']=='P') echo "checked"; ?> />
								<font>Perempuan</font>
							</label>
						</div>
					</div>
				</div>
				<div class="form-group col-xs-6 has-group">
					<label>Group Menu</label>
					<select class="form-control input-sm" name="slgroup" id="slgroup">
						<option value="">-Pilih Group-</option>
						<?php
						$qData = $db->sql("SELECT * FROM _admin_group ORDER BY nama ASC");
						while($result = $db->fetch_assoc($qData)){
							echo "<option value='$result[id_admin_group]' "; if(@$resultEdit['id_admin_group']==$result['id_admin_group']) echo "selected"; echo ">$result[nama]</option>";
						}
						$db->close($qData);
						?>
					</select>
				</div>
			</div>

			<div class="row">&nbsp;</div>
			<table class="hide">		
				<tr>
					<td colspan="3" align="right">
						<button id="save" style="display:none;" class="btn btn-primary" onClick="javascript:
							var obj = document.form_admin;
							var err = '';
							if(obj.txtuser.value==''){ $('#txtuser').removeClass('input-xlarge').addClass('input-error').focus(); err+='<li>User harus di isi</li>'; }
							if(obj.txtpass.value==''){ $('#txtpass').removeClass('input-xlarge').addClass('input-error').focus(); err+='<li>Passwords harus di isi</li>'; }
							if(obj.slgroup.value==''){ $('#slgroup').removeClass('input-xlarge').addClass('input-error').focus(); err+='<li>Group harus di pilih</li>'; }
							<?php if(@$_REQUEST['id']==''){ ?>
							if(obj.txtnama.value==''){ $('#txtnama').removeClass('input-xlarge').addClass('input-error').focus(); err+='<li>Nama harus di isi</li>'; }
							<?php } ?>
							if(err==''){
								obj.action='content.php?module=admin&component=admin&action=process';
								obj.submit();
								if(window.top.document.getElementById('cbadd').checked == false){
									window.top.document.getElementById('dismiss').click();
								}
							}else {
								$('#Modal').click(); $('#error-text').html(err);
							}
						">&nbsp;</button>
					</td>
				</tr>
			</table>
		</form>
		</div>
		
		<!-- Alert Validation Form -->
		<input type="hidden" id="Modal" onclick="javascript: $('#s_alert').fadeIn();" />
		<div class="alert alert-danger" id="s_alert" style="display:none;">
			<button type="button" class="close" onclick="javascript: $('#s_alert').fadeOut();">x</button>
			<strong>Warning : </strong> <div id="error-text" class="error-text"><ul></ul></div>
		</div>
		<!-- End Alert Validation Form -->
		
		<div class="modal fade" id="myModalx" data-backdrop="static">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Group&nbsp;Admin</h4>
					</div>
					<div class="modal-body">
						<iframe name="iframe_group" id="iframe_group" width="98%" height="80" frameborder="0" onload="javascript: autoResize('iframe_group')"></iframe>
					</div>
					<div class="modal-footer">
						<button class="btn btn-danger btn-sm" data-dismiss="modal" aria-hidden="true" id="dismissButton"><i class="fa fa-remove"></i> Batal</button>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>