<?php
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();

$id = @$_REQUEST['id'];

$h = 40;
?>
<html>
	<head>
		<title>Admin</title>		
		<script type="text/javascript" src="includes/ajax.js"></script>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="includes/bootstrap/css/bootstrap.min.css"></link>
		<link rel="stylesheet" href="includes/bootstrap/css/font-awesome.css"></link>
		<link rel="stylesheet" href="includes/dist/css/ionicons.min.css"></link>
		<link rel="stylesheet" href="includes/dist/css/AdminLTE.css"></link>
		<link rel="stylesheet" href="includes/dist/css/skins/_all-skins.min.css"></link>
		<script src="includes/plugins/jQuery/jQuery-2.1.4.min.js"></script>
		
		<style type="text/css">
			body{ overflow-x:hidden; overflow-y:scroll;	}
			.input-xlarge{ height:30px !important; }
			label{ display:inline-block; font-size:14px; font-weight:bold; }
			.label{ display:inline-block; font-size:12px; font-weight:bold; color:green !important; font-style:italic; }
		</style>
	</head>
	<body>
		<div class="container-fluid">
		<?php
		$result = $db->result_assoc("SELECT * FROM _admin WHERE usernames = '$id' ");
		?>
		<table class="table">
			<tr>
				<td width="20%">Usernames&nbsp;</td>
				<td width="1%">&nbsp;:&nbsp;</td>
				<td><?php echo $result['usernames']; ?></td>
			</tr>
			<tr>
				<td>Nama&nbsp;</td>
				<td>&nbsp;:&nbsp;</td>
				<td><?php echo $result['nama']; ?></td>
			</tr>
			<tr>
				<td>Gender&nbsp;</td>
				<td>&nbsp;:&nbsp;</td>
				<td><?php echo $result['jenis_kelamin']; ?></td>
			</tr>
			<tr>
				<td>Email&nbsp;</td>
				<td>&nbsp;:&nbsp;</td>
				<td><?php echo $result['email']; ?></td>
			</tr>
		</table>
		</div>
	</body>
</html>