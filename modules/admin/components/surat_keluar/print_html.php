<?php
	include "globals/config.php";
	include "globals/functions.php";
	$db = new Database();
	$func = new Functions();

	$id = @$_GET['id'];

	$result = $db->result_assoc(" SELECT A.*, B.nama AS jenis_surat, C.nama AS indeks FROM _surat_keluar AS A INNER JOIN _jenis_surat AS B ON(A.id_jenis_surat = B.id_jenis_surat) INNER JOIN _indeks AS C ON(A.id_indeks = C.id_indeks) WHERE id_surat_keluar = '$id' ");

	list($status_umum) = $db->result_row("SELECT status_umum FROM _jenis_surat WHERE id_jenis_surat = '$result[id_jenis_surat]'");

	$namaFile = str_replace('/', '-', $result['id_surat_keluar']);
?>

<html>
	<head>
		<title><?php echo $namaFile; ?></title>
		<style type="text/css">
			#content-print{
				position: relative;
				width: 90%;
				margin: 150px auto;
			}

			.atas-umum{
				margin: 0 auto;
			}

			.atas-umum tr td{
				padding: 1mm;
				font-size: 16px;
			}
		</style>
	</head>
	<body>
		<div id="content-print">
			<?php
				if($status_umum == 'FALSE'){
			?>

			<table class="atas-kanan" cellspacing="0" cellspacing="0" style="width: 100%;">
				<tr>
					<td style="text-align: center; font-size: 25px; font-weight: bold; text-decoration: underline;"><?php echo strtoupper($result['jenis_surat']); ?></td>
				</tr>
				<tr>
					<td style="text-align: center; font-size: 18px;">Nomor : <?php echo $result['id_surat_keluar']; ?></td>
				</tr>
			</table>

			<?php }else{ ?>

			<table class="atas-umum" cellspacing="0" cellspacing="0" border="0" width="100%;">
				<tr>
					<td valign="top" width="40%">
						<table cellspacing="0" cellspacing="0" border="0">
							<tr>
								<td>Nomor</td>
								<td>:</td>
								<td><?php echo $result['id_surat_keluar']; ?></td>
							</tr>
							<tr>
								<td>Lamp</td>
								<td>:</td>
								<td>-</td>
							</tr>
							<tr>
								<td>Hal</td>
								<td>:</td>
								<td><b><?php echo $result['perihal']; ?></b></td>
							</tr>
						</table>
					</td>
					<td width="20%"></td>
					<td width="30%" valign="top">
						<table cellspacing="0" cellspacing="0" border="0">
							<tr>
								<td>Surabaya,</td>
								<td>:</td>
								<td><?php echo $func->report_date_hijriah($result['tanggal']); ?></td>
							</tr>
							<tr>
								<td colspan="2"></td>
								<td><?php echo $func->report_date($result['tanggal']); ?></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<?php } ?>

			<br>

			<table width="100%;">
				<tr>
					<td><?php echo $result['keterangan']; ?></td>
				</tr>
			</table>
		</div>
	</body>
</html>
