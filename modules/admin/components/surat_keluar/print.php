<?php
	ob_start();
	header('Content-type: text/html;charset=utf-8');
	include "globals/config.php";
	include "globals/functions.php";
	$db = new Database();
	$func = new Functions();

	$id = @$_GET['id'];
?>

<style type="text/css">
	.kop-surat tr td{
		border-bottom: 0.5px solid #000;
		padding: 2mm;
	}

	h4{
		font-size: 13px;
		margin: 0;
		padding: 0;
	}

	h2{
		font-size: 17px;
		margin: 0;
		padding: 0;
	}

	p{
		font-size: 12px;
		margin: 0;
		padding: 0;
	}
</style>

<?php
	$result = $db->result_assoc(" SELECT A.*, B.nama AS jenis_surat, C.nama AS indeks FROM _surat_keluar AS A INNER JOIN _jenis_surat AS B ON(A.id_jenis_surat = B.id_jenis_surat) INNER JOIN _indeks AS C ON(A.id_indeks = C.id_indeks) WHERE id_surat_keluar = '$id' ");

	list($status_umum) = $db->result_row("SELECT status_umum FROM _jenis_surat WHERE id_jenis_surat = '$result[id_jenis_surat]'");

	$namaFile = str_replace('/', '-', $result['id_surat_keluar'])
?>

<page backcolor="#FEFEFE" backtop="25mm" backbottom="10mm" backleft="10mm" backright="10mm" style="font-family: times;">
	<style type="text/css">
		.atas-kanan tr td{
			padding: 1mm;
			font-size: 14px;
		}

		.atas-umum tr td{
			padding: 1mm;
			font-size: 16px;
		}

		.p-isi{
			padding-top: 5mm;
			font-size: 14px;
		}
	</style>

	<?php
		if($status_umum == 'FALSE'){
	?>

	<table class="atas-kanan" cellspacing="0" cellspacing="0">
		<tr>
			<td style="width: 180mm; text-align: center; font-size: 25px; font-weight: bold; text-decoration: underline;"><?php echo strtoupper($result['jenis_surat']); ?></td>
		</tr>
		<tr>
			<td style="width: 180mm; text-align: center; font-size: 18px;">Nomor : <?php echo $result['id_surat_keluar']; ?></td>
		</tr>
	</table>

	<?php }else{ ?>

	<table class="atas-umum" cellspacing="0" cellspacing="0" border="0">
		<tr>
			<td valign="top">
				<table cellspacing="0" cellspacing="0" border="0">
					<tr>
						<td>Nomor</td>
						<td>:</td>
						<td><?php echo $result['id_surat_keluar']; ?></td>
					</tr>
					<tr>
						<td>Lamp</td>
						<td>:</td>
						<td>-</td>
					</tr>
					<tr>
						<td width="20mm;">Hal</td>
						<td>:</td>
						<td><b><?php echo $result['perihal']; ?></b></td>
					</tr>
				</table>
			</td>
			<td width="50mm;"></td>
			<td valign="top">
				<table cellspacing="0" cellspacing="0" border="0">
					<tr>
						<td>Surabaya,</td>
						<td>:</td>
						<td><?php echo $func->report_date_hijriah($result['tanggal']); ?></td>
					</tr>
					<tr>
						<td colspan="2"></td>
						<td><?php echo $func->report_date($result['tanggal']); ?></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>


	<?php } ?>

	<br>

	<?php echo $result['keterangan']; ?>

	<style type="text/css">
		.footer-ttd{
			margin-top: 15mm;
			font-size: 15px;
		}
	</style>

	<?php $result_setting = $db->result_assoc("SELECT instansi_kepsek, instansi_kepsek_nip, instansi_kepsek_ttd FROM _setting"); ?>

	<table cellspacing="0" cellspacing="0" border="0" class="footer-ttd">
		<?php
			if($status_umum == 'FALSE'){
		?>
		<tr>
			<td style="width: 120mm;"></td>
			<td style="width: 60mm;">Surabaya, <?php echo $func->report_date_hijriah(date("Y-m-d")); ?> H</td>
		</tr>
		<tr>
			<td style="width: 120mm;"></td>
			<td style="width: 60mm;"><span style="color: #FFF;">Surabaya, </span><?php echo $func->report_date(date("Y-m-d")); ?> M</td>
		</tr>
		<?php } ?>
		<tr>
			<td style="width: 120mm;"></td>
			<td style="width: 60mm;"><b>Kepala Sekolah</b></td>
		</tr>
		<tr>
			<td style="width: 120mm;"></td>
			<td style="width: 60mm;">
				<img src="<?php echo $result_setting['instansi_kepsek_ttd']; ?>" style="width: 30mm;">
			</td>
		</tr>
		<tr>
			<td style="width: 120mm;"></td>
			<td style="width: 60mm;"><b style="text-decoration: underline;"><?php echo $result_setting['instansi_kepsek']; ?></b></td>
		</tr>
		<tr>
			<td style="width: 120mm;"></td>
			<td style="width: 60mm;"><b>NIP. <?php echo $result_setting['instansi_kepsek_nip']; ?></b></td>
		</tr>
	</table>
</page>

<?php
	$content = ob_get_clean();

	require_once('includes/html2pdf/html2pdf.class.php');
	try{
		// $html2pdf = new HTML2PDF('P', 'Legal', 'fr');
		$html2pdf = new Html2Pdf('P', 'A4', 'fr', true, 'UTF-8', array(15, 5, 15, 5));
		$html2pdf->pdf->SetDisplayMode('real');
		$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
		$html2pdf->Output($namaFile.'.pdf');
	}
	catch(HTML2PDF_exception $e) {
		echo $e;
		exit;
	}
?>
