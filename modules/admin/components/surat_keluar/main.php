<?php 	
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();

if(isset($_SESSION[_APP_.'s_userAdmin'])){
	$_SESSION[_APP_.'s_adminPage'] = "module=admin&component=surat_keluar&disposisi=".@$_REQUEST['disposisi']."&id=".@$_REQUEST['id'];
	
	/* User Akses Menu :
		3. Akses Semua
		2. Akses Hapus
		1. Akses Tambah/Edit
		0. Akses View
	*/
	unset($_SESSION[_APP_.'s_accessMenu']);
	if(@$_REQUEST['menu']!='') $_SESSION[_APP_.'s_menuPage'] = @$_REQUEST['menu'];
	list($access) = $db->result_row("SELECT access FROM _admin_menus_access WHERE id_admin_group = '".$_SESSION[_APP_.'s_idGroupAdmin']."' AND id_admin_menus = '".$_SESSION[_APP_.'s_menuPage']."' ");
	$_SESSION[_APP_.'s_accessMenu'] = $access;
	/* End User Akses Menu */
	
	?>
	
	<!-- Header & Breadcrumb -->
	<?php
	list($level, $menus, $icon, $link, $parent) = $db->result_row("SELECT level, nama, icon, link, id_admin_menus_parent FROM _admin_menus WHERE id_admin_menus = '".$_SESSION[_APP_.'s_menuPage']."' ");
	?>
	<section class="content-header">
		<h1><?php echo $menus; ?></h1>
		<ol class="breadcrumb">
		<?php
		$menux[] = "<li class='active'><a style='text-decoration:none;'>".stripslashes($icon)." ".str_replace(" ", "&nbsp;", $menus)."</a></li>";
		for($i=($level-1); $i>=0; $i--){
			list($levelx, $menusx, $iconx, $linkx, $parentx) = $db->result_row("SELECT level, nama, icon, link, id_admin_menus_parent FROM _admin_menus WHERE id_admin_menus = '$parent' AND level = '$i' ");
			$menux[] = "<li><a style='text-decoration:none;cursor:pointer;' onclick=\"".stripslashes($linkx)."\">".stripslashes($iconx)." ".str_replace(" ", "&nbsp;", $menusx)."</a></li>";
			$parent = $parentx;
		}
		$cmenu = count($menux);
		for($a=$cmenu; $a>=0; $a--){
			echo $menux[$a];
		}
		?>
		</ol>
	</section>
	<!-- End Header & Breadcrumb -->
	
	<?php
		if(@$_REQUEST['disposisi'] == 'true'){
			$id_surat_keluar = $_REQUEST['id'];
			$result_sm = $db->result_assoc("SELECT * FROM _surat_keluar WHERE id_surat_keluar = '$id_surat_keluar'");
			$result_disposisi = $db->result_assoc("SELECT A.*, B.nama AS karyawan FROM _disposisi AS A INNER JOIN _karyawan AS B ON(A.nip = B.nip) WHERE id_surat_keluar = '$id_surat_keluar'");
	?>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-body">
						<table class="table">
							<tr>
								<td style="width: 20%;">Tgl. Terima, No. Agenda</td>
								<td style="width: 1%;">:</td>
								<td>
									<?php echo $func->report_date($result_sm['tanggal_terima']); ?>, <b><?php echo $result_sm['no_agenda']; ?></b>

									<a href="#form-disposisi" class="btn btn-primary pull-right"><i class="fa fa-edit"></i> Form disposisi</a>
								</td>
							</tr>
							<tr>
								<td>Dari</td>
								<td>:</td>
								<td><?php echo $result_sm['sumber']; ?></td>
							</tr>
							<tr>
								<td>Tanggal, No. Surat</td>
								<td>:</td>
								<td><?php echo $func->report_date($result_sm['tanggal_surat']); ?>, <b><?php echo $result_sm['no_surat']; ?></b></td>
							</tr>
							<tr>
								<td>Perihal</td>
								<td>:</td>
								<td><?php echo $result_sm['perihal']; ?></td>
							</tr>
							<tr>
								<td>Keterangan</td>
								<td>:</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td colspan="3"><?php echo $result_sm['keterangan']; ?></td>
							</tr>
						</table>
					</div>
				</div>
				<div class="box" id="form-disposisi">
					<div class="box-body">
						<div class="row">
							<div class="form-group col-xs-4 has-id">
								<label>Disposisikan ke</label>
								<div class="input-group col-xs-12">
									<input type="hidden" name="nip" id="nip" value="<?php echo @$result_disposisi['nip']; ?>" />
									<input type="text" name="nama_karyawan" id="nama_karyawan" autocomplete="off" class="form-control input-sm" placeholder="Disposisikan ke ..." readonly value="<?php echo (@$result_disposisi['nip'] != '') ? @$result_disposisi['nip']." (".@$result_disposisi['karyawan'].")" : ""; ?>" />
									<span class="input-group-addon" style="cursor: pointer;" onclick="javascript: document.getElementById('iframe_choose').src='content.php?module=admin&component=surat_keluar&action=karyawan'; document.getElementById('bntModalChoose').click(); "><i class="fa fa-plus"></i></span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-xs-8 has-id">
								<label>Catatan</label>
								<textarea class="form-control input-sm" name="catatan" id="catatan" placeholder="Catatan ..."><?php echo @$result_disposisi['catatan']; ?></textarea>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-xs-8 has-id">
								<button class="btn btn-primary" onclick="javascript:
									var nip = document.getElementById('nip').value;
									var catatan = document.getElementById('catatan').value;

									if(nip == ''){
										document.getElementById('modal-error-text').innerHTML='<i class=\'fa fa-warning\'></i> Tujuan disposisi harus diisi!';
										document.getElementById('btn-alert').click();
									}else{
										sendRequest('content.php', 'module=admin&component=surat_keluar&action=process&proc=disposisi&id_surat_keluar=<?php echo $result_sm['id_surat_keluar']; ?>&nip='+nip+'&catatan='+catatan, 'content', 'div');
									}
								"><i class="fa fa-refresh"></i> Disposisi Surat</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php
		}else{
	?>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">&nbsp;</h3>
						<div class="box-tools">
							<div class="input-group">
								<div class="input-group-btn">
									<button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#myModal" onclick="javascript: document.getElementById('iframe_surat_keluar').src='content.php?module=admin&component=surat_keluar&action=add'; " <?php if($func->akses('C') == false){ echo "disabled"; } ?>><i class="fa fa-plus"></i> Tambah data</button>
								</div>
								<input type="text" name="table_search" class="form-control input-sm pull-right" id="keyword" placeholder="Search" onkeyup="javascript: sendRequest('content.php', 'module=admin&component=surat_keluar&action=list&ajax=true&keyword='+document.getElementById('keyword').value, 'list', 'div');" />
								<div class="input-group-btn">
									<button class="btn btn-sm btn-default" onclick="javascript: sendRequest('content.php', 'module=admin&component=surat_keluar&action=list&ajax=true&keyword='+document.getElementById('keyword').value, 'list', 'div');"><i class="fa fa-search"></i></button>
								</div>
							</div>
						</div>
					</div>
					
					<div id="list"><?php include "list.php"; ?></div>
				</div>
			</div>
		</div>
	</section>
	<?php
	}
	?>
		
	<div class="modal fade" id="myModal" data-backdrop="static">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Input <?php echo $menus; ?></h4>
				</div>
				<div class="modal-body">
					<iframe name="iframe_surat_keluar" id="iframe_surat_keluar" width="98%" height="900" frameborder="0" scrolling="yes"></iframe>
				</div>
				<div class="modal-footer">
					<label class="pull-left">
						<input type="checkbox" name="cbadd" id="cbadd" value="1" />
						Tambah data lagi.
					</label>
					<button class="btn btn-primary" onclick="javascript: 
					var iframe = document.getElementById('iframe_surat_keluar');
					iframe.contentDocument.getElementById('save').click();  
					"><i class="fa fa-save"></i> Simpan</button>
					<button class="btn btn-danger" data-dismiss="modal" aria-hidden="true" id="dismiss"><i class="fa fa-remove"></i> Batal</button>
				</div>
			</div>
		</div>
	</div>
	
	<input type="hidden" data-toggle="modal" data-target="#myModalChoose" id="bntModalChoose" />
	<div class="modal fade" id="myModalChoose" data-backdrop="static">
		<div class="modal-dialog modal-md">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Silahkan Pilih</h4>
				</div>
				<div class="modal-body">
					<iframe name="iframe_choose" id="iframe_choose" width="98%" height="80" frameborder="0" onload="javascript: autoResize('iframe_choose')"></iframe>
				</div>
				<div class="modal-footer">
					<button class="btn btn-danger" data-dismiss="modal" aria-hidden="true" id="dismissChoode"><i class="fa fa-remove"></i> Batal</button>
				</div>
			</div>
		</div>
	</div>
	
	<input type="hidden" data-toggle="modal" data-target="#modal-alert" id="btn-alert" />
	<div class="modal fade" id="modal-alert" data-backdrop="static">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Peringatan</h4>
				</div>
				<div class="modal-body" style="text-align: center;">
					<p id="modal-error-text"></p>
				</div>
				<div class="modal-footer">
					<button class="btn btn-default" data-dismiss="modal" aria-hidden="true" id="dismissChoode"><i class="fa fa-remove"></i> Tutup</button>
				</div>
			</div>
		</div>
	</div>
<?php 
}else{
	include "modules/admin/components/auth/timeout.php";
}
?>