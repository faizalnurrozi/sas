<?php
include "globals/config.php";
include "globals/functions.php";
include "globals/upload.php";
$db = new Database();
$func = new Functions();

$id = @$_GET['id'];
?>
<html>
	<head>
		<title><?php echo $menus; ?></title>
		
		<script type="text/javascript" src="includes/ajax.js"></script>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="includes/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="includes/bootstrap/css/font-awesome.css">
		<link rel="stylesheet" href="includes/dist/css/ionicons.min.css">
		<link rel="stylesheet" href="includes/dist/css/AdminLTE.css">
		<link rel="stylesheet" href="includes/dist/css/skins/_all-skins.min.css">
		<style type="text/css">
			ul li a{
				color: #555;
			}

			ul li a:hover{
				text-decoration: underline;
				color: #555;
			}
		</style>
	</head>
	<body bgcolor="white">
		<?php $result = $db->result_assoc(" SELECT A.*, B.nama AS jenis_surat, C.nama AS indeks FROM _surat_keluar AS A INNER JOIN _jenis_surat AS B ON(A.id_jenis_surat = B.id_jenis_surat) INNER JOIN _indeks AS C ON(A.id_indeks = C.id_indeks) WHERE id_surat_keluar = '$id' "); ?>
		<table class="table">
			<tr>
				<td width="20%">No. Surat</td>
				<td width="1%">&nbsp;:&nbsp;</td>
				<td><?php echo $result['id_surat_keluar']; ?></td>
			</tr>
			<tr>
				<td>Jenis Surat</td>
				<td>&nbsp;:&nbsp;</td>
				<td><?php echo $result['jenis_surat']; ?></td>
			</tr>
			<tr>
				<td>Indeks</td>
				<td>&nbsp;:&nbsp;</td>
				<td><?php echo $result['indeks']; ?></td>
			</tr>
			<tr>
				<td>Tanggal Surat</td>
				<td>&nbsp;:&nbsp;</td>
				<td><?php echo $func->report_date($result['tanggal']); ?></td>
			</tr>
			<tr>
				<td>Tanggal Hijriah</td>
				<td>&nbsp;:&nbsp;</td>
				<td><?php echo $func->report_date_hijriah($result['tanggal']); ?></td>
			</tr>
			<tr>
				<td>Pengolah</td>
				<td>&nbsp;:&nbsp;</td>
				<td><?php echo $result['pengolah']; ?></td>
			</tr>
			<tr>
				<td>Tujuan Surat</td>
				<td>&nbsp;:&nbsp;</td>
				<td><?php echo $result['tujuan_surat']; ?></td>
			</tr>
			<tr>
				<td>Perihal</td>
				<td>&nbsp;:&nbsp;</td>
				<td><?php echo $result['perihal']; ?></td>
			</tr>
			<tr>
				<td valign="top">Lampiran</td>
				<td valign="top">&nbsp;:&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td colspan="3">
					<ul>
						<?php
							$query_lampiran = $db->sql("SELECT lampiran FROM _surat_keluar_lampiran WHERE id_surat_keluar = '$id'");
							while($result_lampiran = $db->fetch_assoc($query_lampiran)){
								$handle 		= new upload($result_lampiran['lampiran']);

								echo "<li><a href='".$result_lampiran['lampiran']."' target='_blank'>".$func->mimetype2FontAwesome($result_lampiran['lampiran'])." $handle->file_src_name</a></li>";
							}
						?>
					</ul>
				</td>
			</tr>
			<tr>
				<td colspan="3"><b>Keterangan</b></td>
			</tr>
			<tr>
				<td colspan="3"><?php echo $result['keterangan']; ?></td>
			</tr>
		</table>
	</body>
</html>