<?php
include "globals/config.php";
include "globals/functions.php";
include "globals/upload.php";
$db = new Database();
$func = new Functions();
unset($_SESSION[_APP_.'s_source_file']);
?>
<html>
	<head>
		<script type="text/javascript" src="includes/ajax.js"></script>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="includes/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="includes/bootstrap/css/font-awesome.css">
		<link rel="stylesheet" href="includes/dist/css/ionicons.min.css">
		<link rel="stylesheet" href="includes/dist/css/AdminLTE.css">
		<link rel="stylesheet" href="includes/dist/css/skins/_all-skins.min.css">

		<script src="includes/plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script src="includes/plugins/jQuery/jquery.form.js"></script>
		<script src="includes/bootstrap/js/bootstrap.min.js"></script>
		<script src="includes/dist/js/app.min.js"></script>

		<script type="text/javascript" src="includes/plugins/datepicker/bootstrap-datepicker.min.js"></script>
		<link rel="stylesheet" href="includes/plugins/datepicker/bootstrap-datepicker3.css"/>

		<script language="JavaScript">
			$(function () {
				var options={
					format: 'dd/mm/yyyy',
					todayHighlight: true,
					autoclose: true,
				};
				$('#tanggal').datepicker(options);
			});
		</script>
		
		<script src="includes/tinymce/tinymce.min.js"></script>
		<script>
			tinymce.init({
				selector:'.tinymce',
				plugins: [
					"table", 
					"advlist autolink lists link image charmap preview hr anchor pagebreak", 
					"searchreplace wordcount visualblocks visualchars code",
					"insertdatetime media nonbreaking table contextmenu directionality",
					"emoticons template paste textcolor colorpicker textpattern imagetools codesample"
				],
				toolbar: 'undo redo | bold italic sizeselect fontselect fontsizeselect | hr alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor backcolor',
				fontsize_formats: "8px 10px 11px 12px 13px 14px 18px 24px 36px",
			});
		</script>
	</head>
	<body>
		<?php
		$proc = $_REQUEST['proc'];
		$keterangan = "";
		
		if($proc == 'generate_id'){

			$id_jenis_surat	= $_REQUEST['id_jenis_surat'];
			$id_indeks		= $_REQUEST['id_indeks'];
			$tahun 			= date("Y");
			list($instansi_id) = $db->result_row("SELECT instansi_id FROM _setting");

			/**
			 * Cek status umum
			 */
			
			list($status_umum) = $db->result_row("SELECT status_umum FROM _jenis_surat WHERE id_jenis_surat = '$id_jenis_surat'");

			$kd_jenis_surat = ($status_umum == 'FALSE') ? $id_jenis_surat.'/' : '';

			/**
			 * Mengambil nilai urutan terakhir
			 */
			
			list($urutan) = $db->result_row("SELECT SUBSTRING(id_surat_keluar,1,3) FROM _surat_keluar WHERE SUBSTRING(id_surat_keluar,-4) = '$tahun' ORDER BY SUBSTRING(id_surat_keluar,1,3) DESC");
			$urutan++;

			/**
			 * No_Urut/Jenis_Surat/Identitas_Sekolah/Indeks/Tahun
			 */
			
			$id_surat_keluar = $func->number_pad($urutan,3)."/".$kd_jenis_surat.$instansi_id."/".$id_indeks."/".$tahun;

			if($status_umum == 'TRUE'){
				list($salam_pembuka, $salam_penutup) = $db->result_row("SELECT salam_pembuka, salam_penutup FROM _setting");
				$keterangan = $salam_pembuka."<br>".$salam_penutup;
			}
		}

		if(@$_REQUEST['id'] != ''){
			$qEditAdmin = "SELECT * FROM _surat_keluar WHERE id_surat_keluar = '$_REQUEST[id]'";
			$dataEdit = $db->sql($qEditAdmin);
			$resultEdit = $db->fetch_assoc($dataEdit);

			$query_lampiran = $db->sql("SELECT lampiran FROM _surat_keluar_lampiran WHERE id_surat_keluar = '$_REQUEST[id]'");
			while($result_lampiran = $db->fetch_assoc($query_lampiran)){
				$_SESSION[_APP_.'s_source_file'][] = $result_lampiran['lampiran'];
			}

			$id_surat_keluar 	= $resultEdit['id_surat_keluar'];
			$keterangan 		= $resultEdit['keterangan'];
		}
		?>		
		<div class="container-fluid">
		<form name="form_surat_keluar" method="POST" action="javascript: void(null);" enctype="multipart/form-data">
			<?php if(@$_REQUEST['id'] == ''){ ?>
			<input type="hidden" id="proc" name="proc" value="add" />
			<?php }else{ ?>
			<input type="hidden" id="proc" name="proc" value="update" />
			<input type="hidden" id="txtidx" name="txtidx" value="<?php echo $resultEdit['id_surat_keluar']; ?>" />
			<?php } ?>
			<div class="row">
				<div class="col-xs-6">
					<div class="row">
						<div class="form-group col-xs-12 has-id_surat_keluar">
							<label>No. Surat</label>
							<input type="text" autocomplete="off" class="form-control input-sm" placeholder="No. surat ..." name="id_surat_keluar" id="id_surat_keluar" value="<?php echo @$id_surat_keluar; ?>" readonly style="font-weight: bold; text-align: center;" />
						</div>
					</div>
					<div class="row">
						<div class="form-group col-xs-6 has-id_jenis_surat">
							<label>Jenis Surat</label>
							<select class="form-control input-sm" name="id_jenis_surat" id="id_jenis_surat" onchange="javascript: window.location.href='content.php?module=admin&component=surat_keluar&action=add&proc=generate_id&id_jenis_surat='+document.getElementById('id_jenis_surat').value+'&id_indeks='+document.getElementById('id_indeks').value;" <?php if($resultEdit['id_surat_keluar'] != '') echo "disabled"; ?>>
								<option value="">-Jenis Surat-</option>
								<?php
									$q_js = $db->sql("SELECT * FROM _jenis_surat ORDER BY id_jenis_surat ASC");
									while($r_js = $db->fetch_assoc($q_js)){
										echo "<option value='$r_js[id_jenis_surat]'"; if($r_js['id_jenis_surat'] == $resultEdit['id_jenis_surat'] || $r_js['id_jenis_surat'] == $_REQUEST['id_jenis_surat']) echo "selected"; echo ">($r_js[id_jenis_surat]) $r_js[nama]</option>";
									}
								?>
							</select>
							<span id="load-jenis-surat"></span>
						</div>
						<div class="form-group col-xs-6 has-id_indeks">
							<label>Indeks</label>
							<select class="form-control input-sm" name="id_indeks" id="id_indeks" onchange="javascript: window.location.href='content.php?module=admin&component=surat_keluar&action=add&proc=generate_id&id_jenis_surat='+document.getElementById('id_jenis_surat').value+'&id_indeks='+document.getElementById('id_indeks').value;" <?php if($resultEdit['id_surat_keluar'] != '') echo "disabled"; ?>>
								<option value="">-Indeks-</option>
								<?php
									$q_js = $db->sql("SELECT * FROM _indeks ORDER BY id_indeks ASC");
									while($r_js = $db->fetch_assoc($q_js)){
										echo "<option value='$r_js[id_indeks]'"; if($r_js['id_indeks'] == $resultEdit['id_indeks'] || $r_js['id_indeks'] == $_REQUEST['id_indeks']) echo "selected"; echo ">($r_js[id_indeks]) $r_js[nama]</option>";
									}
								?>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="form-group col-xs-6 has-tanggal">
							<label>Tanggal Surat</label>
							<div class="input-group col-xs-12">
								<input type="text" name="tanggal" id="tanggal" class="form-control input-sm" value="<?php if(@$resultEdit['id_surat_keluar'] != '') echo $func->implode_date($resultEdit['tanggal']); else echo date("d/m/Y"); ?>" readonly onchange="javascript: ajaxInput('content.php', 'module=admin&component=surat_keluar&action=process&proc=tanggal_hijriah&tanggal='+this.value, 'tanggal_hijriah');" />
								<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
							</div>
						</div>

						<div class="form-group col-xs-6 has-id">
							<label>Tanggal Hijriah</label>
							<div class="input-group col-xs-12">
								<input type="text" name="tanggal_hijriah" id="tanggal_hijriah" class="form-control input-sm" value="<?php if(@$resultEdit['id_surat_keluar'] != '') echo $func->implode_date_hijriah($resultEdit['tanggal']); else echo $func->implode_date_hijriah(date("Y-m-d")); ?>" readonly />
								<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-6">
					<div class="form-group col-xs-12 has-pengolah">
						<label>Pengolah</label>
						<input type="text" autocomplete="off" class="form-control input-sm" placeholder="Pengolah ..." name="pengolah" id="pengolah" value="<?php echo @$resultEdit['pengolah']; ?>" />
					</div>
					<div class="form-group col-xs-12 has-tujuan_surat">
						<label>Tujuan Surat</label>
						<input type="text" autocomplete="off" class="form-control input-sm" placeholder="Tujuan surat ..." name="tujuan_surat" id="tujuan_surat" value="<?php echo @$resultEdit['tujuan_surat']; ?>" onkeyup="javascript: document.getElementById('keterangan').value=this.value;" />
					</div>
					<div class="form-group col-xs-12 has-perihal">
						<label>Perihal</label>
						<textarea class="form-control input-sm" placeholder="Perihal ..." name="perihal" id="perihal"><?php echo @$resultEdit['perihal']; ?></textarea>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="form-group col-xs-12 has-keterangan">
					<label>Keterangan</label>
					<textarea class="form-control input-sm tinymce" placeholder="Keterangan ..." name="keterangan" id="keterangan" style="height: 400px;"><?php echo @$keterangan; ?></textarea>
				</div>
			</div>
			
			<table class="hide">
				<tr><td colspan="3">&nbsp;</td></tr>
				<tr>
					<td colspan="3" align="left">
						<button style="display:none;" id="save" class="btn btn-primary" onclick="javascript:
							var obj = document.form_surat_keluar;
							var errText = '';
							var err = '';
							var errAlert = '';
							if(obj.id_surat_keluar.value==''){
								$('.has-id_surat_keluar').addClass('has-error').focus();
								errText = 'Nomor surat harus di isi';
								err += '<li>'+errText+'</li>';
								errAlert += errText+'\n';
							}
							if(obj.id_jenis_surat.value==''){
								$('.has-id_jenis_surat').addClass('has-error').focus();
								errText = 'Jenis surat harus di isi';
								err += '<li>'+errText+'</li>';
								errAlert += errText+'\n';
							}
							if(obj.id_indeks.value==''){
								$('.has-id_indeks').addClass('has-error').focus();
								errText = 'Indeks surat harus di isi';
								err += '<li>'+errText+'</li>';
								errAlert += errText+'\n';
							}
							if(err==''){
								obj.action='content.php?module=admin&component=surat_keluar&action=process';
								obj.submit();
								
								if(window.top.document.getElementById('cbadd').checked == false){
									window.top.document.getElementById('dismiss').click();
								}
							}else{ 
								$('#Modal').click(); $('#error-text').html(err);
								alert(errAlert);
							}
						"></button>
						<a class="btn hidden" id="reset" onclick="javascript: document.form_surat_keluar.reset();"></a>
					</td>
				</tr>
			</table>
		</form>


		<div class="row">
			<div class="form-group col-xs-12" id="detail">
				<label>Upload lampiran (.jpg, .gif, .png, .pdf, .doc, .docx)</label>
				<?php include("detail.php"); ?>
			</div>
		</div>

		</div>
		
		<!-- Alert Validation Form -->
		<input type="hidden" id="Modal" onclick="javascript: $('#s_alert').fadeIn(); $('#s_alert').delay(3000); $('#s_alert').fadeOut(500);" />
		<div class="alert alert-danger" id="s_alert">
			<button type="button" class="close" onclick="javascript: $('#s_alert').fadeOut();">x</button>
			<strong>Warning : </strong> <div id="error-text" class="error-text"><ul></ul></div>
		</div>
		<!-- End Alert Validation Form -->
		
		<script src="includes/bootstrap/bootstrap.js"></script>
		<script type="text/javascript">
			$("[rel=tooltip]").tooltip();
			$(function() {
				$('.demo-cancel-click').click(function(){return false;});
			});
		</script>
		
	</body>
</html>