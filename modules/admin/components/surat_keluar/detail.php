<script type="text/javascript">
function upload_image() {
	var bar = $('#bar');
	var percent = $('#percent');

	$('#myForm').ajaxForm({
		beforeSubmit: function() {
			$('#progress_div').removeClass('hide');
			var percentVal = '0%';
			percent.width(percentVal);
		},

		uploadProgress: function(event, position, total, percentComplete) {
			var percentVal = percentComplete + '%';
			percent.width(percentVal);
		},

		success: function() {
			var percentVal = '100%';
			percent.width(percentVal);
		},

		complete: function(xhr) {
			var json = JSON.parse(xhr.responseText);

			// console.log(json);

			if(xhr.responseText){
				$('#progress_div').addClass('hide');
				$('#upload_file').val('');
				$('.btn-submit-file').addClass('hide');
				$('.btn-choose-file').removeClass('hide');

				if(json.status == true){
					var resultImage = '';
					resultImage += '<div class="item-file result-file" data-index="'+json.index+'">';
					resultImage += '	<a class="remove-image" data-thumb="'+json.source_file+'" data-target="'+json.index+'"><i class="fa fa-times"></i></a>';
					resultImage += '	<div class="file">';
					resultImage += '		<div class="icon-thumb">';
					resultImage += '			'+json.mime;
					resultImage += '		</div>';
					resultImage += '		<p>'+json.nama+'</p>';
					resultImage += '	</div>';
					resultImage += '</div>';

					$('.block-upload form').append(resultImage);
				}else{
					var err = 'Terjadi kesalahan ketika upload, periksa kembali file anda.';
					$('#alert-file').fadeIn();
					$('#alert-file').delay(3000);
					$('#alert-file').fadeOut(500);
					$('#alert-file .error-text').html(err);
				}
			}
		}
	}); 
}
</script>

<div class="block-upload">
	<div class="alert alert-danger" id="alert-file">
		<button type="button" class="close" onclick="javascript: $('#alert-file').fadeOut();">x</button>
		<div class="error-text"></div>
	</div>
	<form action="content.php?module=admin&component=surat_keluar&action=process&proc=upload_file" id="myForm" name="frmupload" method="post" enctype="multipart/form-data">
		<input type="file" id="upload_file" name="upload_file" class="hide" onchange="javascript: $('.btn-submit-file').removeClass('hide'); $('.btn-choose-file').addClass('hide'); " />
		<div class="item-file upload-file">
			<div class="icon">
				<div class="progress hide" id="progress_div">
					<div class="bar" id="bar">
						<div class="percent" id="percent"></div>
					</div>
				</div>
			</div>
			<button type="button" class="button-choose btn-choose-file" onclick="$('#upload_file').click();">Pilih file</button>
			<button type="submit" name='submit_image' class="button-upload hide btn-submit-file" onclick="upload_image();">Upload file</button>
		</div>
		<?php
			if(count(@$_SESSION[_APP_.'s_source_file']) > 0){
				$i = 0;
				foreach($_SESSION[_APP_.'s_source_file'] as $source){
					$handle = new upload($source);
					$file_src_name	= explode('.', $handle->file_src_name);
					$namaFile	= substr($file_src_name[0], 0, 15);

					echo "<div class='item-file result-file' data-index='$i' data-edit='true'>";
					echo "	<a class='remove-image' data-thumb='$source' data-target='$i'><i class='fa fa-times'></i></a>";
					echo "	<div class='file'>";
					echo "		<div class='icon-thumb'>";
					echo "			".$func->mimetype2FontAwesome($source, "fa-4x");
					echo "		</div>";
					echo "		<p>$namaFile</p>";
					echo "	</div>";
					echo "</div>";
					$i++;
				}
			}
		?>
	</form>
</div>

<script>
$('.block-upload').on('click', '.remove-image', function(){
	var source 	= $(this).data('thumb');
	var target 	= $(this).data('target');
	var edit 	= $(this).data('edit');

	if(edit == 'true'){
		$('div.result-file[data-index="'+target+'"]').remove();
	}else{
		$.ajax({
			dataType	:'json',
			url 		: "content.php",
			data 		: {
				module		: 'admin',
				component	: 'surat_keluar',
				action		: 'process',
				proc		: 'delete_file',
				sourcex		: source,
				targetx		: target
			},
			success		: function(result){
				if(result.status == true){
					$('div.result-file[data-index="'+target+'"]').remove();
				}

				var indexNew = 0;
				$('.result-file').each(function(){
					$(this).attr('data-index', indexNew);
					$(this).find('.remove-image').attr('data-target', indexNew);
					indexNew++;
				});
			}
		});
	}
});
</script>