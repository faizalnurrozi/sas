<?php 
include "globals/config.php";
include "globals/functions.php";
include "globals/upload.php";
$db 	= new Database();
$func 	= new Functions();

switch(@$_REQUEST['proc']){
	case 'add' :
		# ---------------------#
		# Penangkapan variable #
		# ---------------------#
		
		$id_surat_keluar= $_POST['id_surat_keluar'];
		$id_jenis_surat = $_POST['id_jenis_surat'];
		$id_indeks 		= $_POST['id_indeks'];
		$tanggal 		= $func->explode_date($_POST['tanggal']);
		$pengolah 		= $_POST['pengolah'];
		$tujuan_surat	= $_POST['tujuan_surat'];
		$perihal 		= $_POST['perihal'];
		$keterangan 	= $_POST['keterangan'];

		$data_insert	= array('id_surat_keluar' => $id_surat_keluar, 'id_jenis_surat' => $id_jenis_surat, 'id_indeks' => $id_indeks, 'tanggal' => $tanggal, 'pengolah' => $pengolah, 'tujuan_surat' => $tujuan_surat, 'perihal' => $perihal, 'keterangan' => $keterangan);
		
		$hqData = $db->insert("_surat_keluar", $data_insert);

		if(count(@$_SESSION[_APP_.'s_source_file']) > 0){
			$i = 0;
			foreach($_SESSION[_APP_.'s_source_file'] as $lampiran){
				$hqData = $db->insert("_surat_keluar_lampiran", array("id_surat_keluar" => $id_surat_keluar, "lampiran" => $lampiran));
			}
		}

		if($hqData==true) $_SESSION[_APP_.'s_message_info'] = "Tambah data berhasil";
		else $_SESSION[_APP_.'s_message_error'] = "Tambah data gagal";
		
		#---------- * ----------#
		
		
		# ----------------#
		# Pencatatan Logs #
		# ----------------#
		
		/* ----- Activity Logs (Insert) ------ */
		$func->activity_logs_insert("_surat_keluar", $id, $_SESSION[_APP_.'s_userAdmin']);
		/* ----- End Activity Logs (Insert) ------ */
		
		#---------- * ----------#
		
		
		# ------------#
		# Kirim balik #
		# ------------#
		
		echo "<script>window.parent.sendRequest('content.php', 'module=admin&component=surat_keluar&action=list&ajax=true', 'list', 'div');</script>";
		echo "<meta http-equiv='refresh' content='0;url=content.php?module=admin&component=surat_keluar&action=add' />";
		
		#---------- * ----------#
		
		break;
		
	case 'update' :
		# ---------------------#
		# Penangkapan variable #
		# ---------------------#
		
		$id_surat_keluar= $_POST['id_surat_keluar'];
		$tanggal 		= $func->explode_date($_POST['tanggal']);
		$pengolah 		= $_POST['pengolah'];
		$tujuan_surat	= $_POST['tujuan_surat'];
		$perihal 		= $_POST['perihal'];
		$keterangan 	= $_POST['keterangan'];
		
		# ----------------#
		# Pencatatan Logs #
		# ----------------#

		$data_update	= array('tanggal' => $tanggal, 'pengolah' => $pengolah, 'tujuan_surat' => $tujuan_surat, 'perihal' => $perihal, 'keterangan' => $keterangan);

		
		/* ----- Activity Logs (Update) ------ */
		$func->activity_logs_update("_surat_keluar", $data_update, array('id_surat_keluar' => $id_surat_keluar), 'OR', $_SESSION[_APP_.'s_userAdmin']);
		/* ----- End Activity Logs (Update) ------ */
		
		#---------- * ----------#
		
		
		
		# --------------#
		# Proses Update #
		# --------------#
		
		$hqData = $db->update("_surat_keluar", $data_update, array('id_surat_keluar' => $id_surat_keluar));

		$db->delete("_surat_keluar_lampiran", array('id_surat_keluar' => $id_surat_keluar));
		if(count(@$_SESSION[_APP_.'s_source_file']) > 0){
			$i = 0;
			foreach($_SESSION[_APP_.'s_source_file'] as $lampiran){
				$hqData = $db->insert("_surat_keluar_lampiran", array("id_surat_keluar" => $id_surat_keluar, "lampiran" => $lampiran));
			}
		}
		
		if($hqData==true) $_SESSION[_APP_.'s_message_info'] = "Update data berhasil";
		else $_SESSION[_APP_.'s_message_error'] = "Update data gagal";
		
		#---------- * ----------#
		
		
		# ------------#
		# Kirim balik #
		# ------------#
		
		echo "<script>window.parent.sendRequest('content.php', 'module=admin&component=surat_keluar&action=list&ajax=true', 'list', 'div');</script>";
		echo "<meta http-equiv='refresh' content='0;url=content.php?module=admin&component=surat_keluar&action=add' />";
		
		#---------- * ----------#
		break;
		
	case 'delete' :
		# ---------------------#
		# Penangkapan variable #
		# ---------------------#
		
		$idx = @$_REQUEST['id'];
		
		#---------- * ----------#
		
		
		
		# --------------#
		# Proses Delete #
		# --------------#
		
		$queryLampiran = $db->sql("SELECT lampiran FROM _surat_keluar_lampiran WHERE id_surat_keluar = '$idx'");
		while(list($file_lampiran) = $db->fetch_row($queryLampiran)){
			if(file_exists($file_lampiran)) unlink($file_lampiran);
		}
		
		$hqData = $db->delete("_surat_keluar", array('id_surat_keluar' => $idx));
		$hqData = $db->delete("_surat_keluar_lampiran", array('id_surat_keluar' => $idx));
		
		if($hqData==true) $_SESSION[_APP_.'s_message_info'] = "Hapus data berhasil";
		else $_SESSION[_APP_.'s_message_error'] = "Hapus data gagal";
		
		#---------- * ----------#
		
		
		# ----------------#
		# Pencatatan Logs #
		# ----------------#
		
		/* ----- Activity Logs (Delete) ------ */
		$func->activity_logs_delete("_surat_keluar", $idx, $_SESSION[_APP_.'s_userAdmin']);
		/* ----- End Activity Logs (Delete) ------ */
		
		#---------- * ----------#
		
		
		# ------------#
		# Kirim balik #
		# ------------#
		
		header("location: content.php?module=admin&component=surat_keluar&action=list&ajax=true");
		
		#---------- * ----------#
		break;

	case "tanggal_hijriah":
		$tanggalHijrian = $func->implode_date_hijriah($func->explode_date($_REQUEST['tanggal']));
		echo $tanggalHijrian;
		break;

	case "upload_file":
		$handle = new upload($_FILES['upload_file']);
		$tujuan = '';

		$handle->allowed = array('application/pdf', 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'image/jpeg', 'image/gif', 'image/png');
		if ($handle->uploaded){
			$handle->process('./resources/');

			if ($handle->processed) {

				$handle->clean();

				$file_src_name			= explode('.', $handle->file_src_name);
				$data['nama'] 			= substr($file_src_name[0], 0, 15);
				$data['source_file'] 	= $handle->file_dst_pathname;
				$data['mime'] 			= $func->mimetype2FontAwesome($handle->file_dst_pathname, "fa-4x");
				$data['status'] 		= true;

				$_SESSION[_APP_.'s_source_file'][] = $data['source_file'];

				$data_akhir = count($_SESSION[_APP_.'s_source_file'])-1;
				$data['index'] = $data_akhir;
				// $data['index'] = 0;

			}else{
				$data['nama'] = '';
				$data['status'] = false;
			}
		}else{
			$data['nama'] = '';
			$data['status'] = false;
		}

		echo json_encode($data);
		break;

	case "delete_file":
		$source = $_REQUEST['sourcex'];
		$target = $_REQUEST['targetx'];

		unset($_SESSION[_APP_.'s_source_file'][$target]);
		$_SESSION[_APP_.'s_source_file'] = array_values($_SESSION[_APP_.'s_source_file']);
		unlink($source);

		$data['status'] = true;

		echo json_encode($data);
		break;
}
?>