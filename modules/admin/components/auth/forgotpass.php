<?php
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();
?>
<html>
	<head>		
		<script type="text/javascript" src="includes/ajax.js"></script>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="includes/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="includes/bootstrap/css/font-awesome.css">
		<link rel="stylesheet" href="includes/dist/css/ionicons.min.css">
		<link rel="stylesheet" href="includes/dist/css/AdminLTE.css">
		<!--<link rel="stylesheet" href="includes/dist/css/skins/_all-skins.min.css">-->

		<script src="includes/plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script src="includes/bootstrap/js/bootstrap.min.js"></script>
		<script src="includes/dist/js/app.min.js"></script>
		
		<script type="text/javascript">
		function cek_email(email){
			var RegExp = /^((([a-z]|[0-9]|!|#|$|%|&|'|\*|\+|\-|\/|=|\?|\^|_|`|\{|\||\}|~)+(\.([a-z]|[0-9]|!|#|$|%|&|'|\*|\+|\-|\/|=|\?|\^|_|`|\{|\||\}|~)+)*)@((((([a-z]|[0-9])([a-z]|[0-9]|\-){0,61}([a-z]|[0-9])\.))*([a-z]|[0-9])([a-z]|[0-9]|\-){0,61}([a-z]|[0-9])\.)[\w]{2,4}|(((([0-9]){1,3}\.){3}([0-9]){1,3}))|(\[((([0-9]){1,3}\.){3}([0-9]){1,3})\])))$/ 
			if(RegExp.test(email)){
				return true;
			}else{
				return false;
			}
		}
		</script>
	</head>
	
	<body>
		<div class="container">
			<div class="callout callout-info">
				Untuk reset password dapat menghubungi cabang dinas.
			</div>
			<div class="row">
				<div class="col-xs-12">
					<b>Atau</b><p>&nbsp;</p>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<input type="email" name="txtemail" id="txtemail" placeholder="Masukkan Email aktif yang sudah terdaftar..." class="form-control" onkeyup="javascript: if(this.value==''){ document.getElementById('btnProses').disabled=true; }else{ if(cek_email(this.value)==true){ document.getElementById('btnProses').disabled=false; }else{ document.getElementById('btnProses').disabled=true; } }" />
					<button type="button" id="btnProses" class="btn btn-primary pull-right" onclick="javascript: if(document.getElementById('txtemail').value==''){ document.getElementById('txtemail').focus(); }else{ sendRequest('content.php', 'module=admin&component=auth&action=process&proc=forgotpass&email='+document.getElementById('txtemail').value, 'proses', 'div'); }" disabled="true"><i class="fa fa-refresh"></i> Proses</button>
				</div>
			</div>
			<div id="proses"></div>
		</div>
	</body>
</html>
