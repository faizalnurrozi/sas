<?php
global $db;
?>
<div class="login-box">
	
	<?php if(isset($_SESSION[_APP_.'s_message_info'])){ ?>
	<div class="alert alert-info">
		<button type="button" class="close" data-dismiss="alert">x</button>
		<strong></strong> <?php echo $_SESSION[_APP_.'s_message_info']; unset($_SESSION[_APP_.'s_message_info']); ?>
	</div>
	<?php } ?>
	<?php if(isset($_SESSION[_APP_.'s_message_error'])){ ?>
	<div class="alert alert-error">
		<button type="button" class="close" data-dismiss="alert">x</button>
		<?php echo $_SESSION[_APP_.'s_message_error']; unset($_SESSION[_APP_.'s_message_error']); ?>
	</div>
	<?php } ?>

	<!--
	<div class="alert alert-warning">
		<button type="button" class="close" data-dismiss="alert">x</button>
		<strong>Mohon maaf, sedang maintenance data.</strong>
	</div>
	-->
	
	<div class="login-box-body" id="login-form">
		<div class="alert alert-info">
			<h3 style="float:left; margin-top:0px; margin-right:20px;"><img src="<?php echo $logo; ?>" width="75" /></h3>
			<h2 style="margin-top:0px; font-weight:bolder;"><?php echo _TITLE_; ?></h2>
		</div>
		<?php include "form.php"; ?>  
		<div class="alert" style="opacity:0.7; text-align:center; border-top:1px solid grey; margin-bottom:-10px;">
			<span><?php echo _FOOTER_TEXT_; ?></span><br/>
			<?php echo _VERSION_; ?>
		</div>
	</div>	
	
	<!-- Modal -->
	<input type="hidden" data-toggle="modal" data-target="#modal-sekolah" id="btnModalSekolah" />
	<div class="modal fade" id="modal-sekolah" data-backdrop="static">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Pencarian Data Sekolah</h4>
				</div>
				<div class="modal-body">
					<iframe name="iframe_sekolah" id="iframe_sekolah" width="100%" frameborder="0" style="min-height: 450px;"></iframe>
				</div>
				<div class="modal-footer">
					<button class="btn btn-danger" data-dismiss="modal" aria-hidden="true" id="dismissSekolah"><i class="fa fa-remove"></i> Tutup</button>
				</div>
			</div>
		</div>
	</div>
	<!-- End Modal -->
	
	<!-- Modal -->
	<input type="hidden" data-toggle="modal" data-target="#modal-forgotpass" id="btnModalForgotPass" />
	<div class="modal fade" id="modal-forgotpass" data-backdrop="static">
		<div class="modal-dialog modal-md">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Lupa Password.</h4>
				</div>
				<div class="modal-body">
					<iframe name="iframe_forgotpass" id="iframe_forgotpass" width="100%" frameborder="0" style="min-height: 275px;" scrolling="no"></iframe>
				</div>
				<div class="modal-footer">
					<button class="btn btn-danger" data-dismiss="modal" aria-hidden="true" id="dismissForgotPass"><i class="fa fa-remove"></i> Tutup</button>
				</div>
			</div>
		</div>
	</div>
	<!-- End Modal -->
</div>