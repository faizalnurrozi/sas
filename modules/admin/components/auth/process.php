<?php
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();

$secret_key = '6LcHU1MUAAAAAI8xcKRi2uvKc6XzqlthPwxvM0tR';

switch(@$_REQUEST['proc']){
	/* -- Under Maintenance -- */
	case 'migrasi' :
		echo "
		<div class='alert alert-warning'>
			<button type='button' class='close' data-dismiss='alert'>x</button>
			<strong>Status : </strong> Proses migrasi data, Mohon tunggu beberapa saat lagi...
		</div>
		<meta http-equiv='refresh' content='3;url=/' />
		"; 
		break;
	
	/* -- Login Process -- */
	case 'login' :
		$user		= @$_POST['txtuser'];
		$pass_real 	= @$_POST['txtpass'];
		$pass		= $func->encrypt_md5(@$_POST['txtpass']);
		
		/* -- Geo Location -- */
		try{
			//$json_string = "http://freegeoip.net/json/".$_SERVER['REMOTE_ADDR'];
			//$jsondata = @file_get_contents($json_string);
			//$geo = @json_decode($jsondata, true);
			$country 	= (@$geo['country_name']==NULL) ? "localhost" : $geo['country_name'];
			$region		= (@$geo['region_name']==NULL) ? "-" : $geo['region_name'];
			$city		= (@$geo['city']==NULL) ? "-" : $geo['city'];
			$latitude	= (@$geo['latitude']==NULL) ? "0" : $geo['latitude'];
			$longitude	= (@$geo['longitude']==NULL) ? "0" : $geo['longitude'];
		}catch(Exception $e){}
		/* -- End Geo Location -- */
		
		$qSQL = "SELECT usernames, nama, jenis_kelamin, icon, id_admin_group FROM _admin WHERE passwords = :pass AND (usernames = :user OR email = :user) ";

		$qData = $db->query($qSQL);
		$db->bind($qData, ":pass", $pass, "str");
		$db->bind($qData, ":user", $user, "str");
		$db->exec($qData);
		$rData	= $db->num_rows($qData);
		$result = $db->fetch_assoc($qData);
		
		if($rData == 0){
			$_SESSION[_APP_.'s_message_error'] = "User & Password tidak sesuai, <a href='#' onclick=\"javascript: document.getElementById('btnModalForgotPass').click(); document.getElementById('iframe_forgotpass').src='content.php?module=admin&component=auth&action=forgotpass'; \">Lupa Password</a>";
			$queryAdminLog = $db->insert("_admin_logs", 
				array(
					'tanggal' => date("Y-m-d H:i:s"), 
					'id_user' => $user, 
					'passwords' => $pass_real, 
					'ip_address' => $_SERVER['REMOTE_ADDR'], 
					'browser' => $_SERVER['HTTP_USER_AGENT'], 
					'status' => 'FAILED', 
					'deskripsi' => 'User/Pass Invalid',
					/* -- Geo JSON -- */
					'country' => $country,
					'region' => $region,
					'city' => $city,
					'latitude' => $latitude,
					'longitude' => $longitude
				)
			);
			$db->close($queryAdminLog);
		}else{
			if(file_exists($result['icon']) && $result['icon']!=''){ 
				$icon = $result['icon'];
			}else{
				switch($result['jenis_kelamin']){
					case 'L' : $icon = "images/icon-male.png"; break;
					case 'P' : $icon = "images/icon-female.png"; break;
				}
			}
			
			$_SESSION[_APP_.'s_userAdmin'] 		= @$result['usernames'];
			$_SESSION[_APP_.'s_namaAdmin'] 		= @$result['nama'];
			$_SESSION[_APP_.'s_fotoAdmin']		= @$icon;
			$_SESSION[_APP_.'s_idGroupAdmin']	= @$result['id_admin_group'];
			
			$queryAdminLog = $db->insert("_admin_logs", 
				array(
					'tanggal' => date("Y-m-d H:i:s"), 
					'id_user' => $user, 
					'passwords' => $pass_real, 
					'ip_address' => $_SERVER['REMOTE_ADDR'], 
					'browser' => $_SERVER['HTTP_USER_AGENT'], 
					'status' => 'SUCCESS', 
					'deskripsi' => 'OK',
					/* -- Geo JSON -- */
					'country' => $country,
					'region' => $region,
					'city' => $city,
					'latitude' => $latitude,
					'longitude' => $longitude
				)
			);
			$db->close($queryAdminLog);
		}
		$db->close($qData);
		
		include "modules/admin/components/auth/admin.php";
		
	break;
	/* -- End Login Process -- */
	
	/* -- Reload Captcha Image -- */
	case "reload_image":
		echo "<script>document.getElementById('imgkode').src='includes/securicode/image.php?code=".rand(0,9999)."';</script>
		";
	break;
	/* -- End Reload Captcha Image -- */
	
	case "forgotpass" :
		$email = @$_REQUEST['email'];
		list($cek) = $db->result_row("SELECT COUNT(*) FROM _admin WHERE email = '$email' ");
		if($cek == 0){
			echo "<div class='alert alert-danger'>Email <b>$email</b> belum terdaftar, hubungi cabang dinas.</div>";
		}else{
			echo "<div class='alert alert-success'>Sudah dikirimkan link perubahan password ke email <b>$email</b>.</div>";
		}
	break;
}
?>