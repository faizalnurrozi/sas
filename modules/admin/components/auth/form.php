<form name="form_login" action="javascript: void(0);" method="post">
	<div class="row">
		<div class="col-xs-12">
			<div class="form-group has-feedback has-username">
				<input type="text" class="form-control" name="txtuser" id="txtuser" onkeyup="javascript: if(event.keyCode==13){ document.getElementById('txtpass').focus(); }" autocomplete="off" placeholder="Username..." onclick="javascript: this.select();" />
				<span class="glyphicon glyphicon-user form-control-feedback"></span>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="form-group has-feedback has-password">
				<input type="password" class="form-control" name="txtpass" id="txtpass" onkeyup="javascript: if(event.keyCode==13){  document.getElementById('btn-login').click(); }" placeholder="Password..." />
				<span class="glyphicon glyphicon-eye-open form-control-feedback eye-open" style="cursor:pointer; pointer-events: all;" onclick=" $('.eye-open').addClass('hide'); $('.eye-close').removeClass('hide'); document.getElementById('txtpass').type='text'; "></span>
				<span class="glyphicon glyphicon-eye-close form-control-feedback eye-close hide" style="margin-top:10px; cursor:pointer; pointer-events: all;" onclick=" $('.eye-open').removeClass('hide'); $('.eye-close').addClass('hide'); document.getElementById('txtpass').type='password'; " ></span>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12">
			<button type="button" id="btn-login" class="btn btn-primary btn-block btn-flat" onclick="javascript: 
			var obj = document.form_login;
			var err = '';
			if(obj.txtuser.value==''){ $('.has-username').addClass('has-error').focus(); err+='<li>Username tidak boleh kosong.</li>'; }
			if(obj.txtpass.value==''){ $('.has-password').addClass('has-error').focus(); err+='<li>Password tidak boleh kosong.</li>'; }
			if(err==''){
				obj.action='content.php?module=admin&component=auth&action=process&proc=login';
				obj.submit();
			}else{ 
				$('#Modal').click(); $('#error-text').html(err);
			} " ><i class="fa fa-sign-in"></i> Masuk</button>
		</div>
	</div>
</form>

<!-- Alert Validation Form -->
<input type="hidden" id="Modal" onclick="javascript: $('#s_alert').fadeIn(); $('#s_alert').delay(3000); $('#s_alert').fadeOut(500);" />
<div class="alert alert-danger" id="s_alert">
	<button type="button" class="close" onclick="javascript: $('#s_alert').fadeOut();">x</button>
	<strong>Warning : </strong> <div id="error-text" class="error-text"><ul></ul></div>
</div>
<!-- End Alert Validation Form -->