<?php 	
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();

if(isset($_SESSION[_APP_.'s_userAdmin'])){
	$_SESSION[_APP_.'s_adminPage'] = "module=admin&component=laporan_surat_keluar";
	
	/* User Akses Menu :
		3. Akses Semua
		2. Akses Hapus
		1. Akses Tambah/Edit
		0. Akses View
	*/
	unset($_SESSION[_APP_.'s_accessMenu']);
	if(@$_REQUEST['menu']!='') $_SESSION[_APP_.'s_menuPage'] = @$_REQUEST['menu'];
	list($access) = $db->result_row("SELECT access FROM _admin_menus_access WHERE id_admin_group = '".$_SESSION[_APP_.'s_idGroupAdmin']."' AND id_admin_menus = '".$_SESSION[_APP_.'s_menuPage']."' ");
	$_SESSION[_APP_.'s_accessMenu'] = $access;
	/* End User Akses Menu */
	
	?>
	
	<!-- Header & Breadcrumb -->
	<?php
	list($level, $menus, $icon, $link, $parent) = $db->result_row("SELECT level, nama, icon, link, id_admin_menus_parent FROM _admin_menus WHERE id_admin_menus = '".$_SESSION[_APP_.'s_menuPage']."' ");
	?>
	<section class="content-header">
		<h1><?php echo $menus; ?></h1>
		<ol class="breadcrumb">
		<?php
		$menux[] = "<li class='active'><a style='text-decoration:none;'>".stripslashes($icon)." ".str_replace(" ", "&nbsp;", $menus)."</a></li>";
		for($i=($level-1); $i>=0; $i--){
			list($levelx, $menusx, $iconx, $linkx, $parentx) = $db->result_row("SELECT level, nama, icon, link, id_admin_menus_parent FROM _admin_menus WHERE id_admin_menus = '$parent' AND level = '$i' ");
			$menux[] = "<li><a style='text-decoration:none;cursor:pointer;' onclick=\"".stripslashes($linkx)."\">".stripslashes($iconx)." ".str_replace(" ", "&nbsp;", $menusx)."</a></li>";
			$parent = $parentx;
		}
		$cmenu = count($menux);
		for($a=$cmenu; $a>=0; $a--){
			echo $menux[$a];
		}
		?>
		</ol>
	</section>
	<!-- End Header & Breadcrumb -->
		
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<iframe name="iframe_laporan_surat_keluar" id="iframe_laporan_surat_keluar" width="98%" height="400" frameborder="0" src="content.php?module=admin&component=laporan_surat_keluar&action=add&id=<?php echo $_REQUEST['id']; ?>" style="margin-top: 20px;"></iframe>
				</div>
			</div>
		</div>
	</section>
	
	<input type="hidden" data-toggle="modal" data-target="#myModalChoose" id="bntModalChoose" />
	<div class="modal fade" id="myModalChoose" data-backdrop="static">
		<div class="modal-dialog modal-md">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Silahkan Pilih</h4>
				</div>
				<div class="modal-body">
					<iframe name="iframe_choose" id="iframe_choose" width="98%" height="400" frameborder="0"></iframe>
				</div>
				<div class="modal-footer">
					<button class="btn btn-danger" data-dismiss="modal" aria-hidden="true" id="dismissChoode"><i class="fa fa-remove"></i> Batal</button>
				</div>
			</div>
		</div>
	</div>
<?php 
}else{
	include "modules/admin/components/auth/timeout.php";
}
?>