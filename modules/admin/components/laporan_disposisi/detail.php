<?php
	if(@$_REQUEST['ajax'] == 'true'){
		include "globals/config.php";
		include "globals/functions.php";	
		$db = new Database();
		$func = new Functions();
	}

	$tanggal_awal 	= substr($func->explode_date(@$_REQUEST['tanggal_awal']),0,10);
	$tanggal_akhir 	= substr($func->explode_date(@$_REQUEST['tanggal_akhir']),0,10);

	$query_disposisi = $db->sql("SELECT A.*, B.nip, B.catatan, C.nama AS tujuan_disposisi FROM _surat_masuk AS A INNER JOIN _disposisi AS B ON(A.id_surat_masuk = B.id_surat_masuk) INNER JOIN _karyawan AS C ON(B.nip = C.nip) WHERE A.tanggal_terima BETWEEN '$tanggal_awal' AND '$tanggal_akhir'");
	$jumlah_data = $query_disposisi->rowCount();
?>
<table class="table-bordered table-hover table-striped detail">
	<thead>
		<tr>
			<th width="1%">No.</th>
			<th>No. Agenda</th>
			<th>No. Surat</th>
			<th>Tgl. Terima</th>
			<th>Pengirim</th>
			<th>Perihal</th>
			<th>Tujuan Disposisi</th>
			<th>Isi Disposisi</th>
		</tr>
	</thead>
	
	<tbody>
		<?php
			if($jumlah_data == 0){
				echo "<tr>";
				echo "	<td align='center' colspan='8'><i>Data kosong.</i></td>";
				echo "</tr>";
			}else{
				$no = 1;
				while($result_disposisi = $db->fetch_assoc($query_disposisi)){
					echo "<tr>";
					echo "	<td align='center'>$no.</td>";
					echo "	<td>$result_disposisi[no_agenda]</td>";
					echo "	<td>$result_disposisi[no_surat]</td>";
					echo "	<td>".$func->report_date($result_disposisi['tanggal_terima'])." M (".$func->report_date_hijriah($result_disposisi['tanggal_terima'])." H)</td>";
					echo "	<td>$result_disposisi[sumber]</td>";
					echo "	<td>$result_disposisi[perihal]</td>";
					echo "	<td>$result_disposisi[tujuan_disposisi]</td>";
					echo "	<td>$result_disposisi[catatan]</td>";
					echo "</tr>";

					$no++;
				}
			}
		?>
	</tbody>
</table>