<?php 	
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();
$msc = microtime(true);

if(isset($_SESSION[_APP_.'s_userAdmin'])){
	$_SESSION[_APP_.'s_adminPage'] = "module=admin&component=admin_traffic&add=false";
	
	/* User Akses Menu :
		3. Akses Semua
		2. Akses Hapus
		1. Akses Tambah/Edit
		0. Akses View
	*/
	unset($_SESSION[_APP_.'s_accessMenu']);
	if(@$_REQUEST['menu']!='') $_SESSION[_APP_.'s_menuPage'] = $_REQUEST['menu'];
	
	$queryAccess = $db->sql("SELECT access FROM _admin_menus_access WHERE id_admin_group = '".$_SESSION[_APP_.'s_idGroupAdmin']."' AND id_admin_menus = '".$_SESSION[_APP_.'s_menuPage']."' ");
	$resultAccess = $db->fetch_assoc($queryAccess);
	$access = $resultAccess['access'];
	$_SESSION[_APP_.'s_accessMenu'] = $access;
	/* End User Akses Menu */
	
	?>
	
	<!-- Header & Breadcrumb -->
	<?php
	$queryMenu = $db->sql("SELECT level, nama, icon, link, id_admin_menus_parent FROM _admin_menus WHERE id_admin_menus = '".$_SESSION[_APP_.'s_menuPage']."'");
	$resultMenu = $db->fetch_assoc($queryMenu);
	$level 	= $resultMenu['level'];
	$menus 	= $resultMenu['nama'];
	$icon 	= $resultMenu['icon'];
	$link 	= $resultMenu['link'];
	$link 	= $resultMenu['link'];
	$parent	= $resultMenu['id_admin_menus_parent'];
	?>
	<section class="content-header">
		<h1><?php echo $menus; ?></h1>
		<ol class="breadcrumb">
		<?php
		$menux[] = "<li class='active'><a style='text-decoration:none;'>".stripslashes($icon)." ".str_replace(" ", "&nbsp;", $menus)."</a></li>";
		for($i=($level-1); $i>=0; $i--){
			$queryMenux = $db->sql("SELECT level, nama, icon, link, id_admin_menus_parent FROM _admin_menus WHERE id_admin_menus = '$parent' AND level = '$i'");
			$resultMenux = $db->fetch_assoc($queryMenux);
			$menusx 	= $resultMenux['nama'];
			$linkx 		= $resultMenux['link'];
			$iconx 		= $resultMenux['icon'];
			$menux[] = "<li><a style='text-decoration:none;cursor:pointer;' onclick=\"".stripslashes($linkx)."\">".stripslashes($iconx)." ".str_replace(" ", "&nbsp;", $menusx)."</a></li>";
			$db->close($queryMenux);
		}
		$cmenu = count($menux);
		for($a=$cmenu; $a>=0; $a--){
			echo @$menux[$a];
		}
		?>
		</ol>
	</section>
	<!-- End Header & Breadcrumb -->
		
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header" style="text-align:center;">
						<img src="images/loadpage.gif" class="loader" id="loaderAdd" />
						<iframe name="iframe_admin_traffic" id="iframe_admin_traffic" src="content.php?module=admin&component=admin_traffic&action=view" width="98%" onload="javascript: autoResize('iframe_admin_traffic'); document.getElementById('loaderAdd').style.display='none';" scrolling="no" frameborder="0" scrolling="no"></iframe>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php 
}else{
	include "modules/admin/components/auth/timeout.php";
}
?>