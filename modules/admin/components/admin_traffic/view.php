<?php
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();
?>
<html>
	<head>
		<script type="text/javascript" src="includes/ajax.js"></script>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="includes/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="includes/bootstrap/css/font-awesome.css">
		<link rel="stylesheet" href="includes/dist/css/ionicons.min.css">
		<link rel="stylesheet" href="includes/dist/css/AdminLTE.css">
		<!--<link rel="stylesheet" href="includes/dist/css/skins/_all-skins.min.css">-->

		<script src="includes/plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script src="includes/bootstrap/js/bootstrap.min.js"></script>
		<script src="includes/dist/js/app.min.js"></script>
		
		<!--<script type="text/javascript" src="includes/jquery.min.js"></script>-->
		<script type="text/javascript" src="includes/jqplot/jquery.jqplot.js"></script>
		<script type="text/javascript" src="includes/jqplot/jqplot.barRenderer.js"></script>
		<script type="text/javascript" src="includes/jqplot/jqplot.pieRenderer.js"></script>
		<script type="text/javascript" src="includes/jqplot/jqplot.categoryAxisRenderer.js"></script>
		<script type="text/javascript" src="includes/jqplot/jqplot.pointLabels.js"></script>
		<script type="text/javascript" src="includes/jqplot/jqplot.logAxisRenderer.js"></script>
		<script type="text/javascript" src="includes/jqplot/jqplot.canvasAxisLabelRenderer.js"></script>
		<script type="text/javascript" src="includes/jqplot/jqplot.canvasAxisTickRenderer.js"></script>
		<link rel="stylesheet" type="text/css" href="includes/jqplot/jquery.jqplot.css" />
		
		<link href="includes/popup-calendar/dhtmlgoodies_calendar.css" rel="stylesheet">
		<script src="includes/popup-calendar/dhtmlgoodies_calendar.js"></script>	
		
	</head>
	<body>
		<form method="POST" action="javascript: void(null);">
			<div class="row">
				<div class="form-group col-xs-12">
					<?php
					if(@$_REQUEST['tglawal']=="") $tglawal = "01/".date("m/Y"); else $tglawal = $_REQUEST['tglawal'];
					if(@$_REQUEST['tglakhir']=="") $tglakhir = date("d/m/Y"); else $tglakhir = $_REQUEST['tglakhir'];
					?>
					<label>Traffic User (Tgl. Mulai - Sampai) : </label><br/>
					<input type="text" name="txttanggalawal" id="txttanggalawal" class="input-sm col-sm" placeholder="Tanggal Awal..." onclick="javascript: displayCalendar(document.getElementById('txttanggalawal'), 'dd/mm/yyyy', this);" readonly value="<?php echo @$tglawal; ?>" style="width:100px;" />
					<input type="text" name="txttanggalakhir" id="txttanggalakhir" class="input-sm col-sm" placeholder="Tanggal Akhir..." onclick="javascript: displayCalendar(document.getElementById('txttanggalakhir'), 'dd/mm/yyyy', this);" readonly value="<?php echo @$tglakhir; ?>" style="width:100px;" />
					<button class="btn btn-sm btn-warning" onclick="javascript: window.location.href='content.php?module=admin&component=admin_traffic&action=view&tglawal='+document.getElementById('txttanggalawal').value+'&tglakhir='+document.getElementById('txttanggalakhir').value; "><i class="fa fa-search"></i> View Data</button>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-xs-12">
					<h4>Grafik Traffic User Periode : <?php echo @$tglawal; ?> - <?php echo @$tglakhir; ?></h4>
					<div class="box">
						<div class="box-body table-responsive">
							<div id="chart1" style="width:100%; height:300px;"></div>	
							<script class="code" type="text/javascript">
								$(document).ready(function(){
						        $.jqplot.config.enablePlugins = true;
						        var s1 = [
								<?php
								$start = strtotime($func->explode_date(@$tglawal));
								$end = strtotime($func->explode_date(@$tglakhir));
								// start and end are seconds, so I convert it to days 
								$diff = ($end - $start) / 86400; 
								for ($i=0; $i<=$diff; $i++) {
									// just multiply 86400 and add it to $start
									// using strtotime('+1 day' ...) looks nice but is expensive.
									// you could also have a cumulative value, but this was quicker
									// to type
									$date = $start + ($i * 86400); 
									$tgl[$i] = date('d', $date);
									$tgl_bln_thn[$i] = date('Y-m-d', $date);
								}
								
								$n=0; $x=count($tgl_bln_thn);
								foreach($tgl_bln_thn as $t){
									$qData = $db->sql("SELECT DISTINCT(id_user) FROM _admin_logs WHERE DATE_FORMAT(tanggal, '%Y-%m-%d') = '$t' ");
									$jumlah = $db->num_rows($qData);
									$db->close($qData);
									echo $jumlah;
									if($n<$x) echo ", ";
									$n++;
								}
								?>
								];
								var s2 = [
								<?php
								$start = strtotime($func->explode_date(@$tglawal));
								$end = strtotime($func->explode_date(@$tglakhir));
								// start and end are seconds, so I convert it to days 
								$diff = ($end - $start) / 86400; 
								for ($i=0; $i<=$diff; $i++) {
									// just multiply 86400 and add it to $start
									// using strtotime('+1 day' ...) looks nice but is expensive.
									// you could also have a cumulative value, but this was quicker
									// to type
									$date = $start + ($i * 86400); 
									$tgl[$i] = date('d', $date);
									$tgl_bln_thn[$i] = date('Y-m-d', $date);
								}
								
								$n=0; $x=count($tgl_bln_thn);
								foreach($tgl_bln_thn as $t){
									$qData = $db->sql("SELECT DISTINCT(id_user) FROM _admin_logs WHERE DATE_FORMAT(tanggal, '%Y-%m-%d') = '$t' AND status = 'SUCCESS' ");
									$jumlah = $db->num_rows($qData);
									$db->close($qData);
									echo $jumlah;
									if($n<$x) echo ", ";
									$n++;
								}
								?>
								];
						        var ticks = [
								<?php
								$n=0; $x=count($tgl);
								foreach($tgl as $t){
									echo "'".$t."'";
									if($n<$x) echo ", ";
									$n++;
								}
								?>
								];
						         
						        plot1 = $.jqplot('chart1', [s1, s2], {
						            // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
						            animate: !$.jqplot.use_excanvas,
						            seriesDefaults:{
						                renderer:$.jqplot.BarRenderer,
						                pointLabels: { show: true }
						            },
									legend: {
										show: true,
										location: 'n',
										placement: 'insideGrid',
										labels: ['User Login','User Login Success']
									}, 
						            axes: {
						                xaxis: {
						                    renderer: $.jqplot.CategoryAxisRenderer,
						                    ticks: ticks,
											label: 'Tanggal'
						                }
						            },
						            highlighter: { show: false }
						        });
						     
						        $('#chart1').bind('jqplotDataClick', 
						            function (ev, seriesIndex, pointIndex, data) {
						                //$('#info1').html('series: '+seriesIndex+', point: '+pointIndex+', data: '+data);
						            }
						        );
						    });
							</script>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-xs-12">
					<h4>Grafik Traffic Aktifitas User Periode : <?php echo @$tglawal; ?> - <?php echo @$tglakhir; ?></h4>
					<div class="box">
						<div class="box-body table-responsive">
							<div id="chart3" style="width:100%; height:300px;"></div>	
							<script class="code" type="text/javascript">
								$(document).ready(function(){
						        $.jqplot.config.enablePlugins = true;
						        var s1 = [
								<?php
								$start = strtotime($func->explode_date(@$tglawal));
								$end = strtotime($func->explode_date(@$tglakhir));
								// start and end are seconds, so I convert it to days 
								$diff = ($end - $start) / 86400; 
								for ($i=0; $i<=$diff; $i++) {
									// just multiply 86400 and add it to $start
									// using strtotime('+1 day' ...) looks nice but is expensive.
									// you could also have a cumulative value, but this was quicker
									// to type
									$date = $start + ($i * 86400); 
									$tgl[$i] = date('d', $date);
									$tgl_bln_thn[$i] = date('Y-m-d', $date);
								}
								
								$n=0; $x=count($tgl_bln_thn);
								foreach($tgl_bln_thn as $t){
									list($jumlah) = $db->result_row("SELECT COUNT(*) AS jumlah FROM _activity_logs WHERE DATE_FORMAT(action_date, '%Y-%m-%d') = '$t' ");
									echo $jumlah;
									if($n<$x) echo ", ";
									$n++;
								}
								?>
								];
						        var ticks = [
								<?php
								$n=0; $x=count($tgl);
								foreach($tgl as $t){
									echo "'".$t."'";
									if($n<$x) echo ", ";
									$n++;
								}
								?>
								];
						         
						        plot1 = $.jqplot('chart3', [s1], {
						            // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
						            animate: !$.jqplot.use_excanvas,
									seriesColors:['#dede00'],
						            seriesDefaults:{
						                renderer:$.jqplot.BarRenderer,
						                pointLabels: { show: true }
						            },
						            axes: {
						                xaxis: {
						                    renderer: $.jqplot.CategoryAxisRenderer,
						                    ticks: ticks,
											label: 'Tanggal'
						                }
						            },
						            highlighter: { show: false }
						        });
						     
						        $('#chart3').bind('jqplotDataClick', 
						            function (ev, seriesIndex, pointIndex, data) {
						                //$('#info1').html('series: '+seriesIndex+', point: '+pointIndex+', data: '+data);
						            }
						        );
						    });
							</script>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-xs-12">
					<h4>Grafik Rata - rata Traffic Aktifitas User (Per Detik) Periode : <?php echo @$tglawal; ?> - <?php echo $tglakhir; ?></h4>
					<div class="box">
						<div class="box-body table-responsive">
							<div id="chart4" style="width:100%; height:300px;"></div>	
							<script class="code" type="text/javascript">
								$(document).ready(function(){
						        $.jqplot.config.enablePlugins = true;
						        var s1 = [
								<?php
								$start = strtotime($func->explode_date($tglawal));
								$end = strtotime($func->explode_date($tglakhir));
								// start and end are seconds, so I convert it to days 
								$diff = ($end - $start) / 86400; 
								for ($i=0; $i<=$diff; $i++) {
									// just multiply 86400 and add it to $start
									// using strtotime('+1 day' ...) looks nice but is expensive.
									// you could also have a cumulative value, but this was quicker
									// to type
									$date = $start + ($i * 86400); 
									$tgl[$i] = date('d', $date);
									$tgl_bln_thn[$i] = date('Y-m-d', $date);
								}
								
								$n=0; $x=count($tgl_bln_thn);
								foreach($tgl_bln_thn as $t){
									list($jumlah) = $db->result_row("SELECT MAX(A.jumlah) FROM (SELECT COUNT(*) AS jumlah, DATE_FORMAT(action_date, '%d-%m-%Y %H:%i:%s') AS jam FROM _activity_logs WHERE DATE_FORMAT(action_date, '%Y-%m-%d') = '$t' GROUP BY DATE_FORMAT(action_date, '%Y-%m-%d %H:%i:%s')) AS A ");
									echo ($jumlah==NULL) ? 0 : $jumlah;
									if($n<$x) echo ", ";
									$n++;
								}
								?>
								];
						        var ticks = [
								<?php
								$n=0; $x=count($tgl);
								foreach($tgl as $t){
									echo "'".$t."'";
									if($n<$x) echo ", ";
									$n++;
								}
								?>
								];
						         
						        plot1 = $.jqplot('chart4', [s1], {
						            // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
						            animate: !$.jqplot.use_excanvas,
									seriesColors:['#dedede'],
						            seriesDefaults:{
						                renderer:$.jqplot.BarRenderer,
						                pointLabels: { show: true }
						            },
						            axes: {
						                xaxis: {
						                    renderer: $.jqplot.CategoryAxisRenderer,
						                    ticks: ticks,
											label: 'Tanggal'
						                }
						            },
						            highlighter: { show: false }
						        });
						     
						        $('#chart4').bind('jqplotDataClick', 
						            function (ev, seriesIndex, pointIndex, data) {
						                //$('#info1').html('series: '+seriesIndex+', point: '+pointIndex+', data: '+data);
						            }
						        );
						    });
							</script>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-xs-12">
					<h4>Traffic User Rank Periode : <?php echo @$tglawal; ?> - <?php echo @$tglakhir; ?></h4>
					<div class="box">
						<div class="box-body table-responsive">
							<table class="table table-bordered" style="font-size:12px;">
								<thead>
								<tr class="table-list-header" width="100%">
									<th width="20%">User</th>
									<th width="40">Nama</th>
									<th width="30">Instansi</th>
									<th width="10%">Total&nbsp;Logs</th>
								</tr>
								</thead>
								<tbody>
								<?php
								$qData = $db->sql("
								SELECT 	A.id_user, (SELECT nama FROM _admin WHERE usernames = A.id_user) AS admin, 
										(SELECT C.nama FROM _admin AS B INNER JOIN _cabang AS C ON (B.id_cabang=C.id_cabang) WHERE B.usernames = A.id_user) AS cabang, 
										(SELECT C.nama FROM _admin AS B INNER JOIN _sekolah AS C ON (B.id_sekolah=C.id_sekolah) WHERE B.usernames = A.id_user) AS sekolah, 
                                        (SELECT D.nama FROM _admin AS B INNER JOIN _gtk AS C ON (B.id_gtk=C.id_gtk) INNER JOIN _sekolah AS D ON (C.id_sekolah=D.id_sekolah) WHERE B.usernames = A.id_user) AS gtk_sekolah, COUNT(id) AS aktifitas 
								FROM _admin_logs AS A 
								WHERE A.tanggal BETWEEN  '".substr($func->explode_date(@$tglawal), 0, 10)." 00:00:00' AND '".substr($func->explode_date(@$tglakhir), 0, 10)." 23:59:59'
								GROUP BY A.id_user 
								ORDER BY aktifitas DESC ");
								/*$qData = $db->sql("SELECT * FROM vw_logs WHERE tanggal BETWEEN  '".substr($func->explode_date($tglawal), 0, 10)." 00:00:00' AND '".substr($func->explode_date($tglakhir), 0, 10)." 23:59:59' ");*/
								while($result = $db->fetch_assoc($qData)){
									$instansi = $result['cabang'].$result['sekolah'].$result['gtk_sekolah'];
								?>
								<tr class="table-list-row">
									<td><?php echo $result['id_user']; ?></td>
									<td align="left"><?php echo ($result['admin']=='') ? "-" : $result['admin']; ?></td>
									<td align="left"><?php echo ($instansi=='') ? "-" : $instansi; ?></td>
									<td align="right"><?php echo $result['aktifitas']; ?></td>
								</tr>
								<?php 
								}
								$db->close($qData);
								?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</form>
	</body>
</html>