<?php
if(@$_REQUEST['ajax']=='true'){
	session_start();
	include "globals/config.php";
	include "globals/functions.php";
	$db = new Database();
	$func = new Functions();
}
$limit = _LIMIT_;
?>

<!-- Alert Process -->
<?php if(isset($_SESSION[_APP_.'s_message_info'])){ ?>
<div class="box-body table-responsive">
<div class="alert alert-info">
	<button type="button" class="close" data-dismiss="alert">x</button>
	<strong>Status : </strong> <?php echo $_SESSION[_APP_.'s_message_info']; unset($_SESSION[_APP_.'s_message_info']); ?>
</div>
</div>
<?php } ?>
<?php if(isset($_SESSION[_APP_.'s_message_error'])){ ?>
<div class="box-body table-responsive">
<div class="alert alert-danger">
	<button type="button" class="close" data-dismiss="alert">x</button>
	<strong>Error : </strong> <?php echo $_SESSION[_APP_.'s_message_error']; unset($_SESSION[_APP_.'s_message_error']); ?>
</div>
</div>
<?php } ?>
<!-- Alert Process -->

<?php
/*Sorting*/
if(@$_POST['sort']=='reset'){
	$_SESSION[_APP_.'s_field_prospek'] = "created_date";
	$_SESSION[_APP_.'s_sort_prospek'] = "DESC";
	$iconsort = "<i class='fa fa-fw fa-caret-up'></i>";
}
switch(@$_POST['field']){
	case 'prospek_tanggal' : $_SESSION[_APP_.'s_field_prospek'] = "prospek_tanggal"; break;
	case 'prospek_nama' : $_SESSION[_APP_.'s_field_prospek'] = "prospek_nama"; break;
	case 'prospek_telepon' : $_SESSION[_APP_.'s_field_prospek'] = "prospek_telepon"; break;
	case 'prospek_tipe' : $_SESSION[_APP_.'s_field_prospek'] = "prospek_tipe"; break;
	default : 
		if(!isset($_SESSION[_APP_.'s_field_prospek'])){
			$_SESSION[_APP_.'s_field_prospek'] = "prospek_tanggal";
		}
		break;
}
if(!isset($_SESSION[_APP_.'s_sort_prospek'])){
	$_SESSION[_APP_.'s_sort_prospek'] = "ASC";
	$iconsort = "<i class='fa fa-fw fa-caret-down'></i>";
}else{
	switch(@$_POST['act']){
		case 'sort' :
			if($_SESSION[_APP_.'s_sort_prospek'] == "ASC"){
				$_SESSION[_APP_.'s_sort_prospek'] = "DESC";
				$iconsort = "<i class='fa fa-fw fa-caret-down'></i>";
			}else if($_SESSION[_APP_.'s_sort_prospek'] == "DESC"){
				$_SESSION[_APP_.'s_sort_prospek'] = "ASC";
				$iconsort = "<i class='fa fa-fw fa-caret-up'></i>";
			}
			break;
		case 'paging' :
			if($_SESSION[_APP_.'s_sort_prospek'] == "ASC"){
				$iconsort = "<i class='fa fa-fw fa-caret-up'></i>";
			}else if($_SESSION[_APP_.'s_sort_prospek'] == "DESC"){
				$iconsort = "<i class='fa fa-fw fa-caret-down'></i>";
			}
			break;
	}
}
/*End Sorting*/

list($jabatan_sales, $jabatan_spv) = $db->result_row("SELECT jabatan_sales, jabatan_supervisor FROM _setting");

if(@$_REQUEST['start']=='') $start = 0; else $start = @$_REQUEST['start'];

if(!isset($msc)) $msc = microtime(true);
//$db->beginTransaction();
$qSQL 	= "
SELECT 	*  FROM _prospek 
WHERE 	id_karyawan_sales = '".$_SESSION[_APP_.'s_idKaryawan']."' AND 
		(prospek_nama LIKE :keyword OR prospek_telepon LIKE :keyword OR prospek_tipe LIKE :keyword) 
ORDER BY ".$_SESSION[_APP_.'s_field_prospek']." ".$_SESSION[_APP_.'s_sort_prospek'];
$hqSQL 	= $db->query($qSQL);
$db->bind($hqSQL, ":keyword", "%".$keyword."%", "str");
$db->exec($hqSQL);
$totalData = $db->num_rows($hqSQL);
$qSQL	.= " LIMIT $start, $limit"; //echo $qSQL;
$hqSQL = $db->query($qSQL);
$db->bind($hqSQL, ":keyword", "%".$keyword."%", "str");
$db->exec($hqSQL);
//$db->commit();
$totalLimit = $db->num_rows($hqSQL);
?>

<div class="box-body table-responsive">
	<table class="table table-bordered table-hover table-striped">
		<thead>
			<tr>
				<th width="1%">No.</th>
				<th width="13%" class="sort" onclick="javascript: sendRequest('content.php', 'module=admin&component=prospek&action=list&ajax=true&act=sort&field=prospek_nama', 'list', 'div');">Nama&nbsp;<?php if($_SESSION[_APP_.'s_field_prospek'] == 'prospek_nama') echo $iconsort; ?></th>
				<th class="sort" onclick="javascript: sendRequest('content.php', 'module=admin&component=prospek&action=list&ajax=true&act=sort&field=prospek_telepon', 'list', 'div');">Telp./HP&nbsp;<?php if($_SESSION[_APP_.'s_field_prospek'] == 'prospek_telepon') echo $iconsort; ?></th>
				<th class="sort" onclick="javascript: sendRequest('content.php', 'module=admin&component=prospek&action=list&ajax=true&act=sort&field=prospek_tipe', 'list', 'div');">Tipe&nbsp;Cust.<?php if($_SESSION[_APP_.'s_field_prospek'] == 'prospek_tipe') echo $iconsort; ?></th>
				<th class="sort">Sales&nbsp;</th>
				<th class="sort">Status&nbsp;</th>
				<th class="sort">Catatan&nbsp;</th>
			</tr>
		</thead>
		
		<tbody>
			<?php
			if($totalData=='0'){
				echo "<tr><td colspan='7' align='center'>Data belum ada</td></tr>";
				$db->close($hqSQL);
			}else{
				$no = $start+1;
				while($hasil = $db->fetch_assoc($hqSQL)){
					echo "<tr class='table-list-row'>";
					echo "<td align='center'>".$no.".</td>";
					echo "<td align='left'>".$func->highlight($hasil['prospek_nama'], $keyword)."</td>";
					echo "<td align='left'>".$func->highlight($hasil['prospek_telepon'], $keyword)."</td>";
					echo "<td align='left'>".$func->highlight($hasil['prospek_tipe'], $keyword)."</td>";
					list($namaSales) = $db->result_row("SELECT nama FROM _karyawan WHERE id_karyawan = '$hasil[id_karyawan_sales]' ");
					echo "<td align='left'>$namaSales</td>";
					
					/* -- Status Prospek -- */
					switch($hasil['prospek_tipe']){
						case 'HOT-A' :
						case 'HOT-B' :
							switch($hasil['prospek_approval']){
								case 'APPLY' : $status = "<label class='label label-info'><i class='fa fa-info'></i> PROSPEK - APPLY</label>";	break;
								case 'VALID' : $status = "<label class='label label-success'><i class='fa fa-thumbs-up'></i> PROSPEK - VALID</label>"; break;
								case 'INVALID' : $status = "<label class='label label-danger'><i class='fa fa-thumbs-down'></i> PROSPEK - INVALID</label>"; break;
							}
							if($hasil['status_batal']=='TRUE') $status = "<label class='label label-warning'><i class='fa fa-remove'></i> BATAL</label>";
							break;
						case 'SPK' :
							switch($hasil['spk_approval']){
								case 'APPLY' : $status = "<label class='label label-info'><i class='fa fa-info'></i> SPK - APPLY</label>"; break;
								case 'APPROVE' : $status = "<label class='label label-success'><i class='fa fa-thumbs-up'></i> SPK - APPROVED</label>"; break;
								case 'PENDING' : $status = "<label class='label label-warning'><i class='fa fa-thumbs-down'></i> SPK - PENDING</label>"; break;
							}
							break;
					}
					echo "<td align='left'>$status</td>";
					echo "<td align='left'>".$func->highlight($hasil['catatan'], $keyword)."</td>";					
					
					echo "</tr>";
					
					$no++;
				}
				$db->close($hqSQL);
			}
			?>
		</tbody>
	</table>
</div>
<div class="box-body">
	<div class="row">
		<div class="col-sm-5">
			<i>
				<?php 
				echo "Ditampilkan <b>".($totalLimit)."</b> sampai <b>".($start+$totalLimit)."</b> dari <b>$totalData</b> total data"; 
				?>
			</i>
		</div>
		<div class="col-sm-7">
			<ul class="pagination pagination-sm pull-right">
			<?php
			if($start != 0) echo "<li><a href='#' onclick=\"sendRequest('content.php','module=admin&component=prospek&action=list&ajax=true&start=".($start-$limit)."&keyword=$keyword', 'list', 'div');\">Prev</a></li>";
			$jumlahPage = $totalData/$limit;
			for($a=0;$a<$jumlahPage;$a++){
				$x = $a * $limit;
				if($start==$a*$limit){
					echo "<li class='active'><a href='#'>".($a+1)."</a></li>";
				}else{
					echo "<li><a href='#' onclick=\"sendRequest('content.php','module=admin&component=prospek&action=list&ajax=true&start=".($a*$limit)."&keyword=$keyword', 'list', 'div');\">".($a+1)."</a></li>";
				}
			}
			 if($start != @$x) echo "<li><a href='#' onclick=\"sendRequest('content.php','module=admin&component=prospek&action=list&ajax=true&start=".($start+$limit)."&keyword=$keyword', 'list', 'div');\">Next</a></li>";
			?>
			</ul>
		</div>
	</div>
</div>

<!-- Modals -->
<div class="modal fade" id="modal-view" data-backdrop="static">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">View</h4>
			</div>
			<div class="modal-body">
				<img src="images/ring-alt.gif" class="loader" id="loaderView" />
				<iframe name="iframe_view" id="iframe_view" width="98%" height="300" frameborder="0" onload="javascript: autoResize('iframe_view'); document.getElementById('loaderView').style.display='none';"></iframe>
			</div>
			<div class="modal-footer">
				<button class="btn btn-warning" data-dismiss="modal" aria-hidden="true" id="dismissView"><i class="fa fa-remove"></i> Tutup</button>
			</div>
		</div>
	</div>
</div>

<input type="hidden" id="id_delete" value="" />
<div class="modal fade" id="modal-delete" data-backdrop="static">
	<div class="modal-dialog modal-xs">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Pembatalan</h4>
			</div>
			<div class="modal-body">
				Apakah anda yakin ingin membatalkan ? Berikan Alasan :<br>
				<textarea name="txtcatatan" id="txtcatatan" placeholder="Alasan Pembatalan..." class="form-control"></textarea>
			</div>
			<div class="modal-footer">
				<button class="btn btn-default" data-dismiss="modal" aria-hidden="true" id="dismissDelete"><i class="fa fa-repeat"></i> Tutup</button>
				<button class="btn btn-danger" data-dismiss="modal" aria-hidden="true" onclick="javascript: sendRequest('content.php', 'module=admin&component=prospek&action=process&proc=batal&start=<?php echo $start; ?>&keyword=<?php echo $keyword; ?>&id='+document.getElementById('id_delete').value+'&catatan='+document.getElementById('txtcatatan').value, 'list', 'div');"><i class="fa fa-remove"></i> Proses</button>
			</div>
		</div>
	</div>
</div>
<!-- End of Modals -->