<?php
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();
?>
<html>
	<head>
		<meta charset="UTF-8">
		<script type="text/javascript" src="includes/ajax.js"></script>
		<!--<script type="text/javascript" src="includes/jquery-1.8.2.min.js"></script>-->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="includes/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="includes/bootstrap/css/font-awesome.css">
		<link rel="stylesheet" href="includes/dist/css/ionicons.min.css">
		<link rel="stylesheet" href="includes/dist/css/AdminLTE.css">
		<!--<link rel="stylesheet" href="includes/dist/css/skins/_all-skins.min.css">-->

		<script src="includes/plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script src="includes/bootstrap/js/bootstrap.min.js"></script>
		<script src="includes/dist/js/app.min.js"></script>
		
		<!-- Input-mask -->
		<script src="includes/plugins/select2/select2.full.min.js"></script>
		<script src="includes/plugins/input-mask/jquery.inputmask.js"></script>
		<script src="includes/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
		<script src="includes/plugins/input-mask/jquery.inputmask.extensions.js"></script>
		<!-- End of Input-mask -->
		
		<script src="includes/tinymce/tinymce.min.js"></script>
		<script>
			tinymce.init({
				selector:'.tinymce',
				plugins: [
					"table", 
					"advlist autolink lists link image charmap preview hr anchor pagebreak", 
					"searchreplace wordcount visualblocks visualchars code",
					"insertdatetime media nonbreaking table contextmenu directionality",
					"emoticons template paste textcolor colorpicker textpattern imagetools codesample"
				],
				toolbar: 'undo redo | bold italic sizeselect fontselect fontsizeselect | hr alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor backcolor',
				fontsize_formats: "8px 10px 11px 12px 13px 14px 18px 24px 36px",
			});
		</script>
		
		<script language="JavaScript">
		$(function () {
			$("[data-mask]").inputmask();
		});
		</script>
	</head>
	<body>
		<!-- Message Confirmation -->
		<?php if(isset($_SESSION[_APP_.'s_message_info'])){ ?>
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert" onclick="javascript: $('.alert').fadeOut();">x</button>
			<strong>Status : </strong> <?php echo $_SESSION[_APP_.'s_message_info']; unset($_SESSION[_APP_.'s_message_info']); ?>
		</div>
		<?php } ?>
		<?php if(isset($_SESSION[_APP_.'s_message_error'])){ ?>
		<div class="alert alert-error">
			<button type="button" class="close" data-dismiss="alert" onclick="javascript: $('.alert').fadeOut();">x</button>
			<strong>Error : </strong> <?php echo $_SESSION[_APP_.'s_message_error']; unset($_SESSION[_APP_.'s_message_error']); ?>
		</div>
		<?php } ?>
		<!-- End Message Confirmation -->
		
		<?php
		$qEdit = "SELECT * FROM _setting";
		$dataEdit = $db->sql($qEdit);
		$resultEdit = $db->fetch_assoc($dataEdit);
		$db->close($dataEdit);
		?>		
		<form name="form_setting" method="POST" action="javascript: void(null);" enctype="multipart/form-data">
			<input type="hidden" name="proc" id="proc" value="update" />
			
			<ul class="nav nav-tabs" id="myTab">
				<li class="active"><a href="#profil">Profil Instansi</a></li>
				<li><a href="#web">Web Properti</a></li>
				<li><a href="#template-salam">Template Salam</a></li>
			</ul>
			 
			<div class="tab-content">
				<div class="tab-pane active" id="profil">
					<table width="100%">
						<tr><td colspan="3">&nbsp;<input type="hidden" name="txtinstansiid" id="txtinstansiid" value="<?php echo $resultEdit['instansi_id']; ?>" /></td></tr>
						<tr>
							<td valign="top" width="30%">
								<label><b>Nama Instansi</b></label>
								<input type="text" name="txtinstansinama" id="txtinstansinama" class="form-control input-sm" value="<?php echo $resultEdit['instansi_nama']; ?>" />
							</td>
							<td valign="top">
								<label><b>Nama Instansi (Pendek)</b></label>
								<input type="text" name="txtinstansiperusahaan" id="txtinstansiperusahaan" value="<?php echo $resultEdit['instansi_nama_perusahaan']; ?>" class="form-control input-sm" />
							</td>
							<td valign="top" width="30%">
								<label><b>Alamat Instansi</b></label>
								<input type="text" name="txtinstansialamat" id="txtinstansialamat" value="<?php echo $resultEdit['instansi_alamat']; ?>" class="form-control input-sm" />
							</td>
						</tr>
						<tr><td colspan="3">&nbsp;</td></tr>
						<tr>
							<td valign="top">
								<label><b>Telepon Instansi</b></label>
								<input type="text" name="txtinstansitelepon" id="txtinstansitelepon" value="<?php echo $resultEdit['instansi_telepon']; ?>" class="form-control input-sm" />
							</td>
							<td valign="top">
								<label><b>Fax Instansi</b></label>
								<input type="text" name="txtinstansifax" id="txtinstansifax" value="<?php echo $resultEdit['instansi_fax']; ?>" class="form-control input-sm" />
							</td>
							<td valign="top">
								<label><b>Kota Instansi</b></label>
								<input type="text" name="txtinstansikota" id="txtinstansikota" value="<?php echo $resultEdit['instansi_kota']; ?>" class="form-control input-sm" />
							</td>
						</tr>
						<tr><td colspan="3">&nbsp;</td></tr>
						<tr>
							<td valign="top">
								<label><b>Logo Instansi</b></label><br/>
								<label><input type="checkbox" name="cblogo" id="cblogo" value="1" />&nbsp;Centang untuk ganti logo.</label>
								<input type="file" name="filelogo" id="filelogo" class="input-xlarge" /><br/>
								<?php
								if(file_exists($resultEdit['instansi_logo'])){
									echo "<img src='$resultEdit[instansi_logo]' style='max-width:100px;' />";
								}
								?>
							</td>
							<td valign="top">
								<label style="display: block;"><b>NIP - Nama Kepala Sekolah</b></label>
								<input type="text" name="txtisntansikepseknip" id="txtisntansikepseknip" value="<?php echo $resultEdit['instansi_kepsek_nip']; ?>" class="form-control input-sm" style="width: 150px; display: inline-block;" />
								<input type="text" name="txtisntansikepsek" id="txtisntansikepsek" value="<?php echo $resultEdit['instansi_kepsek']; ?>" class="form-control input-sm" style="width: 200px; display: inline-block;" />
							</td>
							<td valign="top">
								<label><b>TTD Kepala Sekolah</b></label><br/>
								<label><input type="checkbox" name="cbkepsekttd" id="cbkepsekttd" value="1" />&nbsp;Centang untuk ganti ttd kepala sekolah.</label>
								<input type="file" name="filekepsekttd" id="filekepsekttd" class="input-xlarge" /><br/>
								<?php
								if(file_exists($resultEdit['instansi_kepsek_ttd'])){
									echo "<img src='$resultEdit[instansi_kepsek_ttd]' style='max-width:100px;' />";
								}
								?>
							</td>
						</tr>
					</table>
				</div>
				<div class="tab-pane" id="web">
					<table width="100%">
						<tr><td colspan="3">&nbsp;</td></tr>
						<tr>
							<td valign="top" width="30%">
								<label><b>Web Title</b></label>
								<input type="text" name="txtwebtitle" id="txtwebtitle" class="form-control input-sm" value="<?php echo $resultEdit['web_title']; ?>" />
							</td>
							<td valign="top">
								<label><b>Web Header</b></label>
								<input type="text" name="txtwebheader" id="txtwebheader" value="<?php echo $resultEdit['web_header']; ?>" class="form-control input-sm" />
							</td>
							<td valign="top" width="30%">
								<label><b>Web Footer</b></label>
								<input type="text" name="txtwebfooter" id="txtwebfooter" value="<?php echo $resultEdit['web_footer']; ?>" class="form-control input-sm" />
							</td>
						</tr>
						<tr><td colspan="3">&nbsp;</td></tr>
						<tr>
							<td valign="top">
								<label><b>Web Favicon</b></label><br/>
								<label><input type="checkbox" name="cbfavicon" id="cbfavicon" value="1" />&nbsp;Centang untuk ganti logo.</label>
								<input type="file" name="filefavicon" id="filefavicon" class="input-xlarge" /><br/>
								<?php
								if(file_exists($resultEdit['web_favicon'])){
									echo "<img src='$resultEdit[web_favicon]' style='max-width:100px;' />";
								}
								?>
							</td>
							<td valign="top">&nbsp;</td>
							<td valign="top">
								<label><b>Web Background</b></label><br/>
								<label><input type="checkbox" name="cbbackground" id="cbbackground" value="1" />&nbsp;Centang untuk ganti background.</label>
								<input type="file" name="filebackground" id="filebackground" class="input-xlarge" /><br/>
								<?php
								if(file_exists($resultEdit['web_background'])){
									echo "<img src='$resultEdit[web_background]' style='max-width:100px;' />";
								}
								?>
							</td>
						</tr>
					</table>
				</div>
				<div class="tab-pane" id="template-salam">
					<table width="95%">
						<tr><td colspan="3">&nbsp;</td></tr>
						<tr>
							<td valign="top">
								<label><b>Salam Pembuka</b></label>
								<textarea name="txtsalampembuka" id="txtsalampembuka" class="form-control input-sm tinymce"><?php echo $resultEdit['salam_pembuka']; ?></textarea>
							</td>
						</tr>
						<tr><td colspan="3">&nbsp;</td></tr>
						<tr>
							<td valign="top">
								<label><b>Salam Penutup</b></label>
								<textarea name="txtsalampenutup" id="txtsalampenutup" class="form-control input-sm tinymce"><?php echo $resultEdit['salam_penutup']; ?></textarea>
							</td>
						</tr>
					</table>
				</div>
			</div>
			
			<script>
				$('#myTab a').click(function (e) {
					e.preventDefault();
					$(this).tab('show');
				})
			</script>
			
			<table width="100%">
				<tr>
					<td colspan="3"><hr/></td>
				</tr>
				<tr>
					<td colspan="3">
						<button class="btn btn-primary" onclick="javascript: var obj = document.form_setting; obj.action='content.php?module=admin&component=setting&action=process';"><i class="fa fa-save"></i> Simpan</button>
					</td>
				</tr>
			</table>
		</form>
		<!--
		<script src="includes/bootstrap/bootstrap.js"></script>
		<script type="text/javascript">
			$("[rel=tooltip]").tooltip();
			$(function() {
				$('.demo-cancel-click').click(function(){return false;});
			});
		</script>
		-->
		<div class="modal fade" id="myModal" data-backdrop="static">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Karyawan</h4>
					</div>
					<div class="modal-body">
						<iframe name="iframe_karyawan" id="iframe_karyawan" width="98%" frameborder="0" onload="javascript: autoResize('iframe_karyawan')"></iframe>
					</div>
					<div class="modal-footer">
						<button class="btn btn-danger btn-sm" data-dismiss="modal" aria-hidden="true" id="dismissButton"><i class="fa fa-remove"></i> Batal</button>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>