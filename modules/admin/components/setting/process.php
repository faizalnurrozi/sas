<?php 
include "globals/config.php";
include "globals/functions.php";

$db = new Database();
$func = new Functions();

switch(@$_REQUEST['proc']){
	/* -- Update Setting -- */
	case 'update' :
		$instansi_id		= @$_POST['txtinstansiid'];
		$instansi_nama		= @$_POST['txtinstansinama'];
		$instansi_perusahaan= @$_POST['txtinstansiperusahaan'];
		$instansi_alamat	= @$_POST['txtinstansialamat'];
		$instansi_telepon	= @$_POST['txtinstansitelepon'];
		$instansi_fax		= @$_POST['txtinstansifax'];
		$instansi_kota		= @$_POST['txtinstansikota'];
		$instansi_kepsek_nip= @$_POST['txtisntansikepseknip'];
		$instansi_kepsek 	= @$_POST['txtisntansikepsek'];
		$web_title			= @$_POST['txtwebtitle'];
		$web_header			= @$_POST['txtwebheader'];
		$web_footer			= @$_POST['txtwebfooter'];
		
		$cek_logo			= @$_POST['cblogo'];
		$cek_ttd			= @$_POST['cbkepsekttd'];
		$cek_favicon		= @$_POST['cbfavicon'];
		$cek_background		= @$_POST['cbbackground'];
		
		$salam_pembuka		= @$_POST['txtsalampembuka'];
		$salam_penutup		= @$_POST['txtsalampenutup'];
		
		if($cek_logo == '1'){
			list($foto)	= $db->result_row("SELECT instansi_logo FROM _setting");
			if(file_exists($foto) && $foto != ''){ unlink($foto); }			
			if($func->is_image(@$_FILES['filelogo'])==true){
				$asal	= @$_FILES['filelogo']['tmp_name'];
				$tujuan	= "images/".str_replace(' ', '-', @$_FILES['filelogo']['name']);
				move_uploaded_file($asal, $tujuan);
			}
		}
		
		if($cek_ttd == '1'){
			list($foto)	= $db->result_row("SELECT instansi_kepsek_ttd FROM _setting");
			if(file_exists($foto) && $foto != ''){ unlink($foto); }			
			if($func->is_image(@$_FILES['filekepsekttd'])==true){
				$asal	= @$_FILES['filekepsekttd']['tmp_name'];
				$instansi_kepsek_ttd	= "images/".str_replace(' ', '-', @$_FILES['filekepsekttd']['name']);
				move_uploaded_file($asal, $instansi_kepsek_ttd);
			}
		}
		
		if($cek_favicon == '1'){
			list($favicon) = $db->result_row("SELECT web_favicon FROM _setting");
			if(file_exists($favicon) && $favicon != ''){ unlink($favicon); }
			if($func->is_image(@$_FILES['filefavicon'])==true){
				$asal_fav	= @$_FILES['filefavicon']['tmp_name'];
				$tujuan_fav	= "images/fav-".str_replace(' ', '-', @$_FILES['filefavicon']['name']);
				move_uploaded_file($asal_fav, $tujuan_fav);
			}
		}
		
		if($cek_background == '1'){
			list($bg) = $db->result_row("SELECT web_background FROM _setting");
			if(file_exists($bg) && $bg != ''){ unlink($bg); }
			if($func->is_image(@$_FILES['filebackground'])==true){
				$asal_bg	= @$_FILES['filebackground']['tmp_name'];
				$tujuan_bg	= "images/bg-".str_replace(' ', '-', @$_FILES['filebackground']['name']);
				move_uploaded_file($asal_bg, $tujuan_bg);
			}
		}
		
		$qData = " 
			UPDATE _setting 
			SET	instansi_nama	= '$instansi_nama',
				instansi_nama_perusahaan = '$instansi_perusahaan',
				instansi_alamat	= '$instansi_alamat',
				instansi_telepon= '$instansi_telepon',
				instansi_fax	= '$instansi_fax', ";
		if($cek_logo == '1'){
			$qData .= " instansi_logo	= '$tujuan', ";
		}
		if($cek_ttd == '1'){
			$qData .= " instansi_kepsek_ttd	= '$instansi_kepsek_ttd', ";
		}
		if($cek_favicon == '1'){
			$qData .= " web_favicon	= '$tujuan_fav', ";
		}
		if($cek_background == '1'){
			$qData .= " web_background	= '$tujuan_bg', ";
		}
		$qData .= "	
				instansi_kota	= '$instansi_kota',
				instansi_kepsek_nip	= '$instansi_kepsek_nip',
				instansi_kepsek	= '$instansi_kepsek',
				web_title	= '$web_title',
				web_header	= '$web_header',
				web_footer	= '$web_footer',
				salam_pembuka = '$salam_pembuka',
				salam_penutup = '$salam_penutup'
				"; 
		$hqData = $db->sql($qData);
		if($hqData==true) $_SESSION[_APP_.'s_message_info'] = "Update data <b>General Setting</b> berhasil";
		else $_SESSION[_APP_.'s_message_error'] = "Update data <b>General Setting</b> gagal ";
		$db->close($hqData);
		
		/* ----- Activity Logs (Update) ------ */
		$func->activity_logs_update("_setting",
			array(
				'instansi_nama'		=>	$instansi_nama,
				'instansi_nama_perusahaan'	=>	$instansi_perusahaan,
				'instansi_alamat'	=>	$instansi_alamat,
				'instansi_kota'		=>	$instansi_kota,
				'instansi_telepon'	=>	$instansi_telepon,
				'instansi_fax'		=>	$instansi_fax,
				'web_title'			=>	$web_title,
				'web_header'		=>	$web_header,
				'web_footer'		=>	$web_footer
			),
			array('instansi_id' => $instansi_id), 'OR', $_SESSION[_APP_.'s_userAdmin']
		);
		/* ----- End Activity Logs (Update) ------ */
		
		echo "<meta http-equiv='refresh' content='0;url=content.php?module=admin&component=setting&action=add' />";
	break;
	/* -- End Keterangan Pengajuan -- */
}
?>