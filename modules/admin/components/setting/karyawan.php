<?php
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();
?>
<html>
	<head>
		<title>Data Karyawan</title>
		<script type="text/javascript" src="includes/ajax.js"></script>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="includes/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="includes/bootstrap/css/font-awesome.css">
		<link rel="stylesheet" href="includes/dist/css/ionicons.min.css">
		<link rel="stylesheet" href="includes/dist/css/AdminLTE.css">
		<link rel="stylesheet" href="includes/dist/css/skins/_all-skins.min.css">

		<script src="includes/plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script src="includes/bootstrap/js/bootstrap.min.js"></script>
		<script src="includes/dist/js/app.min.js"></script>
	</head>
	<body>
		<table class="table table-striped table-bordered">
			<tr>
				<th width="3%">No.</th>
				<th>NIP</th>
				<th>Nama</th>
				<th>Jabatan</th>
				<th width="10%">&nbsp;</th>
			</tr>
			<?php
			$hqData = $db->sql("SELECT A.nip, A.nama, B.nama FROM _karyawan AS A INNER JOIN _jabatan AS B ON (A.id_jabatan=B.id_jabatan) ORDER BY A.nama ASC");
			$rowData = $db->num_rows($hqData);
			if($rowData == 0){
				echo "<tr><td colspan='5' align='center'><i>Data belum ada</i></td></tr>";
			}else{
				$no=1;
				while(list($nip, $nama, $jabatan) = $db->fetch_assoc($hqData)){
					echo "<tr class='table-list-row'>";
					echo "<td align='center'>$no.</td>";
					echo "<td>$nip</td>";
					echo "<td>$nama</td>";
					echo "<td>$jabatan</td>";
					echo "
					<td align='center'>
						<button class='btn btn-sm btn-primary' onclick=\"javascript: window.parent.document.getElementById('hdmengetahui').value='$nip'; window.parent.document.getElementById('txtmengetahui').value='$nama';  window.parent.document.getElementById('dismissButton').click(); \"><i class='fa fa-check'></i></button>
					</td>";
					echo "</tr>";
					$no++;
				}
			}
			?>
		</table>
		<p>&nbsp;</p>
	</body>
</html>