<?php
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();
?>
<html>
	<head>
		<script type="text/javascript" src="includes/ajax.js"></script>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="includes/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="includes/bootstrap/css/font-awesome.css">
		<link rel="stylesheet" href="includes/dist/css/ionicons.min.css">
		<link rel="stylesheet" href="includes/dist/css/AdminLTE.css">
		<link rel="stylesheet" href="includes/dist/css/skins/_all-skins.min.css">

		<script src="includes/plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script src="includes/bootstrap/js/bootstrap.min.js"></script>
		<script src="includes/dist/js/app.min.js"></script>
	</head>
	<body>
		<?php
		if(@$_REQUEST['id'] != ''){
			$qEditAdmin = "SELECT * FROM _propinsi WHERE id_propinsi = '$_REQUEST[id]'";
			$dataEdit = $db->sql($qEditAdmin);
			$resultEdit = $db->fetch_assoc($dataEdit);
			$db->close($dataEdit);
		}
		?>		
		<div class="container-fluid">
		<form name="form_propinsi" method="POST" action="javascript: void(null);">
			<?php if(@$_REQUEST['id'] == ''){ ?>
			<input type="hidden" id="proc" name="proc" value="add" />
			<?php }else{ ?>
			<input type="hidden" id="proc" name="proc" value="update" />
			<input type="hidden" id="txtidx" name="txtidx" value="<?php echo @$resultEdit['id_propinsi']; ?>" />
			<?php } ?>
			<div class="row">
				<div class="form-group col-xs-4 has-id">
					<label>Kode&nbsp;Provinsi</label>
					<input type="text" name="txtid" id="txtid" value="<?php echo @$resultEdit['id_propinsi']; ?>" autocomplete="off" class="form-control input-sm" style="width:150px" placeholder="Kode Propinsi..." />
				</div>
				<div class="form-group col-xs-8 has-nama">
					<label>Nama&nbsp;Propinsi</label>
					<input type="text" name="txtnama" id="txtnama" value="<?php echo $resultEdit['nama']; ?>" autocomplete="off" class="form-control input-sm" placeholder="Nama Propinsi..." />
				</div>
			</div>
			
			<table class="hide">
				<tr><td colspan="3">&nbsp;</td></tr>
				<tr>
					<td colspan="3" align="left">
						<button style="display:none;" id="save" class="btn btn-primary" onclick="javascript:
							var obj = document.form_propinsi;
							var err = '';
							if(obj.txtid.value==''){ $('.has-id').addClass('has-error').focus(); err+='<li>Kode Provinsi harus di isi</li>'; }
							if(obj.txtnama.value==''){ $('.has-nama').addClass('has-error').focus(); err+='<li>Nama jabatan harus di isi</li>'; }
							if(err==''){
								obj.action='content.php?module=admin&component=propinsi&action=process';
								obj.submit();
								
								if(window.top.document.getElementById('cbadd').checked == false){
									window.top.document.getElementById('dismiss').click();
								}
							}else{ 
								$('#Modal').click(); $('#error-text').html(err);
							}
						"></button>
						<a class="btn hidden" id="reset" onclick="javascript: document.form_propinsi.reset();"></a>
					</td>
				</tr>
			</table>
		</form>
		</div>
		
		<!-- Alert Validation Form -->
		<input type="hidden" id="Modal" onclick="javascript: $('#s_alert').fadeIn(); $('#s_alert').delay(3000); $('#s_alert').fadeOut(500);" />
		<div class="alert alert-danger" id="s_alert">
			<button type="button" class="close" onclick="javascript: $('#s_alert').fadeOut();">x</button>
			<strong>Warning : </strong> <div id="error-text" class="error-text"><ul></ul></div>
		</div>
		<!-- End Alert Validation Form -->
		
		<script src="includes/bootstrap/bootstrap.js"></script>
		<script type="text/javascript">
			$("[rel=tooltip]").tooltip();
			$(function() {
				$('.demo-cancel-click').click(function(){return false;});
			});
		</script>
		
	</body>
</html>