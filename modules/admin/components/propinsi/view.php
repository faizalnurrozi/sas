<?php
include "globals/config.php";
include "globals/functions.php";
$db = new Database();
$func = new Functions();

$id = @$_GET['id'];
?>
<html>
	<head>
		<script type="text/javascript" src="includes/ajax.js"></script>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="includes/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="includes/bootstrap/css/font-awesome.css">
		<link rel="stylesheet" href="includes/dist/css/ionicons.min.css">
		<link rel="stylesheet" href="includes/dist/css/AdminLTE.css">
		<link rel="stylesheet" href="includes/dist/css/skins/_all-skins.min.css">
	</head>
	<body>
		<?php
		list($idx, $namax) = $db->result_row(" SELECT id_propinsi, nama FROM _propinsi WHERE id_propinsi = '$id' ");
		?>
		<div class="container-fluid">
		<table class="table">
			<tr>
				<td width="20%">Kode&nbsp;</td>
				<td width="1%">&nbsp;:&nbsp;</td>
				<td><?php echo $idx; ?></td>
			</tr>
			<tr>
				<td>Nama&nbsp;Provinsi&nbsp;</td>
				<td>&nbsp;:&nbsp;</td>
				<td><?php echo $namax; ?></td>
			</tr>
		</table>
		</div>
	</body>
</html>