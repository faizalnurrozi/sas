<?php 
class Functions{
	public function generate_password($digit){ $val=rand(1,1000); $str=substr(md5($val),0,$digit); return $str; } 
	public function telepon($nomer){$cnomer=str_split(trim($nomer));switch($cnomer[0]){case '0':$resnomer="+62";for($i=1;$i<=count($cnomer);$i++)$resnomer.=$cnomer[$i];return $resnomer;break;case '+':return trim($nomer);break;case '6':return "+".trim($nomer);break;default :return "+62".trim($nomer);break;}} 
	public function terbilang($x){ 
		$abil=array("","satu","dua","tiga","empat","lima","enam","tujuh","delapan","sembilan","sepuluh","sebelas"); 
		if($x<12) return " ".$abil[$x]; 
		elseif($x<20) return $this->terbilang($x-10)."belas"; 
		elseif($x<100) return $this->terbilang($x/10)." puluh".$this->terbilang($x%10); 
		elseif($x<200) return " seratus".$this->terbilang($x-100); 
		elseif($x<1000) return $this->terbilang($x/100)." ratus".$this->terbilang($x%100); 
		elseif($x<2000) return " seribu".$this->terbilang($x-1000); 
		elseif($x<1000000) return $this->terbilang($x/1000)." ribu".$this->terbilang($x%1000); 
		elseif($x<1000000000) return $this->terbilang($x/1000000)." juta".$this->terbilang($x%1000000); 
		elseif($x<1000000000000) return $this->terbilang($x/1000000000)." milyar".$this->terbilang($x%1000000000); 
	} 
	public function nama_hari($hari){switch($hari){case '1':$day="Senin";break;case '2':$day="Selasa";break;case '3':$day="Rabu";break;case '4':$day="Kamis";break;case '5':$day="Jumat";break;case '6':$day="Sabtu";break;case '7':$day="Minggu";break;}return $day;} 
	public function send_mail($fromUser,$fromEmail,$toUser,$toEmail,$subject,$message){$headers='MIME-Version: 1.0' . "\r\n";$headers.='Content-type: text/html; charset=iso-8859-1' . "\r\n";$headers.='To: '.$toUser.' <'.$toEmail.'>' . "\r\n";$headers.='From: '.$fromUser.' <'.$fromEmail.'>' . "\r\n";mail($toEmail,$subject,$message,$headers);} 
	public function icon_mime($extension){switch(strtolower($extension)){case "doc":$icon="globals/icons/doc.gif";break;case "docx":$icon="globals/icons/doc.gif";break;case "pdf":$icon="globals/icons/pdf.gif";break;case "ppt":$icon="globals/icons/ppt.gif";break;case "pps":$icon="globals/icons/ppt.gif";break;case "xls":$icon="globals/icons/xls.gif";break;case "xlsx":$icon="globals/icons/xls.gif";break;case "zip":$icon="globals/icons/zip.gif";break;case "rar":$icon="globals/icons/rar.gif";break;case "swf":$icon="globals/icons/swf.gif";break;case "gif":$icon="globals/icons/pic.gif";break;case "jpg":$icon="globals/icons/pic.gif";break;case "jpeg":$icon="globals/icons/pic.gif";break;case "png":$icon="globals/icons/pic.gif";break;case "txt":$icon="globals/icons/txt.gif";break;case "sql":$icon="globals/icons/txt.gif";break;case "php":$icon="globals/icons/txt.gif";break;case "flv":$icon="globals/icons/vid.gif";break;case "mp4":$icon="globals/icons/vid.gif";break;default :$icon="globals/icons/unknown.gif";break;}return $icon;} 
	public function read_file($Dir){$files=null;if(file_exists($Dir)){if($handle=opendir($Dir)){$i=0;while(false!==($file=readdir($handle))){if($file!="."&&$file!=".."&&$file!=""){$files[$i]=$file;}$i++;}closedir($handle);}}else{echo "Folder atau File <b>$Dir</b> tidak ada.";}return $files;} 
	public function resize_image($srcImg,$ratio,$type){if(file_exists($srcImg)){list($w,$h)=getimagesize($srcImg);createImage($srcImg,"temp",$type,$w*$ratio,$h*$ratio);unlink($srcImg);createImage("temp",$srcImg,$type,$w*$ratio,$h*$ratio);unlink("temp");}else{echo "Gambar tidak ada";}} 
	public function merge_image($srcAwal,$srcInsert,$Result,$type,$wResult,$hResult){$awal=imagecreatefromstring(file_get_contents($srcAwal));$insert=imagecreatefromstring(file_get_contents($srcInsert));imagecopymerge($awal,$insert,0,0,0,0,$wResult,$hResult,100);switch($type){case "gif":imagegif($awal,$Result);break;case "jpg":imagejpeg($awal,$Result);break;case "png":imagepng($awal,$Result);break;default :imagegif($awal,$Result);break;}} 
	public function create_image($Source,$Result,$type,$wResult,$hResult){list($width,$height)=getimagesize($Source);$asli=imagecreatefromstring(file_get_contents($Source));$hasil=imagecreatetruecolor($wResult,$hResult);imagecopyresized($hasil,$asli,0,0,0,0,$wResult,$hResult,$width,$height);switch($type){case "gif":imagegif($hasil,$Result);break;case "jpg":imagejpeg($hasil,$Result);break;case "jpg":imagepng($hasil,$Result);break;default :imagegif($hasil,$Result);break;}} 
	public function is_mobile(){return preg_match("/(android|blackberry|bolt|boost|docomo|fone|mini|mobi|palm|phone|tablet|up\.browser|up\.link|webos|wos)/i",$_SERVER["HTTP_USER_AGENT"]);} 
	public function ticket($String){$tick=uniqid();$len=strlen($tick)/2;$words=str_split($tick);$res="";for($i=$len;$i<strlen($tick);$i++){$res.=$words[$i];}return $String.$res;} 
	public function encrypt_md5($Pass){ $ssaP=strrev($Pass); return md5($ssaP); } 
	public function search_day($Date){ 
		$Day = date("D", strtotime($Date)); 
		switch($Day){
			case 'Sun':$Hari="Minggu"; break; 
			case 'Mon':$Hari="Senin"; break; 
			case 'Tue':$Hari="Selasa"; break; 
			case 'Wed':$Hari="Rabu"; break; 
			case 'Thu':$Hari="Kamis"; break; 
			case 'Fri':$Hari="Jum'at"; break; 
			case 'Sat':$Hari="Sabtu"; break; 
			default :$Hari=$Day; break; 
		} 
		return $Hari; 
	} 
	public function greeting(){$Jam=date("H");switch($Jam){case "00":case "01":case "02":case "03":case "04":case "05":case "06":	case "07":case "08":case "09":case "10":case "11":case "12":$Salam	="Pagi";break;case "13":case "14":case "15":$Salam="Siang";	break;case "16":case "17":case "18":$Salam="Sore";break;case "19":case "20":case "21":case "22":case "23":$Salam="Malam";break;}return($Salam);} 
	public function explode_date($Tgl){ $ArrayTanggal=explode("/",$Tgl); $Tanggal=@$ArrayTanggal[2]."-".@$ArrayTanggal[1]."-".@$ArrayTanggal[0]." 00:00:00"; return @$Tanggal; } 
	public function explode_date_time($Tgl){$ArrayTanggal=explode("/",$Tgl);$Tanggal=$ArrayTanggal[2]."-".$ArrayTanggal[1]."-".$ArrayTanggal[0]." ".date("h:i:s");return($Tanggal);} 
	public function flip_date($Tgl){ $ArrayTanggal=explode("/",$Tgl);$Tanggal=$ArrayTanggal[2]."-".$ArrayTanggal[1]."-".$ArrayTanggal[0];return($Tanggal);} 
	public function implode_date($Tgl){$pos=strpos($Tgl,'/');if($pos===false){$Year=substr($Tgl,0,4);$Month=substr($Tgl,5,2);$Date=substr($Tgl,8,2);$Tanggal=$Date."/".$Month."/".$Year;}else{$Tanggal=$Tgl;}if($Tanggal=='//'){$Tanggal='';}return($Tanggal);} 
	public function implode_date_time($Tgl){$pos=strpos($Tgl,'/');if($pos===false){$Year=substr($Tgl,0,4);$Month=substr($Tgl,5,2);$Date=substr($Tgl,8,2);$Jam=substr($Tgl,11,8);$Tanggal=$Date."/".$Month."/".$Year." ".$Jam;}else{$Tanggal=$Tgl;}if($Tanggal=='//'){$Tanggal='';}return($Tanggal);} 
	public function is_leap_year($Thn){$LeapY=$Thn%4;if($LeapY==0){return(true);}else{return(false);}} 
	public function last_day($Bln,$Thn){switch($Bln){case "1":case "3":case "5":case "7":case "8":case "10":case "12":$Tanggal=31;break;case "2":if(IsLeapYear($Thn)){$Tanggal=29;}else{$Tanggal=28;}break;case "4":case "6":case "9":case "11":$Tanggal=30;break;}return($Tanggal);} 
	public function last_days($Month,$Year){static $lasts=array(false,31,28,31,30,31,30,31,31,30,31,30,31);if($Month<01||$Month>12){return (false);}if($Month==02){if(checkdate(2,29,$Year)){return (29);}return(28);}return($lasts[$Month]);} 
	public function report_date($Tgl){$Year=substr($Tgl,0,4);	$Month=substr($Tgl,5,2);$Date=substr($Tgl,8,2);switch($Month){case "01":$Month="Januari"; break; case "02":$Month="Februari"; break; case "03":$Month="Maret"; break; case "04":$Month="April"; break; case "05": $Month="Mei"; break;case "06":$Month="Juni"; break; case "07":$Month="Juli"; break; case "08":$Month="Agustus"; break; case "09":$Month="September"; break; case "10":$Month="Oktober"; break; case "11":$Month="November"; break; case "12":	$Month="Desember"; break;} $Tanggal=$Date." ".$Month." ".$Year;return($Tanggal);} 
	public function report_date_simple($Tgl){$Year=substr($Tgl,0,4); $Month=substr($Tgl,5,2); $Date=substr($Tgl,8,2); switch($Month){case "01":$Month="Jan"; break; case "02":$Month="Feb"; break; case "03":$Month="Mar"; break; case "04":$Month="Apr"; break; case "05":$Month="Mei"; break;case "06":$Month="Jun"; break; case "07":$Month="Jul"; break; case "08":$Month="Agt"; break; case "09":$Month="Sep";break; case "10":$Month="Okt"; break; case "11":$Month="Nop"; break; case "12":$Month="Des"; break;} $Tanggal=$Date." ".$Month." ".$Year; return($Tanggal); } 
	public function report_date_time($Tgl){$Year=substr($Tgl,0,4);$Month=substr($Tgl,5,2);$Date=substr($Tgl,8,2);$Hour=substr($Tgl,11,2);$Minute=substr($Tgl,14,2);$Second=substr($Tgl,17,2);switch($Month){case "01":$Month="Januari";break;case "02":$Month="Febuari";break;case "03":$Month="Maret";break;case "04":$Month="April";break;case "05":$Month="Mei";break;case "06":$Month="Juni";break;case "07":$Month="Juli";break;case "08":$Month="Agustus";break;case "09":$Month="September";break;case "10":$Month="Oktober";break;case "11":$Month="November";break;case "12":$Month="Desember";break;}$Tanggal=$Date." ".$Month." ".$Year."  ".$Hour.":".$Minute.":".$Second;return($Tanggal);} 
	public function convert_date_time($Before){$datetime=explode(" ",$Before);$date_elements=explode("-",$datetime[0]);$time_elements=explode(":",$datetime[1]);$After=mktime($time_elements[0],$time_elements[1],$time_elements[2],$date_elements[1],$date_elements[2],$date_elements[0]);return($After);} 
	public function decode_date($Before){$datetime=explode(" ",$Before);$date_elements=explode("-",$datetime[0]);return($date_elements);} 
	public function date_diff2($interval,$datefrom,$dateto,$using_timestamps=false){if(!$using_timestamps){$datefrom=strtotime($datefrom,0);$dateto=strtotime($dateto,0);}$difference=$dateto-$datefrom;switch($interval){case 'yyyy':$years_difference=floor($difference /31536000);if(mktime(date("H",$datefrom),date("i",$datefrom),date("s",$datefrom),date("n",$datefrom),date("j",$datefrom),date("Y",$datefrom)+$years_difference)>$dateto){$years_difference--;}if(mktime(date("H",$dateto),date("i",$dateto),date("s",$dateto),date("n",$dateto),date("j",$dateto),date("Y",$dateto)-($years_difference+1))>$datefrom){$years_difference++;}$datediff=$years_difference;break;case "q":$quarters_difference=floor($difference/8035200);while(mktime(date("H",$datefrom),date("i",$datefrom),date("s",$datefrom),date("n",$datefrom)+($quarters_difference*3),date("j",$dateto),date("Y",$datefrom))<$dateto){	$months_difference++;}$quarters_difference--;$datediff=$quarters_difference;break;case "m":$months_difference=floor($difference/2678400);while(mktime(date("H",$datefrom),date("i",$datefrom),date("s",$datefrom),date("n",$datefrom)+($months_difference),date("j",$dateto),date("Y",$datefrom))<$dateto){$months_difference++;}$months_difference--;	$datediff=$months_difference;break;case 'y':$datediff=date("z",$dateto)-date("z",$datefrom);break;case "d":$datediff=floor($difference/86400);break;case "w":$days_difference=floor($difference/86400);$weeks_difference=floor($days_difference/7);$first_day=date("w",$datefrom);$days_remainder=floor($days_difference%7);$odd_days=$first_day+$days_remainder;if($odd_days>7){$days_remainder--;}if($odd_days>6){$days_remainder--;}$datediff=($weeks_difference*5)+$days_remainder;break;case "ww":$datediff=floor($difference/604800);break;case "h":$datediff=floor($difference/3600);break;case "n":$datediff=floor($difference/60);break;default:$datediff=$difference;break;}return $datediff;} 
	public function cek_null($DateValue){if(($DateValue<>'0000-00-00')and(isset($DateValue))){return true;}else{return false;}} 
	public function date_add2($v,$d=null,$f="d/m/Y"){$d=($d?$d:date("Y-m-d"));return date($f,strtotime($v." days",strtotime($d)));} 
	public function get_time($date){return substr($date,11);} 
	function cut_time($Time,$Format="hm"){$Hour=substr($Time,0,2);$Minute	=substr($Time,3,2);$Second	=substr($Time,6,2);switch($Format){case 'h':$rTime=$Hour;break;case 'm':$rTime=$Minute;break;case 's':$rTime=$Second;break;case 'hm':$rTime=$Hour.":".$Minute;break;case 'ms':$rTime=$Minute.":".$Second;break;default :$rTime=$Time;break;}return $rTime;} 
	public function cut_string($Strings,$Length){$str=explode(" ",$Strings);$str_r="";for($i=0;$i<$Length;$i++){$str_r.=@$str[$i]." ";}return @$str_r;} 
	public function explode_time($Strings){$str=explode(":",$Strings);$str_r="";for($i=0;$i<2;$i++){$str_r.=$str[$i];if($i<1)$str_r.=":";}return $str_r;} 
	public function highlight($String,$Keyword){if($Keyword==null){return @$String;}else{$chString=str_split($String);$lenKey=strlen($Keyword);$strResult=@$chString;for($i=0;$i<count(@$chString);$i++){$strKey="";for($a=$i;$a<($i+$lenKey);$a++){$strKey.=@$chString[$a];}if(strtolower($strKey)==strtolower($Keyword)){for($b=$i;$b<($i+$lenKey);$b++){$strResult[$b]="<b style='color:blue;'><i>".@$chString[$b]."</i></b>";}}}return implode("",@$strResult);}} 
	public function key_seo($Keyword,$Len){$avoid=array(",",".","'","/","\\","\\\\","\"","-","=");$Key=str_replace($avoid,"",$Keyword);$raw=explode(" ",$Key);$rawKata="";for($i=0;$i<count($raw);$i++){if(trim($raw[$i])!=""){$rawKata.=$raw[$i]." ";}}	$Kata=str_replace(" ","-",trim($rawKata));$Words=explode("-",$Kata);$seo="";$Length=count($Words);if($Length>$Len){	for($i=0;$i<$Len;$i++){	if($i<($Len-1))$seo.=$Words[$i]."-";else $seo.=$Words[$i];}}else{for($i=0;$i<$Length;$i++){	if($i<($Length-1))$seo.=$Words[$i]."-";else $seo.=$Words[$i];}}return $seo;} 
	public function cek_file_exists($path, $base_url, $call_back){ 
		$path_info = $this->parse_path(); 
		$component = @$path_info['call_parts'][0]; 
		$action = @$path_info['call_parts'][1]; 
		if(file_exists($path) && is_file($path)){
			return include($path);
		}else{
			//return "<meta http-equiv='refresh' content='url=0;".$base_url.$call_back."' />";
			return include $call_back;
		}
	} 
	public function parse_path(){
		$path = array();
		if(isset($_SERVER['REQUEST_URI'])) {
			$request_path = explode('?', @$_SERVER['REQUEST_URI']);
			$path['base'] = rtrim(dirname(@$_SERVER['SCRIPT_NAME']), '\/');
			$path['call_utf8'] = substr(urldecode(@$request_path[0]), strlen($path['base']) + 1);
			$path['call'] = utf8_decode($path['call_utf8']);
			if ($path['call'] == basename(@$_SERVER['PHP_SELF'])) {
				$path['call'] = '';
			}
			$path['call_parts'] = explode('/', $path['call']);			
			$path['query_utf8'] = urldecode(@$request_path[1]);
			$path['query'] = utf8_decode(urldecode(@$request_path[1]));
			$vars = explode('&', $path['query']);
			foreach($vars as $var) {
				$t = explode('=', @$var);
				$path['query_vars'][$t[0]] = @$t[1];
			}
		}
		return $path;
	} 
	public function throw_url($path, $base_url){
		$path_info = $this->parse_path();	
		$module	= $path;
		$component 	= @$path_info['call_parts'][0];
		$action 	= @$path_info['call_parts'][1];	
		if(empty($component) || $component==''){
			$link="modules/".$module."/components/home/main.php";
		}else{
			if(empty($action) || $action==''){
				$link="modules/".$module."/components/".$component."/main.php";
			}else{
				$link="modules/".$module."/components/".$component."/".$action.".php";
			}
		}	
		return $this->cek_file_exists($link, $base_url, "error.php");
	} 
	public function clear_url($url){$name=str_replace(' ','.',$url);	$name=str_replace(',','',$name);$name=str_replace('?','',$name);$name=str_replace('/','',$name);return $name;} 
	public function number_pad($number,$n){return str_pad((int) $number,$n,"0",STR_PAD_LEFT);}
	public function search_multiple($field,$opf='|',$keys,$opk='&'){$fields=explode(' ',trim($field));$kata=explode(' ',trim($keys));$res='';$f=count($fields);$x=count($kata);$n=1;foreach($fields as $fie){if($fie!=''){$i=1;$res.=" ( ";foreach($kata as $a){if($a!=''){$res.=" $fie LIKE '%$a%' ";if($i<$x)$res.=($opk=='|')?" OR ":" AND ";}$i++;}$res.=" ) ";if($n<$f){$res.=($opf=='|')?" OR ":" AND ";}}$n++;}return $res;} 
	public function highlight_multiple($str,$keys){$kata=explode(' ',trim($keys));$resf=$str;foreach($kata as $k){$chString=str_split($resf);$lenKey=strlen($k);$res=$chString;for($i=0;$i<count($chString);$i++){$strKey='';for($a=$i;$a<($i+$lenKey);$a++){$strKey.=$chString[$a];}if(strtolower($strKey)==strtolower($k)){for($b=$i;$b<($i+$lenKey);$b++){$res[$b]="<b style='color:blue;'><i>".$chString[$b]."</i></b>";}}}$resf=implode("",$res);}return $resf;}
	public function akses($crud){
		$stats = false;
		switch(strtoupper($crud)){
			case 'C' : $stats = (substr($_SESSION[_APP_.'s_accessMenu'],0,1)=='1') ? true : false; break;
			case 'R' : $stats = (substr($_SESSION[_APP_.'s_accessMenu'],1,1)=='1') ? true : false; break;
			case 'U' : $stats = (substr($_SESSION[_APP_.'s_accessMenu'],2,1)=='1') ? true : false; break;
			case 'D' : $stats = (substr($_SESSION[_APP_.'s_accessMenu'],3,1)=='1') ? true : false; break;
			case 'CR':
			case 'RC': $stats = (substr($_SESSION[_APP_.'s_accessMenu'],0,1)=='1' && substr($_SESSION[_APP_.'s_accessMenu'],1,1)=='1') ? true : false; break;
			case 'CRU' :
			case 'RUC' :
			case 'UCR' : $stats = (substr($_SESSION[_APP_.'s_accessMenu'],0,1)=='1' && substr($_SESSION[_APP_.'s_accessMenu'],1,1)=='1' && substr($_SESSION[_APP_.'s_accessMenu'],2,1)=='1') ? true : false; break;
			case 'CRUD' :
			case 'DCRU' :
			case 'UDCR' :
			case 'RUDC' : $stats = (substr($_SESSION[_APP_.'s_accessMenu'],0,1)=='1' && substr($_SESSION[_APP_.'s_accessMenu'],1,1)=='1' && substr($_SESSION[_APP_.'s_accessMenu'],2,1)=='1' && substr($_SESSION[_APP_.'s_accessMenu'],3,1)=='1') ? true : false; break;
		}
		return $stats;
	}
	public function keygen($length=10){
		$key = ''; 
		list($usec, $sec) = explode(' ', microtime()); 
		mt_srand((float) $sec + ((float) $usec * 100000)); 
		$inputs = array_merge(range('z','a'), range(0,9), range('A','Z')); 
		for($i=0; $i<$length; $i++){
			$key .= $inputs{ mt_rand(0,61) }; 
		} 
		return $key;
	}
	public function activity_logs_insert($table, $idx, $by, $notes=''){
		include_once "config.php";
		$dbx = new Database(); 
		/* ----- Activity Logs (Insert) ------ */
		$stmt = $dbx->sql("INSERT INTO _activity_logs (`table`, `id_trans`, `notes`, `action`, `action_by`, `action_date`, `action_ip`) VALUES ('".$table."', '".$idx."', '".$notes."', 'CREATE', '".$by."', NOW(), '".@$_SERVER['REMOTE_ADDR']."') ");
		$dbx->close($stmt);
		/* ----- End Activity Logs (Insert) ------ */
	}
	public function activity_logs_update($table, $contents, $wc=null, $wo='OR', $by, $notes=''){
		include_once "config.php";
		$dbx = new Database(); 	
		$field=""; $value=""; $x=count($contents); $i=1; $n=1; $index=0;
		foreach(array_keys($contents) as $key){
			$fields[$index] = $key;
			$values[$index] = $contents[$key];
			$field .= "`".$key."`";
			$value .= "'".$contents[$key]."'";
			if($x > $i){ $field .= ", "; $value .= ", "; }
			$i++; $n++; $index++;
		}	
		if($wc!=null){
			$where_cont=""; $x=count($wc); $i=1; $n=1; $index=0;
			foreach(array_keys($wc) as $key){
				$idx[$index] = $wc[$key];
				$where_cont .= "`".$key."` = '".$wc[$key]."'";
				if($x > $i) $where_cont .= " ".$wo." ";
				$i++; $n++;
			}
		}else $where_cont = "true";
		/* ----- Activity Logs (Update) ------ */
		$qTable = $dbx->sql("SELECT ".$field." FROM ".$table." WHERE ".$where_cont);
		$rTable = $dbx->fetch_assoc($qTable);
		$index=0; $s=0;
		foreach(array_keys($contents) as $key){
			if($rTable[$fields[$index]] != $values[$index]){ 
				$fieldx[$s] = $fields[$index]; 
				$dataOld[$s]= $rTable[$fields[$index]]; 
				$dataNew[$s]= $values[$index]; 
				$s++;
			} 
			$index++;
		}
		if(isset($fieldx)){
			$x=0;
			foreach($fieldx as $f){
				$ax = $dbx->insert("_activity_logs", 
					array(	'table' => $table, 
							'field' => $f, 
							'id_trans' => $idx[0], 
							'data_old' => $dataOld[$x], 
							'data_new' => $dataNew[$x], 
							'notes' => $notes, 
							'action' => 'UPDATE', 
							'action_by' => $by, 
							'action_date' => date("Y-m-d h:i:s"), 
							'action_ip' => $_SERVER['REMOTE_ADDR']
						)
					);
				$dbx->close($ax);
				$x++;
			}
		}
		$dbx->close($qTable);
		/* ----- End Activity Logs (Update) ------ */
	}			
	public function activity_logs_delete($table, $idx, $by, $notes=''){
		include_once "config.php";
		$dbx = new Database();
		/* ----- Activity Logs (Delete) ------ */
		$stmt = $dbx->sql("INSERT INTO _activity_logs (`table`, `id_trans`, `notes`, `action`, `action_by`, `action_date`, `action_ip`) VALUES ('".$table."', '".$idx."', '".$notes."', 'DELETE', '".$by."', NOW(), '".@$_SERVER['REMOTE_ADDR']."')");
		$dbx->close($stmt);
		/* ----- End Activity Logs (Delete) ------ */
	}
	public function print_logs($idx, $by){
		include_once "config.php";
		$dbx = new Database();
		/* ----- Print Logs ------- */
		$stmt = $dbx->insert("_print_logs", array('id_transaksi' => $idx, 'printed_by' => $by, 'printed_date' => date("Y-m-d h:i:s"), 'ip_address' => @$_SERVER['REMOTE_ADDR']));
		$dbx->close($stmt);
		/* ----- End Print Logs ------- */
	}
	public function push_notification($tos, $title, $desc, $link, $table, $idx, $icon="<i class='fa fa-book'></i>"){
		include_once "config.php";
		$dbx = new Database();
		foreach($tos as $to){
			$stmt = $dbx->insert("_notifikasi", array('usernames' => $to, 'judul_notifikasi' => $title, 'isi_notifikasi' => $desc, 'link' => $link, 'icon' => $icon, 'tanggal' => date("Y-m-d h:i:s"), 'status_open' => 'BELUM', 'status_push' => 'TRUE', 'tabel' => $table, 'idx' => $idx));
			$dbx->close($stmt);
		}
	}
	public function clear_notification($tos, $table, $idx){
		include_once "config.php";
		$dbx = new Database();
		foreach($tos as $to){
			$stmt = $dbx->update("_notifikasi", array('status_open' => 'SUDAH'), array('usernames' => $to, 'tabel' => $table, 'idx' => $idx), 'AND');
			$dbx->close($stmt);
		}
	}
	public function get_web_title(){
		include_once "config.php";
		$dbx = new Database();
		$qWeb = $dbx->select("_setting", array('web_title'));
		list($webtitle) = $dbx->fetch_row($qWeb);
		$dbx->close($qWeb);
		return $webtitle;
	}
	public function selengkapnya($konten){
		$i = strpos($konten, '<!--selengkapnya-->');
		if ($i !== false) {
			$i += strlen('<!--selengkapnya-->');
			return substr($konten, 0, $i);
		}
		else return $konten;
	}
	public function cutwords($sentence, $word_count){
		$space_count = 0;
		$print_string = '';
		for($i=0;$i<strlen($sentence);$i++){
			if($sentence[$i]==' ')
				$space_count ++;
				$print_string .= $sentence[$i];
			if($space_count == $word_count)
			break;
		}
		echo $print_string;
	}
	public function is_image($fileupload){
		$mime = array(
			'image/gif' => 'gif',
            'image/jpeg' => 'jpeg',
            'image/png' => 'png',
            'application/x-shockwave-flash' => 'swf',
            'image/psd' => 'psd',
            'image/bmp' => 'bmp',
            'image/tiff' => 'tiff',
            'image/tiff' => 'tiff',
            'image/jp2' => 'jp2',
            'image/iff' => 'iff',
            'image/vnd.wap.wbmp' => 'bmp',
            'image/xbm' => 'xbm',
            'image/vnd.microsoft.icon' => 'ico'
		);
		$files = $fileupload;
		$file_name = $files['name'];
		$file_info = getimagesize($files['tmp_name']);
		if(empty($file_info)){
			return false;
		}else{
			$file_mime = $file_info['mime'];
			$ext = strtolower(substr(strrchr($file_name, "."), 1));
			if($ext=='jpc' || $ext=='jpx' || $ext=='jb2'){
				$extension = $ext;
			}else{
				$extension = ($mime[$file_mime] == 'jpeg') ? 'jpg' : $mime[$file_mime];
			}
			if(!$extension){
				//$extension = '';  
				//$file_name = str_replace('.', '', $file_name); 
				return false;
			}else return true;
		}
	}
	
	/**
	 * generate CSRF token
	 * 
	 * @author  Joe Sexton <joe@webtipblog.com>
	 * @param   string $formName
	 * @return  string  
	 */
	 
	public function generate_token($formName){
		$secretKey = 'gsfhs154aergz2#';
		if(!session_id()){
			session_start();
		}
		$sessionId = session_id();	 
		return sha1($formName.$sessionId.$secretKey);	 
	}
	
	/**
	 * check CSRF token
	 * 
	 * @author  Joe Sexton <joe@webtipblog.com>
	 * @param   string $token
	 * @param   string $formName
	 * @return  boolean  
	 */
	public function check_token($token, $formName){
		return $token === $this->generate_token($formName);
	}
	public function filter($value){
		$search = array("\\",  "\x00", "\n",  "\r",  "'",  '"', "\x1a");
		$replace = array("\\\\","\\0","\\n", "\\r", "\'", '\"', "\\Z");
		return str_replace($search, $replace, $value);
	}
	public function create_thumbs( $src, $dest, $thumbWidth ){
		$ext = strtolower(strrchr($src, "."));
		switch($ext){
			case '.jpeg':
			case '.jpg' :
				// load image and get image size
				$img = imagecreatefromjpeg($src);
				$width = imagesx( $img );
				$height = imagesy( $img );
				// calculate thumbnail size
				$new_width = $thumbWidth;
				$new_height = floor( $height * ( $thumbWidth / $width ) );
				// create a new temporary image
				$tmp_img = imagecreatetruecolor( $new_width, $new_height );
				// copy and resize old image into new image 
				imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );
				// save thumbnail into a file
				return imagejpeg( $tmp_img, $dest );
			break;
			case '.gif' :
				// load image and get image size
				$img = imagecreatefromgif($src);
				$width = imagesx( $img );
				$height = imagesy( $img );
				// calculate thumbnail size
				$new_width = $thumbWidth;
				$new_height = floor( $height * ( $thumbWidth / $width ) );
				// create a new temporary image
				$tmp_img = imagecreatetruecolor( $new_width, $new_height );
				// copy and resize old image into new image 
				imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );
				// save thumbnail into a file
				return imagegif( $tmp_img, $dest );
			break;
			case '.png' :
				// load image and get image size
				$img = imagecreatefrompng($src);
				$width = imagesx( $img );
				$height = imagesy( $img );
				// calculate thumbnail size
				$new_width = $thumbWidth;
				$new_height = floor( $height * ( $thumbWidth / $width ) );
				// create a new temporary image
				$tmp_img = imagecreatetruecolor( $new_width, $new_height );
				// copy and resize old image into new image 
				imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );
				// save thumbnail into a file
				return imagepng( $tmp_img, $dest );
			break;
			case '.bmp' :
				// load image and get image size
				$img = imagecreatefromwbmp($src);
				$width = imagesx( $img );
				$height = imagesy( $img );
				// calculate thumbnail size
				$new_width = $thumbWidth;
				$new_height = floor( $height * ( $thumbWidth / $width ) );
				// create a new temporary image
				$tmp_img = imagecreatetruecolor( $new_width, $new_height );
				// copy and resize old image into new image 
				imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );
				// save thumbnail into a file
				return imagewbmp( $tmp_img, $dest );
			break;
		}
	}
	public function mime_type($filename) {
		$idx = explode('.', $filename);
		$count_explode = count($idx);
		$idx = strtolower($idx[$count_explode-1]);
 
		$mimet = array(	
			'ai' =>'application/postscript',
			'aif' =>'audio/x-aiff',
			'aifc' =>'audio/x-aiff',
			'aiff' =>'audio/x-aiff',
			'asc' =>'text/plain',
			'atom' =>'application/atom+xml',
			'avi' =>'video/x-msvideo',
			'bcpio' =>'application/x-bcpio',
			'bmp' =>'image/bmp',
			'cdf' =>'application/x-netcdf',
			'cgm' =>'image/cgm',
			'cpio' =>'application/x-cpio',
			'cpt' =>'application/mac-compactpro',
			'crl' =>'application/x-pkcs7-crl',
			'crt' =>'application/x-x509-ca-cert',
			'csh' =>'application/x-csh',
			'css' =>'text/css',
			'dcr' =>'application/x-director',
			'dir' =>'application/x-director',
			'djv' =>'image/vnd.djvu',
			'djvu' =>'image/vnd.djvu',
			'doc' =>'application/msword',
			'dtd' =>'application/xml-dtd',
			'dvi' =>'application/x-dvi',
			'dxr' =>'application/x-director',
			'eps' =>'application/postscript',
			'etx' =>'text/x-setext',
			'ez' =>'application/andrew-inset',
			'gif' =>'image/gif',
			'gram' =>'application/srgs',
			'grxml' =>'application/srgs+xml',
			'gtar' =>'application/x-gtar',
			'hdf' =>'application/x-hdf',
			'hqx' =>'application/mac-binhex40',
			'html' =>'text/html',
			'html' =>'text/html',
			'ice' =>'x-conference/x-cooltalk',
			'ico' =>'image/x-icon',
			'ics' =>'text/calendar',
			'ief' =>'image/ief',
			'ifb' =>'text/calendar',
			'iges' =>'model/iges',
			'igs' =>'model/iges',
			'jpe' =>'image/jpeg',
			'jpeg' =>'image/jpeg',
			'jpg' =>'image/jpeg',
			'js' =>'application/x-javascript',
			'kar' =>'audio/midi',
			'latex' =>'application/x-latex',
			'm3u' =>'audio/x-mpegurl',
			'man' =>'application/x-troff-man',
			'mathml' =>'application/mathml+xml',
			'me' =>'application/x-troff-me',
			'mesh' =>'model/mesh',
			'mid' =>'audio/midi',
			'midi' =>'audio/midi',
			'mif' =>'application/vnd.mif',
			'mov' =>'video/quicktime',
			'movie' =>'video/x-sgi-movie',
			'mp2' =>'audio/mpeg',
			'mp3' =>'audio/mpeg',
			'mpe' =>'video/mpeg',
			'mpeg' =>'video/mpeg',
			'mpg' =>'video/mpeg',
			'mpga' =>'audio/mpeg',
			'ms' =>'application/x-troff-ms',
			'msh' =>'model/mesh',
			'mxu m4u' =>'video/vnd.mpegurl',
			'nc' =>'application/x-netcdf',
			'oda' =>'application/oda',
			'ogg' =>'application/ogg',
			'pbm' =>'image/x-portable-bitmap',
			'pdb' =>'chemical/x-pdb',
			'pdf' =>'application/pdf',
			'pgm' =>'image/x-portable-graymap',
			'pgn' =>'application/x-chess-pgn',
			'php' =>'application/x-httpd-php',
			'php4' =>'application/x-httpd-php',
			'php3' =>'application/x-httpd-php',
			'phtml' =>'application/x-httpd-php',
			'phps' =>'application/x-httpd-php-source',
			'png' =>'image/png',
			'pnm' =>'image/x-portable-anymap',
			'ppm' =>'image/x-portable-pixmap',
			'ppt' =>'application/vnd.ms-powerpoint',
			'ps' =>'application/postscript',
			'qt' =>'video/quicktime',
			'ra' =>'audio/x-pn-realaudio',
			'ram' =>'audio/x-pn-realaudio',
			'ras' =>'image/x-cmu-raster',
			'rdf' =>'application/rdf+xml',
			'rgb' =>'image/x-rgb',
			'rm' =>'application/vnd.rn-realmedia',
			'roff' =>'application/x-troff',
			'rtf' =>'text/rtf',
			'rtx' =>'text/richtext',
			'sgm' =>'text/sgml',
			'sgml' =>'text/sgml',
			'sh' =>'application/x-sh',
			'shar' =>'application/x-shar',
			'shtml' =>'text/html',
			'silo' =>'model/mesh',
			'sit' =>'application/x-stuffit',
			'skd' =>'application/x-koan',
			'skm' =>'application/x-koan',
			'skp' =>'application/x-koan',
			'skt' =>'application/x-koan',
			'smi' =>'application/smil',
			'smil' =>'application/smil',
			'snd' =>'audio/basic',
			'spl' =>'application/x-futuresplash',
			'src' =>'application/x-wais-source',
			'sv4cpio' =>'application/x-sv4cpio',
			'sv4crc' =>'application/x-sv4crc',
			'svg' =>'image/svg+xml',
			'swf' =>'application/x-shockwave-flash',
			't' =>'application/x-troff',
			'tar' =>'application/x-tar',
			'tcl' =>'application/x-tcl',
			'tex' =>'application/x-tex',
			'texi' =>'application/x-texinfo',
			'texinfo' =>'application/x-texinfo',
			'tgz' =>'application/x-tar',
			'tif' =>'image/tiff',
			'tiff' =>'image/tiff',
			'tr' =>'application/x-troff',
			'tsv' =>'text/tab-separated-values',
			'txt' =>'text/plain',
			'ustar' =>'application/x-ustar',
			'vcd' =>'application/x-cdlink',
			'vrml' =>'model/vrml',
			'vxml' =>'application/voicexml+xml',
			'wav' =>'audio/x-wav',
			'wbmp' =>'image/vnd.wap.wbmp',
			'wbxml' =>'application/vnd.wap.wbxml',
			'wml' =>'text/vnd.wap.wml',
			'wmlc' =>'application/vnd.wap.wmlc',
			'wmlc' =>'application/vnd.wap.wmlc',
			'wmls' =>'text/vnd.wap.wmlscript',
			'wmlsc' =>'application/vnd.wap.wmlscriptc',
			'wmlsc' =>'application/vnd.wap.wmlscriptc',
			'wrl' =>'model/vrml',
			'xbm' =>'image/x-xbitmap',
			'xht' =>'application/xhtml+xml',
			'xhtml' =>'application/xhtml+xml',
			'xls' =>'application/vnd.ms-excel',
			'xml xsl' =>'application/xml',
			'xpm' =>'image/x-xpixmap',
			'xslt' =>'application/xslt+xml',
			'xul' =>'application/vnd.mozilla.xul+xml',
			'xwd' =>'image/x-xwindowdump',
			'xyz' =>'chemical/x-xyz',
			'zip' =>'application/zip'
		); 
		if(isset($mimet[$idx] )){ return $mimet[$idx]; }else{ return 'application/octet-stream'; }
	}
	public function fetch_url($url){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 20);
		$result = curl_exec($ch);
		curl_close($ch); 
		return $result;
	}
	public function time_diff($tmt){
		include_once "config.php";
		$dbx = new Database();
		list($selisih) = $dbx->result_row("SELECT DATEDIFF(NOW(), '".$tmt."') AS selisih ");
		$tahun = $selisih / 365;
		$tahun = (int) $tahun;
		$tahun_sisa = $selisih % 365;
		$bulan = $tahun_sisa / 30;
		$bulan = (int) $bulan;
		$bulan_sisa = $tahun_sisa % 30;
		$hari = (int) $bulan_sisa;
		return $tahun." Tahun ".$bulan." Bulan ".$hari." Hari ";
	}


	public function GregorianToHijriah($GYear, $GMonth, $GDay) {
		$y = $GYear;
		$m = $GMonth;
		$d = $GDay;
		$jd = gregoriantojd($m, $d, $y);
		$l = $jd - 1948440 + 10632;
		$n = (int) (( $l - 1 ) / 10631);
		$l = $l - 10631 * $n + 354;
		$j = ( (int) (( 10985 - $l ) / 5316)) * ( (int) (( 50 * $l) / 17719)) + (
		(int) ( $l / 5670 )) * ( (int) (( 43 * $l ) / 15238 ));
		$l = $l - ( (int) (( 30 - $j ) / 15 )) * ( (int) (( 17719 * $j ) / 50)) - (
		(int) ( $j / 16 )) * ( (int) (( 15238 * $j ) / 43 )) + 29;
		$m = (int) (( 24 * $l ) / 709 );
		$d = $l - (int) (( 709 * $m ) / 24);
		$y = 30 * $n + $j - 30;

		$Hijriah['year'] = $y;
		$Hijriah['month'] = $this->number_pad($m,2);
		$Hijriah['day'] = $this->number_pad($d,2);

		return $Hijriah['year']."-".$Hijriah['month']."-".$Hijriah['day'];
	}

	public function MonthOhHijriah($mth){
		switch($mth){
			case "01": $month = "Muharram"; break;
			case "02": $month = "Safar"; break;
			case "03": $month = "Rabbiul Awal"; break;
			case "04": $month = "Rabbiul Akhir"; break;
			case "05": $month = "Jumadil Awal"; break;
			case "06": $month = "Jumadil Akhir"; break;
			case "07": $month = "Rajab"; break;
			case "08": $month = "Sya'ban"; break;
			case "09": $month = "Ramadhan"; break;
			case "10": $month = "Syawwal"; break;
			case "11": $month = "Dzulqo'dah"; break;
			case "12": $month = "Dzulhijjah"; break;
		}

		return $month;
	}

	public function report_date_hijriah($Tgl){
		$Year 	= substr($Tgl,0,4);
		$Month 	= substr($Tgl,5,2);
		$Date 	= substr($Tgl,8,2);

		$date_hijriah 	= $this->GregorianToHijriah($Year, $Month, $Date);

		$YearHijriah 	= substr($date_hijriah,0,4);
		$MonthHijriah 	= $this->MonthOhHijriah(substr($date_hijriah,5,2));
		$DateHijriah 	= substr($date_hijriah,8,2);
		
		$Tanggal=$DateHijriah." ".$MonthHijriah." ".$YearHijriah;
		return($Tanggal);
	}

	public function implode_date_hijriah($Tgl){
		$Year 	= substr($Tgl,0,4);
		$Month 	= substr($Tgl,5,2);
		$Date 	= substr($Tgl,8,2);

		$date_hijriah 	= $this->GregorianToHijriah($Year, $Month, $Date);

		$YearHijriah 	= substr($date_hijriah,0,4);
		$MonthHijriah 	= substr($date_hijriah,5,2);
		$DateHijriah 	= substr($date_hijriah,8,2);
		
		$Tanggal=$DateHijriah."/".$MonthHijriah."/".$YearHijriah;
		return($Tanggal);
	}

	public function explode_date_hijriah($Tgl){
		$ArrayTanggal 	= explode("/",$Tgl);
		$Tanggal 		= @$ArrayTanggal[2]."-".@$ArrayTanggal[1]."-".@$ArrayTanggal[0]." 00:00:00";
		return @$Tanggal;
	}

	function mimetype2FontAwesome($mimetype = null, $size = null) {
		switch(mime_content_type($mimetype)) {
			// PDF
			case 'application/pdf':
				$fa = 'file-pdf-o';
			break;
			// Plain text
			case 'text/plain':
				$fa = 'file-text-o';
			break;
			// Audio
			case 'audio/basic':
			case 'audio/L24':
			case 'audio/mp4':
			case 'audio/mpeg':
			case 'audio/ogg':
			case 'audio/flac':
			case 'audio/opus':
			case 'audio/vorbis':
			case 'audio/vnd.rn-realaudio':
			case 'audio/vnd.wave':
			case 'audio/webm':
			case 'audio/x-aac':
			case 'audio/x-caf':
				$fa = 'file-audio-o';
			break;
			// Video
			case 'video/avi':
			case 'video/mpeg':
			case 'video/mp4':
			case 'video/ogg':
			case 'video/quicktime':
			case 'video/webm':
			case 'video/x-matroska':
			case 'video/x-ms-wmv':
			case 'video/x-fkv':
				$fa = 'file-video-o';
			break;
					// Powerpoint
			case 'application/vnd.ms-powerpoint':
			case 'application/vnd.openxmlformats-officedocument.presentationml.presentation':
			case 'application/vnd.openxmlformats-officedocument.presentationml.template':
			case 'application/vnd.openxmlformats-officedocument.presentationml.slideshow':
			case 'application/vnd.ms-powerpoint.addin.macroEnabled.12':
			case 'application/vnd.ms-powerpoint.presentation.macroEnabled.12':
			case 'application/vnd.ms-powerpoint.template.macroEnabled.12':
			case 'application/vnd.ms-powerpoint.slideshow.macroEnabled.12':
				$fa = 'file-powerpoint-o';
			break;
					// Word
			case 'application/msword':
			case 'application/vnd.openxmlformats-officedocument.wordprocessingml.template':
			case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
			case 'application/vnd.ms-word.document.macroEnabled.12':
			case 'application/vnd.ms-word.template.macroEnabled.12':
				$fa = 'file-word-o';
			break;
			// Excel
			case 'application/vnd.ms-excel':
			case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
			case 'application/vnd.openxmlformats-officedocument.spreadsheetml.template':
			case 'application/vnd.ms-excel.sheet.macroEnabled.12':
			case 'application/vnd.ms-excel.template.macroEnabled.12':
			case 'application/vnd.ms-excel.addin.macroEnabled.12':
			case 'application/vnd.ms-excel.sheet.binary.macroEnabled.12':
			case 'text/csv':
				$fa = 'file-excel-o';
			break;
			case 'application/json':
			case 'application/javascript':
			case 'application/xhtml+xml':
			case 'application/xml':
			case 'text/xml':
			case 'text/javascript':
			case 'text/html':
			case 'text/cmd':
			case 'text/css':
			case 'text/vcard':
			case 'text/x-markdown':
			case 'text/x-jquery-tmpl':
				$fa = 'file-code-o';
			break;
			// Archive
			case 'application/x-rar-compressed':
			case 'application/x-7z-compressed':
			case 'application/zip':
			case 'application/gzip':
				$fa = 'file-archive-o';
			break;
			// Image
			case 'image/gif':
			case 'image/jpeg':
			case 'image/png':
			case 'image/bmp':
			case 'image/svg+xml':
			case 'image/tiff':
			case 'image/vnd.djvu':
			case 'image/x-xcf':
				$fa = 'file-image-o';
			break;
			// All the others
			default:
				$fa = 'file';
			break;
		}

		return "<i class='fa $size fa-$fa'></i>";
	}
}
?>